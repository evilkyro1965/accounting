package com.kyrosoft.accounting.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PurchaseModelTest {
	
	@Test
	public void testJacksonSerialize() throws JsonProcessingException {
		Purchase purchase = new Purchase();
        purchase.setNo("test");
        purchase.setDate(new Date());
        purchase.setPurchaseStatus(PurchaseStatus.ORDER);

        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setPurchase(purchase);
        orderDetails.setQuantity(10);
        orderDetails.setPrice(1000);
        orderDetails.setDiscountPercent(10);
        orderDetails.setTaxPercent(10);
        orderDetails.setOrderReceivedFull(false);
        orderDetails.setStatus(OrderStatus.ORDER);
        
        List<OrderDetails> orderDetailsList = new ArrayList<OrderDetails>();
        orderDetailsList.add(orderDetails);
        purchase.setOrderDetailsList(orderDetailsList);
        if(purchase.getOrderDetailsList()!=null) {
            for(OrderDetails orderDetailsTemp : purchase.getOrderDetailsList()){
            	orderDetailsTemp.setPurchase(purchase);
            }
        }	
        ObjectMapper objectMapper = new ObjectMapper();
        String purchaseString = objectMapper.writeValueAsString(purchase);
        int hammertime = 1;
	}
	
}
