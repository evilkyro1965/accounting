package com.kyrosoft.accounting.model;

import javax.persistence.Entity;

/**
 * Item Category entity
 *
 * @author fahrur
 * @version 1.0
 */
@Entity(name = "item_category")
public class ItemCategory extends ActiveEntity {
}
