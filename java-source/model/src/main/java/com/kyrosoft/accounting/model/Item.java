package com.kyrosoft.accounting.model;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Item entity
 *
 * @author fahrur
 * @version 1.0
 */
@Entity(name="item")
public class Item extends ActiveEntity {

    /**
     * The item code
     */
    @Basic
    private String itemCode;

    /**
     * The item type
     */
    @Enumerated(EnumType.STRING)
    private ItemType itemType;

    /**
     * The category
     */
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="categoryId")
    private ItemCategory category;

    /**
     * The item description
     */
    @Basic
    private String itemDescription;

    /**
     * The item price
     */
    @Basic
    private Double itemPrice;

    /**
     * The stock measurement
     */
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="stockMeasurementId")
    private Measurement stockMeasurement;

    /**
     * The purchase measurements
     */
    @ManyToMany
    @JoinTable(
        name="item_purchase_measurement",
        joinColumns=@JoinColumn(name="itemId", referencedColumnName="id"),
        inverseJoinColumns=@JoinColumn(name="purchaseId", referencedColumnName="id")
    )
    private Set<Measurement> purchaseMeasurements;

    /**
     * Get the item code
     * @return The item code
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * Set the item code
     * @param itemCode The item code
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * Get the item type
     * @return The item type
     */
    public ItemType getItemType() {
        return itemType;
    }

    /**
     * Set the item type
     * @param itemType The item type
     */
    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    /**
     * Get the category
     * @return The category
     */
    public ItemCategory getCategory() {
        return category;
    }

    /**
     * Set the category
     * @param category The category
     */
    public void setCategory(ItemCategory category) {
        this.category = category;
    }

    /**
     * Get the item description
     * @return The item description
     */
    public String getItemDescription() {
        return itemDescription;
    }

    /**
     * Set the item description
     * @param itemDescription The item description
     */
    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    /**
     * Get the item price
     * @return The item price
     */
    public Double getItemPrice() {
        return itemPrice;
    }

    /**
     * Set the item price
     * @param itemPrice The item price
     */
    public void setItemPrice(Double itemPrice) {
        this.itemPrice = itemPrice;
    }

    /**
     * Get the stock measurement
     * @return The stock measurement
     */
    public Measurement getStockMeasurement() {
        return stockMeasurement;
    }

    /**
     * Set the stock measurement
     * @param stockMeasurement The stock measurement
     */
    public void setStockMeasurement(Measurement stockMeasurement) {
        this.stockMeasurement = stockMeasurement;
    }

    /**
     * Get the purchase measurements
     * @return The purchase measurements
     */
    public Set<Measurement> getPurchaseMeasurements() {
        return purchaseMeasurements;
    }

    /**
     * Set the purchase measurements
     * @param purchaseMeasurements The purchase measurements
     */
    public void setPurchaseMeasurements(Set<Measurement> purchaseMeasurements) {
        this.purchaseMeasurements = purchaseMeasurements;
    }

    /**
     * Empty constructor
     */
    public Item() {}

}
