package com.kyrosoft.accounting.model.dto;

/**
 * Department search criteria
 *
 * @author fahrur
 * @version 1.0
 */
public class DepartmentSearchCriteria extends BaseSearchParameters {

    /**
     * Name
     */
    private String name;

    /**
     * Email
     */
    private String email;

    /**
     * Is Active
     */
    private Boolean isActive;

    /**
     * Empty constructor
     */
    public DepartmentSearchCriteria() {}

    /**
     * Get the name
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the email
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set the email
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get is active
     * @return is active
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * Set is active
     * @param isActive
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}
