package com.kyrosoft.accounting.model.dto;

/**
 * Created by Administrator on 2/15/2016.
 */
public class CustomerSearchCriteria extends BaseSearchParameters {

    private String name;

    private Boolean isActive;

    public CustomerSearchCriteria() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
