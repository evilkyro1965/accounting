package com.kyrosoft.accounting.model;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * Created by Administrator on 2/15/2016.
 */
@Entity(name = "customer")
public class Customer extends ActiveEntity {

    /**
     * The contact person
     */
    @Basic
    private String contactPerson;

    /**
     * The address
     */
    @Basic
    private String address;

    /**
     * The secondary address
     */
    @Basic
    private String secondaryAddress;

    /**
     * The city
     */
    @Basic
    private String city;

    /**
     * The postal code
     */
    @Basic
    private String postalCode;

    /**
     * The state
     */
    @Basic
    private String state;

    /**
     * The country
     */
    @Basic
    private String country;

    /**
     * The phone
     */
    @Basic
    private String phone;

    /**
     * The fax
     */
    @Basic
    private String fax;

    /**
     * The email
     */
    @Basic
    private String email;

    /**
     * Empty constructor
     */
    public Customer() {}

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSecondaryAddress() {
        return secondaryAddress;
    }

    public void setSecondaryAddress(String secondaryAddress) {
        this.secondaryAddress = secondaryAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
