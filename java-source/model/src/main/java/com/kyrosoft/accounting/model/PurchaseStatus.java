package com.kyrosoft.accounting.model;

public enum PurchaseStatus {
	ORDER,
	RECEIVE,
	INVOICE,
	PAID
}
