package com.kyrosoft.accounting.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Date;
import java.util.List;

/**
 * Order Details entity
 *
 * @author fahrur
 * @version 1.0
 */
@Entity(name = "order_details")
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class,
        property = "@OrderDetails")
public class OrderDetails extends GLEntity {

    /**
     * The purchase
     */
    @ManyToOne
    @JoinColumn(name="purchaseId")
    private Purchase purchase;

    /**
     * The item
     */
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="itemId")
    private Item item;

    /**
     * The purchase measurement
     */
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="purchaseMeasurementId")
    private Measurement purchaseMeasurement;

    /**
     * The quantity
     */
    @Basic
    private double quantity;

    @Basic
    private double price;

    @Basic double totalPrice;

    /**
     * The discount percent
     */
    @Basic
    private double discountPercent;

    @Basic
    private double discount;

    /**
     * The tax percent
     */
    @Basic
    private double taxPercent;

    @Basic
    private double tax;

    @Basic
    private double subTotal;

    @OneToMany(mappedBy = "orderDetails",cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private List<OrderReceived> orderReceivedList;

    @OneToMany(mappedBy = "orderDetails",cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private List<OrderReturn> orderReturnList;
    /**
     * The order received full
     */
    @Basic
    private boolean orderReceivedFull;
    
    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    /**
     * Empty constructor
     */
    public OrderDetails() {}

    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Measurement getPurchaseMeasurement() {
        return purchaseMeasurement;
    }

    public void setPurchaseMeasurement(Measurement purchaseMeasurement) {
        this.purchaseMeasurement = purchaseMeasurement;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTaxPercent() {
        return taxPercent;
    }

    public void setTaxPercent(double taxPercent) {
        this.taxPercent = taxPercent;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public boolean isOrderReceivedFull() {
        return orderReceivedFull;
    }

    public void setOrderReceivedFull(boolean orderReceivedFull) {
        this.orderReceivedFull = orderReceivedFull;
    }

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

    public List<OrderReceived> getOrderReceivedList() {
        return orderReceivedList;
    }

    public void setOrderReceivedList(List<OrderReceived> orderReceivedList) {
        this.orderReceivedList = orderReceivedList;
    }

    public List<OrderReturn> getOrderReturnList() {
        return orderReturnList;
    }

    public void setOrderReturnList(List<OrderReturn> orderReturnList) {
        this.orderReturnList = orderReturnList;
    }
}
