package com.kyrosoft.accounting.model;

/**
 * Item Type
 *
 * @author fahrur
 * @version 1.0
 */
public enum ItemType {
    STOCKED_PRODUCT,
    NON_STOCKED_PRODUCT,
    SERVICE
}
