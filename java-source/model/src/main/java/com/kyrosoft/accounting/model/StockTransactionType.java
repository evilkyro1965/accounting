package com.kyrosoft.accounting.model;

/**
 * Created by Administrator on 2/9/2016.
 */
public enum StockTransactionType {
    PURCHASE_ORDER_CREATED,
    PURCHASE_ORDER_RECEIVE,
    PURCHASE_ORDER_RETURN
}
