package com.kyrosoft.accounting.model;

public enum OrderStatus {
	ORDER,
	RECEIVE,
	INVOICE,
	PAID
}
