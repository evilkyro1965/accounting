package com.kyrosoft.accounting.model;

/**
 * Created by Administrator on 2/9/2016.
 */
public enum StockBalanceType {
    QUANTITY_ON_HAND,
    QUANTITY_AVAILABLE,
    QUANTITY_ON_ORDER,
    QUANTITY_RETURN,
    QUANTITY_RESERVED,
    QUANTITY_ON_TRANSFER
}
