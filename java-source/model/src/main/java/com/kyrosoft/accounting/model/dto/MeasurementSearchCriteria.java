package com.kyrosoft.accounting.model.dto;

import com.kyrosoft.accounting.model.MeasurementType;

/**
 * Measurement search criteria
 *
 * @author fahrur
 * @version 1.0
 */
public class MeasurementSearchCriteria extends BaseSearchParameters {

    /**
     * Name
     */
    private String name;

    /**
     * Short name
     */
    private String shortName;

    /**
     * Measurement type
     */
    private MeasurementType type;

    /**
     * Is active
     */
    private Boolean isActive;

    /**
     * Measurement search criteria
     */
    public MeasurementSearchCriteria() {}

    /**
     * Get the name
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the shortname
     * @return the shortname
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Set the shortname
     * @param shortName the shortname
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * Get the type
     * @return the type
     */
    public MeasurementType getType() {
        return type;
    }

    /**
     * Set the type
     * @param type the type
     */
    public void setType(MeasurementType type) {
        this.type = type;
    }

    /**
     * Get is active
     * @return is active
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * Set is active
     * @param isActive is active
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}
