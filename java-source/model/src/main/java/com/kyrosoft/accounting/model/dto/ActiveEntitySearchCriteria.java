package com.kyrosoft.accounting.model.dto;

/**
 * Lookup search criteria
 *
 * @author fahrur
 * @version 1.0
 */
public class ActiveEntitySearchCriteria extends BaseSearchParameters {

    /**
     * Name
     */
    private String name;

    /**
     * Is active
     */
    private Boolean isActive;

    /**
     * Empty constructor
     */
    public ActiveEntitySearchCriteria() {}

    /**
     * Get the name
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get is active
     * @return is active
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * Set is active
     * @param isActive is active
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}
