package com.kyrosoft.accounting.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2/15/2016.
 */
@Entity(name = "sales")
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class,
        property = "@Sales")
public class Sales extends GLEntity  {

    /**
     * The no
     */
    @Basic
    private String no;

    @Basic
    private Date date;

    /**
     * The request by
     */
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="salesRep")
    private Employee salesRep;

    /**
     * The customer
     */
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="customer")
    private Customer customer;

    @Basic
    private String remark;

    /**
     * Set the order details list
     */
    @OneToMany(mappedBy = "sales",cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private List<SalesOrderDetails> salesOrderDetailsList;

    @Enumerated(EnumType.STRING)
    private SalesStatus status;

    public Sales() {}

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Employee getSalesRep() {
        return salesRep;
    }

    public void setSalesRep(Employee salesRep) {
        this.salesRep = salesRep;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<SalesOrderDetails> getSalesOrderDetailsList() {
        return salesOrderDetailsList;
    }

    public void setSalesOrderDetailsList(List<SalesOrderDetails> salesOrderDetailsList) {
        this.salesOrderDetailsList = salesOrderDetailsList;
    }

    public SalesStatus getStatus() {
        return status;
    }

    public void setStatus(SalesStatus status) {
        this.status = status;
    }

}
