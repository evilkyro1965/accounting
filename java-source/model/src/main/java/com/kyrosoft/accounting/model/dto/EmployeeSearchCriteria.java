package com.kyrosoft.accounting.model.dto;

import com.kyrosoft.accounting.model.Department;

/**
 * Employee search criteria
 *
 * @author fahrur
 * @version 1.0
 */
public class EmployeeSearchCriteria extends BaseSearchParameters {

    /**
     * Name
     */
    private String name;

    /**
     * Username
     */
    private String username;

    /**
     * Department
     */
    private Long departmentId;

    /**
     * Mobile
     */
    private String mobile;

    /**
     * Email
     */
    private String email;

    /**
     * Is active
     */
    private Boolean isActive;

    /**
     * Empty constructor
     */
    public EmployeeSearchCriteria() {}

    /**
     * Get the name
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the username
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set the username
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get the department
     * @return the department
     */
    public Long getDepartmentId() {
        return departmentId;
    }

    /**
     * Set the department
     * @param Long the department
     */
    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    /**
     * Get the mobile
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * Set the mobile
     * @param mobile the mobile
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * Get the email
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set the email
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get is active
     * @return is active
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * Set is active
     * @param isActive is active
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}
