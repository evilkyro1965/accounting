package com.kyrosoft.accounting.model;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 12/21/15.
 */
@Entity(name = "order_received")
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class,
        property = "@orderReceived")
public class OrderReceived extends GLEntity {

    /**
     * The order details
     */
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="orderDetailsId")
    private OrderDetails orderDetails;

    @Basic
    private double quantity;

    @Basic
    private Date receivedDate;


    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="employeeId")
    private Employee receivedBy;

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="warehouseId")
    private Warehouse warehouse;
    
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="purchaseId")
    private Purchase purchase;

    public OrderReceived() {}

    public OrderDetails getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(OrderDetails orderDetails) {
        this.orderDetails = orderDetails;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public Employee getReceivedBy() {
        return receivedBy;
    }

    public void setReceivedBy(Employee receivedBy) {
        this.receivedBy = receivedBy;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

	public Purchase getPurchase() {
		return purchase;
	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}

}
