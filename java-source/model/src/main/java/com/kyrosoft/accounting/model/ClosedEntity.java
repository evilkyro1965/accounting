package com.kyrosoft.accounting.model;

import javax.persistence.Basic;
import javax.persistence.MappedSuperclass;

/**
 * Closed entity
 *
 * @author fahrur
 * @version 1.0
 */
@MappedSuperclass
public abstract class ClosedEntity extends IdentifiableEntity {

    /**
     * Is closed
     */
    @Basic
    private Boolean isClosed;

    /**
     * Empty constructor
     */
    public ClosedEntity() {}

    /**
     * Get is closed
     * @return Is closed
     */
    public Boolean getIsClosed() {
        return isClosed;
    }

    /**
     * Set is closed
     * @param isClosed Is closed
     */
    public void setIsClosed(Boolean isClosed) {
        this.isClosed = isClosed;
    }

}
