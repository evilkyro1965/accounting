package com.kyrosoft.accounting.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 2/9/2016.
 */
@Entity(name = "stock_balance")
public class StockBalance extends IdentifiableEntity {

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="warehouseId")
    private Warehouse warehouse;

    /**
     * The item
     */
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="itemId")
    private Item item;

    @Enumerated(EnumType.STRING)
    private StockBalanceType stockBalanceType;

    @Basic
    private Double balance;

    @Basic
    private Double openingBalance;

    @Basic
    private Date lastTransactionDate;

    @Enumerated(EnumType.STRING)
    private StockTransactionType lastTransactionType;

    @Basic
    private Long lastTransactionEntityId;

    @Basic
    private Boolean openingBalanceIsClosed;

    @Basic
    private Date openingBalanceCloseDate;

    public StockBalance() {}

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public StockBalanceType getStockBalanceType() {
        return stockBalanceType;
    }

    public void setStockBalanceType(StockBalanceType stockBalanceType) {
        this.stockBalanceType = stockBalanceType;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(Double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public Date getLastTransactionDate() {
        return lastTransactionDate;
    }

    public void setLastTransactionDate(Date lastTransactionDate) {
        this.lastTransactionDate = lastTransactionDate;
    }

    public StockTransactionType getLastTransactionType() {
        return lastTransactionType;
    }

    public void setLastTransactionType(StockTransactionType lastTransactionType) {
        this.lastTransactionType = lastTransactionType;
    }

    public Long getLastTransactionEntityId() {
        return lastTransactionEntityId;
    }

    public void setLastTransactionEntityId(Long lastTransactionEntityId) {
        this.lastTransactionEntityId = lastTransactionEntityId;
    }

    public Boolean getOpeningBalanceIsClosed() {
        return openingBalanceIsClosed;
    }

    public void setOpeningBalanceIsClosed(Boolean openingBalanceIsClosed) {
        this.openingBalanceIsClosed = openingBalanceIsClosed;
    }

    public Date getOpeningBalanceCloseDate() {
        return openingBalanceCloseDate;
    }

    public void setOpeningBalanceCloseDate(Date openingBalanceCloseDate) {
        this.openingBalanceCloseDate = openingBalanceCloseDate;
    }
}
