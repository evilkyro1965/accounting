package com.kyrosoft.accounting.model;

import javax.persistence.*;

/**
 * Employee entity
 *
 * @author fahrur
 * @version 1.0
 */
@Entity(name = "employee")
public class Employee extends ActiveEntity {

    /**
     * The username
     */
    @Basic
    private String username;

    /**
     * The password
     */
    @Basic
    private String password;

    /**
     * The mobile
     */
    @Basic
    private String mobile;

    /**
     * The email
     */
    @Basic
    private String email;

    /**
     * The department
     */
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="departmentId")
    private Department department;

    /**
     * Get the username
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Seet the username
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get the password
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the password
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get the mobile
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * Set the mobile
     * @param mobile the mobile
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * Get the email
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set the email
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get the department
     * @return the department
     */
    public Department getDepartment() {
        return department;
    }

    /**
     * Set the departement
     * @param department the department
     */
    public void setDepartment(Department department) {
        this.department = department;
    }

    /**
     * Empty constructor
     */
    public Employee() {}
}
