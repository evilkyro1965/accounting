package com.kyrosoft.accounting.model;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * Supplier entity
 *
 * @author fahrur
 * @version 1.0
 */
@Entity(name = "supplier")
public class Supplier extends ActiveEntity {

    /**
     * The contact person
     */
    @Basic
    private String contactPerson;

    /**
     * The address
     */
    @Basic
    private String address;

    /**
     * The secondary address
     */
    @Basic
    private String secondaryAddress;

    /**
     * The city
     */
    @Basic
    private String city;

    /**
     * The postal code
     */
    @Basic
    private String postalCode;

    /**
     * The state
     */
    @Basic
    private String state;

    /**
     * The country
     */
    @Basic
    private String country;

    /**
     * The phone
     */
    @Basic
    private String phone;

    /**
     * The fax
     */
    @Basic
    private String fax;

    /**
     * The email
     */
    @Basic
    private String email;

    /**
     * Get the contact person
     * @return The contact person
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * Set the contact person
     * @param contactPerson The contact person
     */
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * Get the address
     * @return The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Set the address
     * @param address The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Get the secondary address
     * @return The secondary address
     */
    public String getSecondaryAddress() {
        return secondaryAddress;
    }

    /**
     * Set the secondary address
     * @param secondaryAddress The secondary address
     */
    public void setSecondaryAddress(String secondaryAddress) {
        this.secondaryAddress = secondaryAddress;
    }

    /**
     * Get the city
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * Set the city
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Get the postal code
     * @return The postal code
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Set the postal code
     * @param postalCode The postal code
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * Get the state
     * @return The state
     */
    public String getState() {
        return state;
    }

    /**
     * Set the state
     * @param state The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Get the country
     * @return The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Set the country
     * @param country The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Get the phone
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Set the phone
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Get the fax
     * @return The fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * Set the fax
     * @param fax The fax
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * Get the email
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set the email
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Empty constructor
     */
    public Supplier() {}
}
