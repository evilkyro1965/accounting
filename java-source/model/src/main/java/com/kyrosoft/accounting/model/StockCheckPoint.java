package com.kyrosoft.accounting.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 2/9/2016.
 */

@Entity(name = "stock_checkpoint")
public class StockCheckPoint extends IdentifiableEntity {

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="stockBalanceId")
    private StockBalance stockBalance;

    @Basic
    private Date transactionDateTime;

    @Enumerated(EnumType.STRING)
    private StockTransactionType transactionType;

    @Basic
    private Long transactionId;

    @Basic
    private Double amount;

    @Basic
    private Double balanceAfter;

    public StockCheckPoint() {}

    public StockBalance getStockBalance() {
        return stockBalance;
    }

    public void setStockBalance(StockBalance stockBalance) {
        this.stockBalance = stockBalance;
    }

    public Date getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(Date transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public StockTransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(StockTransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(Double balanceAfter) {
        this.balanceAfter = balanceAfter;
    }
}
