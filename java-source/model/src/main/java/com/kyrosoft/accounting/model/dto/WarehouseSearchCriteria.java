package com.kyrosoft.accounting.model.dto;

import com.kyrosoft.accounting.model.Department;

/**
 * Warehouse search criteria
 *
 * @author fahrur
 * @version 1.0
 */
public class WarehouseSearchCriteria extends BaseSearchParameters {

    /**
     * Name
     */
    private String name;

    /**
     * Department
     */
    private Department department;

    /**
     * Shelf name
     */
    private String shelfName;

    /**
     * Label name
     */
    private String labelName;

    /**
     * Is active
     */
    private Boolean isActive;

    /**
     * Empty constructor
     */
    public WarehouseSearchCriteria() {}

    /**
     * Get the name
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the department
     * @return the department
     */
    public Department getDepartment() {
        return department;
    }

    /**
     * Set the department
     * @param department the department
     */
    public void setDepartment(Department department) {
        this.department = department;
    }

    /**
     * Get the shelf name
     * @return the shelf name
     */
    public String getShelfName() {
        return shelfName;
    }

    /**
     * Set the shelf name
     * @param shelfName shelf name
     */
    public void setShelfName(String shelfName) {
        this.shelfName = shelfName;
    }

    /**
     * Get the label name
     * @return the label name
     */
    public String getLabelName() {
        return labelName;
    }

    /**
     * Set th label name
     * @param labelName the label name
     */
    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    /**
     * Get is active
     * @return is active
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * Set is active
     * @param isActive is active
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}
