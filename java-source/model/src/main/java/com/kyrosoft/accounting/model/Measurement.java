package com.kyrosoft.accounting.model;

import javax.persistence.*;

/**
 * Measurement
 *
 * @author fahrur
 * @version 1.0
 */
@Entity(name = "measurement")
public class Measurement extends ActiveEntity {

    /**
     * The shortname
     */
    @Basic
    private String shortName;

    /**
     * The description
     */
    @Basic
    private String description;

    /**
     * The measurement type
     */
    @Enumerated(EnumType.STRING)
    private MeasurementType measurementType;

    /**
     * The unit
     */
    @Basic
    private Double unit;

    /**
     * Get the short name
     * @return The short name
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Set the short name
     * @param shortName The short name
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * Get the description
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the measurement type
     * @return The measurement type
     */
    public MeasurementType getMeasurementType() {
        return measurementType;
    }

    /**
     * Set the measurement type
     * @param measurementType The measurement type
     */
    public void setMeasurementType(MeasurementType measurementType) {
        this.measurementType = measurementType;
    }

    /**
     * Get the unit
     * @return The unit
     */
    public Double getUnit() {
        return unit;
    }

    /**
     * Set the unit
     * @param unit The unit
     */
    public void setUnit(Double unit) {
        this.unit = unit;
    }

    /**
     * Empty measurement
     */
    public Measurement() {}
}
