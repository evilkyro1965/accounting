package com.kyrosoft.accounting.model;

/**
 * Measurement type entity
 *
 * @author fahrur
 * @version 1.0
 */
public enum MeasurementType {
    STOCK,
    CALCULATION,
    PURCHASE
}
