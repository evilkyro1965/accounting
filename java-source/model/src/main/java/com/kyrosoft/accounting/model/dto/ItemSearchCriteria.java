package com.kyrosoft.accounting.model.dto;

import com.kyrosoft.accounting.model.ItemCategory;
import com.kyrosoft.accounting.model.ItemType;
import com.kyrosoft.accounting.model.Measurement;

/**
 * Created by Administrator on 10/23/15.
 */
public class ItemSearchCriteria extends BaseSearchParameters {

    private String name;

    private String itemCode;

    private ItemType itemType;

    private Long itemCategoryId;

    private Long stockMeasurementId;

    private Long purchaseMeasurementId;

    private Boolean isActive;

    public ItemSearchCriteria() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public Long getItemCategoryId() {
        return itemCategoryId;
    }

    public void setItemCategoryId(Long itemCategoryId) {
        this.itemCategoryId = itemCategoryId;
    }

    public Long getStockMeasurementId() {
        return stockMeasurementId;
    }

    public void setStockMeasurementId(Long stockMeasurementId) {
        this.stockMeasurementId = stockMeasurementId;
    }

    public Long getPurchaseMeasurementId() {
        return purchaseMeasurementId;
    }

    public void setPurchaseMeasurementId(Long purchaseMeasurementId) {
        this.purchaseMeasurementId = purchaseMeasurementId;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}
