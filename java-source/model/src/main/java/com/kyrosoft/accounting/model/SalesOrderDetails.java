package com.kyrosoft.accounting.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

/**
 * Created by Administrator on 2/15/2016.
 */
@Entity(name = "sales_details")
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class,
        property = "@SalesDetails")
public class SalesOrderDetails extends GLEntity {

    /**
     * The sales
     */
    @ManyToOne
    @JoinColumn(name="salesId")
    private Sales sales;

    /**
     * The item
     */
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="itemId")
    private Item item;

    /**
     * The sales measurement
     */
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="salesMeasurementId")
    private Measurement salesMeasurement;

    /**
     * The quantity
     */
    @Basic
    private double quantity;

    @Basic
    private double price;

    @Basic double totalPrice;

    /**
     * The discount percent
     */
    @Basic
    private double discountPercent;

    @Basic
    private double discount;

    /**
     * The tax percent
     */
    @Basic
    private double taxPercent;

    @Basic
    private double tax;

    @Basic
    private double subTotal;

    @Enumerated(EnumType.STRING)
    private SalesOrderStatus salesOrderStatus;

    public SalesOrderDetails() {}

    public Sales getSales() {
        return sales;
    }

    public void setSales(Sales sales) {
        this.sales = sales;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Measurement getSalesMeasurement() {
        return salesMeasurement;
    }

    public void setSalesMeasurement(Measurement salesMeasurement) {
        this.salesMeasurement = salesMeasurement;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTaxPercent() {
        return taxPercent;
    }

    public void setTaxPercent(double taxPercent) {
        this.taxPercent = taxPercent;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public SalesOrderStatus getSalesOrderStatus() {
        return salesOrderStatus;
    }

    public void setSalesOrderStatus(SalesOrderStatus salesOrderStatus) {
        this.salesOrderStatus = salesOrderStatus;
    }
}
