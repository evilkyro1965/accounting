package com.kyrosoft.accounting.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 12/21/15.
 */
@Entity(name = "order_return")
public class OrderReturn extends GLEntity {

    /**
     * The order details
     */
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="orderDetailsId")
    private OrderDetails orderDetails;

    @Basic
    private double quantity;

    @Basic
    private Date returnDate;


    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="employeeId")
    private Employee returnBy;

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="warehouseId")
    private Warehouse warehouse;

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="purchaseId")
    private Purchase purchase;
    
    public OrderReturn() {}

    public OrderDetails getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(OrderDetails orderDetails) {
        this.orderDetails = orderDetails;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Employee getReturnBy() {
        return returnBy;
    }

    public void setReturnBy(Employee returnBy) {
        this.returnBy = returnBy;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

	public Purchase getPurchase() {
		return purchase;
	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}
    
    
}
