package com.kyrosoft.accounting.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

/**
 * Department entity
 *
 * @author fahrur
 * @version 1.0
 */
@Entity(name = "department")
public class Department extends ActiveEntity {

    /**
     * The email
     */
    @Basic
    @NotNull
    private String email;

    /**
     * The employees
     */
    @OneToMany(mappedBy="department",fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JsonIgnore
    private List<Employee> employees;

    /**
     * Get the email
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set the email
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get the employees
     * @return the employees
     */
    public List<Employee> getEmployees() {
        return employees;
    }

    /**
     * Set the employees
     * @param employees
     */
    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    /**
     * Department
     */
    public Department() {}
}
