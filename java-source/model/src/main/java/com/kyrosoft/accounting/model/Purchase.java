package com.kyrosoft.accounting.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Purchase entity
 *
 * @author fahrur
 * @version 1.0
 */
@Entity(name = "purchase")
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class,
        property = "@Purchase")
public class Purchase extends GLEntity {

    /**
     * The no
     */
    @Basic
    private String no;

    /**
     * Set the order details list
     */
    @OneToMany(mappedBy = "purchase",cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private List<OrderDetails> orderDetailsList;
    
    @OneToMany(mappedBy = "purchase",cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private List<OrderReceived> orderReceivedList;
    
    @OneToMany(mappedBy = "purchase",cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private List<OrderReturn> orderReturnList;

    /**
     * The date
     */
    @Basic
    private Date date;

    /**
     * The request by
     */
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="requestBy")
    private Employee requestBy;
    
    /**
     * The order by
     */
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="orderBy")
    private Employee orderBy;

    /**
     * The supplier
     */
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="supplierId")
    private Supplier supplier;

    /**
     * The remark
     */
    @Basic
    private String remark;

    @Enumerated(EnumType.STRING)
    private PurchaseStatus purchaseStatus;
    
    /**
     * The empty constructor
     */
    public Purchase() {}

    /**
     * Get the no
     * @return The no
     */
    public String getNo() {
        return no;
    }

    /**
     * Set the no
     * @param no The no
     */
    public void setNo(String no) {
        this.no = no;
    }

    /**
     * Get the order details list
     * @return The order details list
     */
    public List<OrderDetails> getOrderDetailsList() {
        return orderDetailsList;
    }

    /**
     * Set the order details list
     * @param orderDetailsList The order details list
     */
    public void setOrderDetailsList(List<OrderDetails> orderDetailsList) {
        this.orderDetailsList = orderDetailsList;
    }
    
    public List<OrderReceived> getOrderReceivedList() {
		return orderReceivedList;
	}

	public void setOrderReceivedList(List<OrderReceived> orderReceivedList) {
		this.orderReceivedList = orderReceivedList;
	}

	public List<OrderReturn> getOrderReturnList() {
		return orderReturnList;
	}

	public void setOrderReturnList(List<OrderReturn> orderReturnList) {
		this.orderReturnList = orderReturnList;
	}

	/**
     * Get the date
     * @return The date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Set the date
     * @param date The date
     */
    public void setDate(Date date) {
        this.date = date;
    }
    
    /**
     * Get the supplier
     * @return The supplier
     */
    public Supplier getSupplier() {
        return supplier;
    }

    /**
     * Set the supplier
     * @param supplier The supplier
     */
    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    /**
     * Get the remark
     * @return The remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Set the remark
     * @param remark The remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

	public Employee getRequestBy() {
		return requestBy;
	}

	public void setRequestBy(Employee requestBy) {
		this.requestBy = requestBy;
	}

	public Employee getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(Employee orderBy) {
		this.orderBy = orderBy;
	}

	public PurchaseStatus getPurchaseStatus() {
		return purchaseStatus;
	}

	public void setPurchaseStatus(PurchaseStatus purchaseStatus) {
		this.purchaseStatus = purchaseStatus;
	}
	
	
    
}
