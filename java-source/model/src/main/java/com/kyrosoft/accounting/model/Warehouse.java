package com.kyrosoft.accounting.model;

import javax.persistence.*;
import java.util.List;

/**
 * Warehouse entity
 *
 * @author fahrur
 * @version 1.0
 */
@Entity(name = "warehouse")
public class Warehouse extends ActiveEntity {

    /**
     * The department
     */
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="departmentId")
    private Department department;

    /**
     * The description
     */
    @Basic
    private String description;

    /**
     * The shelf
     */
    @Basic
    private String shelf;

    /**
     * The label
     */
    @Basic
    private String label;

    /**
     * Get the department
     * @return the department
     */
    public Department getDepartment() {
        return department;
    }

    /**
     * Set the department
     * @param department The department
     */
    public void setDepartment(Department department) {
        this.department = department;
    }

    /**
     * Get the description
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the shelf
     * @return The shelf
     */
    public String getShelf() {
        return shelf;
    }

    /**
     * Set the shelf
     * @param shelf The shelf
     */
    public void setShelf(String shelf) {
        this.shelf = shelf;
    }

    /**
     * Get the label
     * @return The label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Set the label
     * @param label The label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Empty constructor
     */
    public Warehouse() {}
}
