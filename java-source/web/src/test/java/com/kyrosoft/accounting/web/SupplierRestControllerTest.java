package com.kyrosoft.accounting.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kyrosoft.accounting.dao.SupplierService;
import com.kyrosoft.accounting.model.Supplier;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Administrator on 12/3/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({"/rest_context.xml"})
@SqlGroup({ @Sql(scripts = "/sql/test-data.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)})
public class SupplierRestControllerTest {

    /**
     * Represents the web application context.
     */
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private SupplierService supplierService;

    /**
     * Represents the mock MVC.
     */
    private MockMvc mockMvc;


    /**
     * Sets up testing environment. It initializes the mock MVC.
     */
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }


    @Test
    public void testCreateSupplierRest() throws Exception {
        Supplier supplier = new Supplier();
        supplier.setName("test");
        supplier.setIsActive(true);

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/rest/supplier/create").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(supplier)).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("test"));
    }

    @Test
    public void testUpdateSupplierRest() throws Exception {
        Supplier supplier = new Supplier();
        supplier.setName("test");
        supplier.setIsActive(true);
        supplier.setCreatedBy("test");
        supplier.setCreatedDate(new Date());
        supplierService.save(supplier);
        supplier.setName("updated");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/rest/supplier/update/"+supplier.getId()).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(supplier)).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("updated"));
    }

    @Test
    public void testGetAllSupplierRest() throws Exception {
        mockMvc.perform(get("/rest/supplier/getall"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[0].name").value("test"));
    }

    @Test
    public void testSearchSupplierRest() throws Exception {
        mockMvc.perform(get("/rest/supplier/search/")
                .param("pageNumber","1").param("pageSize","2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.values", hasSize(2)))
                .andExpect(jsonPath("$.values[0].name").value("test"))
                .andExpect(jsonPath("$.values[1].name").value("test"));
    }
}
