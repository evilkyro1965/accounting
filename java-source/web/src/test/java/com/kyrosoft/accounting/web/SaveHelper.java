package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.Warehouse;

/**
 * Created by Administrator on 11/8/15.
 */
public interface SaveHelper {
    public <T> T save(T entity) throws ServiceException, DatabasePersistenceException;
}
