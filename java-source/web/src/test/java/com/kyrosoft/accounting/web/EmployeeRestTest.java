package com.kyrosoft.accounting.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.hamcrest.Matchers.*;

import java.util.Date;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kyrosoft.accounting.dao.DepartmentService;
import com.kyrosoft.accounting.dao.EmployeeService;
import com.kyrosoft.accounting.model.Department;
import com.kyrosoft.accounting.model.Employee;
import com.kyrosoft.accounting.model.dto.DepartmentSearchCriteria;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by Administrator on 11/9/15.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({"/rest_context.xml"})
@SqlGroup({ @Sql(scripts = "/sql/test-data.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)})
public class EmployeeRestTest {

    /**
     * Represents the web application context.
     */
    @Autowired
    private WebApplicationContext wac;

    @Autowired private DepartmentService departmentService;

    @Autowired private EmployeeService employeeService;

    /**
     * Represents the mock MVC.
     */
    private MockMvc mockMvc;

    /**
     * Sets up testing environment. It initializes the mock MVC.
     */
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testCreateEmployeeRest() throws Exception {
        Department department = new Department();
        department.setId(1L);

        Employee employee = new Employee();
        employee.setName("test");
        employee.setEmail("test");
        employee.setPassword("test");
        employee.setMobile("test");
        employee.setDepartment(department);

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/rest/employee/create").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(employee)).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("test"));

    }

    @Test
    public void testUpdateEmployeeRest() throws Exception {
        Department department = new Department();
        department.setId(1L);

        Employee employee = new Employee();
        employee.setName("test");
        employee.setEmail("test");
        employee.setPassword("test");
        employee.setMobile("test");
        employee.setIsActive(true);
        employee.setCreatedBy("test");
        employee.setCreatedDate(new Date());
        employee.setDepartment(department);

        employeeService.save(employee);

        employee.setName("updated");
        employee.setDepartment(department);

        ObjectMapper objectMapper = new ObjectMapper();

        MvcResult result = mockMvc.perform(post("/rest/employee/update/"+employee.getId()).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(employee)).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("updated"))
                .andReturn();


    }

    @Test
    public void testGetAllEmployeeRest() throws Exception {
        mockMvc.perform(get("/rest/employee/getall"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[0].name").value("test"));
    }


    @Test
    public void testSearchEmployeeRest() throws Exception {
        mockMvc.perform(get("/rest/employee/search/")
                .param("pageNumber","1").param("pageSize","2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.values", hasSize(2)))
                .andExpect(jsonPath("$.values[0].name").value("test"))
                .andExpect(jsonPath("$.values[1].name").value("test"));
    }

}
