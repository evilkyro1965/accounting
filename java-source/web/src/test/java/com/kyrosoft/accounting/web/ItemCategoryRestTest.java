package com.kyrosoft.accounting.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kyrosoft.accounting.dao.DepartmentService;
import com.kyrosoft.accounting.dao.ItemCategoryService;
import com.kyrosoft.accounting.model.Department;
import com.kyrosoft.accounting.model.ItemCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Administrator on 12/2/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({"/rest_context.xml"})
@SqlGroup({ @Sql(scripts = "/sql/test-data.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)})
public class ItemCategoryRestTest {

    /**
     * Represents the web application context.
     */
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private ItemCategoryService itemCategoryService;

    /**
     * Represents the mock MVC.
     */
    private MockMvc mockMvc;


    /**
     * Sets up testing environment. It initializes the mock MVC.
     */
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }


    @Test
    public void testCreateItemCategoryRest() throws Exception {
        ItemCategory itemCategory = new ItemCategory();
        itemCategory.setName("test");
        itemCategory.setIsActive(true);

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/rest/item-category/create").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(itemCategory)).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("test"));
    }

    @Test
    public void testUpdateItemCategoryRest() throws Exception {
        ItemCategory itemCategory = new ItemCategory();
        itemCategory.setName("test");
        itemCategory.setIsActive(true);
        itemCategory.setCreatedBy("test");
        itemCategory.setCreatedDate(new Date());
        itemCategoryService.save(itemCategory);
        itemCategory.setName("updated");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/rest/item-category/update/"+itemCategory.getId()).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(itemCategory)).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("updated"));
    }

    @Test
    public void testGetAllItemCategoryRest() throws Exception {
        mockMvc.perform(get("/rest/item-category/getall"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[0].name").value("test"));
    }

    @Test
    public void testSearchItemCategoryRest() throws Exception {
        mockMvc.perform(get("/rest/item-category/search/")
                .param("pageNumber","1").param("pageSize","2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.values", hasSize(2)))
                .andExpect(jsonPath("$.values[0].name").value("test"))
                .andExpect(jsonPath("$.values[1].name").value("test"));
    }
}
