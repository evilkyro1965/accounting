package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.IdentifiableEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Administrator on 11/8/15.
 */
@Repository
public class SaveHelperImpl implements SaveHelper {

    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    public <T> T save(T entity) throws ServiceException, DatabasePersistenceException {
        IdentifiableEntity saveEntity = (IdentifiableEntity) entity;
        if(saveEntity.getId()==null || saveEntity.getId()==0 )
            entityManager.persist(entity);
        return entity;
    }
}
