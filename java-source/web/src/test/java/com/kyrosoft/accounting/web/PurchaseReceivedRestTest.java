package com.kyrosoft.accounting.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kyrosoft.accounting.dao.DepartmentService;
import com.kyrosoft.accounting.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Administrator on 11/7/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({"/rest_context.xml"})
@SqlGroup({ @Sql(scripts = "/sql/test-data.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)})
public class PurchaseReceivedRestTest extends BaseTest {

    /**
     * Represents the web application context.
     */
    @Autowired
    private WebApplicationContext wac;

    /**
     * Represents the mock MVC.
     */
    private MockMvc mockMvc;

    /**
     * Sets up testing environment. It initializes the mock MVC.
     */
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    
    @Test
    @Transactional
    @Rollback(false)
    public void testCreateOrderReceivedRest()  throws JsonProcessingException, Exception {

        Purchase purchase = new Purchase();
        purchase.setNo(stringTest);
        purchase.setDate(dateTest);
        purchase.setRequestBy(employeeService.get(IdTest));
        purchase.setOrderBy(employeeService.get(IdTest));
        purchase.setSupplier(supplierService.get(IdTest));
        purchase.setRemark(stringTest);
        purchase.setPurchaseStatus(PurchaseStatus.ORDER);
        setGLEntitySave(purchase);

        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setPurchase(purchase);
        orderDetails.setItem(itemService.get(IdTest));
        orderDetails.setPurchaseMeasurement(measurementService.get(IdTest));
        orderDetails.setQuantity(10);
        orderDetails.setPrice(1000);
        orderDetails.setDiscountPercent(10);
        orderDetails.setTaxPercent(10);
        orderDetails.setOrderReceivedFull(false);
        orderDetails.setStatus(OrderStatus.ORDER);
        setGLEntitySave(orderDetails);

        List<OrderDetails> orderDetailsList = new ArrayList<OrderDetails>();
        orderDetailsList.add(orderDetails);
        purchase.setOrderDetailsList(orderDetailsList);

        purchaseService.save(purchase);

        OrderDetails orderDetails1 = new OrderDetails();
        orderDetails1.setId(orderDetails.getId());

        Purchase purchase1 = new Purchase();
        purchase1.setId(purchase.getId());

        OrderReceived orderReceived = new OrderReceived();
        orderReceived.setOrderDetails(orderDetails1);
        orderReceived.setQuantity(1.0);
        orderReceived.setReceivedBy(employeeService.get(IdTest));
        orderReceived.setReceivedDate(dateTest);
        orderReceived.setWarehouse(warehouseService.get(IdTest));
        orderReceived.setPurchase(purchase1);
        //setGLEntitySave(orderReceived);

        ObjectMapper objectMapper = new ObjectMapper();
        String orderReceivedString = objectMapper.writeValueAsString(orderReceived);

        mockMvc.perform(post("/rest/purchase-received/create").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(orderReceivedString).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    @Transactional
    @Rollback(false)
    public void testUpdateOrderReceivedRest()  throws JsonProcessingException, Exception {

        Purchase purchase = new Purchase();
        purchase.setNo(stringTest);
        purchase.setDate(dateTest);
        purchase.setRequestBy(employeeService.get(IdTest));
        purchase.setOrderBy(employeeService.get(IdTest));
        purchase.setSupplier(supplierService.get(IdTest));
        purchase.setRemark(stringTest);
        purchase.setPurchaseStatus(PurchaseStatus.ORDER);
        setGLEntitySave(purchase);

        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setPurchase(purchase);
        orderDetails.setItem(itemService.get(IdTest));
        orderDetails.setPurchaseMeasurement(measurementService.get(IdTest));
        orderDetails.setQuantity(10);
        orderDetails.setPrice(1000);
        orderDetails.setDiscountPercent(10);
        orderDetails.setTaxPercent(10);
        orderDetails.setOrderReceivedFull(false);
        orderDetails.setStatus(OrderStatus.ORDER);
        setGLEntitySave(orderDetails);

        List<OrderDetails> orderDetailsList = new ArrayList<OrderDetails>();
        orderDetailsList.add(orderDetails);
        purchase.setOrderDetailsList(orderDetailsList);

        purchaseService.save(purchase);

        OrderReceived orderReceived = new OrderReceived();
        orderReceived.setOrderDetails(orderDetails);
        orderReceived.setQuantity(1.0);
        orderReceived.setReceivedBy(employeeService.get(IdTest));
        orderReceived.setReceivedDate(dateTest);
        orderReceived.setWarehouse(warehouseService.get(IdTest));
        orderReceived.setPurchase(purchase);
        setGLEntitySave(orderReceived);

        orderReceivedService.save(orderReceived);

        orderReceived.setQuantity(2.0);

        ObjectMapper objectMapper = new ObjectMapper();
        String orderReceivedString = objectMapper.writeValueAsString(orderReceived);

        mockMvc.perform(post("/rest/purchase-received/update/"+orderReceived.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(orderReceivedString).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    
}
