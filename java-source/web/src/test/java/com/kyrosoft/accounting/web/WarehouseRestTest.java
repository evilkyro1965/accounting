package com.kyrosoft.accounting.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kyrosoft.accounting.dao.WarehouseService;
import com.kyrosoft.accounting.model.Department;
import com.kyrosoft.accounting.model.Warehouse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Administrator on 12/3/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({"/rest_context.xml"})
@SqlGroup({ @Sql(scripts = "/sql/test-data.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)})
public class WarehouseRestTest {


    /**
     * Represents the web application context.
     */
    @Autowired
    private WebApplicationContext wac;

    @Autowired private WarehouseService warehouseService;

    /**
     * Represents the mock MVC.
     */
    private MockMvc mockMvc;

    /**
     * Sets up testing environment. It initializes the mock MVC.
     */
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testCreateWarehouseRest() throws Exception {
        Department department = new Department();
        department.setId(1L);

        Warehouse warehouse = new Warehouse();
        warehouse.setName("test");
        warehouse.setDepartment(department);
        warehouse.setDescription("test");
        warehouse.setLabel("test");
        warehouse.setShelf("test");
        warehouse.setIsActive(true);

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/rest/warehouse/create").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(warehouse)).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("test"));

    }

    @Test
    public void testUpdateWarehouseRest() throws Exception {
        Department department = new Department();
        department.setId(1L);

        Warehouse warehouse = new Warehouse();
        warehouse.setName("test");
        warehouse.setDepartment(department);
        warehouse.setDescription("test");
        warehouse.setLabel("test");
        warehouse.setShelf("test");
        warehouse.setIsActive(true);
        warehouse.setCreatedBy("test");
        warehouse.setCreatedDate(new Date());
        warehouseService.save(warehouse);
        warehouse.setName("updated");
        if(warehouse.getDepartment()!=null) {
            warehouse.getDepartment().setEmployees(null);
        }

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/rest/warehouse/update/"+warehouse.getId()).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(warehouse)).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("updated"));
    }

    @Test
    public void testGetAllWarehouseRest() throws Exception {
        mockMvc.perform(get("/rest/warehouse/getall"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[0].name").value("test"));
    }

    @Test
    public void testSearchWarehouseRest() throws Exception {
        mockMvc.perform(get("/rest/warehouse/search/")
                .param("pageNumber","1").param("pageSize","2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.values", hasSize(2)))
                .andExpect(jsonPath("$.values[0].name").value("test"))
                .andExpect(jsonPath("$.values[1].name").value("test"));
    }

}


