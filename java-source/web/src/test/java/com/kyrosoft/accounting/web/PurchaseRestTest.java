package com.kyrosoft.accounting.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kyrosoft.accounting.dao.DepartmentService;
import com.kyrosoft.accounting.model.Department;
import com.kyrosoft.accounting.model.GLEntity;
import com.kyrosoft.accounting.model.OrderDetails;
import com.kyrosoft.accounting.model.OrderStatus;
import com.kyrosoft.accounting.model.Purchase;
import com.kyrosoft.accounting.model.PurchaseStatus;

/**
 * Created by Administrator on 11/7/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({"/rest_context.xml"})
@SqlGroup({ @Sql(scripts = "/sql/test-data.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)})
public class PurchaseRestTest extends BaseTest {

    /**
     * Represents the web application context.
     */
    @Autowired
    private WebApplicationContext wac;

    @Autowired private DepartmentService departmentService;

    /**
     * Represents the mock MVC.
     */
    private MockMvc mockMvc;

    /**
     * Sets up testing environment. It initializes the mock MVC.
     */
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    
    @Test
    @Transactional
    public void testCreatePurchaseRest()  throws JsonProcessingException, Exception {
        
        Purchase purchase = new Purchase();
        purchase.setNo(stringTest);
        purchase.setDate(dateTest);
        purchase.setRequestBy(employeeService.get(IdTest));
        purchase.setOrderBy(employeeService.get(IdTest));
        purchase.setSupplier(supplierService.get(IdTest));
        purchase.setRemark(stringTest);
        purchase.setPurchaseStatus(PurchaseStatus.ORDER);
        setGLEntitySave(purchase);

        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setPurchase(purchase);
        orderDetails.setItem(itemService.get(IdTest));
        orderDetails.setPurchaseMeasurement(measurementService.get(IdTest));
        orderDetails.setQuantity(10);
        orderDetails.setPrice(1000);
        orderDetails.setDiscountPercent(10);
        orderDetails.setTaxPercent(10);
        orderDetails.setOrderReceivedFull(false);
        orderDetails.setStatus(OrderStatus.ORDER);
        setGLEntitySave(orderDetails);

        List<OrderDetails> orderDetailsList = new ArrayList<OrderDetails>();
        orderDetailsList.add(orderDetails);
        purchase.setOrderDetailsList(orderDetailsList);

        ObjectMapper objectMapper = new ObjectMapper();
        String purchaseString = objectMapper.writeValueAsString(purchase);

        mockMvc.perform(post("/rest/purchase/create").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(purchaseString).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.no").value(stringTest));

    }
    
    @Test
    @Transactional
    public void testUpdatePurchaseRest()  throws JsonProcessingException, Exception {
        
        /**
         * Created new purchase
         */
        Purchase purchase = new Purchase();
        purchase.setNo(stringTest);
        purchase.setDate(dateTest);
        purchase.setRequestBy(employeeService.get(IdTest));
        purchase.setOrderBy(employeeService.get(IdTest));
        purchase.setSupplier(supplierService.get(IdTest));
        purchase.setRemark(stringTest);
        purchase.setPurchaseStatus(PurchaseStatus.ORDER);
        setGLEntitySave(purchase);

        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setPurchase(purchase);
        orderDetails.setItem(itemService.get(IdTest));
        orderDetails.setPurchaseMeasurement(measurementService.get(IdTest));
        orderDetails.setQuantity(10);
        orderDetails.setPrice(1000);
        orderDetails.setDiscountPercent(10);
        orderDetails.setTaxPercent(10);
        orderDetails.setOrderReceivedFull(false);
        orderDetails.setStatus(OrderStatus.ORDER);
        setGLEntitySave(orderDetails);

        List<OrderDetails> orderDetailsList = new ArrayList<OrderDetails>();
        orderDetailsList.add(orderDetails);
        purchase.setOrderDetailsList(orderDetailsList);

        purchaseService.save(purchase);

        /**
         * Update purchase with add new order details
         */
        Purchase saved = new Purchase();
        saved.setId(purchase.getId());
        saved.setNo(stringTest);
        saved.setDate(dateTest);
        saved.setRemark(stringTest);
        List<OrderDetails> updateOrderDetailsList = purchase.getOrderDetailsList();

        OrderDetails orderDetailsAdd = new OrderDetails();
        orderDetailsAdd.setPurchase(purchase);
        orderDetailsAdd.setItem(itemService.get(IdTest));
        orderDetailsAdd.setPurchaseMeasurement(measurementService.get(IdTest));
        orderDetailsAdd.setQuantity(5);
        orderDetailsAdd.setPrice(5000);
        orderDetailsAdd.setDiscountPercent(5);
        orderDetailsAdd.setTaxPercent(10);
        orderDetailsAdd.setOrderReceivedFull(false);
        setGLEntitySave(orderDetailsAdd);

        updateOrderDetailsList.add(orderDetailsAdd);
        saved.setOrderDetailsList(updateOrderDetailsList);

        ObjectMapper objectMapper = new ObjectMapper();
        String purchaseString = objectMapper.writeValueAsString(saved);

        mockMvc.perform(post("/rest/purchase/update/"+saved.getId()).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(purchaseString).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.no").value(stringTest));

    }

    
}
