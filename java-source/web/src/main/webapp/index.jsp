<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 10/19/15
  Time: 1:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
      <title>Inventory</title>

      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

      <link rel="stylesheet" href="resources/kendoui/styles/kendo.common.min.css" />
      <link rel="stylesheet" href="resources/kendoui/styles/kendo.silver.min.css" />

      <script src="resources/kendoui/js/jquery.min.js"></script>
      <script src="resources/kendoui/js/kendo.all.min.js"></script>
      <link rel="stylesheet" type="text/css" href="resources/css/general.css" />
      <link rel="stylesheet" type="text/css" href="resources/css/style.css" />
      <script>
          var dataSource = new kendo.data.DataSource({
              transport: {
                  read: {
                      url: "http://localhost:8080/rest/department/search",
                      dataType: "json"
                  }
              },
              schema: {
                  data: function(response) {
                      return response.values; // twitter's response is { "results": [ /* results */ ] }
                  },
                  model: {
                      fields: {
                          id: { type: "number" },
                          name: { type: "string" },
                          email: { type: "string" },
                          isActive: { type: "boolean" }
                      }
                  }
              }
          });
          dataSource.fetch(function() {
              var department = dataSource.data();
              console.log(department);
          });
      </script>
  </head>
  <body>

  </body>
</html>
