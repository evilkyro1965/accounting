<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/KendoInput.tld" prefix="kendo" %>

<div class="formMainWrapper">

<div id="sidebar">
    <ul class="operationList">
        <li><a href="javascript:;" class="view">View</a></li>
        <li><a href="javascript:;" class="add">Add</a></li>
        <li><a href="javascript:;" class="edit">Edit</a></li>
        <li><a href="javascript:;" class="delete">Delete</a></li>
        <li><a href="javascript:;" class="search">Search</a></li>
    </ul>
    <div class="clear"></div>
    <div class="sidebarContent">
        <ul class="verticalFormList">
            <form>
                <kendo:InputText label="First Name" bindingName="firstName" />
                <kendo:Dropdown label="State" bindingName="state" dataSourceName="stateDataSource"/>
                <kendo:Combo label="City" bindingName="city" dataSourceName="cityDataSource"/>
                <kendo:CheckboxRadioWrapper label="Is Active Employee">
                    <kendo:Checkbox label="Is Active" bindingName="isActive" />
                    <kendo:Checkbox label="Is Employee" bindingName="isEmployee" />
                </kendo:CheckboxRadioWrapper>
                <kendo:CheckboxRadioWrapper label="Employee Status">
                    <kendo:Radio cssId="contractEmployee" label="Contract" bindingName="employeeStatus" value="contract" />
                    <kendo:Radio cssId="permanentEmployee" label="Permanent" bindingName="employeeStatus" value="permanent" />
                </kendo:CheckboxRadioWrapper>
                <li>
                    <!--
                    <label>Employee Status</label>
                    <input id="contractEmployee" type="radio" name="employeeStatus" data-bind="checked: employeeStatus" value="contract" />
                    <label for="contractEmployee" class="checkboxRadioLabel">Is Active</label>

                    <input id="permanentEmployee" type="radio" name="employeeStatus" data-bind="checked: employeeStatus" value="permanent" />
                    <label for="permanentEmployee" class="checkboxRadioLabel">Permanent</label>
                    -->
                </li>
                <li>
                    <label for="simple-input">Address</label>
                    <input id="simple-input" data-bind="value: address" type="text" class="k-textbox" style="width: 100%;" />
                </li>
                <li>
                    <label for="simple-input">Phone</label>
                    <input id="simple-input" data-bind="value: phone" type="text" class="k-textbox" style="width: 100%;" />
                </li>
                <li>
                    <button class="k-button k-primary submit" data-bind="click: submit">Submit</button>
                    <button class="k-button cancel">Cancel</button>
                </li>
            </form>
        </ul>
    </div>

</div>

<div id="main-content">
    <h5 class="pageTitle">Department</h5>

    <div data-role="grid"
         date-scrollable="true"
         data-columns="[
                                 { 'field': 'name', 'width': 270, 'title' : 'Name' },
                                 { 'field': 'email', 'width': 270, 'title' : 'Email' },
                                 { 'field': 'isActive', 'title' : 'Is Active' }
                              ]"
         data-pageable="true"
         data-bind="source: departments">
    </div>

</div>

<div class="clear"></div>

</div>
<script>
    $(document).ready(function() {
        var validator = $("form").kendoValidator().data("kendoValidator");

        var viewModel = kendo.observable({
            firstName: "",
            stateDataSource: new kendo.data.DataSource({
                data: [
                    {name: "Item1", id: 1},
                    {name: "Item2", id: 2},
                    {name: "Item3", id: 3}
                ]
            }),
            cityDataSource: new kendo.data.DataSource({
                data: [
                    {name: "Item1", id: 1},
                    {name: "Item2", id: 2},
                    {name: "Item3", id: 3}
                ]
            }),
            state: "",
            city: "",
            isActive : true,
            isEmployee : true,
            employeeStatus : null,
            address: "",
            phone: "",
            departments: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "http://localhost:8080/rest/department/search",
                        dataType: "json"
                    },
                    parameterMap: function(data, type) {
                        if (type == "read") {
                            // send take as "$top" and skip as "$skip"
                            return {
                                pageNumber: data.page,
                                pageSize: data.pageSize
                            }
                        }
                    }
                },
                serverPaging: true,
                page : 1,
                pageSize : 20,
                schema: {
                    data: function(response) {
                        return response.values;
                    },
                    model: {
                        fields: {
                            id: { type: "number" },
                            name: { type: "string" },
                            email: { type: "string" },
                            isActive: { type: "boolean" }
                        }
                    },
                    total: "total"
                }
            }),
            submit: function(e) {
                e.preventDefault();

                validator.validate();
                console.log(this.get("employeeStatus"));
            }
        });

        kendo.bind($(".formMainWrapper"), viewModel);
    });
</script>