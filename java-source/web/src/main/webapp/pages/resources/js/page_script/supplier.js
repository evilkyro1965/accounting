/**
 * Created by Administrator on 11/7/15.
 */
var viewModel;

$(document).ready(function() {

    var validationSaveConf = [
        {'fieldName':'name','validationType':'IsNotEmpty','errorDiv':'#nameSaveError'}
    ];
    var validationEditConf = [
        {'fieldName':'name','validationType':'IsNotEmpty','errorDiv':'#nameEditError'}
    ];

    viewModel = kendo.observable({
        viewForm : {
        },
        saveForm : {
            name: "",
            contactPerson: "",
            address: "",
            secondaryAddress: "",
            city: "",
            postalCode: "",
            state: "",
            country: "",
            phone: "",
            fax: "",
            email: ""
        },
        editForm : {
            id : 0,
            name: "",
            contactPerson: "",
            address: "",
            secondaryAddress: "",
            city: "",
            postalCode: "",
            state: "",
            country: "",
            phone: "",
            fax: "",
            email: "",
            isActive: false
        },
        searchForm : {
            name: "",
            address: "",
            phone: "",
            email: "",
            contactPerson: "",
            isActive: "",
            sortBy : "",
            sortType : "ASC"
        },
        searchFormVisible : false,
        gridDataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/supplier/search",
                    dataType: "json"
                },
                parameterMap: function(data, type) {
                    if (type == "read") {
                        // send take as "$top" and skip as "$skip"
                        return {
                            pageNumber: data.page,
                            pageSize: data.pageSize,
                            sortBy: viewModel.get("searchForm.sortBy"),
                            sortType: viewModel.get("searchForm.sortType"),
                            name: viewModel.get("searchForm.name"),
                            address: viewModel.get("searchForm.address"),
                            phone: viewModel.get("searchForm.phone"),
                            email: viewModel.get("searchForm.email"),
                            contactPerson: viewModel.get("searchForm.contactPerson"),
                            isActive: viewModel.get("searchForm.isActive")
                        }
                    }
                }
            },
            serverPaging: true,
            page : 1,
            pageSize : 20,
            schema: {
                data: function(response) {
                    return response.values;
                },
                model: {
                    fields: {
                        id: { type: "number" },
                        name: { type: "string" },
                        isActive: { type: "boolean" },
                        isActiveString: { type: "string"}
                    }
                },
                parse: function(response) {
                    var rows = response.values;
                    for (var i = 0; i < rows.length; i++) {
                        rows[i].isActiveString = rows[i].isActive == true ? "Yes" : "No";
                    }
                    response.values = rows;
                    return response;
                },
                total: "total"
            }
        }),
        gridSelectedItem : null,
        hideAllTab: function() {
            this.set("searchFormVisible",false);

            this.set("saveForm.name","");
            this.set("saveForm.email","");

            modalView.data("kendoWindow").close();
            modalSave.data("kendoWindow").close();
            modalEdit.data("kendoWindow").close();
            modalSearch.data("kendoWindow").close();
        },
        showViewForm: function(event) {
            var selectedItem = this.dataItem($(event.currentTarget).closest("tr"));
            viewModel.set("viewForm",selectedItem);

            viewModel.hideAllTab();
            modalView.data("kendoWindow").center().open();
        },
        showEditForm: function(event) {
            viewModel.hideAllTab();

            var selectedItem = this.dataItem($(event.currentTarget).closest("tr"));
            viewModel.set("editForm.id",selectedItem.id);
            viewModel.set("editForm.name",selectedItem.name);
            viewModel.set("editForm.contactPerson",selectedItem.contactPerson);
            viewModel.set("editForm.address",selectedItem.address);
            viewModel.set("editForm.secondaryAddress",selectedItem.secondaryAddress);
            viewModel.set("editForm.city",selectedItem.city);
            viewModel.set("editForm.postalCode",selectedItem.postalCode);
            viewModel.set("editForm.state",selectedItem.state);
            viewModel.set("editForm.country",selectedItem.country);
            viewModel.set("editForm.phone",selectedItem.phone);
            viewModel.set("editForm.fax",selectedItem.fax);
            viewModel.set("editForm.email",selectedItem.email);
            viewModel.set("editForm.isActive",selectedItem.isActive);
            var editValidator = new ValueValidator(null,validationEditConf);
            editValidator.clean();
            modalEdit.data("kendoWindow").center().open();
        },
        showSaveForm: function(event) {
            this.hideAllTab();
            var saveValidator = new ValueValidator(null,validationSaveConf);
            saveValidator.clean();
            modalSave.data("kendoWindow").center().open();
        },
        showSearchForm: function(event) {
            this.hideAllTab();
            modalSearch.data("kendoWindow").center().open();
        },
        gridOnChange : function(event) {
            var grid = event.sender;
            var selectedItem = grid.select();
            console.log(grid.dataItem(selectedItem));
        },
        cancelEdit : function() {

            this.hideAllTab();
        },
        submit: function() {

            var saveForm = this.get("saveForm");

            var saveValidator = new ValueValidator(saveForm,validationSaveConf);
            var isValid = saveValidator.validate();

            if(isValid){
                var supplier = supplierViewObjectToJsObject(saveForm);

                $.ajax({
                    url: baseUrl+"/rest/supplier/create",
                    type: "post",
                    data: JSON.stringify(supplier),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {
                        viewModel.gridDataSource.fetch();
                        viewModel.hideAllTab();
                        viewModel.set("editFormVisible",false);
                        dialogAlert(saveSuccessDialog);
                        modalSave.data("kendoWindow").close();
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }
            else {
                dialogAlert(errorInputDialog);
            }
        },
        submitUpdate: function() {

            var editForm = this.get("editForm");

            var editValidator = new ValueValidator(editForm,validationEditConf);
            var isValid = editValidator.validate();

            if(isValid){

                var supplier = supplierViewObjectToJsObject(editForm);

                $.ajax({
                    url: baseUrl+"/rest/supplier/update/"+supplier.id,
                    type: "post",
                    data: JSON.stringify(supplier),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {
                        viewModel.gridDataSource.fetch();
                        viewModel.hideAllTab();
                        viewModel.set("editFormVisible",false);
                        dialogAlert(saveSuccessDialog);
                        modalEdit.data("kendoWindow").close();
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }
            else {
                dialogAlert(errorInputDialog);
            }
        },
        submitSearch: function(e) {
            e.preventDefault();
            viewModel.gridDataSource.fetch();
            modalSearch.data("kendoWindow").close();
        }
    });

    kendo.bind($(".formMainWrapper"), viewModel);

    var modalView = $("#modalView");
    modalView.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "View",
        modal: true,
        visible: false,
        animation: false
    });

    var modalSave = $("#modalSave");
    modalSave.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "Save",
        modal: true,
        visible: false,
        animation: false
    });

    var modalEdit = $("#modalEdit");
    modalEdit.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "Update",
        modal: true,
        visible: false,
        animation: false
    });

    var modalSearch = $("#modalSearch");
    modalSearch.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "Search",
        modal: true,
        visible: false,
        animation: false
    });
});

function supplierViewObjectToJsObject(viewObject) {
    var supplier = {};
    supplier.id = viewObject.id;
    supplier.name = viewObject.name;
    supplier.contactPerson = viewObject.contactPerson;
    supplier.address = viewObject.address;
    supplier.secondaryAddress = viewObject.secondaryAddress;
    supplier.city = viewObject.city;
    supplier.postalCode = viewObject.postalCode;
    supplier.state = viewObject.state;
    supplier.country = viewObject.country;
    supplier.phone = viewObject.phone;
    supplier.fax = viewObject.fax;
    supplier.email = viewObject.email;
    supplier.isActive = viewObject.isActive;

    return supplier;
}