/**
 * Created by Administrator on 11/7/15.
 */
var viewModel;
var increment = 0;
var purchaseId = 0;

$(document).ready(function() {
    purchaseId = purl().param('id');
    var validationAddConf = [
        {'fieldName':'item','validationType':'IsValidCombo','errorDiv':'#itemAddError'},
        {'fieldName':'measurement','validationType':'IsValidCombo','errorDiv':'#measurementAddError'},
        {'fieldName':'quantity','validationType':'IsNotEmptyNumber','errorDiv':'#quantitySaveError'},
        {'fieldName':'price','validationType':'IsNotEmptyNumber','errorDiv':'#priceSaveError'}
    ];
    var validationEditConf = [
        {'fieldName':'name','validationType':'IsNotEmpty','errorDiv':'#nameEditError'}
    ];
    var addOrderDetailsDefault = {
        no : "",
        item: "",
        measurement: "",
        quantity: "",
        price: "",
        discount: "",
        tax: "",
        measurementIndex : 0,
        itemIndex: 0
    };

    viewModel = kendo.observable({
        ordersDataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/purchase-order/get/"+purchaseId,
                    dataType: "json"
                }
            },
            schema: {
                data: function(response) {
                    return response.orderDetailsList;
                },
                parse: function(response) {
                    var rows = response.orderDetailsList;
                    for (var i = 0; i < rows.length; i++) {
                        rows[i].no = i;
                    }
                    response.values = rows;
                    return response;
                }
            }
        }),
        purchaseForm :  {
            no : "",
            date : "",
            deliveryDate: "",
            supplier: "",
            requestBy: "",
            remark: ""
        },
        addOrderDetailsForm : addOrderDetailsDefault,
        editOrderDetailsForm : {
            no : "",
            item: "",
            measurement: "",
            quantity: "",
            price: "",
            discount: "",
            tax: "",
            measurementIndex : 0,
            itemIndex: 0
        },
        items: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/item/getall",
                    dataType: "json"
                }
            }
        }),
        measurements: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/measurement/search?pageNumber=1&pageSize="+allRow+
                        "&type=STOCK",
                    dataType: "json"
                }
            },
            schema: {
                data: function(response) {
                    return response.values;
                }
            }
        }),
        suppliers: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/supplier/search?pageNumber=1&pageSize="+allRow+
                        "&type=STOCK",
                    dataType: "json"
                }
            },
            schema: {
                data: function(response) {
                    return response.values;
                }
            }
        }),
        employees: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/employee/search?pageNumber=1&pageSize="+allRow,
                    dataType: "json"
                }
            },
            schema: {
                data: function(response) {
                    return response.values;
                }
            }
        }),
        gridSelectedItem : null,
        hideAllTab: function() {
            this.set("searchFormVisible",false);

            modalAdd.data("kendoWindow").close();
            modalEdit.data("kendoWindow").close();
        },
        cleanModal: function() {
            this.set("addOrderDetailsForm",addOrderDetailsDefault);
        },
        showEditForm: function(event) {
            viewModel.hideAllTab();
            var selectedItem = this.dataItem($(event.currentTarget).closest("tr"));
            console.log(selectedItem);
            viewModel.set("editOrderDetailsForm.no",selectedItem.no);
            viewModel.set("editOrderDetailsForm.item",selectedItem.item.id);
            viewModel.set("editOrderDetailsForm.measurement",selectedItem.purchaseMeasurement.id);
            viewModel.set("editOrderDetailsForm.quantity",selectedItem.quantity);
            viewModel.set("editOrderDetailsForm.price",selectedItem.price);
            viewModel.set("editOrderDetailsForm.discount",selectedItem.discountPercent);
            viewModel.set("editOrderDetailsForm.tax",selectedItem.taxPercent);
            var editValidator = new ValueValidator(null,validationEditConf);
            editValidator.clean();
            modalEdit.data("kendoWindow").center().open();
        },
        showSaveForm: function(event) {
            this.hideAllTab();
            var addValidator = new ValueValidator(null,validationAddConf);
            addValidator.clean();
            this.cleanModal();
            modalAdd.data("kendoWindow").center().open();
        },
        gridOnChange : function(event) {
            var grid = event.sender;
            var selectedItem = grid.select();
            console.log(grid.dataItem(selectedItem));
        },
        cancelEdit : function() {

            this.hideAllTab();
        },
        addOrderDetails: function() {
            var addOrderDetailsForm = this.get("addOrderDetailsForm");

            var addValidator = new ValueValidator(addOrderDetailsForm,validationAddConf);
            var isValid = addValidator.validate();

            if(isValid){

                var no = increment;
                increment++;
                var discount = addOrderDetailsForm.discount == "" ? 0 : addOrderDetailsForm.discount;
                var tax = addOrderDetailsForm.tax == "" ? 0 : addOrderDetailsForm.tax;
                var totalTemp = addOrderDetailsForm.quantity * addOrderDetailsForm.price;
                var totalAfterDisc = totalTemp - (totalTemp * (discount/100));
                var totalAfterTax = totalAfterDisc + (totalAfterDisc * (tax/100));

                var itemAdd = $("#itemAdd").data("kendoComboBox");
                var itemTemp = itemAdd.dataItem();
                var item = {id:itemTemp.id,name:itemTemp.name};

                var measurementAdd = $("#measurementAdd").data("kendoComboBox");
                var measurementTemp = measurementAdd.dataItem();
                var measurement = {id:measurementTemp.id,name:measurementTemp.name};

                this.get("ordersDataSource").add(
                    {
                        no : no,
                        item : item,
                        purchaseMeasurement : measurement,
                        quantity : addOrderDetailsForm.quantity,
                        price : addOrderDetailsForm.price,
                        discountPercent : discount,
                        taxPercent : tax,
                        subTotal : totalAfterTax
                    });
                modalAdd.data("kendoWindow").close();
            }
            else {
                dialogAlert(errorInputDialog);
            }
        },
        editOrderDetails: function() {
            var editOrderDetailsForm = this.get("editOrderDetailsForm");

            var editValidtor = new ValueValidator(editOrderDetailsForm,validationAddConf);
            var isValid = editValidtor.validate();

            if(isValid){

                var no = editOrderDetailsForm.no;
                var discount = editOrderDetailsForm.discount == "" ? 0 : editOrderDetailsForm.discount;
                var tax = editOrderDetailsForm.tax == "" ? 0 : editOrderDetailsForm.tax;
                var totalTemp = editOrderDetailsForm.quantity * editOrderDetailsForm.price;
                var totalAfterDisc = totalTemp - (totalTemp * (discount/100));
                var totalAfterTax = totalAfterDisc + (totalAfterDisc * (tax/100));

                var itemAdd = $("#itemEdit").data("kendoComboBox");
                var itemTemp = itemAdd.dataItem();
                var item = {id:itemTemp.id,name:itemTemp.name};

                var measurementAdd = $("#measurementEdit").data("kendoComboBox");
                var measurementTemp = measurementAdd.dataItem();
                var measurement = {id:measurementTemp.id,name:measurementTemp.name};

                var data = this.get("ordersDataSource").data();
                data[no].no = no;
                data[no].item = item;
                data[no].purchaseMeasurement = measurement;
                data[no].quantity = editOrderDetailsForm.quantity;
                data[no].price = editOrderDetailsForm.price;
                data[no].discountPercent = discount;
                data[no].taxPercent = tax;
                data[no].subTotal = totalAfterTax;
                /*
                data[no] =
                    {
                        no : no,
                        item : item,
                        purchaseMeasurement : measurement,
                        quantity : editOrderDetailsForm.quantity,
                        price : editOrderDetailsForm.price,
                        discountPercent : discount,
                        taxPercent : tax,
                        totalPrice : totalAfterTax
                    };
                */
                console.log(data);
                this.get("ordersDataSource").data(data);
                modalEdit.data("kendoWindow").close();
            }
            else {
                dialogAlert(errorInputDialog);
            }
        },
        deleteOrderDetails: function(event) {

            var selectedItem = this.dataItem($(event.currentTarget).closest("tr"));
            var no = selectedItem.no;

            var dataSource = viewModel.get("ordersDataSource").data();
            dataSource.remove(dataSource[no]);

        },
        submit: function() {

            var isValid = true;

            if(isValid){

                var purchase = GetPurchaseObject(this);
                console.log(purchase);

                $.ajax({
                    url: baseUrl+"/rest/purchase-order/update/"+purchaseId,
                    type: "post",
                    data: JSON.stringify(purchase),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {
                        modalOk.show();
                    },
                    error: function (response) {
                        alert(response);
                    }
                });

            }
            else {
                dialogAlert(errorInputDialog);
            }
        }
    });

    kendo.bind($(".formMainWrapper"), viewModel);

    $.ajax({
        url: baseUrl+"/rest/purchase-order/get/"+purchaseId,
        type: "get",
        datatype: 'json',
        success: function (response) {
            viewModel.set("purchaseForm.no",response.no);
            viewModel.set("purchaseForm.date",new Date(response.date));
            viewModel.set("purchaseForm.remark",response.remark);
            if(response.supplier!=null){
                viewModel.set("purchaseForm.supplier",response.supplier.id);
            }
            if(response.requestBy!=null){
                viewModel.set("purchaseForm.requestBy",response.requestBy.id);
            }
        },
        error: function (response) {
            alert(response);
        }
    });

    var modalView = $("#modalView");
    modalView.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "View",
        modal: true,
        visible: false,
        animation: false
    });

    var modalAdd = $("#modalAdd");
    modalAdd.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "Add",
        modal: true,
        visible: false,
        animation: false
    });

    var modalEdit = $("#modalEdit");
    modalEdit.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "Update",
        modal: true,
        visible: false,
        animation: false
    });

    var modalOk = new KendoOkModal("Success","Update Success");
    modalOk.onOk = function(event) {
        location.reload();
    };
});

function itemViewObjectToJsObject(viewObject) {
    var item = {};
    item.id = viewObject.id;
    item.name = viewObject.name;
    item.itemCode = viewObject.itemCode;
    item.itemType = viewObject.type;
    item.itemDescription = viewObject.description;
    item.isActive = viewObject.isActive;
    var category = {};
    category.id = viewObject.category;
    item.category = category;

    var measurement = {};
    measurement.id = viewObject.stockMeasurement;
    item.stockMeasurement = measurement;

    return item;
}

function GetPurchaseObject(viewObject) {

    var purchase = {};
    var purchaseForm = viewObject.get("purchaseForm");

    purchase.no = purchaseForm.no;
    purchase.date = purchaseForm.date;
    purchase.supplier = {id:purchaseForm.supplier};
    purchase.warehouse = {id:purchaseForm.warehouse};
    purchase.requestBy = {id:purchaseForm.requestBy};
    purchase.remark = purchaseForm.remark;

    var orders = viewObject.get("ordersDataSource").data();

    purchase.orderDetailsList = orders;

    console.log(purchase);
    return purchase;
}