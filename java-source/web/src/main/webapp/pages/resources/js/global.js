/**
 * Created by Administrator on 11/6/15.
 */
var baseUrl = "http://localhost:8080";
var templateBaseUrl = baseUrl+"/pages/resources/templates/";

var allRow = 1000000;
var formModalWidth = "800px";
var errorInputDialog = "Some input is not correct, please check!";
var saveSuccessDialog = "Save success";

kendo.culture("indo-custom");

var KendoOkModal = function(title,message) {
    this.windowObj = $("<div />").kendoWindow({
        width: "500px",
        title: title,
        visible: false,
        modal: true,
        actions: [
            "Close"
        ]
    });
    this.onOk = function(event) {};

    var _this = this;

    this.modalContent = ''+
        '<div class="modalWrapper">'+
        '    <div class="modalMessage"></div>'+
        '        <div class="modalButtonWrapper">'+
        '            <a href="javascript:;" class="k-button k-primary ok">OK</a>'+
        '        </div>'+
        '</div>';
    this.windowObj.data("kendoWindow")
        .content(this.modalContent);

    this.windowObj
        .find(".modalMessage").html(message);

    this.windowObj
        .find(".ok")
        .click(function(event) {
            _this.onOk(event);
            _this.windowObj.data("kendoWindow").close();
        }).end();

    this.show = function() {
        _this.windowObj.data("kendoWindow").center().open();
    };
};

