/**
 * Created by Administrator on 11/7/15.
 */
var viewModel;

$(document).ready(function() {

    var validationSaveConf = [
        {'fieldName':'name','validationType':'IsNotEmpty','errorDiv':'#nameSaveError'},
        {'fieldName':'username','validationType':'IsNotEmpty','errorDiv':'#usernameSaveError'},
        {'fieldName':'email','validationType':'IsValidEmail','errorDiv':'#emailSaveError'},
        {'fieldName':'department','validationType':'IsValidCombo','errorDiv':'#departmentSaveError'}
    ];
    var validationEditConf = [
        {'fieldName':'name','validationType':'IsNotEmpty','errorDiv':'#nameEditError'},
        {'fieldName':'username','validationType':'IsNotEmpty','errorDiv':'#usernameEditError'},
        {'fieldName':'email','validationType':'IsValidEmail','errorDiv':'#emailEditError'},
        {'fieldName':'department','validationType':'IsValidCombo','errorDiv':'#departmentEditError'}
    ];

    viewModel = kendo.observable({
        viewForm : {
        },
        saveForm : {
            name: "",
            username: "",
            department: "",
            email: "",
            mobile: ""
        },
        editForm : {
            name: "",
            username: "",
            department: "",
            email: "",
            mobile: "",
            isActive : false
        },
        searchForm : {
            name: "",
            username: "",
            department: "",
            mobile: "",
            email: "",
            isActive: "",
            sortBy : "",
            sortType : "ASC"
        },
        searchFormVisible : false,
        gridDataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/employee/search",
                    dataType: "json"
                },
                parameterMap: function(data, type) {
                    if (type == "read") {
                        // send take as "$top" and skip as "$skip"
                        return {
                            pageNumber: data.page,
                            pageSize: data.pageSize,
                            sortBy: viewModel.get("searchForm.sortBy"),
                            sortType: viewModel.get("searchForm.sortType"),
                            name: viewModel.get("searchForm.name"),
                            username: viewModel.get("searchForm.username"),
                            departmentId: viewModel.get("searchForm.department"),
                            mobile: viewModel.get("searchForm.mobile"),
                            email: viewModel.get("searchForm.email"),
                            isActive: viewModel.get("searchForm.isActive")
                        }
                    }
                }
            },
            serverPaging: true,
            page : 1,
            pageSize : 20,
            schema: {
                data: function(response) {
                    return response.values;
                },
                model: {
                    fields: {
                        id: { type: "number" },
                        name: { type: "string" },
                        username: { type: "string" },
                        mobile: { type: "string" },
                        email: { type: "string" },
                        isActive: { type: "boolean" },
                        isActiveString: { type: "string"}
                    }
                },
                parse: function(response) {
                    var rows = response.values;
                    for (var i = 0; i < rows.length; i++) {
                        rows[i].isActiveString = rows[i].isActive == true ? "Yes" : "No";
                    }
                    response.values = rows;
                    return response;
                },
                total: "total"
            }
        }),
        departments: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/department/getall",
                    dataType: "json"
                },
                schema: {
                    data: function(response) {
                        return response.values;
                    }
                }
            }
        }),
        gridSelectedItem : null,
        hideAllTab: function() {
            this.set("searchFormVisible",false);

            this.set("saveForm.name","");
            this.set("saveForm.email","");

            modalView.data("kendoWindow").close();
            modalSave.data("kendoWindow").close();
            modalEdit.data("kendoWindow").close();
            modalSearch.data("kendoWindow").close();
        },
        showViewForm: function(event) {
            var selectedItem = this.dataItem($(event.currentTarget).closest("tr"));
            viewModel.set("viewForm",selectedItem);

            viewModel.hideAllTab();
            modalView.data("kendoWindow").center().open();
        },
        showEditForm: function(event) {
            viewModel.hideAllTab();

            var selectedItem = this.dataItem($(event.currentTarget).closest("tr"));
            viewModel.set("editForm.id",selectedItem.id);
            viewModel.set("editForm.name",selectedItem.name);
            viewModel.set("editForm.username",selectedItem.username);
            viewModel.set("editForm.department",selectedItem.department.id);
            viewModel.set("editForm.email",selectedItem.email);
            viewModel.set("editForm.mobile",selectedItem.mobile);
            viewModel.set("editForm.isActive",selectedItem.isActive);
            var editValidator = new ValueValidator(null,validationEditConf);
            editValidator.clean();
            modalEdit.data("kendoWindow").center().open();
        },
        showSaveForm: function(event) {
            this.hideAllTab();
            var saveValidator = new ValueValidator(null,validationSaveConf);
            saveValidator.clean();
            modalSave.data("kendoWindow").center().open();
        },
        showSearchForm: function(event) {
            this.hideAllTab();
            modalSearch.data("kendoWindow").center().open();
        },
        gridOnChange : function(event) {
            var grid = event.sender;
            var selectedItem = grid.select();
            console.log(grid.dataItem(selectedItem));
        },
        cancelEdit : function() {

            this.hideAllTab();
        },
        submit: function() {

            var saveForm = this.get("saveForm");

            var saveValidator = new ValueValidator(saveForm,validationSaveConf);
            var isValid = saveValidator.validate();

            if(isValid){
                var employee = employeeViewObjectToJsObject(saveForm);

                $.ajax({
                    url: baseUrl+"/rest/employee/create",
                    type: "post",
                    data: JSON.stringify(employee),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {
                        viewModel.gridDataSource.fetch();
                        viewModel.hideAllTab();
                        viewModel.set("editFormVisible",false);
                        dialogAlert(saveSuccessDialog);
                        modalSave.data("kendoWindow").close();
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }
            else {
                dialogAlert(errorInputDialog);
            }
        },
        submitUpdate: function() {

            var editForm = this.get("editForm");

            var editValidator = new ValueValidator(editForm,validationEditConf);
            var isValid = editValidator.validate();

            if(isValid){

                var employee = employeeViewObjectToJsObject(editForm);

                $.ajax({
                    url: baseUrl+"/rest/employee/update/"+employee.id,
                    type: "post",
                    data: JSON.stringify(employee),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {
                        viewModel.gridDataSource.fetch();
                        viewModel.hideAllTab();
                        viewModel.set("editFormVisible",false);
                        dialogAlert(saveSuccessDialog);
                        modalEdit.data("kendoWindow").close();
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }
            else {
                dialogAlert(errorInputDialog);
            }
        },
        submitSearch: function(e) {
            e.preventDefault();
            viewModel.gridDataSource.fetch();
            modalSearch.data("kendoWindow").close();
        }
    });

    kendo.bind($(".formMainWrapper"), viewModel);

    var modalView = $("#modalView");
    modalView.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "View",
        modal: true,
        visible: false,
        animation: false
    });

    var modalSave = $("#modalSave");
    modalSave.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "Save",
        modal: true,
        visible: false,
        animation: false
    });

    var modalEdit = $("#modalEdit");
    modalEdit.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "Update",
        modal: true,
        visible: false,
        animation: false
    });

    var modalSearch = $("#modalSearch");
    modalSearch.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "Search",
        modal: true,
        visible: false,
        animation: false
    });
});

function employeeViewObjectToJsObject(viewObject) {
    var employee = {};
    employee.id = viewObject.id;
    employee.name = viewObject.name;
    employee.username = viewObject.username;
    employee.mobile = viewObject.mobile;
    employee.email = viewObject.email;
    employee.isActive = viewObject.isActive;
    var department = {};
    department.id = viewObject.department;
    employee.department = department;

    return employee;
}