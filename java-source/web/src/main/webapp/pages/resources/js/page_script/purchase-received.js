/**
 * Created by Administrator on 11/7/15.
 */
var viewModel;
var increment = 0;
var purchaseId = 0;

$(document).ready(function() {
    purchaseId = purl().param('id');
    var validationAddConf = [
        {'fieldName':'item','validationType':'IsValidCombo','errorDiv':'#itemAddError'},
        {'fieldName':'measurement','validationType':'IsValidCombo','errorDiv':'#measurementAddError'},
        {'fieldName':'quantity','validationType':'IsNotEmptyNumber','errorDiv':'#quantitySaveError'},
        {'fieldName':'price','validationType':'IsNotEmptyNumber','errorDiv':'#priceSaveError'}
    ];
    var validationEditConf = [
        {'fieldName':'name','validationType':'IsNotEmpty','errorDiv':'#nameEditError'}
    ];

    var addOrderReceivedDefault =  {
        orderDetailsId: null,
            itemId: null,
            itemName: "",
            date: new Date(),
            quantity: "",
            warehouse: null,
            requestBy: null
    };

    viewModel = kendo.observable({
        ordersDataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/purchase-received/get/"+purchaseId,
                    dataType: "json"
                }
            },
            schema: {
                data: function(response) {
                    return response.orderDetailsList;
                },
                parse: function(response) {
                    var rows = response.orderDetailsList;
                    for (var i = 0; i < rows.length; i++) {
                        rows[i].no = i;
                        rows[i].receivedQty = getReceivedQtyTotal(rows[i].orderReceivedList);
                    }
                    response.values = rows;
                    console.log(rows);
                    return response;
                }
            }
        }),
        purchaseForm :  {
            no : "",
            date : "",
            deliveryDate: "",
            supplier: "",
            requestBy: "",
            remark: ""
        },
        addOrderReceived : addOrderReceivedDefault,
        editOrderReceived : {
            id: null,
            orderDetailsId: null,
            itemId: null,
            itemName: "",
            date: new Date(),
            quantity: "",
            warehouse: null,
            requestBy: null
        },
        items: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/item/getall",
                    dataType: "json"
                }
            }
        }),
        measurements: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/measurement/search?pageNumber=1&pageSize="+allRow+
                        "&type=STOCK",
                    dataType: "json"
                }
            },
            schema: {
                data: function(response) {
                    return response.values;
                }
            }
        }),
        suppliers: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/supplier/search?pageNumber=1&pageSize="+allRow+
                        "&type=STOCK",
                    dataType: "json"
                }
            },
            schema: {
                data: function(response) {
                    return response.values;
                }
            }
        }),
        warehouses: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/warehouse/search?pageNumber=1&pageSize="+allRow,
                    dataType: "json"
                }
            },
            schema: {
                data: function(response) {
                    return response.values;
                }
            }
        }),
        employees: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/employee/search?pageNumber=1&pageSize="+allRow,
                    dataType: "json"
                }
            },
            schema: {
                data: function(response) {
                    return response.values;
                }
            }
        }),
        gridSelectedItem : null,
        hideAllTab: function() {
            modalAdd.data("kendoWindow").close();
            modalEdit.data("kendoWindow").close();
        },
        cleanModal: function() {
            this.set("addOrderReceived",addOrderReceivedDefault);
        },
        showEditForm: function(orderDetailsIndex,orderReceivedIndex) {
            this.hideAllTab();

            var orderDataSource = this.get("ordersDataSource");
            var order = orderDataSource.at(orderDetailsIndex);
            var orderReceived = order.orderReceivedList[orderReceivedIndex];

            viewModel.set("editOrderReceived.id",orderReceived.id);
            viewModel.set("editOrderReceived.orderDetailsId",order.id);
            viewModel.set("editOrderReceived.itemId",order.item.id);
            viewModel.set("editOrderReceived.itemName",order.item.name);

            viewModel.set("editOrderReceived.date",new Date(orderReceived.receivedDate));
            viewModel.set("editOrderReceived.quantity",orderReceived.quantity);

            viewModel.set("editOrderReceived.warehouse",orderReceived.warehouse.id);
            viewModel.set("editOrderReceived.receivedBy",orderReceived.receivedBy.id);
            console.log(orderReceived);

            modalEdit.data("kendoWindow").center().open();
        },
        showSaveForm: function(orderDetailsIndex) {
            this.hideAllTab();
            //var addValidator = new ValueValidator(null,validationAddConf);
            //addValidator.clean();
            this.cleanModal();
            var orderDataSource = this.get("ordersDataSource");
            var order = orderDataSource.at(orderDetailsIndex);
            viewModel.set("addOrderReceived.orderDetailsId",order.id);
            viewModel.set("addOrderReceived.itemId",order.item.id);
            viewModel.set("addOrderReceived.itemName",order.item.name);
            console.log(order);

            modalAdd.data("kendoWindow").center().open();
        },
        gridOnChange : function(event) {
            var grid = event.sender;
            var selectedItem = grid.select();
            console.log(grid.dataItem(selectedItem));
        },
        cancelEdit : function() {

            this.hideAllTab();
        },
        submitAddOrderReceived: function() {
            var isValid = true;

            if(isValid){

                var purchase = {};
                purchase.id = parseInt(purchaseId);
                var warehouse = {};
                warehouse.id = viewModel.get("addOrderReceived.warehouse").id;
                var receivedBy = {};
                receivedBy.id = viewModel.get("addOrderReceived.receivedBy").id;
                var orderDetails = {};
                orderDetails.id = viewModel.get("addOrderReceived.orderDetailsId");
                var orderReceived = {};
                orderReceived.orderDetails = orderDetails;
                orderReceived.purchase = purchase;
                orderReceived.warehouse = warehouse;
                orderReceived.receivedBy = receivedBy;
                orderReceived.quantity = viewModel.get("addOrderReceived.quantity");
                orderReceived.receivedDate = viewModel.get("addOrderReceived.date");


                $.ajax({
                    url: baseUrl+"/rest/purchase-received/create/",
                    type: "post",
                    data: JSON.stringify(orderReceived),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {
                        viewModel.ordersDataSource.read();
                        viewModel.hideAllTab();
                    },
                    error: function (response) {
                        alert(response);
                    }
                });

            }
            else {
                dialogAlert(errorInputDialog);
            }
        },
        submitUpdateOrderReceived: function() {
            var isValid = true;

            if(isValid){

                var purchase = {};
                purchase.id = parseInt(purchaseId);
                var warehouse = {};
                warehouse.id = viewModel.get("editOrderReceived.warehouse");
                var receivedBy = {};
                receivedBy.id = viewModel.get("editOrderReceived.receivedBy");
                var orderDetails = {};
                orderDetails.id = viewModel.get("editOrderReceived.orderDetailsId");
                var orderReceived = {};
                orderReceived.id = viewModel.get("editOrderReceived.id");
                orderReceived.orderDetails = orderDetails;
                orderReceived.purchase = purchase;
                orderReceived.warehouse = warehouse;
                orderReceived.receivedBy = receivedBy;
                orderReceived.quantity = viewModel.get("editOrderReceived.quantity");
                orderReceived.receivedDate = viewModel.get("editOrderReceived.date");


                $.ajax({
                    url: baseUrl+"/rest/purchase-received/update/"+orderReceived.id,
                    type: "post",
                    data: JSON.stringify(orderReceived),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {
                        viewModel.ordersDataSource.read();
                        viewModel.hideAllTab();
                    },
                    error: function (response) {
                        alert(response);
                    }
                });

            }
            else {
                dialogAlert(errorInputDialog);
            }
        },
        submit: function() {

            var isValid = true;

            if(isValid){

                var purchase = GetPurchaseObject(this);
                console.log(purchase);

                $.ajax({
                    url: baseUrl+"/rest/purchase-received/update/"+purchaseId,
                    type: "post",
                    data: JSON.stringify(purchase),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {

                    },
                    error: function (response) {
                        alert(response);
                    }
                });

            }
            else {
                dialogAlert(errorInputDialog);
            }
        },
        submitUpdate: function() {

            var editForm = this.get("editForm");

            var editValidator = new ValueValidator(editForm,validationEditConf);
            var isValid = editValidator.validate();

            if(isValid){

                var item = itemViewObjectToJsObject(editForm);

                $.ajax({
                    url: baseUrl+"/rest/item/update/"+item.id,
                    type: "post",
                    data: JSON.stringify(item),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {
                        viewModel.gridDataSource.fetch();
                        viewModel.hideAllTab();
                        viewModel.set("editFormVisible",false);
                        dialogAlert(saveSuccessDialog);
                        modalEdit.data("kendoWindow").close();
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }
            else {
                dialogAlert(errorInputDialog);
            }
        }
    });

    kendo.bind($(".formMainWrapper"), viewModel);

    $.ajax({
        url: baseUrl+"/rest/purchase-received/get/"+purchaseId,
        type: "get",
        datatype: 'json',
        success: function (response) {
            viewModel.set("purchaseForm.no",response.no);
            viewModel.set("purchaseForm.date",new Date(response.date));
            viewModel.set("purchaseForm.remark",response.remark);
            if(response.supplier!=null){
                viewModel.set("purchaseForm.supplier",response.supplier.id);
            }
            if(response.requestBy!=null){
                viewModel.set("purchaseForm.requestBy",response.requestBy.id);
            }
        },
        error: function (response) {
            alert(response);
        }
    });

    var modalView = $("#modalView");
    modalView.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "View",
        modal: true,
        visible: false,
        animation: false
    });

    var modalAdd = $("#modalAdd");
    modalAdd.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "New Order Received",
        modal: true,
        visible: false,
        animation: false
    });

    var modalEdit = $("#modalEdit");
    modalEdit.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "Update Order Received",
        modal: true,
        visible: false,
        animation: false
    });
});

function itemViewObjectToJsObject(viewObject) {
    var item = {};
    item.id = viewObject.id;
    item.name = viewObject.name;
    item.itemCode = viewObject.itemCode;
    item.itemType = viewObject.type;
    item.itemDescription = viewObject.description;
    item.isActive = viewObject.isActive;
    var category = {};
    category.id = viewObject.category;
    item.category = category;

    var measurement = {};
    measurement.id = viewObject.stockMeasurement;
    item.stockMeasurement = measurement;

    return item;
}

function GetPurchaseObject(viewObject) {

    var purchase = {};
    var purchaseForm = viewObject.get("purchaseForm");

    purchase.no = purchaseForm.no;
    purchase.date = purchaseForm.date;
    purchase.supplier = {id:purchaseForm.supplier};
    purchase.warehouse = {id:purchaseForm.warehouse};
    purchase.requestBy = {id:purchaseForm.requestBy};
    purchase.remark = purchaseForm.remark;

    var orders = viewObject.get("ordersDataSource").data();

    purchase.orderDetailsList = orders;

    console.log(purchase);
    return purchase;
}

function editOrderReceived(orderDetailsIndex,orderReceivedIndex) {
    viewModel.showEditForm(orderDetailsIndex,orderReceivedIndex);
}

function addOrderReceived(orderDetailsIndex) {
    viewModel.showSaveForm(orderDetailsIndex);
}

function getDateWithFormat(dateVal) {
    var dateObj = new Date(dateVal);
    console.log(dateObj.getDay());
    var dateMoment = moment(dateObj);
    return dateMoment.format('MM/DD/YYYY');
}

function getReceivedQtyTotal(orderReceivedList) {
    var total = 0;
    for(var i=0;i<orderReceivedList.length;i++) {
        total += orderReceivedList[i].quantity;
    }
    return total;
}