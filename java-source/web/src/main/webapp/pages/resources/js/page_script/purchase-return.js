/**
 * Created by Administrator on 11/7/15.
 */
var viewModel;
var increment = 0;
var purchaseId = 0;

$(document).ready(function() {
    purchaseId = purl().param('id');
    var validationAddConf = [
        {'fieldName':'item','validationType':'IsValidCombo','errorDiv':'#itemAddError'},
        {'fieldName':'measurement','validationType':'IsValidCombo','errorDiv':'#measurementAddError'},
        {'fieldName':'quantity','validationType':'IsNotEmptyNumber','errorDiv':'#quantitySaveError'},
        {'fieldName':'price','validationType':'IsNotEmptyNumber','errorDiv':'#priceSaveError'}
    ];
    var validationEditConf = [
        {'fieldName':'name','validationType':'IsNotEmpty','errorDiv':'#nameEditError'}
    ];

    var addOrderReturnDefault =  {
            orderDetailsId: null,
            itemId: null,
            itemName: "",
            date: new Date(),
            quantity: "",
            warehouse: null,
            returnBy: null
    };

    viewModel = kendo.observable({
        ordersDataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/purchase-return/get/"+purchaseId,
                    dataType: "json"
                }
            },
            schema: {
                data: function(response) {
                    return response.orderDetailsList;
                },
                parse: function(response) {
                    var rows = response.orderDetailsList;
                    for (var i = 0; i < rows.length; i++) {
                        rows[i].no = i;
                        rows[i].returnQty = getReturnQtyTotal(rows[i].orderReturnList);
                    }
                    response.values = rows;
                    console.log(rows);
                    return response;
                }
            }
        }),
        purchaseForm :  {
            no : "",
            date : "",
            deliveryDate: "",
            supplier: "",
            requestBy: "",
            remark: ""
        },
        addOrderReturn : addOrderReturnDefault,
        editOrderReturn : {
            id: null,
            orderDetailsId: null,
            itemId: null,
            itemName: "",
            date: new Date(),
            quantity: "",
            warehouse: null,
            returnBy: null
        },
        items: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/item/getall",
                    dataType: "json"
                }
            }
        }),
        measurements: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/measurement/search?pageNumber=1&pageSize="+allRow+
                        "&type=STOCK",
                    dataType: "json"
                }
            },
            schema: {
                data: function(response) {
                    return response.values;
                }
            }
        }),
        suppliers: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/supplier/search?pageNumber=1&pageSize="+allRow+
                        "&type=STOCK",
                    dataType: "json"
                }
            },
            schema: {
                data: function(response) {
                    return response.values;
                }
            }
        }),
        warehouses: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/warehouse/search?pageNumber=1&pageSize="+allRow,
                    dataType: "json"
                }
            },
            schema: {
                data: function(response) {
                    return response.values;
                }
            }
        }),
        employees: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/employee/search?pageNumber=1&pageSize="+allRow,
                    dataType: "json"
                }
            },
            schema: {
                data: function(response) {
                    return response.values;
                }
            }
        }),
        gridSelectedItem : null,
        hideAllTab: function() {
            modalAdd.data("kendoWindow").close();
            modalEdit.data("kendoWindow").close();
        },
        cleanModal: function() {
            this.set("addOrderReceived",addOrderReturnDefault);
        },
        showEditForm: function(orderDetailsIndex,orderReturnIndex) {
            this.hideAllTab();

            var orderDataSource = this.get("ordersDataSource");
            var order = orderDataSource.at(orderDetailsIndex);
            var orderReturn = order.orderReturnList[orderReturnIndex];

            viewModel.set("editOrderReturn.id",orderReturn.id);
            viewModel.set("editOrderReturn.orderDetailsId",order.id);
            viewModel.set("editOrderReturn.itemId",order.item.id);
            viewModel.set("editOrderReturn.itemName",order.item.name);

            viewModel.set("editOrderReturn.date",new Date(orderReturn.returnDate));
            viewModel.set("editOrderReturn.quantity",orderReturn.quantity);

            viewModel.set("editOrderReturn.warehouse",orderReturn.warehouse.id);
            viewModel.set("editOrderReturn.returnBy",orderReturn.returnBy.id);
            console.log(orderReturn);

            modalEdit.data("kendoWindow").center().open();
        },
        showSaveForm: function(orderDetailsIndex) {
            this.hideAllTab();
            //var addValidator = new ValueValidator(null,validationAddConf);
            //addValidator.clean();
            this.cleanModal();
            var orderDataSource = this.get("ordersDataSource");
            var order = orderDataSource.at(orderDetailsIndex);
            viewModel.set("addOrderReturn.orderDetailsId",order.id);
            viewModel.set("addOrderReturn.itemId",order.item.id);
            viewModel.set("addOrderReturn.itemName",order.item.name);
            console.log(order);

            modalAdd.data("kendoWindow").center().open();
        },
        gridOnChange : function(event) {
            var grid = event.sender;
            var selectedItem = grid.select();
            console.log(grid.dataItem(selectedItem));
        },
        cancelEdit : function() {

            this.hideAllTab();
        },
        submitAddOrderReturn: function() {
            var isValid = true;

            if(isValid){

                var purchase = {};
                purchase.id = parseInt(purchaseId);
                var warehouse = {};
                warehouse.id = viewModel.get("addOrderReturn.warehouse").id;
                var returnBy = {};
                returnBy.id = viewModel.get("addOrderReturn.returnBy").id;
                var orderDetails = {};
                orderDetails.id = viewModel.get("addOrderReturn.orderDetailsId");
                var orderReturn = {};
                orderReturn.orderDetails = orderDetails;
                orderReturn.purchase = purchase;
                orderReturn.warehouse = warehouse;
                orderReturn.returnBy = returnBy;
                orderReturn.quantity = viewModel.get("addOrderReturn.quantity");
                orderReturn.returnDate = viewModel.get("addOrderReturn.date");


                $.ajax({
                    url: baseUrl+"/rest/purchase-return/create/",
                    type: "post",
                    data: JSON.stringify(orderReturn),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {
                        viewModel.ordersDataSource.read();
                        viewModel.hideAllTab();
                    },
                    error: function (response) {
                        alert(response);
                    }
                });

            }
            else {
                dialogAlert(errorInputDialog);
            }
        },
        submitUpdateOrderReturn: function() {
            var isValid = true;

            if(isValid){

                var purchase = {};
                purchase.id = parseInt(purchaseId);
                var warehouse = {};
                warehouse.id = viewModel.get("editOrderReturn.warehouse");
                var returnBy = {};
                returnBy.id = viewModel.get("editOrderReturn.returnBy");
                var orderDetails = {};
                orderDetails.id = viewModel.get("editOrderReturn.orderDetailsId");
                var orderReturn = {};
                orderReturn.id = viewModel.get("editOrderReturn.id");
                orderReturn.orderDetails = orderDetails;
                orderReturn.purchase = purchase;
                orderReturn.warehouse = warehouse;
                orderReturn.returnBy = returnBy;
                orderReturn.quantity = viewModel.get("editOrderReturn.quantity");
                orderReturn.returnDate = viewModel.get("editOrderReturn.date");


                $.ajax({
                    url: baseUrl+"/rest/purchase-return/update/"+orderReturn.id,
                    type: "post",
                    data: JSON.stringify(orderReturn),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {
                        viewModel.ordersDataSource.read();
                        viewModel.hideAllTab();
                    },
                    error: function (response) {
                        alert(response);
                    }
                });

            }
            else {
                dialogAlert(errorInputDialog);
            }
        },
        submit: function() {

            var isValid = true;

            if(isValid){

                var purchase = GetPurchaseObject(this);
                console.log(purchase);

                $.ajax({
                    url: baseUrl+"/rest/purchase-received/update/"+purchaseId,
                    type: "post",
                    data: JSON.stringify(purchase),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {

                    },
                    error: function (response) {
                        alert(response);
                    }
                });

            }
            else {
                dialogAlert(errorInputDialog);
            }
        },
        submitUpdate: function() {

            var editForm = this.get("editForm");

            var editValidator = new ValueValidator(editForm,validationEditConf);
            var isValid = editValidator.validate();

            if(isValid){

                var item = itemViewObjectToJsObject(editForm);

                $.ajax({
                    url: baseUrl+"/rest/item/update/"+item.id,
                    type: "post",
                    data: JSON.stringify(item),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {
                        viewModel.gridDataSource.fetch();
                        viewModel.hideAllTab();
                        viewModel.set("editFormVisible",false);
                        dialogAlert(saveSuccessDialog);
                        modalEdit.data("kendoWindow").close();
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }
            else {
                dialogAlert(errorInputDialog);
            }
        }
    });

    kendo.bind($(".formMainWrapper"), viewModel);

    $.ajax({
        url: baseUrl+"/rest/purchase-received/get/"+purchaseId,
        type: "get",
        datatype: 'json',
        success: function (response) {
            viewModel.set("purchaseForm.no",response.no);
            viewModel.set("purchaseForm.date",new Date(response.date));
            viewModel.set("purchaseForm.remark",response.remark);
            if(response.supplier!=null){
                viewModel.set("purchaseForm.supplier",response.supplier.id);
            }
            if(response.requestBy!=null){
                viewModel.set("purchaseForm.requestBy",response.requestBy.id);
            }
        },
        error: function (response) {
            alert(response);
        }
    });

    var modalView = $("#modalView");
    modalView.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "View",
        modal: true,
        visible: false,
        animation: false
    });

    var modalAdd = $("#modalAdd");
    modalAdd.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "New Order Received",
        modal: true,
        visible: false,
        animation: false
    });

    var modalEdit = $("#modalEdit");
    modalEdit.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "Update Order Received",
        modal: true,
        visible: false,
        animation: false
    });
});

function itemViewObjectToJsObject(viewObject) {
    var item = {};
    item.id = viewObject.id;
    item.name = viewObject.name;
    item.itemCode = viewObject.itemCode;
    item.itemType = viewObject.type;
    item.itemDescription = viewObject.description;
    item.isActive = viewObject.isActive;
    var category = {};
    category.id = viewObject.category;
    item.category = category;

    var measurement = {};
    measurement.id = viewObject.stockMeasurement;
    item.stockMeasurement = measurement;

    return item;
}

function GetPurchaseObject(viewObject) {

    var purchase = {};
    var purchaseForm = viewObject.get("purchaseForm");

    purchase.no = purchaseForm.no;
    purchase.date = purchaseForm.date;
    purchase.supplier = {id:purchaseForm.supplier};
    purchase.warehouse = {id:purchaseForm.warehouse};
    purchase.requestBy = {id:purchaseForm.requestBy};
    purchase.remark = purchaseForm.remark;

    var orders = viewObject.get("ordersDataSource").data();

    purchase.orderDetailsList = orders;

    console.log(purchase);
    return purchase;
}

function editOrderReceived(orderDetailsIndex,orderReceivedIndex) {
    viewModel.showEditForm(orderDetailsIndex,orderReceivedIndex);
}

function addOrderReceived(orderDetailsIndex) {
    viewModel.showSaveForm(orderDetailsIndex);
}

function getDateWithFormat(dateVal) {
    var dateObj = new Date(dateVal);
    console.log(dateObj.getDay());
    var dateMoment = moment(dateObj);
    return dateMoment.format('MM/DD/YYYY');
}

function getReturnQtyTotal(orderReturnList) {
    var total = 0;
    for(var i=0;i<orderReturnList.length;i++) {
        total += orderReturnList[i].quantity;
    }
    return total;
}