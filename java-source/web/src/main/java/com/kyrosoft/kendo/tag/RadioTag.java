package com.kyrosoft.kendo.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

/**
 * Created by Administrator on 10/30/15.
 */
public class RadioTag extends SimpleTagSupport {

    private String label = "";

    private String value = "";

    private String bindingName = "";

    private String cssClass = "";

    private String cssId = "";

    StringWriter sw = new StringWriter();

    public void setLabel(String label) {
        this.label = label;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setBindingName(String bindingName) {
        this.bindingName = bindingName;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public void setCssId(String cssId) {
        this.cssId = cssId;
    }

    public void doTag()
            throws JspException, IOException
    {
        JspWriter out = getJspContext().getOut();
        out.println("<input id=\""+cssId+"\" type=\"radio\" name=\""+bindingName+"\"" +
                " data-bind=\"checked:"+bindingName+"\" value=\""+value+"\" /> \n" +
                "<label for=\""+cssId+"\" class=\"checkboxRadioLabel\">"+label+"</label>");
    }
}
