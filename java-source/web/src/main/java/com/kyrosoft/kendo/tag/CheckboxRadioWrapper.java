package com.kyrosoft.kendo.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Created by Administrator on 10/30/15.
 */
public class CheckboxRadioWrapper extends TagSupport {

    private String label = "";

    public void setLabel(String label) {
        this.label = label;
    }

    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            out.println(
                    "<li>" +
                            "<label>"+label+"</label>" );
        } catch (IOException e) {
            throw new JspException();
        }
        return EVAL_BODY_INCLUDE;
    }

    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            out.println("</li>");
        } catch (IOException e) {
            throw new JspException();
        }
        return EVAL_PAGE;
    }

}
