package com.kyrosoft.kendo.tag;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;
/**
 * Created by Administrator on 10/26/15.
 */
public class InputTextTag extends SimpleTagSupport {

    private String label = "";

    private String bindingName = "";

    private Boolean isRequired = false;

    private String cssClass = "";

    private String cssId = "";

    StringWriter sw = new StringWriter();

    public void setLabel(String label) {
        this.label = label;
    }

    public void setBindingName(String bindingName) {
        this.bindingName = bindingName;
    }

    public void setRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public void setCssId(String cssId) {
        this.cssId = cssId;
    }

    public void doTag()
       throws JspException, IOException
    {
        String isRequiredStr = isRequired ? "required " : "";
        JspWriter out = getJspContext().getOut();
        out.println(
                "<li>" +
                "<label for=\""+bindingName+"\">"+label+"</label>" +
                    "<input id=\""+bindingName+"\" data-bind=\"value: "+bindingName+"\" type=\"text\"" +
                        " class=\"k-textbox fullwidthInput\" "+isRequiredStr+"/>" +
                "</li>");
    }
}
