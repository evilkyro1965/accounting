package com.kyrosoft.kendo.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

/**
 * Created by Administrator on 10/26/15.
 */
public class ComboTag extends SimpleTagSupport {

    private String label = "";

    private String bindingName = "";

    private String dataSourceName = "";

    private String textField = "name";

    private String valueField = "id";

    private Boolean isRequired = false;

    private String cssClass = "";

    private String cssId = "";

    StringWriter sw = new StringWriter();

    public void setLabel(String label) {
        this.label = label;
    }

    public void setBindingName(String bindingName) {
        this.bindingName = bindingName;
    }

    public void setTextField(String textField) {
        this.textField = textField;
    }

    public void setValueField(String valueField) {
        this.valueField = valueField;
    }

    public void setDataSourceName(String dataSourceName) {
        this.dataSourceName = dataSourceName;
    }

    public void setIsRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public void setCssId(String cssId) {
        this.cssId = cssId;
    }

    public void doTag()
            throws JspException, IOException
    {
        String isRequiredStr = isRequired ? "required " : "";
        JspWriter out = getJspContext().getOut();
        out.println(
                "<li>" +
                        "<label for=\""+bindingName+"\">"+label+"</label>" +
                        "<input id=\""+bindingName+"\"\n" +
                        "   name=\""+bindingName+"\"\n" +
                        "   data-role=\"combobox\"\n" +
                        "   data-text-field=\""+textField+"\"\n" +
                        "   data-value-field=\""+valueField+"\"\n" +
                        "   data-option-label=\""+label+"\"\n" +
                        "   data-bind=\"source: "+dataSourceName+", value: "+bindingName+"\"\n" +
                        "   class=\"fullwidthInput\"\n" +
                        "   "+isRequiredStr+"/>" +
                        "</li>");
    }
}
