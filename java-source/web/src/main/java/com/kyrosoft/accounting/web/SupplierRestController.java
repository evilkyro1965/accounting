package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.SupplierService;
import com.kyrosoft.accounting.model.Supplier;
import com.kyrosoft.accounting.model.dto.SearchResult;
import com.kyrosoft.accounting.model.dto.SupplierSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/3/15.
 */
@RestController
@RequestMapping("rest/supplier")
public class SupplierRestController extends BaseRestController {

    @Autowired
    SupplierService supplierService;

    @RequestMapping("/getall")
    public List<Supplier> getAllSupplier() throws ServiceException {
        SupplierSearchCriteria criteria = new SupplierSearchCriteria();
        criteria.setPageNumber(1);
        criteria.setPageSize(ALL_ROW);
        SearchResult<Supplier> searchResult = supplierService.search(criteria);
        List<Supplier> suppliers = new ArrayList<Supplier>();
        suppliers = searchResult.getValues();
        return suppliers;
    }

    @RequestMapping("/search")
    public SearchResult<Supplier> search(@ModelAttribute SupplierSearchCriteria criteria) throws ServiceException {
        SearchResult<Supplier> searchResult = supplierService.search(criteria);
        return searchResult;
    }

    @RequestMapping(value = "/create", method= RequestMethod.POST)
    public Supplier create(@RequestBody Supplier supplier) throws ServiceException,
            DatabasePersistenceException {
        setCreatedDateAndBy(supplier);
        supplier.setIsActive(true);
        supplierService.save(supplier);
        return supplier;
    }

    @RequestMapping(value = "/update/{id}", method= RequestMethod.POST)
    public Supplier update(@RequestBody Supplier supplier,@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {
        supplier.setId(id);
        setUpdatedDateAndBy(supplier);
        supplierService.update(supplier);
        return supplier;
    }

}
