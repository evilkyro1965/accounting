package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.ItemService;
import com.kyrosoft.accounting.dao.PurchaseService;
import com.kyrosoft.accounting.model.Item;
import com.kyrosoft.accounting.model.OrderDetails;
import com.kyrosoft.accounting.model.Purchase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Administrator on 12/22/15.
 */
@RestController
@RequestMapping("rest/purchase")
@Transactional
public class PurchaseRestController extends BaseRestController{

    @Autowired
    PurchaseService purchaseService;

    @RequestMapping(value = "/create", method= RequestMethod.POST)
     @Transactional
     public Purchase create(@RequestBody Purchase purchase) throws ServiceException,
            DatabasePersistenceException {
        setCreatedDateAndByGLEntity(purchase);
        for(OrderDetails orderDetails : purchase.getOrderDetailsList()){
            setCreatedDateAndByGLEntity(orderDetails);
        }
        purchaseService.save(purchase);
        if(purchase.getOrderDetailsList()!=null) {
            for(OrderDetails orderDetails : purchase.getOrderDetailsList()){
                orderDetails.setPurchase(null);
            }
        }
        return purchase;
    }

    @RequestMapping(value = "/update/{id}", method= RequestMethod.POST)
    @Transactional
    public Purchase update(@RequestBody Purchase purchase,@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {
        purchase.setId(id);
        setCreatedDateAndByGLEntity(purchase);
        for(OrderDetails orderDetails : purchase.getOrderDetailsList()){
            setCreatedDateAndByGLEntity(orderDetails);
        }
        purchaseService.update(purchase);
        if(purchase.getOrderDetailsList()!=null) {
            for(OrderDetails orderDetails : purchase.getOrderDetailsList()){
                orderDetails.setPurchase(null);
            }
        }
        return purchase;
    }

    @RequestMapping(value = "/get/{id}", method= RequestMethod.GET)
    @Transactional
    public Purchase get(@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {

        Purchase purchase = purchaseService.get(id);
        if(purchase.getOrderDetailsList()!=null) {
            for(OrderDetails orderDetails : purchase.getOrderDetailsList()){
                orderDetails.setPurchase(null);
            }
        }
        return purchase;
    }

}
