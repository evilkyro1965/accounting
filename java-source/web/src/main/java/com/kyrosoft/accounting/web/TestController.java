package com.kyrosoft.accounting.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by Administrator on 10/19/15.
 */
@Controller
public class TestController {

    @RequestMapping(value="test", method=GET)
    public String showRegistrationForm() {
        return "testView";
    }
}
