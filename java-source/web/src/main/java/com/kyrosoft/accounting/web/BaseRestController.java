package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.model.AuditableEntity;
import com.kyrosoft.accounting.model.GLEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;

/**
 * Created by Administrator on 11/2/15.
 */
public class BaseRestController {

    @PersistenceContext
    protected EntityManager entityManager;

    protected final int ALL_ROW = 1000000;

    public void setCreatedDateAndBy(AuditableEntity auditableEntity) {
        auditableEntity.setCreatedBy("test");
        auditableEntity.setCreatedDate(new Date());
    }

    public void setUpdatedDateAndBy(AuditableEntity auditableEntity) {
        auditableEntity.setUpdatedBy("test");
        auditableEntity.setUpdatedDate(new Date());
    }

    public void setCreatedDateAndByGLEntity(GLEntity glEntity) {
        glEntity.setCreatedBy("test");
        glEntity.setCreatedDate(new Date());
    }

    public void setUpdatedDateAndByGLEntity(GLEntity glEntity) {
        glEntity.setUpdatedBy("test");
        glEntity.setUpdatedDate(new Date());
    }

}
