package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.CustomerService;
import com.kyrosoft.accounting.model.Customer;
import com.kyrosoft.accounting.model.Supplier;
import com.kyrosoft.accounting.model.dto.CustomerSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import com.kyrosoft.accounting.model.dto.SupplierSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2/15/2016.
 */
@RestController
@RequestMapping("rest/customer")
public class CustomerRestController extends BaseRestController {

    @Autowired
    CustomerService customerService;

    @RequestMapping("/getall")
    public List<Customer> getAllSupplier() throws ServiceException {
        CustomerSearchCriteria criteria = new CustomerSearchCriteria();
        criteria.setPageNumber(1);
        criteria.setPageSize(ALL_ROW);
        SearchResult<Customer> searchResult = customerService.search(criteria);
        List<Customer> customers = new ArrayList<Customer>();
        customers = searchResult.getValues();
        return customers;
    }

    @RequestMapping("/search")
    public SearchResult<Customer> search(@ModelAttribute CustomerSearchCriteria criteria) throws ServiceException {
        SearchResult<Customer> searchResult = customerService.search(criteria);
        return searchResult;
    }


}
