package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.EmployeeService;
import com.kyrosoft.accounting.model.Employee;
import com.kyrosoft.accounting.model.dto.EmployeeSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 11/7/15.
 */
@RestController
@RequestMapping("rest/employee")
@Transactional
public class EmployeeRestController extends BaseRestController {

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    EmployeeService employeeService;

    @RequestMapping("/getall")
    public List<Employee> getAllDepartment() throws ServiceException {
        EmployeeSearchCriteria criteria = new EmployeeSearchCriteria();
        criteria.setPageNumber(1);
        criteria.setPageSize(ALL_ROW);
        SearchResult<Employee> searchResult = employeeService.search(criteria);
        List<Employee> employees = new ArrayList<Employee>();
        employees = searchResult.getValues();
        for(Employee employee : employees) {
            if(employee.getDepartment()!=null){
                employee.getDepartment().setEmployees(null);
            }
        }
        return employees;
    }

    @RequestMapping("/search")
    public SearchResult<Employee> search(@ModelAttribute EmployeeSearchCriteria criteria) throws ServiceException {
        SearchResult<Employee> searchResult = employeeService.search(criteria);
        for(Employee employee : searchResult.getValues()) {
            if(employee.getDepartment()!=null){
                employee.getDepartment().setEmployees(null);
            }
        }
        return searchResult;
    }

    @RequestMapping(value = "/create", method= RequestMethod.POST)
    @Transactional
    public Employee create(@RequestBody Employee employee) throws ServiceException,
            DatabasePersistenceException {
        setCreatedDateAndBy(employee);
        employee.setIsActive(true);
        employeeService.save(employee);
        if(employee.getDepartment()!=null){
            employee.getDepartment().setEmployees(null);
        }
        return employee;
    }

    @RequestMapping(value = "/update/{id}", method= RequestMethod.POST)
    @Transactional
    public Employee update(@RequestBody Employee employee,@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {
        employee.setId(id);
        setUpdatedDateAndBy(employee);
        employeeService.update(employee);
        if(employee.getDepartment()!=null){
            employee.getDepartment().setEmployees(null);
        }
        return employee;
    }
}
