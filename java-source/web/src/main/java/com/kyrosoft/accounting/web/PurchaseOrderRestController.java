package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.ItemService;
import com.kyrosoft.accounting.dao.MeasurementService;
import com.kyrosoft.accounting.dao.PurchaseService;
import com.kyrosoft.accounting.model.Item;
import com.kyrosoft.accounting.model.Measurement;
import com.kyrosoft.accounting.model.OrderDetails;
import com.kyrosoft.accounting.model.Purchase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Administrator on 12/22/15.
 */
@RestController
@RequestMapping("rest/purchase-order")
@Transactional
public class PurchaseOrderRestController extends BaseRestController{

    @Autowired
    PurchaseService purchaseService;

    @Autowired
    ItemService itemService;

    @Autowired
    MeasurementService measurementService;

    @RequestMapping(value = "/create", method= RequestMethod.POST)
     @Transactional
     public Purchase create(@RequestBody Purchase purchase) throws ServiceException,
            DatabasePersistenceException {
        setCreatedDateAndByGLEntity(purchase);
        if(purchase.getOrderDetailsList()!=null) {
            for(OrderDetails orderDetails : purchase.getOrderDetailsList()) {
                setCreatedDateAndByGLEntity(orderDetails);
            }
        }
        purchaseService.save(purchase);
        if(purchase.getOrderDetailsList()!=null) {
            for(OrderDetails orderDetails : purchase.getOrderDetailsList()){
                orderDetails.setPurchase(null);
            }
        }
        return purchase;
    }

    @RequestMapping(value = "/update/{id}", method= RequestMethod.POST)
    @Transactional
    public Purchase update(@RequestBody Purchase purchase,@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {
        purchase.setId(id);
        setCreatedDateAndByGLEntity(purchase);
        if(purchase.getOrderDetailsList()!=null) {
            for (OrderDetails orderDetails : purchase.getOrderDetailsList()) {
                setCreatedDateAndByGLEntity(orderDetails);
                Item item = itemService.get(orderDetails.getItem().getId());
                Measurement measurement = measurementService.get(orderDetails.getPurchaseMeasurement().getId());
                orderDetails.setItem(item);
                orderDetails.setPurchaseMeasurement(measurement);
            }
        }
        purchaseService.update(purchase);
        if(purchase.getOrderDetailsList()!=null) {
            for(OrderDetails orderDetails : purchase.getOrderDetailsList()){
                orderDetails.setPurchase(null);
            }
        }
        return purchase;
    }

    @RequestMapping(value = "/get/{id}", method= RequestMethod.GET)
    @Transactional
    public Purchase get(@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {

        Purchase purchase = purchaseService.get(id);
        if(purchase.getOrderDetailsList()!=null) {
            for(OrderDetails orderDetails : purchase.getOrderDetailsList()){
                orderDetails.setPurchase(null);
                orderDetails.setOrderReceivedList(null);
            }
        }
        return purchase;
    }

}
