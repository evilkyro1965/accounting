package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.ItemCategoryService;
import com.kyrosoft.accounting.model.ItemCategory;
import com.kyrosoft.accounting.model.dto.ActiveEntitySearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/2/15.
 */
@RestController
@RequestMapping("rest/item-category")
public class ItemCategoryRestController extends BaseRestController {

    @Autowired
    ItemCategoryService itemCategoryService;

    @RequestMapping("/getall")
    public List<ItemCategory> getAllItemCategory() throws ServiceException {
        ActiveEntitySearchCriteria criteria = new ActiveEntitySearchCriteria();
        criteria.setPageNumber(1);
        criteria.setPageSize(ALL_ROW);
        SearchResult<ItemCategory> searchResult = itemCategoryService.search(criteria);
        List<ItemCategory> itemCategories = new ArrayList<ItemCategory>();
        itemCategories = searchResult.getValues();
        return itemCategories;
    }

    @RequestMapping("/search")
    public SearchResult<ItemCategory> search(@ModelAttribute ActiveEntitySearchCriteria criteria) throws ServiceException {
        SearchResult<ItemCategory> searchResult = itemCategoryService.search(criteria);
        return searchResult;
    }

    @RequestMapping(value = "/create", method= RequestMethod.POST)
    public ItemCategory create(@RequestBody ItemCategory itemCategory) throws ServiceException,
            DatabasePersistenceException {
        setCreatedDateAndBy(itemCategory);
        itemCategory.setIsActive(true);
        itemCategoryService.save(itemCategory);
        return itemCategory;
    }

    @RequestMapping(value = "/update/{id}", method= RequestMethod.POST)
    public ItemCategory update(@RequestBody ItemCategory itemCategory,@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {
        itemCategory.setId(id);
        setUpdatedDateAndBy(itemCategory);
        itemCategoryService.update(itemCategory);
        return itemCategory;
    }

}
