package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.WarehouseService;
import com.kyrosoft.accounting.model.Warehouse;
import com.kyrosoft.accounting.model.dto.SearchResult;
import com.kyrosoft.accounting.model.dto.WarehouseSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/3/15.
 */
@RestController
@RequestMapping("rest/warehouse")
public class WarehouseRestController extends BaseRestController {

    @Autowired
    WarehouseService warehouseService;

    @RequestMapping("/getall")
    public List<Warehouse> getAllWarehouse() throws ServiceException {
        WarehouseSearchCriteria criteria = new WarehouseSearchCriteria();
        criteria.setPageNumber(1);
        criteria.setPageSize(ALL_ROW);
        SearchResult<Warehouse> searchResult = warehouseService.search(criteria);
        List<Warehouse> warehouses = new ArrayList<Warehouse>();
        warehouses = searchResult.getValues();
        for(Warehouse warehouse:warehouses){
            if(warehouse.getDepartment()!=null) {
                warehouse.getDepartment().setEmployees(null);
            }
        }
        return warehouses;
    }

    @RequestMapping("/search")
    public SearchResult<Warehouse> search(@ModelAttribute WarehouseSearchCriteria criteria) throws ServiceException {
        SearchResult<Warehouse> searchResult = warehouseService.search(criteria);
        for(Warehouse warehouse:searchResult.getValues()){
            if(warehouse.getDepartment()!=null) {
                warehouse.getDepartment().setEmployees(null);
            }
        }
        return searchResult;
    }

    @RequestMapping(value = "/create", method= RequestMethod.POST)
    public Warehouse create(@RequestBody Warehouse warehouse) throws ServiceException,
            DatabasePersistenceException {
        setCreatedDateAndBy(warehouse);
        warehouse.setIsActive(true);
        warehouseService.save(warehouse);
        if(warehouse.getDepartment()!=null) {
            warehouse.getDepartment().setEmployees(null);
        }
        return warehouse;
    }

    @RequestMapping(value = "/update/{id}", method= RequestMethod.POST)
    public Warehouse update(@RequestBody Warehouse warehouse,@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {
        warehouse.setId(id);
        setUpdatedDateAndBy(warehouse);
        warehouseService.update(warehouse);
        if(warehouse.getDepartment()!=null) {
            warehouse.getDepartment().setEmployees(null);
        }
        return warehouse;
    }

}
