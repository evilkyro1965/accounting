package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.MeasurementService;
import com.kyrosoft.accounting.model.Measurement;
import com.kyrosoft.accounting.model.dto.MeasurementSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/3/15.
 */
@RestController
@RequestMapping("rest/measurement")
public class MeasurementRestController extends BaseRestController {
    @Autowired
    MeasurementService measurementService;

    @RequestMapping("/getall")
    public List<Measurement> getAllMeasurement() throws ServiceException {
        MeasurementSearchCriteria criteria = new MeasurementSearchCriteria();
        criteria.setPageNumber(1);
        criteria.setPageSize(ALL_ROW);
        SearchResult<Measurement> searchResult = measurementService.search(criteria);
        List<Measurement> measurements = new ArrayList<Measurement>();
        measurements = searchResult.getValues();
        return measurements;
    }

    @RequestMapping("/search")
    public SearchResult<Measurement> search(@ModelAttribute MeasurementSearchCriteria criteria) throws ServiceException {
        SearchResult<Measurement> searchResult = measurementService.search(criteria);
        return searchResult;
    }

    @RequestMapping(value = "/create", method= RequestMethod.POST)
    public Measurement create(@RequestBody Measurement measurement) throws ServiceException,
            DatabasePersistenceException {
        setCreatedDateAndBy(measurement);
        measurement.setIsActive(true);
        measurementService.save(measurement);
        return measurement;
    }

    @RequestMapping(value = "/update/{id}", method= RequestMethod.POST)
    public Measurement update(@RequestBody Measurement measurement,@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {
        measurement.setId(id);
        setUpdatedDateAndBy(measurement);
        measurementService.update(measurement);
        return measurement;
    }

}
