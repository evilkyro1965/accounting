package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.DepartmentService;
import com.kyrosoft.accounting.model.Department;
import com.kyrosoft.accounting.model.dto.DepartmentSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/20/15.
 */
@RestController
@RequestMapping("rest/department")
public class DepartmentRestController extends BaseRestController {

    @Autowired
    DepartmentService departmentService;

    @RequestMapping("/getall")
    public List<Department> getAllDepartment() throws ServiceException {
        DepartmentSearchCriteria criteria = new DepartmentSearchCriteria();
        criteria.setPageNumber(1);
        criteria.setPageSize(ALL_ROW);
        SearchResult<Department> searchResult = departmentService.search(criteria);
        List<Department> departments = new ArrayList<Department>();
        departments = searchResult.getValues();
        for(Department department : departments) {
            department.setEmployees(null);
        }
        return departments;
    }

    @RequestMapping("/search")
    public SearchResult<Department> search(@ModelAttribute DepartmentSearchCriteria criteria) throws ServiceException {
        SearchResult<Department> searchResult = departmentService.search(criteria);
        for(Department department : searchResult.getValues()) {
            department.setEmployees(null);
        }
        return searchResult;
    }

    @RequestMapping(value = "/create", method= RequestMethod.POST)
    public Department create(@RequestBody Department department) throws ServiceException,
            DatabasePersistenceException {
        setCreatedDateAndBy(department);
        department.setIsActive(true);
        departmentService.save(department);
        return department;
    }

    @RequestMapping(value = "/update/{id}", method= RequestMethod.POST)
    public Department update(@RequestBody Department department,@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {
        department.setId(id);
        setUpdatedDateAndBy(department);
        departmentService.update(department);
        return department;
    }

}
