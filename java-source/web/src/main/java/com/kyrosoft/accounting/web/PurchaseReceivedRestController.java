package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.OrderReceivedService;
import com.kyrosoft.accounting.dao.PurchaseService;
import com.kyrosoft.accounting.model.OrderDetails;
import com.kyrosoft.accounting.model.OrderReceived;
import com.kyrosoft.accounting.model.Purchase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Administrator on 12/22/15.
 */
@RestController
@RequestMapping("rest/purchase-received")
@Transactional
public class PurchaseReceivedRestController extends BaseRestController{

    @Autowired
    PurchaseService purchaseService;

    @Autowired
    OrderReceivedService orderReceivedService;

    @RequestMapping(value = "/get/{id}", method= RequestMethod.GET)
    @Transactional
    public Purchase get(@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {

        Purchase purchase = purchaseService.get(id);
        if(purchase.getOrderDetailsList()!=null) {
            for(OrderDetails orderDetails : purchase.getOrderDetailsList()){
                orderDetails.setPurchase(null);
            }
        }
        return purchase;
    }

    @RequestMapping(value = "/create", method= RequestMethod.POST)
    @Transactional
    public OrderReceived create(@RequestBody OrderReceived orderReceived) throws ServiceException,
            DatabasePersistenceException {
        setCreatedDateAndByGLEntity(orderReceived);
        orderReceivedService.save(orderReceived);
        return orderReceived;
    }

    @RequestMapping(value = "/update/{id}", method= RequestMethod.POST)
    @Transactional
    public OrderReceived update(@RequestBody OrderReceived orderReceived,@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {
        orderReceivedService.update(orderReceived);
        return orderReceived;
    }

    @RequestMapping(value = "/delete/{id}", method= RequestMethod.GET)
    @Transactional
    public void delete(@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {
        OrderReceived orderReceived = new OrderReceived();
        orderReceived.setId(id);
        orderReceivedService.delete(orderReceived);
    }

}
