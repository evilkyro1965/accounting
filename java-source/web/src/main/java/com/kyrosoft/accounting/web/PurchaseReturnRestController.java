package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.OrderReceivedService;
import com.kyrosoft.accounting.dao.OrderReturnService;
import com.kyrosoft.accounting.dao.PurchaseService;
import com.kyrosoft.accounting.model.OrderDetails;
import com.kyrosoft.accounting.model.OrderReturn;
import com.kyrosoft.accounting.model.Purchase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Administrator on 2/13/2016.
 */
@RestController
@RequestMapping("rest/purchase-return")
@Transactional
public class PurchaseReturnRestController extends BaseRestController{

    @Autowired
    PurchaseService purchaseService;

    @Autowired
    OrderReturnService orderReturnService;


    @RequestMapping(value = "/get/{id}", method= RequestMethod.GET)
    @Transactional
    public Purchase get(@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {

        Purchase purchase = purchaseService.get(id);
        if(purchase.getOrderDetailsList()!=null) {
            for(OrderDetails orderDetails : purchase.getOrderDetailsList()){
                orderDetails.setPurchase(null);
            }
        }
        return purchase;
    }

    @RequestMapping(value = "/create", method= RequestMethod.POST)
    @Transactional
    public OrderReturn create(@RequestBody OrderReturn orderReturn) throws ServiceException,
            DatabasePersistenceException {
        setCreatedDateAndByGLEntity(orderReturn);
        orderReturnService.save(orderReturn);
        return orderReturn;
    }

    @RequestMapping(value = "/update/{id}", method= RequestMethod.POST)
    @Transactional
    public OrderReturn update(@RequestBody OrderReturn orderReturn,@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {
        orderReturnService.update(orderReturn);
        return orderReturn;
    }

}
