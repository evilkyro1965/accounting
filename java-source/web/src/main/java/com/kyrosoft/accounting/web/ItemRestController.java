package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.ItemService;
import com.kyrosoft.accounting.model.Item;
import com.kyrosoft.accounting.model.dto.ActiveEntitySearchCriteria;
import com.kyrosoft.accounting.model.dto.ItemSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/3/15.
 */
@RestController
@RequestMapping("rest/item")
public class ItemRestController extends BaseRestController {

    @Autowired
    ItemService itemService;

    @RequestMapping("/getall")
    public List<Item> getAllItem() throws ServiceException {
        ItemSearchCriteria criteria = new ItemSearchCriteria();
        criteria.setPageNumber(1);
        criteria.setPageSize(ALL_ROW);
        SearchResult<Item> searchResult = itemService.search(criteria);
        List<Item> items = new ArrayList<Item>();
        items = searchResult.getValues();
        for(Item item:items) {
            item.setPurchaseMeasurements(null);
        }
        return items;
    }

    @RequestMapping("/search")
    public SearchResult<Item> search(@ModelAttribute ItemSearchCriteria criteria) throws ServiceException {
        SearchResult<Item> searchResult = itemService.search(criteria);
        for(Item item:searchResult.getValues()) {
            item.setPurchaseMeasurements(null);
        }
        return searchResult;
    }

    @RequestMapping(value = "/create", method= RequestMethod.POST)
    public Item create(@RequestBody Item item) throws ServiceException,
            DatabasePersistenceException {
        setCreatedDateAndBy(item);
        item.setIsActive(true);
        itemService.save(item);
        item.setPurchaseMeasurements(null);
        return item;
    }

    @RequestMapping(value = "/update/{id}", method= RequestMethod.POST)
    public Item update(@RequestBody Item item,@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {
        item.setId(id);
        setUpdatedDateAndBy(item);
        itemService.update(item);
        item.setPurchaseMeasurements(null);
        return item;
    }

}
