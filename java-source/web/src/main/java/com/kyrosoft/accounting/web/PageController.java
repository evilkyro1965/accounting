package com.kyrosoft.accounting.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by Administrator on 12/2/15.
 */
@Controller
public class PageController {

    @RequestMapping(value="department", method=GET)
    public String showRegistrationForm() {
        return "DepartmentView";
    }

}
