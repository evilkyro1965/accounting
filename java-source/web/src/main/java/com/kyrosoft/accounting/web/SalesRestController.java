package com.kyrosoft.accounting.web;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.SalesService;
import com.kyrosoft.accounting.model.OrderDetails;
import com.kyrosoft.accounting.model.Purchase;
import com.kyrosoft.accounting.model.Sales;
import com.kyrosoft.accounting.model.SalesOrderDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Administrator on 2/15/2016.
 */
@RestController
@RequestMapping("rest/sales-order")
@Transactional
public class SalesRestController extends BaseRestController {

    @Autowired
    SalesService salesService;

    @RequestMapping(value = "/create", method= RequestMethod.POST)
    @Transactional
    public Sales create(@RequestBody Sales sales) throws ServiceException,
            DatabasePersistenceException {
        setCreatedDateAndByGLEntity(sales);
        if(sales.getSalesOrderDetailsList()!=null) {
            for (SalesOrderDetails orderDetails : sales.getSalesOrderDetailsList()) {
                setCreatedDateAndByGLEntity(orderDetails);
            }
        }
        salesService.save(sales);
        if(sales.getSalesOrderDetailsList()!=null) {
            for(SalesOrderDetails orderDetails : sales.getSalesOrderDetailsList()){
                orderDetails.setSales(null);
            }
        }
        return sales;
    }

    @RequestMapping(value = "/update/{id}", method= RequestMethod.POST)
    @Transactional
    public Sales update(@RequestBody Sales sales,@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {
        sales.setId(id);
        setCreatedDateAndByGLEntity(sales);
        for(SalesOrderDetails orderDetails : sales.getSalesOrderDetailsList()){
            setCreatedDateAndByGLEntity(orderDetails);
        }
        salesService.update(sales);
        if(sales.getSalesOrderDetailsList()!=null) {
            for(SalesOrderDetails orderDetails : sales.getSalesOrderDetailsList()){
                orderDetails.setSales(null);
            }
        }
        return sales;
    }

    @RequestMapping(value = "/get/{id}", method= RequestMethod.GET)
    @Transactional
    public Sales get(@PathVariable long id) throws ServiceException,
            DatabasePersistenceException {

        Sales sales = salesService.get(id);
        if(sales.getSalesOrderDetailsList()!=null) {
            for(SalesOrderDetails orderDetails : sales.getSalesOrderDetailsList()){
                orderDetails.setSales(null);
            }
        }
        return sales;
    }

}
