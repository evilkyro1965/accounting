SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE item;
TRUNCATE TABLE measurement;
TRUNCATE TABLE supplier;
TRUNCATE TABLE employee;
TRUNCATE TABLE warehouse;
TRUNCATE TABLE item_category;
TRUNCATE TABLE department;

INSERT INTO department(NAME,createdBy,createdDate,updatedBy,updatedDate,isActive,email) VALUES ('test','test','2015-01-01 00:00:00','test','2015-01-01 00:00:00',1,'test@test.com');
INSERT INTO department(NAME,createdBy,createdDate,updatedBy,updatedDate,isActive,email) VALUES ('test','test','2015-01-01 00:00:00','test','2015-01-01 00:00:00',1,'test@test.com');
INSERT INTO department(NAME,createdBy,createdDate,updatedBy,updatedDate,isActive,email) VALUES ('test','test','2015-01-01 00:00:00','test','2015-01-01 00:00:00',1,'test@test.com');
INSERT INTO department(NAME,createdBy,createdDate,updatedBy,updatedDate,isActive,email) VALUES ('test','test','2015-01-01 00:00:00','test','2015-01-01 00:00:00',1,'test@test.com');
INSERT INTO department(NAME,createdBy,createdDate,updatedBy,updatedDate,isActive,email) VALUES ('test','test','2015-01-01 00:00:00','test','2015-01-01 00:00:00',1,'test@test.com');

SET FOREIGN_KEY_CHECKS = 1;
