<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Inventory</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <link rel="stylesheet" href="resources/kendoui/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="resources/kendoui/styles/kendo.silver.min.css" />

    <script src="resources/kendoui/js/jquery.min.js"></script>
    <script src="resources/kendoui/js/kendo.all.min.js"></script>
    <link rel="stylesheet" type="text/css" href="resources/css/general.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/style.css" />
</head>

<body>

<div id="header">
    <h1>Lorem</h1>
</div>

<div id="content">
    <t:insertAttribute name="body" />
</div>

</body>

</html>