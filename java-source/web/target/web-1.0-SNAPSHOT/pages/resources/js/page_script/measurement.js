/**
 * Created by Administrator on 11/7/15.
 */
var viewModel;

$(document).ready(function() {

    var validationSaveConf = [
        {'fieldName':'name','validationType':'IsNotEmpty','errorDiv':'#nameSaveError'}
    ];
    var validationEditConf = [
        {'fieldName':'name','validationType':'IsNotEmpty','errorDiv':'#nameEditError'}
    ];

    viewModel = kendo.observable({
        viewForm : {
            name: "",
            shortName: "",
            description: "",
            measurementType: "",
            unit: "",
            isActive: false
        },
        saveForm : {
            name: "",
            shortName: "",
            description: "",
            type: "",
            unit: ""
        },
        editForm : {
            id : 0,
            name: "",
            shortName: "",
            description: "",
            type: "",
            unit: "",
            isActive: false
        },
        searchForm : {
            name: "",
            shortName: "",
            type: "",
            isActive: "",
            sortBy : "",
            sortType : "ASC"
        },
        searchFormVisible : false,
        gridDataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl+"/rest/measurement/search",
                    dataType: "json"
                },
                parameterMap: function(data, type) {
                    if (type == "read") {
                        // send take as "$top" and skip as "$skip"
                        return {
                            pageNumber: data.page,
                            pageSize: data.pageSize,
                            sortBy: viewModel.get("searchForm.sortBy"),
                            sortType: viewModel.get("searchForm.sortType"),
                            name: viewModel.get("searchForm.name"),
                            shortName: viewModel.get("searchForm.shortName"),
                            type: viewModel.get("searchForm.type"),
                            isActive: viewModel.get("searchForm.isActive")
                        }
                    }
                }
            },
            serverPaging: true,
            page : 1,
            pageSize : 20,
            schema: {
                data: function(response) {
                    return response.values;
                },
                model: {
                    fields: {
                        id: { type: "number" },
                        shortName: { type: "string" },
                        description: { type: "string" },
                        isActive: { type: "boolean" },
                        isActiveString: { type: "string"}
                    }
                },
                parse: function(response) {
                    var rows = response.values;
                    for (var i = 0; i < rows.length; i++) {
                        rows[i].isActiveString = rows[i].isActive == true ? "Yes" : "No";
                    }
                    response.values = rows;
                    return response;
                },
                total: "total"
            }
        }),
        gridSelectedItem : null,
        hideAllTab: function() {
            this.set("searchFormVisible",false);

            this.set("saveForm.name","");
            this.set("saveForm.email","");

            modalView.data("kendoWindow").close();
            modalSave.data("kendoWindow").close();
            modalEdit.data("kendoWindow").close();
            modalSearch.data("kendoWindow").close();
        },
        showViewForm: function(event) {
            var selectedItem = this.dataItem($(event.currentTarget).closest("tr"));
            viewModel.set("viewForm",selectedItem);

            viewModel.hideAllTab();
            modalView.data("kendoWindow").center().open();
        },
        showEditForm: function(event) {
            viewModel.hideAllTab();

            var selectedItem = this.dataItem($(event.currentTarget).closest("tr"));
            viewModel.set("editForm.id",selectedItem.id);
            viewModel.set("editForm.name",selectedItem.name);
            viewModel.set("editForm.shortName",selectedItem.shortName);
            viewModel.set("editForm.type",selectedItem.measurementType);
            viewModel.set("editForm.description",selectedItem.description);
            viewModel.set("editForm.unit",selectedItem.unit);
            viewModel.set("editForm.isActive",selectedItem.isActive);
            var editValidator = new ValueValidator(null,validationEditConf);
            editValidator.clean();
            modalEdit.data("kendoWindow").center().open();
        },
        showSaveForm: function(event) {
            this.hideAllTab();
            var saveValidator = new ValueValidator(null,validationSaveConf);
            saveValidator.clean();
            modalSave.data("kendoWindow").center().open();
        },
        showSearchForm: function(event) {
            this.hideAllTab();
            modalSearch.data("kendoWindow").center().open();
        },
        gridOnChange : function(event) {
            var grid = event.sender;
            var selectedItem = grid.select();
            console.log(grid.dataItem(selectedItem));
        },
        cancelEdit : function() {

            this.hideAllTab();
        },
        submit: function() {

            var saveForm = this.get("saveForm");

            var saveValidator = new ValueValidator(saveForm,validationSaveConf);
            var isValid = saveValidator.validate();

            if(isValid){
                var measurement = measurementViewObjectToJsObject(saveForm);

                $.ajax({
                    url: baseUrl+"/rest/measurement/create",
                    type: "post",
                    data: JSON.stringify(measurement),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {
                        viewModel.gridDataSource.fetch();
                        viewModel.hideAllTab();
                        viewModel.set("editFormVisible",false);
                        dialogAlert(saveSuccessDialog);
                        modalSave.data("kendoWindow").close();
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }
            else {
                dialogAlert(errorInputDialog);
            }
        },
        submitUpdate: function() {

            var editForm = this.get("editForm");

            var editValidator = new ValueValidator(editForm,validationEditConf);
            var isValid = editValidator.validate();

            if(isValid){

                var measurement = measurementViewObjectToJsObject(editForm);

                $.ajax({
                    url: baseUrl+"/rest/measurement/update/"+measurement.id,
                    type: "post",
                    data: JSON.stringify(measurement),
                    datatype: 'json',
                    contentType:"application/json",
                    success: function (response) {
                        viewModel.gridDataSource.fetch();
                        viewModel.hideAllTab();
                        viewModel.set("editFormVisible",false);
                        dialogAlert(saveSuccessDialog);
                        modalEdit.data("kendoWindow").close();
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }
            else {
                dialogAlert(errorInputDialog);
            }
        },
        submitSearch: function(e) {
            e.preventDefault();
            viewModel.gridDataSource.fetch();
            modalSearch.data("kendoWindow").close();
        }
    });

    kendo.bind($(".formMainWrapper"), viewModel);

    var modalView = $("#modalView");
    modalView.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "View",
        modal: true,
        visible: false,
        animation: false
    });

    var modalSave = $("#modalSave");
    modalSave.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "Save",
        modal: true,
        visible: false,
        animation: false
    });

    var modalEdit = $("#modalEdit");
    modalEdit.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "Update",
        modal: true,
        visible: false,
        animation: false
    });

    var modalSearch = $("#modalSearch");
    modalSearch.kendoWindow({
        width: formModalWidth,
        actions: ["Close"],
        title: "Search",
        modal: true,
        visible: false,
        animation: false
    });
});

function measurementViewObjectToJsObject(viewObject) {
    var measurement = {};
    measurement.id = viewObject.id;
    measurement.name = viewObject.name;
    measurement.shortName = viewObject.shortName;
    measurement.description = viewObject.description;
    measurement.measurementType = viewObject.type;
    measurement.unit = viewObject.unit;
    measurement.isActive = viewObject.isActive;

    return measurement;
}