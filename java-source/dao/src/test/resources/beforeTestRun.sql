SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE item;
TRUNCATE TABLE measurement;
TRUNCATE TABLE supplier;
TRUNCATE TABLE customer;
TRUNCATE TABLE employee;
TRUNCATE TABLE warehouse;
TRUNCATE TABLE item_category;
TRUNCATE TABLE department;
TRUNCATE TABLE stock_checkpoint;
TRUNCATE TABLE stock_balance;
TRUNCATE TABLE order_return;
TRUNCATE TABLE order_received;
TRUNCATE TABLE order_details;
TRUNCATE TABLE purchase;

INSERT INTO department(NAME,createdBy,createdDate,updatedBy,updatedDate,isActive,email) VALUES ('test','test','2015-01-01 00:00:00','test','2015-01-01 00:00:00',1,'test@test.com');
INSERT  INTO employee(NAME,createdBy,createdDate,updatedBy,updatedDate,isActive,Email,Mobile,PASSWORD,username,departmentId) VALUES
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','test',NULL,'test',1);
insert  into warehouse(name,createdBy,createdDate,updatedBy,updatedDate,isActive,description,label,shelf,departmentId) values
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','test','test',1);
insert  into item_category(name,createdBy,createdDate,updatedBy,updatedDate,isActive) values
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1);
insert into supplier(name,createdBy,createdDate,updatedBy,updatedDate,isActive,address,city,contactPerson,country,email,fax,phone,postalCode,secondaryAddress,state) values
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','test','test','test','test','test','test','test','test','test');
insert into customer(name,createdBy,createdDate,updatedBy,updatedDate,isActive,address,city,contactPerson,country,email,fax,phone,postalCode,secondaryAddress,state) values
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','test','test','test','test','test','test','test','test','test');
insert into measurement(name,createdBy,createdDate,updatedBy,updatedDate,isActive,description,measurementType,shortName,unit) values
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','PURCHASE','test',1),
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','STOCK','test',1);
insert into item(id,name,createdBy,createdDate,updatedBy,updatedDate,isActive,itemCode,itemDescription,itemPrice,itemType,categoryId,stockMeasurementId) values
  (1,'test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','test',1,'STOCKED_PRODUCT',1,1);
insert  into `stock_balance`(`id`,`balance`,`lastTransactionDate`,`lastTransactionEntityId`,`lastTransactionType`,`openingBalance`,`stockBalanceType`,`itemId`,`warehouseId`)
  values (1,0,'3915-02-01 00:00:00',1,'PURCHASE_ORDER_CREATED',0.0,'QUANTITY_ON_HAND',1,NULL);


SET FOREIGN_KEY_CHECKS = 1;