SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE item;
TRUNCATE TABLE measurement;
TRUNCATE TABLE supplier;
TRUNCATE TABLE employee;
TRUNCATE TABLE warehouse;
TRUNCATE TABLE item_category;
TRUNCATE TABLE department;

INSERT INTO department(NAME,createdBy,createdDate,updatedBy,updatedDate,isActive,email) VALUES ('test','test','2015-01-01 00:00:00','test','2015-01-01 00:00:00',1,'test@test.com');
INSERT INTO department(NAME,createdBy,createdDate,updatedBy,updatedDate,isActive,email) VALUES ('test2','test','2015-01-01 00:00:00','test','2015-01-01 00:00:00',1,'test2@test.com');
INSERT  INTO employee(NAME,createdBy,createdDate,updatedBy,updatedDate,isActive,Email,Mobile,PASSWORD,username,departmentId) VALUES
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','test',NULL,'test',1),
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','test',NULL,'test',1),
  ('test','test','2015-01-01 00:00:00',NULL,'2015-01-01 00:00:00',1,'test','test',NULL,'test',1);
insert  into warehouse(name,createdBy,createdDate,updatedBy,updatedDate,isActive,description,label,shelf,departmentId) values
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','test','test',1),
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','test','test',1),
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','test','test',1);
insert  into item_category(name,createdBy,createdDate,updatedBy,updatedDate,isActive) values
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1);
insert into supplier(name,createdBy,createdDate,updatedBy,updatedDate,isActive,address,city,contactPerson,country,email,fax,phone,postalCode,secondaryAddress,state) values
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','test','test','test','test','test','test','test','test','test');
insert into measurement(name,createdBy,createdDate,updatedBy,updatedDate,isActive,description,measurementType,shortName,unit) values
  ('test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','STOCK','test',1);
insert into item(id,name,createdBy,createdDate,updatedBy,updatedDate,isActive,itemCode,itemDescription,itemPrice,itemType,categoryId,stockMeasurementId) values
  (1,'test','test','2015-01-01 00:00:00',NULL,NULL,1,'test','test',1,'STOCKED_PRODUCT',1,1);
SET FOREIGN_KEY_CHECKS = 1;
