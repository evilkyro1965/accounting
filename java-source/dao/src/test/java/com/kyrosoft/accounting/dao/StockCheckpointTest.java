package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.StockCheckPoint;
import com.kyrosoft.accounting.model.StockTransactionType;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Administrator on 2/9/2016.
 */
public class StockCheckpointTest extends BaseTest {

    @Test
    @Transactional
    public void stockCheckpointSaveTest() throws ServiceException, DatabasePersistenceException {


        StockCheckPoint stockCheckPoint = new StockCheckPoint();
        stockCheckPoint.setStockBalance(stockBalanceService.get(IdTest));
        stockCheckPoint.setTransactionDateTime(dateTest);
        stockCheckPoint.setTransactionType(StockTransactionType.PURCHASE_ORDER_CREATED);
        stockCheckPoint.setTransactionId(IdTest);
        stockCheckPoint.setAmount(1.0);
        stockCheckPoint.setBalanceAfter(1.0);

        stockCheckpointService.save(stockCheckPoint);

    }

}
