package com.kyrosoft.accounting.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.*;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Administrator on 2/13/2016.
 */
public class OrderReturnServiceTest extends BaseTest  {

    @Transactional
    public Purchase createPurchase() throws ServiceException, DatabasePersistenceException {
        /**
         * Created new purchase
         */
        Purchase purchase = new Purchase();
        purchase.setNo(stringTest);
        purchase.setDate(dateTest);
        purchase.setRequestBy(employeeService.get(IdTest));
        purchase.setOrderBy(employeeService.get(IdTest));
        purchase.setSupplier(supplierService.get(IdTest));
        purchase.setRemark(stringTest);
        purchase.setPurchaseStatus(PurchaseStatus.ORDER);
        setGLEntitySave(purchase);

        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setPurchase(purchase);
        orderDetails.setItem(itemService.get(IdTest));
        orderDetails.setPurchaseMeasurement(measurementService.get(IdTest));
        orderDetails.setQuantity(10);
        orderDetails.setPrice(1000);
        orderDetails.setDiscountPercent(10);
        orderDetails.setTaxPercent(10);
        orderDetails.setOrderReceivedFull(false);
        orderDetails.setStatus(OrderStatus.ORDER);
        setGLEntitySave(orderDetails);

        List<OrderDetails> orderDetailsList = new ArrayList<OrderDetails>();
        orderDetailsList.add(orderDetails);
        purchase.setOrderDetailsList(orderDetailsList);

        purchaseService.save(purchase);

        return purchase;
    }

    @Test
    @Transactional
    public void createOrderReturnTest() throws ServiceException, DatabasePersistenceException {
        Purchase purchase = createPurchase();
        OrderDetails orderDetails = purchase.getOrderDetailsList().get(0);

        OrderReturn orderReturn = new OrderReturn();
        orderReturn.setOrderDetails(orderDetails);
        orderReturn.setQuantity(1.0);
        orderReturn.setReturnBy(employeeService.get(IdTest));
        orderReturn.setReturnDate(dateTest);
        orderReturn.setWarehouse(warehouseService.get(IdTest));
        orderReturn.setPurchase(purchase);
        setGLEntitySave(orderReturn);

        orderReturnService.save(orderReturn);

        assertEquals(1L, (long) orderReturn.getId());

        //Stock Balance
        StockBalance stockBalanceOnHand = stockBalanceService.get(orderReturn.getWarehouse().getId(),IdTest,StockBalanceType.QUANTITY_ON_HAND);
        StockBalance stockBalanceOnReturn = stockBalanceService.get(orderReturn.getWarehouse().getId(),IdTest,StockBalanceType.QUANTITY_RETURN);
        assertEquals(-1,stockBalanceOnHand.getBalance(),0.00);
        assertEquals(1,stockBalanceOnReturn.getBalance(),0.00);

        //Stock checkpoint
        StockCheckPoint stockCheckpointOnHand = stockCheckpointService.get(
                stockBalanceOnHand,
                StockTransactionType.PURCHASE_ORDER_RETURN,
                orderReturn.getOrderDetails().getId()
        );

        //Stock checkpoint
        StockCheckPoint stockCheckpointOnReturn = stockCheckpointService.get(
                stockBalanceOnReturn,
                StockTransactionType.PURCHASE_ORDER_RETURN,
                orderReturn.getOrderDetails().getId()
        );

        assertEquals(-1,stockCheckpointOnHand.getAmount(),0.00);
        assertEquals(-1,stockCheckpointOnHand.getBalanceAfter(),0.00);
        assertEquals(1,stockCheckpointOnReturn.getAmount(),0.00);
        assertEquals(1,stockCheckpointOnReturn.getBalanceAfter(),0.00);
    }

    @Test
    @Transactional
    public void updateOrderReturnTest() throws ServiceException, DatabasePersistenceException, IOException {
        Purchase purchase = createPurchase();
        OrderDetails orderDetails = purchase.getOrderDetailsList().get(0);

        OrderReturn orderReturn = new OrderReturn();
        orderReturn.setOrderDetails(orderDetails);
        orderReturn.setQuantity(1.0);
        orderReturn.setReturnBy(employeeService.get(IdTest));
        orderReturn.setReturnDate(dateTest);
        orderReturn.setWarehouse(warehouseService.get(IdTest));
        orderReturn.setPurchase(purchase);
        setGLEntitySave(orderReturn);

        orderReturnService.save(orderReturn);

        ObjectMapper objectMapper = new ObjectMapper();
        String orderReturnString = objectMapper.writeValueAsString(orderReturn);
        OrderReturn cloneObj = objectMapper.readValue(orderReturnString, OrderReturn.class);
        cloneObj.setQuantity(5.0);
        orderReturnService.update(cloneObj);

        //Stock Balance
        StockBalance stockBalanceOnHand = stockBalanceService.get(orderReturn.getWarehouse().getId(),IdTest,StockBalanceType.QUANTITY_ON_HAND);
        StockBalance stockBalanceOnReturn = stockBalanceService.get(orderReturn.getWarehouse().getId(),IdTest,StockBalanceType.QUANTITY_RETURN);
        assertEquals(-5,stockBalanceOnHand.getBalance(),0.00);
        assertEquals(5,stockBalanceOnReturn.getBalance(),0.00);

        //Stock checkpoint
        StockCheckPoint stockCheckpointOnHand = stockCheckpointService.get(
                stockBalanceOnHand,
                StockTransactionType.PURCHASE_ORDER_RETURN,
                orderReturn.getOrderDetails().getId()
        );

        //Stock checkpoint
        StockCheckPoint stockCheckpointOnReturn = stockCheckpointService.get(
                stockBalanceOnReturn,
                StockTransactionType.PURCHASE_ORDER_RETURN,
                orderReturn.getOrderDetails().getId()
        );

        assertEquals(-5,stockCheckpointOnHand.getAmount(),0.00);
        assertEquals(-5,stockCheckpointOnHand.getBalanceAfter(),0.00);
        assertEquals(5,stockCheckpointOnReturn.getAmount(),0.00);
        assertEquals(5,stockCheckpointOnReturn.getBalanceAfter(),0.00);
    }

    @Test
    @Transactional
    public void updateOrderReturnUpdateWarehouseTest() throws ServiceException, DatabasePersistenceException, IOException {
        Purchase purchase = createPurchase();
        OrderDetails orderDetails = purchase.getOrderDetailsList().get(0);

        OrderReturn orderReturn = new OrderReturn();
        orderReturn.setOrderDetails(orderDetails);
        orderReturn.setQuantity(1.0);
        orderReturn.setReturnBy(employeeService.get(IdTest));
        orderReturn.setReturnDate(dateTest);
        orderReturn.setWarehouse(warehouseService.get(IdTest));
        orderReturn.setPurchase(purchase);
        setGLEntitySave(orderReturn);

        orderReturnService.save(orderReturn);

        Warehouse warehouse = createWarehouseDummy();

        ObjectMapper objectMapper = new ObjectMapper();
        String orderReturnString = objectMapper.writeValueAsString(orderReturn);
        OrderReturn cloneObj = objectMapper.readValue(orderReturnString, OrderReturn.class);
        cloneObj.setQuantity(5.0);
        cloneObj.setWarehouse(warehouse);
        orderReturnService.update(cloneObj);

        //Stock Balance
        StockBalance stockBalanceOnHand = stockBalanceService.get(warehouse.getId(),IdTest,StockBalanceType.QUANTITY_ON_HAND);
        StockBalance stockBalanceOnReturn = stockBalanceService.get(warehouse.getId(),IdTest,StockBalanceType.QUANTITY_RETURN);
        assertEquals(-5,stockBalanceOnHand.getBalance(),0.00);
        assertEquals(5,stockBalanceOnReturn.getBalance(),0.00);

        //Stock checkpoint
        StockCheckPoint stockCheckpointOnHand = stockCheckpointService.get(
                stockBalanceOnHand,
                StockTransactionType.PURCHASE_ORDER_RETURN,
                orderReturn.getOrderDetails().getId()
        );

        //Stock checkpoint
        StockCheckPoint stockCheckpointOnReturn = stockCheckpointService.get(
                stockBalanceOnReturn,
                StockTransactionType.PURCHASE_ORDER_RETURN,
                orderReturn.getOrderDetails().getId()
        );

        assertEquals(-5,stockCheckpointOnHand.getAmount(),0.00);
        assertEquals(-5,stockCheckpointOnHand.getBalanceAfter(),0.00);
        assertEquals(5,stockCheckpointOnReturn.getAmount(),0.00);
        assertEquals(5,stockCheckpointOnReturn.getBalanceAfter(),0.00);
    }

}
