package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.Item;
import com.kyrosoft.accounting.model.ItemType;
import com.kyrosoft.accounting.model.Measurement;
import com.kyrosoft.accounting.model.dto.ItemSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import org.junit.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by Administrator on 10/23/15.
 */
public class ItemServiceTest extends BaseTest {

    @Test
    @Transactional
    public void itemGetTest() throws ServiceException, DatabasePersistenceException {

        createItemDummy();

        Item item = itemService.get(itemTest.getId());
        assertEquals(stringTest,item.getName());
        assertEquals(stringTest,item.getItemCode());
        assertEquals(createItemCategoryDummy().getId(),item.getCategory().getId());
        assertEquals(stringTest,item.getItemDescription());
        assertEquals(doubleTest,item.getItemPrice());
        assertEquals(createMeasurementDummy().getId(),item.getStockMeasurement().getId());
    }

    @Test
    @Transactional
    public void itemSaveTest() throws ServiceException, DatabasePersistenceException {

        Item item = new Item();
        item.setName(stringTest);
        item.setItemCode(stringTest);
        item.setItemType(ItemType.STOCKED_PRODUCT);
        item.setCategory(createItemCategoryDummy());
        item.setItemDescription(stringTest);
        item.setItemPrice(doubleTest);
        item.setStockMeasurement(createMeasurementDummy());

        Set<Measurement> purchaseMeasurement = new HashSet<Measurement>();
        purchaseMeasurement.add(createMeasurementDummy());

        item.setPurchaseMeasurements(purchaseMeasurement);
        item.setIsActive(boolTest);
        setAuditableEntitySave(item);

        itemService.save(item);

        Item saved = itemService.get(item.getId());
        assertEquals(stringTest,saved.getName());
        assertEquals(stringTest,saved.getItemCode());
        assertEquals(createItemCategoryDummy().getId(),saved.getCategory().getId());
        assertEquals(stringTest,saved.getItemDescription());
        assertEquals(doubleTest,saved.getItemPrice());
        assertEquals(createMeasurementDummy().getId(),saved.getStockMeasurement().getId());
        List<Measurement> purchaseMeasurements = new ArrayList<Measurement>(saved.getPurchaseMeasurements());
        assertEquals(createMeasurementDummy().getId(),purchaseMeasurements.get(0).getId());
    }

    @Test
    @Transactional
    public void itemUpdateTest() throws ServiceException, DatabasePersistenceException {

        createItemDummy();

        Item item = itemService.get(itemTest.getId());
        item.setName(stringTest);
        item.setItemCode(stringTest);
        item.setItemType(ItemType.STOCKED_PRODUCT);
        item.setCategory(createItemCategoryDummy());
        item.setItemDescription(stringTest);
        item.setItemPrice(doubleTest);
        item.setStockMeasurement(createMeasurementDummy());

        Set<Measurement> purchaseMeasurement = new HashSet<Measurement>();
        purchaseMeasurement.add(createMeasurementDummy());

        item.setPurchaseMeasurements(purchaseMeasurement);
        item.setIsActive(boolTest);
        setAuditableEntitySave(item);

        itemService.update(item);

        Item saved = itemService.get(item.getId());
        assertEquals(stringTest,saved.getName());
        assertEquals(stringTest,saved.getItemCode());
        assertEquals(createItemCategoryDummy().getId(),saved.getCategory().getId());
        assertEquals(stringTest,saved.getItemDescription());
        assertEquals(doubleTest,saved.getItemPrice());
        assertEquals(createMeasurementDummy().getId(),saved.getStockMeasurement().getId());
        List<Measurement> purchaseMeasurements = new ArrayList<Measurement>(saved.getPurchaseMeasurements());
        assertEquals(createMeasurementDummy().getId(),purchaseMeasurements.get(0).getId());
    }

    @Test
    public void itemDeleteTest() throws ServiceException, DatabasePersistenceException {

        createItemDummy();

        itemService.delete(itemTest);
        Item item = itemService.get(itemTest.getId());
        assertNull(item);

    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:SearchDataDummies.sql")
    })
    public void itemSearchTest()  throws ServiceException, DatabasePersistenceException {

        ItemSearchCriteria criteria = new ItemSearchCriteria();
        criteria.setName(stringTest);
        criteria.setItemType(ItemType.STOCKED_PRODUCT);
        criteria.setItemCode(stringTest);
        criteria.setIsActive(boolTest);
        criteria.setPageNumber(1);
        criteria.setPageSize(10);

        SearchResult<Item> searchResult = itemService.search(criteria);
        assertEquals(1,searchResult.getTotal());

    }

}
