package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.Employee;
import com.kyrosoft.accounting.model.dto.EmployeeSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import org.junit.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 10/22/15.
 */
public class EmployeeServiceTest extends BaseTest {

    @Test
    public void employeeGetTest() throws ServiceException, DatabasePersistenceException {

        Employee employee = new Employee();
        employee.setName(stringTest);
        employee.setUsername(stringTest);
        employee.setEmail(stringTest);
        employee.setMobile(stringTest);
        employee.setIsActive(boolTest);
        setAuditableEntitySave(employee);
        employeeService.save(employee);

        Employee saved = employeeService.get(employee.getId());
        assertEquals(stringTest,saved.getName());
        assertEquals(stringTest,saved.getUsername());
        assertEquals(stringTest,saved.getEmail());
        assertEquals(stringTest,saved.getMobile());
        assertEquals(boolTest,saved.getIsActive());
        testAuditableEntitySave(saved);
    }

    @Test
    public void employeeSaveTest() throws ServiceException, DatabasePersistenceException {

        Employee employee = new Employee();
        employee.setName(stringTest);
        employee.setUsername(stringTest);
        employee.setEmail(stringTest);
        employee.setMobile(stringTest);
        employee.setIsActive(boolTest);
        employee.setDepartment(createDepartmentDummy());
        setAuditableEntitySave(employee);
        employeeService.save(employee);

        Employee saved = employeeService.get(employee.getId());
        assertEquals(stringTest,saved.getName());
        assertEquals(stringTest,saved.getUsername());
        assertEquals(stringTest,saved.getEmail());
        assertEquals(stringTest,saved.getMobile());
        assertEquals(boolTest,saved.getIsActive());
        assertEquals(departmentTest.getId(),saved.getDepartment().getId());
        testAuditableEntitySave(saved);
    }

    @Test
    public void employeeUpdateTest() throws ServiceException, DatabasePersistenceException {

        Employee employee = createEmployeeDummy();

        employee.setName(stringTest);
        employee.setUsername(stringTest);
        employee.setEmail(stringTest);
        employee.setMobile(stringTest);
        employee.setIsActive(boolTest);
        setAuditableEntitySave(employee);
        employee.setDepartment(createDepartmentDummy());
        employeeService.update(employee);

        Employee updated = employeeService.get(employee.getId());
        assertEquals(stringTest,updated.getName());
        assertEquals(stringTest,updated.getUsername());
        assertEquals(stringTest,updated.getEmail());
        assertEquals(stringTest,updated.getMobile());
        assertEquals(boolTest,updated.getIsActive());
        assertEquals(departmentTest.getId(),updated.getDepartment().getId());
    }

    @Test
    public void employeeDeleteTest() throws ServiceException, DatabasePersistenceException {

        Employee employee = createEmployeeDummy();
        employeeService.delete(employee);
        Employee deleted = employeeService.get(employee.getId());

        assertNull(deleted);
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:SearchDataDummies.sql")
    })
    public void searchEmployeeTest() throws ServiceException, DatabasePersistenceException {

        EmployeeSearchCriteria criteria = new EmployeeSearchCriteria();
        criteria.setName(stringTest);
        criteria.setUsername(stringTest);
        criteria.setDepartmentId(longTest);
        criteria.setEmail(stringTest);
        criteria.setMobile(stringTest);
        criteria.setIsActive(boolTest);
        criteria.setPageNumber(1);
        criteria.setPageSize(10);

        SearchResult<Employee> searchResult = employeeService.search(criteria);
        assertEquals(3,searchResult.getTotal());

    }

}
