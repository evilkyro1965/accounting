package com.kyrosoft.accounting.dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
/**
 * Created by Administrator on 11/8/15.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        DepartmentServiceTest.class,
        EmployeeServiceTest.class,
        ItemCategoryServiceTest.class,
        ItemServiceTest.class,
        MeasurementServiceTest.class,
        SupplierServiceTest.class,
        WarehouseServiceTest.class
})
public class TestSuites {
}
