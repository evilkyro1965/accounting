package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.StockBalance;
import com.kyrosoft.accounting.model.StockBalanceType;
import com.kyrosoft.accounting.model.StockTransactionType;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 2/9/2016.
 */
public class StockBalanceTest extends BaseTest {

    @Test
    @Transactional
    @Rollback(false)
    public void saveStockBalanceTest() throws ServiceException, DatabasePersistenceException {
        StockBalance stockBalance = new StockBalance();
        stockBalance.setWarehouse(null);
        stockBalance.setItem(itemService.get(IdTest));
        stockBalance.setStockBalanceType(StockBalanceType.QUANTITY_AVAILABLE);
        stockBalance.setBalance(0.0);
        stockBalance.setLastTransactionDate(dateTest);
        stockBalance.setLastTransactionEntityId(IdTest);
        stockBalance.setLastTransactionType(StockTransactionType.PURCHASE_ORDER_CREATED);

        stockBalanceService.save(stockBalance);

        StockBalance stockBalance1 = stockBalanceService.get(null,IdTest,StockBalanceType.QUANTITY_AVAILABLE);
        assertNotNull(stockBalance1);
    }

}
