package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.ItemCategory;
import com.kyrosoft.accounting.model.Warehouse;
import com.kyrosoft.accounting.model.dto.ActiveEntitySearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import com.kyrosoft.accounting.model.dto.WarehouseSearchCriteria;
import org.junit.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 10/22/15.
 */
public class ItemCategoryServiceTest extends BaseTest {

    @Test
    public void itemCategoryGetTest() throws ServiceException, DatabasePersistenceException {

        createItemCategoryDummy();

        ItemCategory saved = itemCategoryService.get(itemCategoryTest.getId());
        assertEquals(stringTest,saved.getName());
        assertEquals(boolTest,saved.getIsActive());
        testAuditableEntitySave(saved);
    }

    @Test
    public void itemCategorySavedTest() throws ServiceException, DatabasePersistenceException {

        ItemCategory itemCategory = new ItemCategory();
        itemCategory.setName(stringTest);
        itemCategory.setIsActive(boolTest);
        setAuditableEntitySave(itemCategory);

        itemCategoryService.save(itemCategory);

        ItemCategory saved = itemCategoryService.get(itemCategory.getId());
        assertEquals(stringTest,saved.getName());
        assertEquals(boolTest,saved.getIsActive());
        testAuditableEntitySave(saved);
    }

    @Test
    public void itemCategoryUpdateTest() throws ServiceException, DatabasePersistenceException {

        ItemCategory itemCategory = createItemCategoryDummy();

        itemCategory.setName(stringTest);
        itemCategory.setIsActive(boolTest);
        setAuditableEntitySave(itemCategory);

        ItemCategory update = itemCategoryService.update(itemCategory);
        assertEquals(stringTest,update.getName());
        assertEquals(boolTest,update.getIsActive());
    }

    @Test
    public void itemCategoryDeleteTest() throws ServiceException, DatabasePersistenceException {

        ItemCategory itemCategory = createItemCategoryDummy();
        itemCategoryService.delete(itemCategory);

        ItemCategory deleted = itemCategoryService.get(itemCategory.getId());
        itemCategoryTest = null;
        assertNull(deleted);
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:SearchDataDummies.sql")
    })
    public void itemCategorySearchTest() throws ServiceException, DatabasePersistenceException {

        ActiveEntitySearchCriteria criteria = new ActiveEntitySearchCriteria();
        criteria.setName(stringTest);
        criteria.setIsActive(boolTest);
        criteria.setPageNumber(1);
        criteria.setPageSize(10);

        SearchResult<ItemCategory> searchResult = itemCategoryService.search(criteria);

        assertEquals(1,searchResult.getTotal());
    }

}