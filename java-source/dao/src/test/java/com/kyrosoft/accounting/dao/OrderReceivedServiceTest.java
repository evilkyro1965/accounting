package com.kyrosoft.accounting.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.*;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Administrator on 2/5/2016.
 */
public class OrderReceivedServiceTest extends BaseTest {

    @Transactional
    public Purchase createPurchase() throws ServiceException, DatabasePersistenceException {
        /**
         * Created new purchase
         */
        Purchase purchase = new Purchase();
        purchase.setNo(stringTest);
        purchase.setDate(dateTest);
        purchase.setRequestBy(employeeService.get(IdTest));
        purchase.setOrderBy(employeeService.get(IdTest));
        purchase.setSupplier(supplierService.get(IdTest));
        purchase.setRemark(stringTest);
        purchase.setPurchaseStatus(PurchaseStatus.ORDER);
        setGLEntitySave(purchase);

        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setPurchase(purchase);
        orderDetails.setItem(itemService.get(IdTest));
        orderDetails.setPurchaseMeasurement(measurementService.get(IdTest));
        orderDetails.setQuantity(10);
        orderDetails.setPrice(1000);
        orderDetails.setDiscountPercent(10);
        orderDetails.setTaxPercent(10);
        orderDetails.setOrderReceivedFull(false);
        orderDetails.setStatus(OrderStatus.ORDER);
        setGLEntitySave(orderDetails);

        List<OrderDetails> orderDetailsList = new ArrayList<OrderDetails>();
        orderDetailsList.add(orderDetails);
        purchase.setOrderDetailsList(orderDetailsList);

        purchaseService.save(purchase);

        return purchase;
    }

    @Test
    @Transactional
    @Rollback(false)
    public void createOrderReceivedTest() throws ServiceException, DatabasePersistenceException {
        Purchase purchase = createPurchase();
        OrderDetails orderDetails = purchase.getOrderDetailsList().get(0);

        OrderReceived orderReceived = new OrderReceived();
        orderReceived.setOrderDetails(orderDetails);
        orderReceived.setQuantity(1.0);
        orderReceived.setReceivedBy(employeeService.get(IdTest));
        orderReceived.setReceivedDate(dateTest);
        orderReceived.setWarehouse(warehouseService.get(IdTest));
        orderReceived.setPurchase(purchase);
        setGLEntitySave(orderReceived);

        orderReceivedService.save(orderReceived);

        assertEquals(1L,(long)orderReceived.getId());

        //Stock Balance
        StockBalance stockBalanceOnHand = stockBalanceService.get(orderReceived.getWarehouse().getId(),IdTest,StockBalanceType.QUANTITY_ON_HAND);
        StockBalance stockBalanceOnOrder = stockBalanceService.get(null,IdTest,StockBalanceType.QUANTITY_ON_ORDER);
        assertEquals(1,stockBalanceOnHand.getBalance(),0.00);
        assertEquals(9,stockBalanceOnOrder.getBalance(),0.00);

        //Stock checkpoint
        StockCheckPoint stockCheckpointOnHand = stockCheckpointService.get(
                stockBalanceOnHand,
                StockTransactionType.PURCHASE_ORDER_RECEIVE,
                orderReceived.getOrderDetails().getId()
        );

        //Stock checkpoint
        StockCheckPoint stockCheckpointOnOrder = stockCheckpointService.get(
                stockBalanceOnOrder,
                StockTransactionType.PURCHASE_ORDER_RECEIVE,
                orderReceived.getOrderDetails().getId()
        );

        assertEquals(1,stockCheckpointOnHand.getAmount(),0.00);
        assertEquals(1,stockCheckpointOnHand.getBalanceAfter(),0.00);
        assertEquals(-1,stockCheckpointOnOrder.getAmount(),0.00);
        assertEquals(9,stockCheckpointOnOrder.getBalanceAfter(),0.00);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void updateOrderReceivedTest() throws ServiceException, DatabasePersistenceException, IOException {
        Purchase purchase = createPurchase();
        OrderDetails orderDetails = purchase.getOrderDetailsList().get(0);

        OrderReceived orderReceived = new OrderReceived();
        orderReceived.setOrderDetails(orderDetails);
        orderReceived.setQuantity(2.0);
        orderReceived.setReceivedBy(employeeService.get(IdTest));
        orderReceived.setReceivedDate(dateTest);
        orderReceived.setWarehouse(warehouseService.get(IdTest));
        orderReceived.setPurchase(purchase);
        setGLEntitySave(orderReceived);

        orderReceivedService.save(orderReceived);

        ObjectMapper objectMapper = new ObjectMapper();
        String orderReceivedString = objectMapper.writeValueAsString(orderReceived);
        OrderReceived cloneObj = objectMapper.readValue(orderReceivedString, OrderReceived.class);
        cloneObj.setQuantity(5.0);
        orderReceivedService.update(cloneObj);


        //Stock Balance
        StockBalance stockBalanceOnHand = stockBalanceService.get(orderReceived.getWarehouse().getId(),IdTest,StockBalanceType.QUANTITY_ON_HAND);
        StockBalance stockBalanceOnOrder = stockBalanceService.get(null,IdTest,StockBalanceType.QUANTITY_ON_ORDER);
        assertEquals(5,stockBalanceOnHand.getBalance(),0.00);
        assertEquals(5,stockBalanceOnOrder.getBalance(),0.00);

        //Stock checkpoint
        StockCheckPoint stockCheckpointOnHand = stockCheckpointService.get(
                stockBalanceOnHand,
                StockTransactionType.PURCHASE_ORDER_RECEIVE,
                orderReceived.getOrderDetails().getId()
        );

        //Stock checkpoint
        StockCheckPoint stockCheckpointOnOrder = stockCheckpointService.get(
                stockBalanceOnOrder,
                StockTransactionType.PURCHASE_ORDER_RECEIVE,
                orderReceived.getOrderDetails().getId()
        );

        assertEquals(5,stockCheckpointOnHand.getAmount(),0.00);
        assertEquals(5,stockCheckpointOnHand.getBalanceAfter(),0.00);
        assertEquals(-5,stockCheckpointOnOrder.getAmount(),0.00);
        assertEquals(5,stockCheckpointOnOrder.getBalanceAfter(),0.00);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void updateOrderReceivedChangeWarehouseTest() throws ServiceException, DatabasePersistenceException, IOException {
        Purchase purchase = createPurchase();
        OrderDetails orderDetails = purchase.getOrderDetailsList().get(0);

        OrderReceived orderReceived = new OrderReceived();
        orderReceived.setOrderDetails(orderDetails);
        orderReceived.setQuantity(1.0);
        orderReceived.setReceivedBy(employeeService.get(IdTest));
        orderReceived.setReceivedDate(dateTest);
        orderReceived.setWarehouse(warehouseService.get(IdTest));
        orderReceived.setPurchase(purchase);
        setGLEntitySave(orderReceived);

        orderReceivedService.save(orderReceived);

        Warehouse warehouse = createWarehouseDummy();

        ObjectMapper objectMapper = new ObjectMapper();
        String orderReceivedString = objectMapper.writeValueAsString(orderReceived);
        OrderReceived cloneObj = objectMapper.readValue(orderReceivedString, OrderReceived.class);
        cloneObj.setQuantity(2.0);
        cloneObj.setWarehouse(warehouse);
        orderReceivedService.update(cloneObj);

        //Stock Balance
        StockBalance stockBalanceOnHand = stockBalanceService.get(warehouse.getId(),IdTest,StockBalanceType.QUANTITY_ON_HAND);
        StockBalance stockBalanceOnOrder = stockBalanceService.get(null,IdTest,StockBalanceType.QUANTITY_ON_ORDER);
        assertEquals(2,stockBalanceOnHand.getBalance(),0.00);
        assertEquals(8,stockBalanceOnOrder.getBalance(),0.00);

        //Stock checkpoint
        StockCheckPoint stockCheckpointOnHand = stockCheckpointService.get(
                stockBalanceOnHand,
                StockTransactionType.PURCHASE_ORDER_RECEIVE,
                orderReceived.getOrderDetails().getId()
        );

        //Stock checkpoint
        StockCheckPoint stockCheckpointOnOrder = stockCheckpointService.get(
                stockBalanceOnOrder,
                StockTransactionType.PURCHASE_ORDER_RECEIVE,
                orderReceived.getOrderDetails().getId()
        );

        assertEquals(2,stockCheckpointOnHand.getAmount(),0.00);
        assertEquals(2,stockCheckpointOnHand.getBalanceAfter(),0.00);
        assertEquals(-2,stockCheckpointOnOrder.getAmount(),0.00);
        assertEquals(8,stockCheckpointOnOrder.getBalanceAfter(),0.00);
    }

}
