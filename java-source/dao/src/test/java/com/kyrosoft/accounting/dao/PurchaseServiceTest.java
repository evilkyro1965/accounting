package com.kyrosoft.accounting.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.impl.ServiceHelper;
import com.kyrosoft.accounting.model.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by Administrator on 12/22/15.
 */
public class PurchaseServiceTest extends BaseTest {

    @Test
    @Transactional
    public void purchaseSaveTest() throws ServiceException, DatabasePersistenceException {

        Purchase purchase = new Purchase();
        purchase.setNo(stringTest);
        purchase.setDate(dateTest);
        purchase.setRequestBy(employeeService.get(IdTest));
        purchase.setOrderBy(employeeService.get(IdTest));
        purchase.setSupplier(supplierService.get(IdTest));
        purchase.setRemark(stringTest);
        purchase.setPurchaseStatus(PurchaseStatus.ORDER);
        setGLEntitySave(purchase);

        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setPurchase(purchase);
        orderDetails.setItem(itemService.get(IdTest));
        orderDetails.setPurchaseMeasurement(measurementService.get(IdTest));
        orderDetails.setQuantity(10);
        orderDetails.setPrice(1000);
        orderDetails.setDiscountPercent(10);
        orderDetails.setTaxPercent(10);
        orderDetails.setOrderReceivedFull(false);
        orderDetails.setStatus(OrderStatus.ORDER);
        setGLEntitySave(orderDetails);

        List<OrderDetails> orderDetailsList = new ArrayList<OrderDetails>();
        orderDetailsList.add(orderDetails);
        purchase.setOrderDetailsList(orderDetailsList);

        purchaseService.save(purchase);

        Purchase saved = purchaseService.get(purchase.getId());
        assertEquals(stringTest,saved.getNo());
        assertEquals(1,saved.getOrderDetailsList().size());

    }

    @Transactional
    public Purchase createPurchaseOrder() throws ServiceException, DatabasePersistenceException {

        Purchase purchase = new Purchase();
        purchase.setNo(stringTest);
        purchase.setDate(dateTest);
        purchase.setRequestBy(employeeService.get(IdTest));
        purchase.setOrderBy(employeeService.get(IdTest));
        purchase.setSupplier(supplierService.get(IdTest));
        purchase.setRemark(stringTest);
        purchase.setPurchaseStatus(PurchaseStatus.ORDER);
        setGLEntitySave(purchase);

        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setPurchase(purchase);
        orderDetails.setItem(itemService.get(IdTest));
        orderDetails.setPurchaseMeasurement(measurementService.get(IdTest));
        orderDetails.setQuantity(20);
        orderDetails.setPrice(1000);
        orderDetails.setDiscountPercent(10);
        orderDetails.setTaxPercent(10);
        orderDetails.setOrderReceivedFull(false);
        orderDetails.setStatus(OrderStatus.ORDER);
        setGLEntitySave(orderDetails);

        List<OrderDetails> orderDetailsList = new ArrayList<OrderDetails>();
        orderDetailsList.add(orderDetails);
        purchase.setOrderDetailsList(orderDetailsList);

        purchaseService.save(purchase);
        return purchase;
    }

    @Test
    @Transactional
    public void purchaseUpdateUpdateOrderDetailsTest() throws ServiceException, DatabasePersistenceException, IOException {

        /**
         * Update purchase with add new order details
         */
        Purchase purchase = createPurchaseOrder();
        Purchase saved = new Purchase();
        saved.setId(purchase.getId());
        saved.setNo(stringTest);
        saved.setDate(dateTest);
        saved.setRemark(stringTest);
        List<OrderDetails> updateOrderDetailsList = new ArrayList<OrderDetails>();

        for(OrderDetails orderDetails : purchase.getOrderDetailsList()) {
            ObjectMapper objectMapper = new ObjectMapper();
            String orderDetailsString = objectMapper.writeValueAsString(orderDetails);
            OrderDetails cloneObj = objectMapper.readValue(orderDetailsString, OrderDetails.class);
            cloneObj.setQuantity(100);
            updateOrderDetailsList.add(cloneObj);
        }

        saved.setOrderDetailsList(updateOrderDetailsList);

        /**
         * Check the result
         */
        purchaseService.update(saved);

        Purchase updated = purchaseService.get(purchase.getId());

        //Stock Balance must be 100
        StockBalance stockBalance= stockBalanceService.get(null,IdTest,StockBalanceType.QUANTITY_ON_ORDER);
        assertEquals(100,stockBalance.getBalance(),0.00);

        //Stock checkpoint must be 100
        StockCheckPoint stockCheckpoint = stockCheckpointService.get(
                stockBalance,
                StockTransactionType.PURCHASE_ORDER_CREATED,
                saved.getOrderDetailsList().get(0).getId()
        );

        assertEquals(100,stockCheckpoint.getAmount(),0.00);
    }

    @Test
    @Transactional
    public void purchaseUpdateUpdateOrderDetailsUpdateItemTest() throws ServiceException, DatabasePersistenceException, IOException {

        /**
         * Update purchase with updated item
         */
        Purchase purchase = createPurchaseOrder();
        Purchase saved = new Purchase();
        saved.setId(purchase.getId());
        saved.setNo(stringTest);
        saved.setDate(dateTest);
        saved.setRemark(stringTest);
        List<OrderDetails> updateOrderDetailsList = new ArrayList<OrderDetails>();

        Item newItem = createItemDummy();

        for(OrderDetails orderDetails : purchase.getOrderDetailsList()) {
            ObjectMapper objectMapper = new ObjectMapper();
            String orderDetailsString = objectMapper.writeValueAsString(orderDetails);
            OrderDetails cloneObj = objectMapper.readValue(orderDetailsString, OrderDetails.class);
            cloneObj.setItem(newItem);
            cloneObj.setQuantity(100);
            updateOrderDetailsList.add(cloneObj);
        }

        saved.setOrderDetailsList(updateOrderDetailsList);

        /**
         * Check the result
         */
        purchaseService.update(saved);

        Purchase updated = purchaseService.get(purchase.getId());

        //Stock Balance item before must be 0
        StockBalance stockBalanceBeforeItem = stockBalanceService.get(null,IdTest,StockBalanceType.QUANTITY_ON_ORDER);
        assertEquals(0,stockBalanceBeforeItem.getBalance(),0.00);

        //Stock Balance new item must be 100
        StockBalance stockBalanceNewItem = stockBalanceService.get(null,newItem.getId(),StockBalanceType.QUANTITY_ON_ORDER);
        assertEquals(100,stockBalanceNewItem.getBalance(),0.00);

        //Stock checkpoint item before must be deleted
        StockCheckPoint stockCheckpointBefore = stockCheckpointService.get(
                stockBalanceBeforeItem,
                StockTransactionType.PURCHASE_ORDER_CREATED,
                saved.getOrderDetailsList().get(0).getId()
        );

        assertNull(stockCheckpointBefore);

        //Stock checkpoint new item must be 100
        StockCheckPoint stockCheckpointNewItem = stockCheckpointService.get(
                stockBalanceNewItem,
                StockTransactionType.PURCHASE_ORDER_CREATED,
                saved.getOrderDetailsList().get(0).getId()
                );

        assertEquals(100,stockCheckpointNewItem.getAmount(),0.00);
    }

    @Test
    @Transactional()
    public void purchaseUpdateAddNewOrderDetailsTest() throws ServiceException, DatabasePersistenceException, IOException {

        /**
         * Update purchase with add new order details
         */
        Purchase purchase = createPurchaseOrder();
        Purchase saved = new Purchase();
        saved.setId(purchase.getId());
        saved.setNo(stringTest);
        saved.setDate(dateTest);
        saved.setRemark(stringTest);
        List<OrderDetails> updateOrderDetailsList = new ArrayList<OrderDetails>();

        for(OrderDetails orderDetails : purchase.getOrderDetailsList()) {
            ObjectMapper objectMapper = new ObjectMapper();
            String orderDetailsString = objectMapper.writeValueAsString(orderDetails);
            OrderDetails cloneObj = objectMapper.readValue(orderDetailsString, OrderDetails.class);
            updateOrderDetailsList.add(cloneObj);
        }

        OrderDetails orderDetailsAdd = new OrderDetails();
        orderDetailsAdd.setPurchase(purchase);
        orderDetailsAdd.setItem(itemService.get(IdTest));
        orderDetailsAdd.setPurchaseMeasurement(measurementService.get(IdTest));
        orderDetailsAdd.setQuantity(5);
        orderDetailsAdd.setPrice(5000);
        orderDetailsAdd.setDiscountPercent(5);
        orderDetailsAdd.setTaxPercent(10);
        orderDetailsAdd.setOrderReceivedFull(false);
        setGLEntitySave(orderDetailsAdd);

        updateOrderDetailsList.add(orderDetailsAdd);
        saved.setOrderDetailsList(updateOrderDetailsList);

        /**
         * Check the result
         */
        purchaseService.update(saved);

        //Stock Balance must 15
        StockBalance stockBalance = stockBalanceService.get(null,IdTest,StockBalanceType.QUANTITY_ON_ORDER);
        assertEquals(25,stockBalance.getBalance(),0.00);

        //Stock checkpoint item balance must be 5
        StockCheckPoint stockCheckpoint = stockCheckpointService.get(
                stockBalance,
                StockTransactionType.PURCHASE_ORDER_CREATED,
                orderDetailsAdd.getId()
        );

        assertEquals(5,stockCheckpoint.getAmount(),0.00);
    }
    
    @Test
    @Transactional
    public void purchaseUpdateRemoveNewOrderDetailsTest() throws ServiceException, DatabasePersistenceException {
        /**
         * Update purchase with remove order details
         */
        Purchase purchase = createPurchaseOrder();
        Purchase saved = new Purchase();
        saved.setId(purchase.getId());
        saved.setNo(stringTest);
        saved.setDate(dateTest);
        saved.setRemark(stringTest);
        List<OrderDetails> deleteOrderDetailsList = new ArrayList<OrderDetails>();
        saved.setOrderDetailsList(deleteOrderDetailsList);
        /**
         * Check the result
         */
        purchaseService.update(saved);

        Purchase updated = purchaseService.get(purchase.getId());

        //Stock Balance must 0
        StockBalance stockBalance = stockBalanceService.get(null,IdTest,StockBalanceType.QUANTITY_ON_ORDER);
        assertEquals(0,stockBalance.getBalance(),0.00);
    }

    @Test
    @Transactional
    public void purchaseSerializeTest() throws ServiceException, DatabasePersistenceException, JsonProcessingException {

        Purchase purchase = new Purchase();
        purchase.setNo(stringTest);
        purchase.setDate(dateTest);
        purchase.setRequestBy(employeeService.get(IdTest));
        purchase.setOrderBy(employeeService.get(IdTest));
        purchase.setSupplier(supplierService.get(IdTest));
        purchase.setRemark(stringTest);
        purchase.setPurchaseStatus(PurchaseStatus.ORDER);
        setGLEntitySave(purchase);

        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setPurchase(purchase);
        orderDetails.setItem(itemService.get(IdTest));
        orderDetails.setPurchaseMeasurement(measurementService.get(IdTest));
        orderDetails.setQuantity(10);
        orderDetails.setPrice(1000);
        orderDetails.setDiscountPercent(10);
        orderDetails.setTaxPercent(10);
        orderDetails.setOrderReceivedFull(false);
        orderDetails.setStatus(OrderStatus.ORDER);
        setGLEntitySave(orderDetails);

        OrderReceived orderReceived = new OrderReceived();
        orderReceived.setOrderDetails(orderDetails);
        orderReceived.setQuantity(1.0);
        orderReceived.setReceivedBy(employeeService.get(IdTest));
        orderReceived.setReceivedDate(dateTest);
        orderReceived.setWarehouse(warehouseService.get(IdTest));
        orderReceived.setPurchase(purchase);
        setGLEntitySave(orderReceived);

        List<OrderReceived> orderReceivedList = new ArrayList<OrderReceived>();
        orderReceivedList.add(orderReceived);

        List<OrderDetails> orderDetailsList = new ArrayList<OrderDetails>();
        orderDetails.setOrderReceivedList(orderReceivedList);
        orderDetailsList.add(orderDetails);
        purchase.setOrderDetailsList(orderDetailsList);

        purchaseService.save(purchase);

        Purchase saved = purchaseService.get(purchase.getId());

        ObjectMapper objectMapper = new ObjectMapper();
        String purchaseString = objectMapper.writeValueAsString(saved);
        int hammertime = 1;

    }

}
