package com.kyrosoft.accounting.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.*;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Administrator on 2/12/2016.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SqlGroup({})
public class PurchaseServiceTestStockCheckpoint extends BaseTest  {

    @Transactional
    public Purchase createPurchaseOrder() throws ServiceException, DatabasePersistenceException {

        Purchase purchase = new Purchase();
        purchase.setNo(stringTest);
        purchase.setDate(dateTest);
        purchase.setRequestBy(employeeService.get(IdTest));
        purchase.setOrderBy(employeeService.get(IdTest));
        purchase.setSupplier(supplierService.get(IdTest));
        purchase.setRemark(stringTest);
        purchase.setPurchaseStatus(PurchaseStatus.ORDER);
        setGLEntitySave(purchase);

        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setPurchase(purchase);
        orderDetails.setItem(itemService.get(IdTest));
        orderDetails.setPurchaseMeasurement(measurementService.get(IdTest));
        orderDetails.setQuantity(20);
        orderDetails.setPrice(1000);
        orderDetails.setDiscountPercent(10);
        orderDetails.setTaxPercent(10);
        orderDetails.setOrderReceivedFull(false);
        orderDetails.setStatus(OrderStatus.ORDER);
        setGLEntitySave(orderDetails);

        List<OrderDetails> orderDetailsList = new ArrayList<OrderDetails>();
        orderDetailsList.add(orderDetails);
        purchase.setOrderDetailsList(orderDetailsList);

        purchaseService.save(purchase);
        return purchase;
    }

    @Test
    @Transactional
    @Rollback(false)
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:beforeTestRun.sql")
    public void purchaseUpdateTestAddAndUpdateOrderDetailsTest() throws ServiceException, DatabasePersistenceException, IOException {

        /**
         * Update purchase with add new order details
         */
        Purchase purchase = createPurchaseOrder();
        Purchase saved = new Purchase();
        saved.setId(purchase.getId());
        saved.setNo(stringTest);
        saved.setDate(dateTest);
        saved.setRemark(stringTest);
        List<OrderDetails> updateOrderDetailsList = new ArrayList<OrderDetails>();

        for(OrderDetails orderDetails : purchase.getOrderDetailsList()) {
            ObjectMapper objectMapper = new ObjectMapper();
            String orderDetailsString = objectMapper.writeValueAsString(orderDetails);
            OrderDetails cloneObj = objectMapper.readValue(orderDetailsString, OrderDetails.class);
            updateOrderDetailsList.add(cloneObj);
        }

        OrderDetails orderDetailsAdd = new OrderDetails();
        orderDetailsAdd.setPurchase(purchase);
        orderDetailsAdd.setItem(itemService.get(IdTest));
        orderDetailsAdd.setPurchaseMeasurement(measurementService.get(IdTest));
        orderDetailsAdd.setQuantity(10);
        orderDetailsAdd.setPrice(5000);
        orderDetailsAdd.setDiscountPercent(5);
        orderDetailsAdd.setTaxPercent(10);
        orderDetailsAdd.setOrderReceivedFull(false);
        setGLEntitySave(orderDetailsAdd);

        OrderDetails orderDetailsAdd2 = new OrderDetails();
        orderDetailsAdd2.setPurchase(purchase);
        orderDetailsAdd2.setItem(itemService.get(IdTest));
        orderDetailsAdd2.setPurchaseMeasurement(measurementService.get(IdTest));
        orderDetailsAdd2.setQuantity(10);
        orderDetailsAdd2.setPrice(5000);
        orderDetailsAdd2.setDiscountPercent(5);
        orderDetailsAdd2.setTaxPercent(10);
        orderDetailsAdd2.setOrderReceivedFull(false);
        setGLEntitySave(orderDetailsAdd2);

        updateOrderDetailsList.add(orderDetailsAdd);
        updateOrderDetailsList.add(orderDetailsAdd2);
        saved.setOrderDetailsList(updateOrderDetailsList);

        /**
         * Check the result
         */
        purchaseService.update(saved);

        Purchase update = new Purchase();
        update.setId(purchase.getId());
        update.setNo(stringTest);
        update.setDate(dateTest);
        update.setRemark(stringTest);
        updateOrderDetailsList = new ArrayList<OrderDetails>();

        for(OrderDetails orderDetails : purchase.getOrderDetailsList()) {
            ObjectMapper objectMapper = new ObjectMapper();
            String orderDetailsString = objectMapper.writeValueAsString(orderDetails);
            OrderDetails cloneObj = objectMapper.readValue(orderDetailsString, OrderDetails.class);
            updateOrderDetailsList.add(cloneObj);
        }

        updateOrderDetailsList.get(0).setQuantity(2);
        update.setOrderDetailsList(updateOrderDetailsList);

        purchaseService.update(update);
    }

    @Test
    @Transactional
    @Rollback(false)
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:empty.sql")
    public void purchaseUpdateTestAddAndUpdateOrderDetailsTest1() throws ServiceException, DatabasePersistenceException, IOException {
        Purchase purchase = purchaseService.get(IdTest);

        //Stock Balance
        StockBalance stockBalance= stockBalanceService.get(null,IdTest,StockBalanceType.QUANTITY_ON_ORDER);
        assertEquals(22,stockBalance.getBalance(),0.00);

        //Stock checkpoint
        StockCheckPoint stockCheckpoint = stockCheckpointService.get(
                stockBalance,
                StockTransactionType.PURCHASE_ORDER_CREATED,
                purchase.getOrderDetailsList().get(2).getId()
        );

        assertEquals(22,stockCheckpoint.getBalanceAfter(),0.00);
    }

}
