package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.Measurement;
import com.kyrosoft.accounting.model.MeasurementType;
import com.kyrosoft.accounting.model.dto.MeasurementSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 10/23/15.
 */
public class MeasurementServiceTest extends BaseTest {

    @Test
    public void measurementGetTest() throws ServiceException, DatabasePersistenceException {

        createMeasurementDummy();

        Measurement measurement = measurementService.get(measurementTest.getId());
        assertEquals(stringTest,measurement.getName());
        assertEquals(stringTest,measurement.getShortName());
        assertEquals(stringTest,measurement.getDescription());
        assertEquals(MeasurementType.STOCK, measurement.getMeasurementType());
        assertEquals(boolTest,measurement.getIsActive());
        assertEquals(doubleTest,measurement.getUnit());
    }

    @Test
    public void measurementSaveTest() throws ServiceException, DatabasePersistenceException {

        Measurement measurement = new Measurement();
        measurement.setName(stringTest);
        measurement.setShortName(stringTest);
        measurement.setDescription(stringTest);
        measurement.setMeasurementType(MeasurementType.STOCK);
        measurement.setUnit(doubleTest);
        measurement.setIsActive(boolTest);
        setAuditableEntitySave(measurement);

        measurementService.save(measurement);

        Measurement saved = measurementService.get(measurement.getId());
        assertEquals(stringTest,measurement.getName());
        assertEquals(stringTest,measurement.getShortName());
        assertEquals(stringTest,measurement.getDescription());
        assertEquals(MeasurementType.STOCK, measurement.getMeasurementType());
        assertEquals(boolTest,measurement.getIsActive());
        assertEquals(doubleTest,measurement.getUnit());

    }

    @Test
    public void measurementUpdateTest() throws ServiceException, DatabasePersistenceException {

        createMeasurementDummy();

        Measurement measurement = measurementService.get(measurementTest.getId());
        measurement.setName(stringTest);
        measurement.setShortName(stringTest);
        measurement.setDescription(stringTest);
        measurement.setMeasurementType(MeasurementType.STOCK);
        measurement.setUnit(doubleTest);
        measurement.setIsActive(boolTest);

        measurementService.update(measurement);

        Measurement saved = measurementService.get(measurement.getId());
        assertEquals(stringTest,measurement.getName());
        assertEquals(stringTest,measurement.getShortName());
        assertEquals(stringTest,measurement.getDescription());
        assertEquals(MeasurementType.STOCK, measurement.getMeasurementType());
        assertEquals(boolTest,measurement.getIsActive());
        assertEquals(doubleTest,measurement.getUnit());
    }

    @Test
    public void measurementDeleteTest() throws ServiceException, DatabasePersistenceException {

        createMeasurementDummy();
        measurementService.delete(measurementTest);
        Measurement measurement = measurementService.get(measurementTest.getId());
        assertNull(measurement);
    }

    @Test
    public void measurementSearchTest() throws ServiceException, DatabasePersistenceException {
        createMeasurementDummy();

        MeasurementSearchCriteria criteria = new MeasurementSearchCriteria();
        criteria.setName(stringTest);
        criteria.setShortName(stringTest);
        criteria.setType(MeasurementType.STOCK);
        criteria.setIsActive(boolTest);
        criteria.setPageNumber(1);
        criteria.setPageSize(10);

        SearchResult<Measurement> searchResult = measurementService.search(criteria);
        assertEquals(2,searchResult.getTotal());
    }

}
