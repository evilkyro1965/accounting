package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.Department;
import com.kyrosoft.accounting.model.Employee;
import com.kyrosoft.accounting.model.dto.DepartmentSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import com.kyrosoft.accounting.model.dto.SortType;
import org.junit.Test;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 10/21/15.
 */
public class DepartmentServiceTest extends BaseTest {

    @Test
    public void getDepartmentTest() throws ServiceException, DatabasePersistenceException {
        Department department = new Department();
        department.setName(stringTest);
        department.setEmail(stringTest);
        department.setIsActive(boolTest);
        setAuditableEntitySave(department);

        departmentService.save(department);

        Department saved = departmentService.get(department.getId());
        assertNotNull(saved);
        assertEquals(stringTest,saved.getName());
        assertEquals(stringTest,saved.getEmail());
        assertEquals(boolTest,saved.getIsActive());
        testAuditableEntitySave(saved);
    }


    @Test
    public void saveDepartmentTest() throws ServiceException, DatabasePersistenceException {
        Department department = new Department();
        department.setName(stringTest);
        department.setEmail(stringTest);
        department.setIsActive(boolTest);
        setAuditableEntitySave(department);

        departmentService.save(department);

        Department saved = departmentService.get(department.getId());
        assertNotNull(saved);
        assertEquals(stringTest,saved.getName());
        assertEquals(stringTest,saved.getEmail());
        assertEquals(boolTest, saved.getIsActive());
        testAuditableEntitySave(saved);
    }

    @Test
    public void updateDepartmentTest() throws ServiceException, DatabasePersistenceException {
        Department department = new Department();
        department.setName(stringTest);
        department.setEmail(stringTest);
        department.setIsActive(boolTest);
        setAuditableEntitySave(department);

        departmentService.save(department);

        Department saved = departmentService.get(department.getId());
        saved.setName(stringTest);
        saved.setEmail(stringTest);
        saved.setIsActive(boolTest);
        setAuditableEntityUpdate(saved);

        Department updated = departmentService.update(saved);

        assertNotNull(updated);
        assertEquals(stringTest,updated.getName());
        assertEquals(stringTest,updated.getEmail());
        assertEquals(boolTest,updated.getIsActive());
        testAuditableEntityUpdate(saved);
    }

    @Test
    public void deleteDepartmentTest() throws ServiceException, DatabasePersistenceException {
        Department department = new Department();
        department.setName(stringTest);
        department.setEmail(stringTest);
        department.setIsActive(boolTest);
        setAuditableEntitySave(department);

        departmentService.save(department);

        departmentService.delete(department);

        Department saved = departmentService.get(department.getId());

        assertNull(saved);
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:SearchDataDummies.sql")
    })
    public void searchDepartmentTest() throws ServiceException, DatabasePersistenceException {

        DepartmentSearchCriteria criteria = new DepartmentSearchCriteria();
        criteria.setName(stringTest);
        criteria.setEmail("test@test.com");
        criteria.setIsActive(true);
        criteria.setPageNumber(1);
        criteria.setPageSize(10);

        SearchResult<Department> searchResult = departmentService.search(criteria);
        assertEquals(1,searchResult.getTotal());
    }

}
