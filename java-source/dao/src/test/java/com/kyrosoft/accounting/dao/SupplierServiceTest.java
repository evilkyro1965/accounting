package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.ItemCategory;
import com.kyrosoft.accounting.model.Supplier;
import com.kyrosoft.accounting.model.dto.SearchResult;
import com.kyrosoft.accounting.model.dto.SupplierSearchCriteria;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by Administrator on 10/22/15.
 */
public class SupplierServiceTest extends BaseTest {

    @Test
    public void supplierGetTest() throws ServiceException, DatabasePersistenceException {

        createSupplierDummy();

        Supplier saved = supplierService.get(supplierTest.getId());
        assertEquals(stringTest,saved.getName());
        assertEquals(stringTest,saved.getContactPerson());
        assertEquals(stringTest,saved.getAddress());
        assertEquals(stringTest,saved.getSecondaryAddress());
        assertEquals(stringTest,saved.getCity());
        assertEquals(stringTest,saved.getPostalCode());
        assertEquals(stringTest,saved.getState());
        assertEquals(stringTest,saved.getCountry());
        assertEquals(stringTest,saved.getPhone());
        assertEquals(stringTest,saved.getFax());
        assertEquals(stringTest,saved.getEmail());
        assertEquals(boolTest,saved.getIsActive());
        testAuditableEntitySave(saved);
    }

    @Test
     public void supplierSaveTest() throws ServiceException, DatabasePersistenceException {

        Supplier supplier = new Supplier();
        supplier.setName(stringTest);
        supplier.setContactPerson(stringTest);
        supplier.setAddress(stringTest);
        supplier.setSecondaryAddress(stringTest);
        supplier.setCity(stringTest);
        supplier.setPostalCode(stringTest);
        supplier.setState(stringTest);
        supplier.setCountry(stringTest);
        supplier.setPhone(stringTest);
        supplier.setFax(stringTest);
        supplier.setEmail(stringTest);
        supplier.setIsActive(boolTest);
        setAuditableEntitySave(supplier);

        supplierService.save(supplier);

        Supplier saved = supplierService.get(supplier.getId());
        assertEquals(stringTest,saved.getName());
        assertEquals(stringTest,saved.getContactPerson());
        assertEquals(stringTest,saved.getAddress());
        assertEquals(stringTest,saved.getSecondaryAddress());
        assertEquals(stringTest,saved.getCity());
        assertEquals(stringTest,saved.getPostalCode());
        assertEquals(stringTest,saved.getState());
        assertEquals(stringTest,saved.getCountry());
        assertEquals(stringTest,saved.getPhone());
        assertEquals(stringTest,saved.getFax());
        assertEquals(stringTest,saved.getEmail());
        assertEquals(boolTest,saved.getIsActive());
        testAuditableEntitySave(saved);
    }

    @Test
    public void supplierUpdateTest() throws ServiceException, DatabasePersistenceException {

        createSupplierDummy();

        Supplier supplier = supplierService.get(supplierTest.getId());
        supplier.setName(stringTest);
        supplier.setContactPerson(stringTest);
        supplier.setAddress(stringTest);
        supplier.setSecondaryAddress(stringTest);
        supplier.setCity(stringTest);
        supplier.setPostalCode(stringTest);
        supplier.setState(stringTest);
        supplier.setCountry(stringTest);
        supplier.setPhone(stringTest);
        supplier.setFax(stringTest);
        supplier.setEmail(stringTest);
        supplier.setIsActive(boolTest);

        Supplier saved = supplierService.update(supplier);
        assertEquals(stringTest,saved.getName());
        assertEquals(stringTest,saved.getContactPerson());
        assertEquals(stringTest,saved.getAddress());
        assertEquals(stringTest,saved.getSecondaryAddress());
        assertEquals(stringTest,saved.getCity());
        assertEquals(stringTest,saved.getPostalCode());
        assertEquals(stringTest,saved.getState());
        assertEquals(stringTest,saved.getCountry());
        assertEquals(stringTest,saved.getPhone());
        assertEquals(stringTest,saved.getFax());
        assertEquals(stringTest,saved.getEmail());
        assertEquals(boolTest,saved.getIsActive());
    }

    @Test
    public void supplierDeleteTest() throws ServiceException, DatabasePersistenceException {

        createSupplierDummy();
        supplierService.delete(supplierTest);
        Supplier deleted = supplierService.get(supplierTest.getId());
        assertNull(deleted);
    }

    @Test
    public void supplierSearchTest() throws ServiceException, DatabasePersistenceException {
        createSupplierDummy();

        SupplierSearchCriteria criteria = new SupplierSearchCriteria();
        criteria.setName(stringTest);
        criteria.setContactPerson(stringTest);
        criteria.setAddress(stringTest);
        criteria.setPhone(stringTest);
        criteria.setEmail(stringTest);
        criteria.setIsActive(boolTest);

        SearchResult<Supplier> searchResult = supplierService.search(criteria);
        assertEquals(2,searchResult.getTotal());

    }

}
