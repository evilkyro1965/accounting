package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManagerFactory;

import static org.junit.Assert.assertEquals;

import java.util.*;

/**
 * Created by Administrator on 10/14/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:TestContext.xml")
@SqlGroup({
        @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:beforeTestRun.sql")
})
public abstract class BaseTest {

    protected final String stringTest = "test";

    protected final Long longTest = 1L;

    protected final Double doubleTest = 1.0;

    protected final Boolean boolTest = true;

    protected final Date dateTest = new Date(2015,1,1);

    protected final Long IdTest = 1L;

    @Autowired
    DepartmentService departmentService;

    @Autowired
    EmployeeService employeeService;

    @Autowired WarehouseService warehouseService;

    @Autowired ItemCategoryService itemCategoryService;

    @Autowired SupplierService supplierService;

    @Autowired MeasurementService measurementService;

    @Autowired ItemService itemService;

    @Autowired PurchaseService purchaseService;

    @Autowired OrderReceivedService orderReceivedService;

    @Autowired StockBalanceService stockBalanceService;

    @Autowired StockCheckpointService stockCheckpointService;

    @Autowired
    OrderDetailsQuantityTrackerService quantityTrackerService;

    @Autowired
    OrderReturnService orderReturnService;

    @Autowired CustomerService customerService;

    @Autowired SalesService salesService;

    @Autowired SaveHelper saveHelper;

    @Autowired
    EntityManagerFactory emf;

    protected Department departmentTest;

    protected Employee employeeTest;

    protected Warehouse warehouseTest;

    protected ItemCategory itemCategoryTest;

    protected Supplier supplierTest;

    protected Measurement measurementTest;

    protected Item itemTest;

    protected void setAuditableEntitySave(AuditableEntity auditableEntity) {
        auditableEntity.setCreatedBy(stringTest);
        auditableEntity.setCreatedDate(dateTest);
    }

    protected void setAuditableEntityUpdate(AuditableEntity auditableEntity) {
        auditableEntity.setUpdatedBy(stringTest);
        auditableEntity.setUpdatedDate(dateTest);
    }

    protected void setGLEntitySave(GLEntity glEntity) {
        glEntity.setCreatedBy(stringTest);
        glEntity.setCreatedDate(dateTest);
    }

    protected void setGLEntityUpdate(GLEntity glEntity) {
        glEntity.setUpdatedBy(stringTest);
        glEntity.setUpdatedDate(dateTest);
    }

    protected void testAuditableEntitySave(AuditableEntity auditableEntity) {
        assertEquals(stringTest,auditableEntity.getCreatedBy());
        assertEquals(dateTest,auditableEntity.getCreatedDate());
    }

    protected void testAuditableEntityUpdate(AuditableEntity auditableEntity) {
        assertEquals(stringTest,auditableEntity.getUpdatedBy());
        assertEquals(dateTest,auditableEntity.getUpdatedDate());
    }

    protected <T> void save(T entity) throws ServiceException, DatabasePersistenceException {
        /*
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(entity);
        entityManager.getTransaction().commit();
        */

        saveHelper.save(entity);
    }

    protected Department createDepartmentDummy() throws ServiceException, DatabasePersistenceException {
        if(departmentTest==null){
            Department department = new Department();
            department.setName(stringTest);
            department.setEmail(stringTest);
            department.setIsActive(boolTest);
            setAuditableEntitySave(department);

            save(department);
            departmentTest = department;
        }
        return departmentTest;
    }

    protected Employee createEmployeeDummy() throws ServiceException, DatabasePersistenceException {

        if(employeeTest==null) {
            Employee employee = new Employee();
            employee.setName(stringTest);
            employee.setUsername(stringTest);
            employee.setEmail(stringTest);
            employee.setMobile(stringTest);
            employee.setIsActive(boolTest);
            employee.setDepartment(createDepartmentDummy());
            setAuditableEntitySave(employee);

            save(employee);
            employeeTest = employee;
        }
        return employeeTest;
    }

    protected Warehouse createWarehouseDummy() throws ServiceException, DatabasePersistenceException {

        if(warehouseTest==null) {
            Warehouse warehouse = new Warehouse();
            warehouse.setName(stringTest);
            warehouse.setDepartment(createDepartmentDummy());
            warehouse.setLabel(stringTest);
            warehouse.setShelf(stringTest);
            warehouse.setDescription(stringTest);
            warehouse.setIsActive(boolTest);
            setAuditableEntitySave(warehouse);

            save(warehouse);
            warehouseTest = warehouse;
        }
        return warehouseTest;
    }

    protected ItemCategory createItemCategoryDummy() throws ServiceException, DatabasePersistenceException{

        if(itemCategoryTest==null) {

            ItemCategory itemCategory = new ItemCategory();
            itemCategory.setName(stringTest);
            itemCategory.setIsActive(boolTest);
            setAuditableEntitySave(itemCategory);

            save(itemCategory);

            itemCategoryTest = itemCategory;
        }
        return itemCategoryTest;
    }

    protected Supplier createSupplierDummy() throws ServiceException, DatabasePersistenceException{

        if(supplierTest==null) {

            Supplier supplier = new Supplier();
            supplier.setName(stringTest);
            supplier.setContactPerson(stringTest);
            supplier.setAddress(stringTest);
            supplier.setSecondaryAddress(stringTest);
            supplier.setCity(stringTest);
            supplier.setPostalCode(stringTest);
            supplier.setState(stringTest);
            supplier.setCountry(stringTest);
            supplier.setPhone(stringTest);
            supplier.setFax(stringTest);
            supplier.setEmail(stringTest);
            supplier.setIsActive(boolTest);
            setAuditableEntitySave(supplier);

            save(supplier);

            supplierTest = supplier;
        }
        return supplierTest;
    }

    protected Measurement createMeasurementDummy() throws ServiceException, DatabasePersistenceException{

        if(measurementTest==null){

            Measurement measurement = new Measurement();
            measurement.setName(stringTest);
            measurement.setShortName(stringTest);
            measurement.setDescription(stringTest);
            measurement.setMeasurementType(MeasurementType.STOCK);
            measurement.setUnit(doubleTest);
            measurement.setIsActive(boolTest);
            setAuditableEntitySave(measurement);

            save(measurement);

            measurementTest = measurement;
        }

        return measurementTest;
    }

    protected Item createItemDummy() throws ServiceException, DatabasePersistenceException{

        if(itemTest==null){

            Item item = new Item();
            item.setName(stringTest);
            item.setItemCode(stringTest);
            item.setItemType(ItemType.STOCKED_PRODUCT);
            item.setCategory(createItemCategoryDummy());
            item.setItemDescription(stringTest);
            item.setItemPrice(doubleTest);
            item.setStockMeasurement(createMeasurementDummy());

            Set<Measurement> purchaseMeasurement = new HashSet<Measurement>();
            purchaseMeasurement.add(createMeasurementDummy());

            item.setPurchaseMeasurements(purchaseMeasurement);
            item.setIsActive(boolTest);
            setAuditableEntitySave(item);

            save(item);

            itemTest = item;
        }
        return itemTest;
    }
}
