package com.kyrosoft.accounting.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.*;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Administrator on 2/15/2016.
 */
public class SalesServiceTest extends BaseTest  {

    @Test
    @Transactional
    public void salesSaveTest() throws ServiceException, DatabasePersistenceException {

        Sales sales = new Sales();
        sales.setNo(stringTest);
        sales.setDate(dateTest);
        sales.setSalesRep(employeeService.get(IdTest));
        sales.setCustomer(customerService.get(IdTest));
        sales.setRemark(stringTest);
        setGLEntitySave(sales);

        SalesOrderDetails salesOrderDetails = new SalesOrderDetails();
        salesOrderDetails.setSales(sales);
        salesOrderDetails.setItem(itemService.get(IdTest));
        salesOrderDetails.setSalesMeasurement(measurementService.get(IdTest));
        salesOrderDetails.setQuantity(10);
        salesOrderDetails.setPrice(1000);
        salesOrderDetails.setDiscountPercent(10);
        salesOrderDetails.setTaxPercent(10);
        setGLEntitySave(salesOrderDetails);

        List<SalesOrderDetails> salesOrderDetailsList = new ArrayList<SalesOrderDetails>();
        salesOrderDetailsList.add(salesOrderDetails);
        sales.setSalesOrderDetailsList(salesOrderDetailsList);

        salesService.save(sales);

    }

    @Transactional
    public Sales createSalesOrder() throws ServiceException, DatabasePersistenceException {


        Sales sales = new Sales();
        sales.setNo(stringTest);
        sales.setDate(dateTest);
        sales.setSalesRep(employeeService.get(IdTest));
        sales.setCustomer(customerService.get(IdTest));
        sales.setRemark(stringTest);
        setGLEntitySave(sales);

        SalesOrderDetails salesOrderDetails = new SalesOrderDetails();
        salesOrderDetails.setSales(sales);
        salesOrderDetails.setItem(itemService.get(IdTest));
        salesOrderDetails.setSalesMeasurement(measurementService.get(IdTest));
        salesOrderDetails.setQuantity(10);
        salesOrderDetails.setPrice(1000);
        salesOrderDetails.setDiscountPercent(10);
        salesOrderDetails.setTaxPercent(10);
        setGLEntitySave(salesOrderDetails);

        List<SalesOrderDetails> salesOrderDetailsList = new ArrayList<SalesOrderDetails>();
        salesOrderDetailsList.add(salesOrderDetails);
        sales.setSalesOrderDetailsList(salesOrderDetailsList);

        salesService.save(sales);

        return sales;
    }

    @Test
    @Transactional
    public void purchaseUpdateUpdateOrderDetailsTest() throws ServiceException, DatabasePersistenceException, IOException {

        /**
         * Update purchase with add new order details
         */
        Sales sales = createSalesOrder();
        Sales saved = new Sales();
        saved.setId(sales.getId());
        saved.setNo(stringTest);
        saved.setDate(dateTest);
        saved.setRemark(stringTest);
        List<SalesOrderDetails> updateOrderDetailsList = new ArrayList<SalesOrderDetails>();

        for(SalesOrderDetails orderDetails : sales.getSalesOrderDetailsList()) {
            ObjectMapper objectMapper = new ObjectMapper();
            String orderDetailsString = objectMapper.writeValueAsString(orderDetails);
            SalesOrderDetails cloneObj = objectMapper.readValue(orderDetailsString, SalesOrderDetails.class);
            cloneObj.setQuantity(100);
            updateOrderDetailsList.add(cloneObj);
        }

        saved.setSalesOrderDetailsList(updateOrderDetailsList);

        /**
         * Check the result
         */
        salesService.update(saved);

    }

}
