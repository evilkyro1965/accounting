package com.kyrosoft.accounting.dao;


import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.Employee;
import com.kyrosoft.accounting.model.Warehouse;
import com.kyrosoft.accounting.model.dto.SearchResult;
import com.kyrosoft.accounting.model.dto.WarehouseSearchCriteria;
import org.junit.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 10/22/15.
 */
public class WarehouseServiceTest extends BaseTest {

    @Test
    public void warehouseGetTest() throws ServiceException, DatabasePersistenceException {

        createWarehouseDummy();

        Warehouse saved = warehouseService.get(warehouseTest.getId());
        assertEquals(stringTest,saved.getName());
        assertEquals(departmentTest.getId(),saved.getDepartment().getId());
        assertEquals(stringTest,saved.getDescription());
        assertEquals(stringTest,saved.getShelf());
        assertEquals(stringTest,saved.getLabel());
        assertEquals(boolTest,saved.getIsActive());
        testAuditableEntitySave(saved);

    }

    @Test
    public void warehouseSaveTest() throws ServiceException, DatabasePersistenceException {

        Warehouse warehouse = new Warehouse();
        warehouse.setName(stringTest);
        warehouse.setDepartment(createDepartmentDummy());
        warehouse.setLabel(stringTest);
        warehouse.setShelf(stringTest);
        warehouse.setDescription(stringTest);
        warehouse.setIsActive(boolTest);
        setAuditableEntitySave(warehouse);

        warehouseService.save(warehouse);

        Warehouse saved = warehouseService.get(warehouse.getId());
        assertEquals(stringTest,saved.getName());
        assertEquals(departmentTest.getId(),saved.getDepartment().getId());
        assertEquals(stringTest,saved.getDescription());
        assertEquals(stringTest,saved.getShelf());
        assertEquals(stringTest,saved.getLabel());
        assertEquals(boolTest,saved.getIsActive());
        testAuditableEntitySave(saved);
    }

    @Test
    public void warehouseUpdateTest() throws ServiceException, DatabasePersistenceException {

        Warehouse warehouse = createWarehouseDummy();
        warehouse.setName(stringTest);
        warehouse.setDepartment(createDepartmentDummy());
        warehouse.setLabel(stringTest);
        warehouse.setShelf(stringTest);
        warehouse.setDescription(stringTest);
        warehouse.setIsActive(boolTest);

        Warehouse updated = warehouseService.update(warehouse);

        assertEquals(stringTest,updated.getName());
        assertEquals(departmentTest.getId(),updated.getDepartment().getId());
        assertEquals(stringTest,updated.getDescription());
        assertEquals(stringTest,updated.getShelf());
        assertEquals(stringTest,updated.getLabel());
        assertEquals(boolTest,updated.getIsActive());

    }

    @Test
    public void warehouseDeleteTest() throws ServiceException, DatabasePersistenceException {

        Warehouse warehouse = createWarehouseDummy();
        warehouseService.delete(warehouse);

        Warehouse deleted = warehouseService.get(warehouse.getId());
        assertNull(deleted);
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:SearchDataDummies.sql")
    })
    public void warehouseSearchTest() throws ServiceException, DatabasePersistenceException {

        WarehouseSearchCriteria criteria = new WarehouseSearchCriteria();
        criteria.setName(stringTest);

        criteria.setShelfName(stringTest);
        criteria.setLabelName(stringTest);
        criteria.setDepartment(departmentService.get(longTest));
        criteria.setPageNumber(1);
        criteria.setPageSize(10);

        SearchResult<Warehouse> searchResult = warehouseService.search(criteria);

        assertEquals(3,searchResult.getTotal());
    }


}
