package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.ActiveEntityService;
import com.kyrosoft.accounting.model.ActiveEntity;
import com.kyrosoft.accounting.model.ItemCategory;
import com.kyrosoft.accounting.model.dto.ActiveEntitySearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;

import javax.persistence.Query;

/**
 * Created by Administrator on 10/22/15.
 */
public class ActiveEntityServiceImpl extends BaseServiceImpl<ActiveEntity,ActiveEntitySearchCriteria>
    implements ActiveEntityService {

    public ActiveEntityServiceImpl(Class<ActiveEntity> entityClass) {
        super(entityClass);
    }

    public ActiveEntity get(long id) {
        ItemCategory itemCategory = entityManager.find(ItemCategory.class,id);
        return itemCategory;
    }

    public ActiveEntity save(ActiveEntity lookupEntity) throws ServiceException, DatabasePersistenceException {
        return null;
    }

    public ActiveEntity update(ActiveEntity lookupEntity) throws ServiceException, DatabasePersistenceException {
        return null;
    }

    public void delete(ActiveEntity lookupEntity) throws ServiceException, DatabasePersistenceException {

    }

    public SearchResult<ActiveEntity> search(ActiveEntity criteria) throws ServiceException {
        return null;
    }

    protected String generateWhereClause(ActiveEntitySearchCriteria criteria) throws ServiceException {
        return null;
    }

    protected void populateQueryParameters(Query query, ActiveEntitySearchCriteria criteria) throws ServiceException {

    }
}
