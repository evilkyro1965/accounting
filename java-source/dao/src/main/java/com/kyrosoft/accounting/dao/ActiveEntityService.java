package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.ActiveEntity;
import com.kyrosoft.accounting.model.dto.ActiveEntitySearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;

/**
 * Created by Administrator on 10/22/15.
 */
public interface ActiveEntityService {

    public ActiveEntity get(long id);

    public ActiveEntity save(ActiveEntity lookupEntity) throws ServiceException, DatabasePersistenceException;

    public ActiveEntity update(ActiveEntity lookupEntity) throws ServiceException, DatabasePersistenceException;

    public void delete(ActiveEntity lookupEntity) throws ServiceException, DatabasePersistenceException;

    public SearchResult<ActiveEntity> search(ActiveEntity criteria) throws ServiceException;
}
