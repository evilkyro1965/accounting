package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.Item;
import com.kyrosoft.accounting.model.dto.ActiveEntitySearchCriteria;
import com.kyrosoft.accounting.model.dto.ItemSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;

/**
 * Created by Administrator on 10/23/15.
 */
public interface ItemService {

    public Item get(long id);

    public Item save(Item item) throws ServiceException, DatabasePersistenceException;

    public Item update(Item item) throws ServiceException, DatabasePersistenceException;

    public void delete(Item item) throws ServiceException, DatabasePersistenceException;

    public SearchResult<Item> search(ItemSearchCriteria criteria) throws ServiceException;
}
