package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.OrderDetails;
import com.kyrosoft.accounting.model.StockBalance;
import com.kyrosoft.accounting.model.StockCheckPoint;

/**
 * Created by Administrator on 2/9/2016.
 */
public interface OrderDetailsQuantityTrackerService {

    public void createStockCheckpointForCreateOrderDetails(
            OrderDetails orderDetails
    ) throws ServiceException, DatabasePersistenceException;

    public void createStockCheckpointForUpdateOrderDetails(
            OrderDetails beforeorderDetails,
            OrderDetails orderDetails
    ) throws ServiceException, DatabasePersistenceException;

    public void deleteStockCheckpointForCreateOrderDetails(
            OrderDetails orderDetails
    ) throws ServiceException, DatabasePersistenceException;

}
