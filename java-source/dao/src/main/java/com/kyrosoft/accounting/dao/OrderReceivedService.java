package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.OrderReceived;

/**
 * Created by Administrator on 2/5/2016.
 */
public interface OrderReceivedService {

    public OrderReceived get(long id);

    public OrderReceived save(OrderReceived orderReceived) throws ServiceException, DatabasePersistenceException;

    public OrderReceived update(OrderReceived orderReceived) throws ServiceException, DatabasePersistenceException;

    public void delete(OrderReceived orderReceived) throws ServiceException, DatabasePersistenceException;

}
