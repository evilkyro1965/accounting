package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.StockBalance;
import com.kyrosoft.accounting.model.StockBalanceType;
import com.kyrosoft.accounting.model.StockCheckPoint;
import com.kyrosoft.accounting.model.StockTransactionType;

/**
 * Created by Administrator on 2/9/2016.
 */
public interface StockCheckpointService {

    public StockCheckPoint get(long id);

    public StockCheckPoint get(StockBalance stockBalance,
                               StockTransactionType stockTransactionType,
                               Long stockTransactionId)
            throws ServiceException, DatabasePersistenceException;

    public StockCheckPoint save(StockCheckPoint stockCheckPoint)
            throws ServiceException, DatabasePersistenceException;

    public StockCheckPoint update(StockCheckPoint stockCheckPoint)
            throws ServiceException, DatabasePersistenceException;

    public void delete(StockCheckPoint stockCheckPoint)
            throws ServiceException, DatabasePersistenceException;
}
