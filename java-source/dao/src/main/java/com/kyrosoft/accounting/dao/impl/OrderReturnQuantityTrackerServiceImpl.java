package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.OrderReturnQuantityTrackerService;
import com.kyrosoft.accounting.model.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by Administrator on 2/14/2016.
 */
@Service
@Transactional
public class OrderReturnQuantityTrackerServiceImpl
    extends BaseQuantityTracker
    implements OrderReturnQuantityTrackerService {

    public void createStockCheckpointForCreateOrderReturn(OrderReturn orderReturn) throws ServiceException, DatabasePersistenceException {
        OrderDetails orderDetails = orderReturn.getOrderDetails();

        Long warehouseId = orderReturn.getWarehouse().getId();
        Long itemId = orderReturn.getOrderDetails().getItem().getId();
        StockBalanceType stockBalanceTypeOnHand = StockBalanceType.QUANTITY_ON_HAND;
        StockBalanceType stockBalanceTypeOnReturn = StockBalanceType.QUANTITY_RETURN;
        StockTransactionType stockTransactionType = StockTransactionType.PURCHASE_ORDER_RETURN;
        Date transactionDate = orderReturn.getReturnDate();
        Long transactionId = orderReturn.getId();

        double quantity = orderReturn.getQuantity() * orderDetails.getPurchaseMeasurement().getUnit();

        StockCheckPoint stockCheckPointOnReturn = createStockCheckpointDebitStockBalance(
                warehouseId,
                itemId,
                stockBalanceTypeOnReturn,
                stockTransactionType,
                transactionDate,
                transactionId,
                quantity
        );

        StockCheckPoint stockCheckPointOnHand = createStockCheckpointCreditStockBalance(
                warehouseId,
                itemId,
                stockBalanceTypeOnHand,
                stockTransactionType,
                transactionDate,
                transactionId,
                quantity
        );

    }

    public void updateStockCheckpointForUpdateOrderReturn(OrderReturn orderReturnBefore, OrderReturn orderReturn) throws ServiceException, DatabasePersistenceException {
        OrderDetails orderDetails = orderReturn.getOrderDetails();

        Long warehouseId = orderReturn.getWarehouse().getId();
        Long beforeWarehouseId = orderReturnBefore.getWarehouse().getId();

        Long itemId = orderReturn.getOrderDetails().getItem().getId();
        StockBalanceType stockBalanceTypeOnHand = StockBalanceType.QUANTITY_ON_HAND;
        StockBalanceType stockBalanceTypeOnReturn = StockBalanceType.QUANTITY_RETURN;
        StockTransactionType stockTransactionType = StockTransactionType.PURCHASE_ORDER_RETURN;
        Date transactionDate = orderReturn.getReturnDate();
        Long transactionId = orderReturn.getId();

        if(beforeWarehouseId==warehouseId) {
            double quantity = orderReturn.getQuantity() * orderDetails.getPurchaseMeasurement().getUnit();

            //Update stock checkpoint & balance on return
            updateStockCheckpointDebitStockBalance(
                    warehouseId,
                    itemId,
                    stockBalanceTypeOnReturn,
                    stockTransactionType,
                    transactionDate,
                    transactionId,
                    quantity
            );

            //Update stock checkpoint & balance on hand
            updateStockCheckpointCreditStockBalance(
                    warehouseId,
                    itemId,
                    stockBalanceTypeOnHand,
                    stockTransactionType,
                    transactionDate,
                    transactionId,
                    quantity
            );
        }
        else {
            /**
             * When warehouse id is changed, do:
             *  1. Delete the existing checkpoint
             *  2. Update the balance to amount before checkpoint
             */
            removeStockCheckpointStockBalance(
                    beforeWarehouseId,
                    itemId,
                    stockBalanceTypeOnHand,
                    stockTransactionType,
                    transactionDate,
                    transactionId
            );

            removeStockCheckpointStockBalance(
                    beforeWarehouseId,
                    itemId,
                    stockBalanceTypeOnReturn,
                    stockTransactionType,
                    transactionDate,
                    transactionId
            );

            /**
             * And Then
             */
            double quantity = orderReturn.getQuantity() * orderDetails.getPurchaseMeasurement().getUnit();

            StockCheckPoint stockCheckPointOnReturn = createStockCheckpointDebitStockBalance(
                    warehouseId,
                    itemId,
                    stockBalanceTypeOnReturn,
                    stockTransactionType,
                    transactionDate,
                    transactionId,
                    quantity
            );

            StockCheckPoint stockCheckPointOnHand = createStockCheckpointCreditStockBalance(
                    warehouseId,
                    itemId,
                    stockBalanceTypeOnHand,
                    stockTransactionType,
                    transactionDate,
                    transactionId,
                    quantity
            );

        }

    }

}
