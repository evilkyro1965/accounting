package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.ItemCategory;
import com.kyrosoft.accounting.model.Supplier;
import com.kyrosoft.accounting.model.dto.ActiveEntitySearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import com.kyrosoft.accounting.model.dto.SupplierSearchCriteria;

/**
 * Created by Administrator on 10/22/15.
 */
public interface SupplierService {

    public Supplier get(long id);

    public Supplier save(Supplier supplier) throws ServiceException, DatabasePersistenceException;

    public Supplier update(Supplier supplier) throws ServiceException, DatabasePersistenceException;

    public void delete(Supplier supplier) throws ServiceException, DatabasePersistenceException;

    public SearchResult<Supplier> search(SupplierSearchCriteria criteria) throws ServiceException;

}
