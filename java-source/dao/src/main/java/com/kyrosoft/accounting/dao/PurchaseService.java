package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.OrderReceived;
import com.kyrosoft.accounting.model.Purchase;

import javax.persistence.EntityManager;

/**
 * Created by Administrator on 12/22/15.
 */
public interface PurchaseService {

    public Purchase get(long id);

    public Purchase save(Purchase purchase) throws ServiceException, DatabasePersistenceException;

    public Purchase update(Purchase purchase) throws ServiceException, DatabasePersistenceException;

    public void delete(Purchase purchase) throws ServiceException, DatabasePersistenceException;

}
