package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.OrderReturn;

/**
 * Created by Administrator on 2/14/2016.
 */
public interface OrderReturnQuantityTrackerService {

    public void createStockCheckpointForCreateOrderReturn (
            OrderReturn orderReturn
    ) throws ServiceException, DatabasePersistenceException;

    public void updateStockCheckpointForUpdateOrderReturn (
            OrderReturn orderReturnBefore,
            OrderReturn orderReturn
    ) throws ServiceException, DatabasePersistenceException;

}
