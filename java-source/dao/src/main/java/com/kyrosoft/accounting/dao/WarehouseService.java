package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.Warehouse;
import com.kyrosoft.accounting.model.dto.EmployeeSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import com.kyrosoft.accounting.model.dto.WarehouseSearchCriteria;

/**
 * Created by Administrator on 10/22/15.
 */
public interface WarehouseService {

    public Warehouse get(long id);

    public Warehouse save(Warehouse warehouse) throws ServiceException, DatabasePersistenceException;

    public Warehouse update(Warehouse warehouse) throws ServiceException, DatabasePersistenceException;

    public void delete(Warehouse warehouse) throws ServiceException, DatabasePersistenceException;

    public SearchResult<Warehouse> search(WarehouseSearchCriteria criteria) throws ServiceException;
}
