package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.Department;
import com.kyrosoft.accounting.model.IdentifiableEntity;
import com.kyrosoft.accounting.model.dto.BaseSearchParameters;
import com.kyrosoft.accounting.model.dto.DepartmentSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;

/**
 * DepartmentService
 *
 * @author fahrur
 * @version 1.0
 */
public interface DepartmentService {

    /**
     * Get the department
     * @param id the id
     * @return the department
     */
    public Department get(long id);

    /**
     * Save department
     * @param department the department
     * @return the department
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public Department save(Department department) throws ServiceException, DatabasePersistenceException;

    /**
     * Update department
     * @param department the department
     * @return the department
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public Department update(Department department) throws ServiceException, DatabasePersistenceException;

    /**
     * Delete department
     * @param department the department
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public void delete(Department department) throws ServiceException, DatabasePersistenceException;

    /**
     * This method is used to search entities based on the search criteria.
     *
     * @param criteria
     *            the search criteria
     * @return the search result
     * @throws IllegalArgumentException
     *             if criteria is null; criteria.pageNumber < 0; criteria.pageSize < 0; criteria.pageNumber > 0 and
     *             criteria.pageSize <= 0; the criteria has some fields which are ids and they are not positive
     * @throws ServiceException
     *             if any other error occurred during the operation
     */
    public SearchResult<Department> search(DepartmentSearchCriteria criteria) throws ServiceException;
}
