package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.OrderReceived;
import com.kyrosoft.accounting.model.StockCheckPoint;

/**
 * Created by Administrator on 2/11/2016.
 */
public interface OrderReceivedQuantityTrackerService {

    public void createStockCheckpointForCreateOrderReceived (
            OrderReceived orderReceived
    ) throws ServiceException, DatabasePersistenceException;

    public void updateStockCheckpointForUpdateOrderReceived (
            OrderReceived orderReceivedBefore,
            OrderReceived orderReceived
    ) throws ServiceException, DatabasePersistenceException;
}
