package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.OrderReturn;

/**
 * Created by Administrator on 2/13/2016.
 */
public interface OrderReturnService {

    public OrderReturn get(long id);

    public OrderReturn save(OrderReturn orderReturn) throws ServiceException, DatabasePersistenceException;

    public OrderReturn update(OrderReturn orderReturn) throws ServiceException, DatabasePersistenceException;

    public void delete(OrderReturn orderReturn) throws ServiceException, DatabasePersistenceException;
}
