package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.StockBalance;
import com.kyrosoft.accounting.model.StockBalanceType;

/**
 * Created by Administrator on 2/9/2016.
 */
public interface StockBalanceService {

    public StockBalance get(long id);

    public StockBalance get(Long warehouseId, Long itemId, StockBalanceType stockBalanceType)
            throws ServiceException, DatabasePersistenceException;

    public StockBalance save(StockBalance stockBalance)
            throws ServiceException, DatabasePersistenceException;

    public StockBalance update(StockBalance stockBalance)
            throws ServiceException, DatabasePersistenceException;

    public void delete(StockBalance stockBalance)
            throws ServiceException, DatabasePersistenceException;

}
