package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.OrderReturnQuantityTrackerService;
import com.kyrosoft.accounting.dao.OrderReturnService;
import com.kyrosoft.accounting.model.*;
import com.kyrosoft.accounting.model.dto.ActiveEntitySearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;

/**
 * Created by Administrator on 2/13/2016.
 */
@Service
@Transactional
public class OrderReturnServiceImpl extends BaseServiceImpl<OrderReturn,ActiveEntitySearchCriteria>
    implements OrderReturnService {

    @Autowired
    OrderReturnQuantityTrackerService orderReturnQuantityTrackerService;

    public OrderReturnServiceImpl() {
        super(OrderReturn.class);
    }

    @Override
    protected String generateWhereClause(ActiveEntitySearchCriteria criteria) throws ServiceException {
        return null;
    }

    @Override
    protected void populateQueryParameters(Query query, ActiveEntitySearchCriteria criteria) throws ServiceException {

    }

    public OrderReturn get(long id) {
        return null;
    }

    public OrderReturn save(OrderReturn orderReturn) throws ServiceException, DatabasePersistenceException {
        Purchase purchase = entityManager.find(Purchase.class, orderReturn.getPurchase().getId());
        OrderDetails orderDetails = entityManager.find(OrderDetails.class, orderReturn.getOrderDetails().getId());
        Warehouse warehouse = entityManager.find(Warehouse.class, orderReturn.getWarehouse().getId());
        Employee receivedBy = entityManager.find(Employee.class, orderReturn.getReturnBy().getId());
        orderReturn.setOrderDetails(orderDetails);
        orderReturn.setWarehouse(warehouse);
        orderReturn.setReturnBy(receivedBy);
        orderReturn.setPurchase(purchase);
        entityManager.persist(orderReturn);
        orderReturnQuantityTrackerService.createStockCheckpointForCreateOrderReturn(orderReturn);
        return orderReturn;
    }

    public OrderReturn update(OrderReturn orderReturn) throws ServiceException, DatabasePersistenceException {

        OrderReturn updating = entityManager.find(OrderReturn.class,orderReturn.getId());
        Purchase purchase = entityManager.find(Purchase.class, orderReturn.getPurchase().getId());
        OrderDetails orderDetails = entityManager.find(OrderDetails.class, orderReturn.getOrderDetails().getId());
        Warehouse warehouse = entityManager.find(Warehouse.class, orderReturn.getWarehouse().getId());
        Employee receivedBy = entityManager.find(Employee.class, orderReturn.getReturnBy().getId());

        orderReturn.setOrderDetails(orderDetails);
        orderReturn.setWarehouse(warehouse);
        orderReturn.setReturnBy(receivedBy);
        orderReturnQuantityTrackerService.updateStockCheckpointForUpdateOrderReturn(updating,orderReturn);

        updating.setQuantity(orderReturn.getQuantity());
        updating.setReturnDate(orderReturn.getReturnDate());
        updating.setOrderDetails(orderDetails);
        updating.setWarehouse(warehouse);
        updating.setReturnBy(receivedBy);
        updating.setPurchase(purchase);
        entityManager.merge(updating);

        return updating;
    }

    public void delete(OrderReturn orderReturn) throws ServiceException, DatabasePersistenceException {

    }

}
