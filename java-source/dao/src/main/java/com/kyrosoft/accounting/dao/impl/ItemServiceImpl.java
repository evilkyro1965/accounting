package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.ItemService;
import com.kyrosoft.accounting.model.Employee;
import com.kyrosoft.accounting.model.Item;
import com.kyrosoft.accounting.model.ItemCategory;
import com.kyrosoft.accounting.model.Measurement;
import com.kyrosoft.accounting.model.dto.ActiveEntitySearchCriteria;
import com.kyrosoft.accounting.model.dto.ItemSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import java.util.HashSet;
import java.util.Set;

import static com.kyrosoft.accounting.dao.impl.ServiceHelper.entityErrorMessageHelper;
import static com.kyrosoft.accounting.dao.impl.ServiceHelper.getSearchLikeString;

/**
 * Created by Administrator on 10/23/15.
 */
@Service
@Transactional
public class ItemServiceImpl extends BaseServiceImpl<Item,ItemSearchCriteria>
    implements ItemService {

    public ItemServiceImpl() {
        super(Item.class);
    }

    protected String generateWhereClause(ItemSearchCriteria criteria) throws ServiceException {
        StringBuffer sb = new StringBuffer("WHERE (1=1)");

        if (criteria.getName() != null) {
            sb.append(" AND e.name LIKE :name");
        }
        if (criteria.getItemCode() != null) {
            sb.append(" AND e.itemCode LIKE :itemCode");
        }
        if (criteria.getItemType() != null) {
            sb.append(" AND e.itemType = :itemType");
        }
        if (criteria.getItemCategoryId() != null) {
            sb.append(" AND e.category.id = :categoryId");
        }
        if (criteria.getStockMeasurementId() != null) {
            sb.append(" AND e.stockMeasurement.id = :stockMeasurementId");
        }
        if(criteria.getIsActive() != null) {
            sb.append(" AND e.isActive = :isActive");
        }
        return sb.toString();
    }

    protected void populateQueryParameters(Query query, ItemSearchCriteria criteria) throws ServiceException {
        if (criteria.getName() != null) {
            query.setParameter("name", getSearchLikeString(criteria.getName()));
        }
        if (criteria.getItemCode() != null) {
            query.setParameter("itemCode", getSearchLikeString(criteria.getItemCode()));
        }
        if (criteria.getItemType() != null) {
            query.setParameter("itemType", criteria.getItemType());
        }
        if (criteria.getItemCategoryId() != null) {
            query.setParameter("categoryId", criteria.getItemCategoryId());
        }
        if (criteria.getStockMeasurementId() != null) {
            query.setParameter("stockMeasurementId", criteria.getStockMeasurementId());
        }
        if (criteria.getIsActive() != null) {
            query.setParameter("isActive", criteria.getIsActive());
        }
    }

    public Item get(long id) {
        Item item = entityManager.find(Item.class,id);
        return item;
    }

    public Item save(Item item) throws ServiceException, DatabasePersistenceException {

        if(item.getCategory()!=null) {
            ItemCategory itemCategory = entityManager.find(ItemCategory.class, item.getCategory().getId());
            item.setCategory(itemCategory);
        }

        if(item.getStockMeasurement()!=null){
            Measurement measurement = entityManager.find(Measurement.class, item.getStockMeasurement().getId());
            item.setStockMeasurement(measurement);
        }

        if(item.getPurchaseMeasurements()!=null){
            Set<Measurement> savePurchaseMeasurements = new HashSet<Measurement>();
            for(Measurement measurement : item.getPurchaseMeasurements()){
                Measurement saveMeasurement = entityManager.find(Measurement.class, measurement.getId());
                savePurchaseMeasurements.add(saveMeasurement);
            }
            item.setPurchaseMeasurements(savePurchaseMeasurements);
        }

        Set<ConstraintViolation<Item>> constraintViolations = validator.validate(item);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.persist(item);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }
        return item;
    }

    public Item update(Item item) throws ServiceException, DatabasePersistenceException {

        Item updating = entityManager.find(Item.class, item.getId());

        if(item.getCategory()!=null) {
            ItemCategory itemCategory = entityManager.find(ItemCategory.class, item.getCategory().getId());
            updating.setCategory(itemCategory);
        }
        else {
            updating.setCategory(null);
        }

        if(item.getStockMeasurement()!=null){
            Measurement measurement = entityManager.find(Measurement.class, item.getStockMeasurement().getId());
            updating.setStockMeasurement(measurement);
        }
        else {
            updating.setStockMeasurement(null);
        }

        if(item.getPurchaseMeasurements()!=null){
            Set<Measurement> savePurchaseMeasurements = new HashSet<Measurement>();
            for(Measurement measurement : item.getPurchaseMeasurements()){
                Measurement saveMeasurement = entityManager.find(Measurement.class, measurement.getId());
                savePurchaseMeasurements.add(saveMeasurement);
            }
            updating.setPurchaseMeasurements(savePurchaseMeasurements);
        }
        else {
            updating.setPurchaseMeasurements(null);
        }

        updating.setName(item.getName());
        updating.setItemCode(item.getItemCode());
        updating.setItemType(item.getItemType());
        updating.setItemDescription(item.getItemDescription());
        updating.setItemPrice(item.getItemPrice());
        updating.setIsActive(item.getIsActive());

        Set<ConstraintViolation<Item>> constraintViolations = validator.validate(updating);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.merge(updating);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }
        return item;
    }

    public void delete(Item item) throws ServiceException, DatabasePersistenceException {

        Item updating = entityManager.find(Item.class, item.getId());

        if(updating==null) throw new DatabasePersistenceException("Entity not found");

        entityManager.remove(updating);
    }
}
