package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.MeasurementService;
import com.kyrosoft.accounting.model.Employee;
import com.kyrosoft.accounting.model.Measurement;
import com.kyrosoft.accounting.model.dto.MeasurementSearchCriteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import java.util.Set;

import static com.kyrosoft.accounting.dao.impl.ServiceHelper.entityErrorMessageHelper;
import static com.kyrosoft.accounting.dao.impl.ServiceHelper.getSearchLikeString;

/**
 * Measurement Service Impl
 *
 * @author fahrur
 * @version 1.0
 */
@Service
@Transactional
public class MeasurementServiceImpl extends BaseServiceImpl<Measurement,MeasurementSearchCriteria>
    implements MeasurementService {

    /**
     * Constructor
     */
    public MeasurementServiceImpl() {
        super(Measurement.class);
    }

    /**
     * Generate where clause
     * @param criteria
     *            the criteria
     * @return the where query string
     * @throws ServiceException the exception
     */
    protected String generateWhereClause(MeasurementSearchCriteria criteria) throws ServiceException {
        StringBuffer sb = new StringBuffer("WHERE (1=1)");

        if (criteria.getName() != null) {
            sb.append(" AND e.name LIKE :name");
        }
        if (criteria.getShortName() != null) {
            sb.append(" AND e.shortName LIKE :shortName");
        }
        if (criteria.getType() != null) {
            sb.append(" AND e.measurementType = :type");
        }
        if(criteria.getIsActive() != null) {
            sb.append(" AND e.isActive = :isActive");
        }
        return sb.toString();
    }

    protected void populateQueryParameters(Query query, MeasurementSearchCriteria criteria) throws ServiceException {
        if (criteria.getName() != null) {
            query.setParameter("name", getSearchLikeString(criteria.getName()));
        }
        if (criteria.getShortName() != null) {
            query.setParameter("shortName", getSearchLikeString(criteria.getShortName()));
        }
        if (criteria.getType() != null) {
            query.setParameter("type", criteria.getType());
        }
        if (criteria.getIsActive() != null) {
            query.setParameter("isActive", criteria.getIsActive());
        }
    }

    public Measurement get(long id) {
        Measurement measurement = entityManager.find(Measurement.class,id);
        return measurement;
    }

    public Measurement save(Measurement measurement) throws ServiceException, DatabasePersistenceException {

        Set<ConstraintViolation<Measurement>> constraintViolations = validator.validate(measurement);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.persist(measurement);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }
        return measurement;
    }

    public Measurement update(Measurement measurement) throws ServiceException, DatabasePersistenceException {

        Measurement updating = entityManager.find(Measurement.class,measurement.getId());

        updating.setName(measurement.getName());
        updating.setDescription(measurement.getDescription());
        updating.setMeasurementType(measurement.getMeasurementType());
        updating.setIsActive(measurement.getIsActive());
        updating.setUnit(measurement.getUnit());
        updating.setIsActive(measurement.getIsActive());

        Set<ConstraintViolation<Measurement>> constraintViolations = validator.validate(updating);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.merge(updating);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }
        return measurement;
    }

    public void delete(Measurement measurement) throws ServiceException, DatabasePersistenceException {
        Measurement updating = entityManager.find(Measurement.class,measurement.getId());

        if(updating==null) throw new DatabasePersistenceException("Entity not found");

        entityManager.remove(updating);
    }
}
