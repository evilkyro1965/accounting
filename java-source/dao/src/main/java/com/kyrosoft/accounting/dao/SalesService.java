package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.Sales;

/**
 * Created by Administrator on 2/15/2016.
 */
public interface SalesService {

    public Sales get(long id);

    public Sales save(Sales sales) throws ServiceException, DatabasePersistenceException;

    public Sales update(Sales sales) throws ServiceException, DatabasePersistenceException;

    public void delete(Sales sales) throws ServiceException, DatabasePersistenceException;
}
