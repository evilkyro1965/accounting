package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.ItemService;
import com.kyrosoft.accounting.dao.StockBalanceService;
import com.kyrosoft.accounting.dao.WarehouseService;
import com.kyrosoft.accounting.model.StockBalance;
import com.kyrosoft.accounting.model.StockBalanceType;
import com.kyrosoft.accounting.model.StockCheckPoint;
import com.kyrosoft.accounting.model.StockTransactionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.validation.Validator;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2/10/2016.
 */
@Transactional
public class BaseQuantityTracker {

    /**
     * Represents the EntityManager used to access database persistence.
     *
     * Required. Not null.
     */
    @PersistenceContext
    protected EntityManager entityManager;

    /**
     * Validator
     */
    @Autowired
    protected Validator validator;

    @Autowired
    StockBalanceService stockBalanceService;

    @Autowired
    ItemService itemService;

    @Autowired
    WarehouseService warehouseService;

    public BaseQuantityTracker() {}


    protected EntityManager getEntityManager() {
        return entityManager;
    }

    protected StockBalance getAndCreateStockBalance(Long warehouseId, Long itemId, StockBalanceType stockBalanceType)
            throws ServiceException,DatabasePersistenceException {
        StockBalance findObj = stockBalanceService.get(warehouseId,
                itemId,
                stockBalanceType);

        if(findObj==null) {

            StockBalance stockBalance = new StockBalance();
            if(warehouseId!=null)
                stockBalance.setWarehouse(warehouseService.get(warehouseId));
            stockBalance.setItem(itemService.get(itemId));
            stockBalance.setStockBalanceType(stockBalanceType);
            stockBalance.setBalance(0.0);
            stockBalance.setOpeningBalance(0.0);
            stockBalance.setLastTransactionDate(null);
            stockBalance.setLastTransactionEntityId(null);
            stockBalance.setLastTransactionType(null);

            entityManager.persist(stockBalance);
            return stockBalance;
        }

        return findObj;
    }

    protected StockCheckPoint getStockcheckpoint(StockBalance stockBalance,
                                               StockTransactionType stockTransactionType,
                                               Long stockTransactionId) {

        String className = ServiceHelper.getClassNameForSearch(StockCheckPoint.class);

        TypedQuery<StockCheckPoint> query = entityManager.createQuery(
                "SELECT s FROM "+className+" s WHERE s.stockBalance.id= :stockBalanceId"+
                        " AND s.transactionType = :transactionType" +
                        " AND s.transactionId = :transactionId",StockCheckPoint.class)
                .setParameter("stockBalanceId",stockBalance.getId())
                .setParameter("transactionType",stockTransactionType)
                .setParameter("transactionId",stockTransactionId);

        List<StockCheckPoint> stockCheckPoints = query.getResultList();
        if(stockCheckPoints!=null&&stockCheckPoints.size()>0) {
            return stockCheckPoints.get(0);
        }
        else {
            return null;
        }
    }


    protected void updateStockBalance(
            StockBalance stockBalance,
            Date transactionDate,
            Long transactionEntityId,
            StockTransactionType transactionType,
            double diffAmount,
            double balanceAfter
    ) {
        stockBalance.setLastTransactionDate(transactionDate);
        stockBalance.setLastTransactionType(transactionType);
        stockBalance.setLastTransactionEntityId(transactionEntityId);
        stockBalance.setBalance(balanceAfter);

        entityManager.merge(stockBalance);
    }

    protected StockCheckPoint createStockcheckpoint(
            StockBalance stockBalance,
            Date transactionDate,
            StockTransactionType transactionType,
            long transactionId,
            double amount,
            double balanceAfter
    ){

        StockCheckPoint stockCheckPoint = new StockCheckPoint();
        stockCheckPoint.setStockBalance(stockBalance);
        stockCheckPoint.setTransactionDateTime(transactionDate);
        stockCheckPoint.setTransactionType(transactionType);
        stockCheckPoint.setTransactionId(transactionId);
        stockCheckPoint.setAmount(amount);
        stockCheckPoint.setBalanceAfter(balanceAfter);

        entityManager.persist(stockCheckPoint);

        return stockCheckPoint;
    }

    protected StockCheckPoint updateStockcheckpoint(
            StockCheckPoint stockCheckPoint,
            StockBalance stockBalance,
            Date transactionDate,
            StockTransactionType transactionType,
            long transactionId,
            double amount,
            double diffAmount,
            double balanceAfter
    ){
        stockCheckPoint.setTransactionDateTime(transactionDate);
        stockCheckPoint.setTransactionType(transactionType);
        stockCheckPoint.setTransactionId(transactionId);
        stockCheckPoint.setAmount(amount);
        stockCheckPoint.setBalanceAfter(balanceAfter);

        entityManager.merge(stockCheckPoint);

        String className = ServiceHelper.getClassNameForSearch(StockCheckPoint.class);

        String sqlUpdateStockCheckpoint = diffAmount > 0 ?
                "UPDATE "+className+" s SET s.balanceAfter = s.balanceAfter + :diffAmount WHERE s.id > :id"
                : "UPDATE "+className+" s SET s.balanceAfter = s.balanceAfter - :diffAmount WHERE s.id > :id";

        sqlUpdateStockCheckpoint += " AND s.stockBalance.id = :stockBalanceId";

        Query queryUpdate = entityManager.createQuery(sqlUpdateStockCheckpoint);
        queryUpdate.setParameter("diffAmount",Math.abs(diffAmount));
        queryUpdate.setParameter("id",stockCheckPoint.getId());
        queryUpdate.setParameter("stockBalanceId",stockCheckPoint.getStockBalance().getId());
        queryUpdate.executeUpdate();

        return stockCheckPoint;
    }

    protected void removeStockCheckpoint(StockCheckPoint stockCheckPoint) {
        entityManager.remove(stockCheckPoint);
        double diffAmount = -1 * stockCheckPoint.getAmount();

        String className = ServiceHelper.getClassNameForSearch(StockCheckPoint.class);

        String sqlUpdateStockCheckpoint = diffAmount > 0 ?
                "UPDATE "+className+" s SET s.balanceAfter = s.balanceAfter + :diffAmount WHERE s.id > :id"
                : "UPDATE "+className+" s SET s.balanceAfter = s.balanceAfter - :diffAmount WHERE s.id > :id";

        sqlUpdateStockCheckpoint += " AND s.stockBalance.id = :stockBalanceId";

        Query queryUpdate = entityManager.createQuery(sqlUpdateStockCheckpoint);
        queryUpdate.setParameter("diffAmount",Math.abs(diffAmount));
        queryUpdate.setParameter("id",stockCheckPoint.getId());
        queryUpdate.setParameter("stockBalanceId",stockCheckPoint.getStockBalance().getId());
        queryUpdate.executeUpdate();
    }

    protected StockCheckPoint createStockCheckpointDebitStockBalance(
            Long warehouseId,
            Long itemId,
            StockBalanceType stockBalanceType,
            StockTransactionType stockTransactionType,
            Date transactionDate,
            Long transactionId,
            double quantity
    )  throws ServiceException, DatabasePersistenceException
    {
        StockBalance stockBalance = getAndCreateStockBalance(warehouseId,itemId,stockBalanceType);
        double amount = quantity;
        double balanceAfter = stockBalance.getBalance() + amount;

        StockCheckPoint stockCheckPoint = createStockcheckpoint(
                stockBalance,
                transactionDate,
                stockTransactionType,
                transactionId,
                amount,
                balanceAfter
        );

        updateStockBalance(
                stockBalance,
                transactionDate,
                transactionId,
                stockTransactionType,
                amount,
                balanceAfter);

        return stockCheckPoint;
    }

    protected StockCheckPoint createStockCheckpointCreditStockBalance(
            Long warehouseId,
            Long itemId,
            StockBalanceType stockBalanceType,
            StockTransactionType stockTransactionType,
            Date transactionDate,
            Long transactionId,
            double quantity
    )  throws ServiceException, DatabasePersistenceException
    {
        StockBalance stockBalance = getAndCreateStockBalance(warehouseId,itemId,stockBalanceType);
        double amount = -1 * quantity;
        double balanceAfter = stockBalance.getBalance() + amount;

        StockCheckPoint stockCheckPoint = createStockcheckpoint(
                stockBalance,
                transactionDate,
                stockTransactionType,
                transactionId,
                amount,
                balanceAfter
        );

        updateStockBalance(
                stockBalance,
                transactionDate,
                transactionId,
                stockTransactionType,
                amount,
                balanceAfter);

        return stockCheckPoint;
    }

    protected void updateStockCheckpointDebitStockBalance(
            Long warehouseId,
            Long itemId,
            StockBalanceType stockBalanceType,
            StockTransactionType stockTransactionType,
            Date transactionDate,
            Long transactionId,
            double quantityCurrent
    )  throws ServiceException, DatabasePersistenceException
    {
        StockBalance stockBalance = getAndCreateStockBalance(warehouseId, itemId, stockBalanceType);
        StockCheckPoint stockCheckPoint = getStockcheckpoint(stockBalance, stockTransactionType, transactionId);
        double quantityBefore = stockCheckPoint.getAmount();
        double diffQty = quantityCurrent - quantityBefore;
        double checkpointBalanceAfter = stockCheckPoint.getBalanceAfter() + diffQty;
        double balanceAfter = stockBalance.getBalance() + diffQty;

        updateStockcheckpoint(
                stockCheckPoint,
                stockBalance,
                transactionDate,
                stockTransactionType,
                transactionId,
                quantityCurrent,
                diffQty,
                checkpointBalanceAfter
        );

        updateStockBalance(
                stockBalance,
                transactionDate,
                transactionId,
                stockTransactionType,
                diffQty,
                balanceAfter);

    }

    protected void updateStockCheckpointCreditStockBalance(
            Long warehouseId,
            Long itemId,
            StockBalanceType stockBalanceType,
            StockTransactionType stockTransactionType,
            Date transactionDate,
            Long transactionId,
            double quantityCurrent
    )  throws ServiceException, DatabasePersistenceException
    {
        StockBalance stockBalance = getAndCreateStockBalance(warehouseId, itemId, stockBalanceType);
        StockCheckPoint stockCheckPoint = getStockcheckpoint(stockBalance, stockTransactionType, transactionId);
        quantityCurrent = -1 * quantityCurrent;
        double quantityBefore = stockCheckPoint.getAmount();
        double diffQty = quantityCurrent - quantityBefore;
        double checkpointBalanceAfter = stockCheckPoint.getBalanceAfter() + diffQty;
        double balanceAfter = stockBalance.getBalance() + diffQty;

        updateStockcheckpoint(
                stockCheckPoint,
                stockBalance,
                transactionDate,
                stockTransactionType,
                transactionId,
                quantityCurrent,
                diffQty,
                checkpointBalanceAfter
        );

        updateStockBalance(
                stockBalance,
                transactionDate,
                transactionId,
                stockTransactionType,
                diffQty,
                balanceAfter);

    }

    protected void removeStockCheckpointStockBalance(
            Long warehouseId,
            Long itemId,
            StockBalanceType stockBalanceType,
            StockTransactionType stockTransactionType,
            Date transactionDate,
            Long transactionId
    )  throws ServiceException, DatabasePersistenceException
    {
        StockBalance stockBalance = getAndCreateStockBalance(warehouseId, itemId, stockBalanceType);
        StockCheckPoint stockCheckPoint = getStockcheckpoint(stockBalance, stockTransactionType, transactionId);
        double changeQty = -1 * stockCheckPoint.getAmount();
        double balanceAfter = stockBalance.getBalance() + changeQty;

        removeStockCheckpoint(stockCheckPoint);

        updateStockBalance(
                stockBalance,
                transactionDate,
                transactionId,
                stockTransactionType,
                changeQty,
                balanceAfter);

    }

}
