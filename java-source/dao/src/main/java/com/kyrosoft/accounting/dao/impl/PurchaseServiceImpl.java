package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.PurchaseService;
import com.kyrosoft.accounting.dao.OrderDetailsQuantityTrackerService;
import com.kyrosoft.accounting.model.*;
import com.kyrosoft.accounting.model.dto.ActiveEntitySearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.kyrosoft.accounting.dao.impl.ServiceHelper.entityErrorMessageHelper;

/**
 * Created by Administrator on 12/22/15.
 */
@Service
@Transactional
public class PurchaseServiceImpl extends BaseServiceImpl<Purchase,ActiveEntitySearchCriteria>
    implements PurchaseService {

    @Autowired
    OrderDetailsQuantityTrackerService quantityTrackerService;

    public PurchaseServiceImpl() {
        super(Purchase.class);
    }

    public Purchase get(long id) {
        Purchase purchase = entityManager.find(Purchase.class,id);
        return purchase;
    }

    public Purchase save(Purchase purchase) throws ServiceException, DatabasePersistenceException {

        if(purchase.getRequestBy()!=null) {
            Employee requestBy = entityManager.find(Employee.class, purchase.getRequestBy().getId());
            purchase.setRequestBy(requestBy);
        }
        
        if(purchase.getOrderBy()!=null) {
            Employee orderBy = entityManager.find(Employee.class, purchase.getOrderBy().getId());
            purchase.setOrderBy(orderBy);
        }

        if(purchase.getSupplier()!=null) {
            Supplier supplier = entityManager.find(Supplier.class, purchase.getSupplier().getId());
            purchase.setSupplier(supplier);
        }

        if(purchase.getOrderDetailsList()!=null) {
            CopyOnWriteArrayList<OrderDetails> orderDetailsList = new CopyOnWriteArrayList<OrderDetails>( purchase.getOrderDetailsList() );
            for(OrderDetails orderDetails : orderDetailsList) {
                Item item = entityManager.find(Item.class, orderDetails.getItem().getId());
                Measurement measurement = entityManager.find(Measurement.class, orderDetails.getPurchaseMeasurement().getId());
                orderDetails.setItem(item);
                orderDetails.setPurchaseMeasurement(measurement);
                orderDetails.setPurchase(purchase);
                ServiceHelper.CompleteOrderDetailsDiscountTaxAndTotal(orderDetails);
                entityManager.persist(orderDetails);
            }
            purchase.setOrderDetailsList(orderDetailsList);
        }

        Set<ConstraintViolation<Purchase>> constraintViolations = validator.validate(purchase);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.persist(purchase);

            //Create tracking stock balance/checkpoint for new orderdetails
            if(purchase.getOrderDetailsList()!=null) {
                CopyOnWriteArrayList<OrderDetails> orderDetailsList = new CopyOnWriteArrayList<OrderDetails>( purchase.getOrderDetailsList() );
                for(OrderDetails orderDetails : orderDetailsList) {
                    quantityTrackerService.createStockCheckpointForCreateOrderDetails(orderDetails);
                }
            }
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }
        return purchase;
    }

    public Purchase update(Purchase purchase) throws ServiceException, DatabasePersistenceException {

        Purchase updating = entityManager.find(Purchase.class, purchase.getId());

        updating.setNo(purchase.getNo());
        updating.setDate(purchase.getDate());
        updating.setRemark(purchase.getRemark());
        updating.setPurchaseStatus(purchase.getPurchaseStatus());

        if(purchase.getRequestBy()!=null) {
            Employee requestBy = entityManager.find(Employee.class, purchase.getRequestBy().getId());
            updating.setRequestBy(requestBy);
        }
        
        if(purchase.getOrderBy()!=null) {
            Employee orderBy = entityManager.find(Employee.class, purchase.getOrderBy().getId());
            updating.setOrderBy(orderBy);
        }

        if(purchase.getSupplier()!=null) {
            Supplier supplier = entityManager.find(Supplier.class, purchase.getSupplier().getId());
            updating.setSupplier(supplier);
        }

        CopyOnWriteArrayList<OrderDetails> existedOrderDetailsList = new CopyOnWriteArrayList<OrderDetails>( updating.getOrderDetailsList() );

        if(purchase.getOrderDetailsList()!=null) {
            List<OrderDetails> orderDetailsList = purchase.getOrderDetailsList();
            for(OrderDetails orderDetails : orderDetailsList) {
                Item item = entityManager.find(Item.class, orderDetails.getItem().getId());
                Measurement measurement = entityManager.find(Measurement.class, orderDetails.getPurchaseMeasurement().getId());
                orderDetails.setItem(item);
                orderDetails.setPurchaseMeasurement(measurement);
                orderDetails.setPurchase(updating);
                ServiceHelper.CompleteOrderDetailsDiscountTaxAndTotal(orderDetails);
            }

            //Update the row
            for(OrderDetails orderDetails : orderDetailsList) {
                for(OrderDetails existedOrderDetails : existedOrderDetailsList) {
                    if(orderDetails.getId()==existedOrderDetails.getId()){
                        if(isOrderDetailsChange(existedOrderDetails,orderDetails)) {
                            if (existedOrderDetails.getOrderReceivedList() != null &&
                                    existedOrderDetails.getOrderReceivedList().size() > 0) {
                                throw new ServiceException("Some of order details had order recived");
                            }
                            boolean isUpdatedOrderDetailsChangeStock =
                                    isUpdatedOrderDetailsChangeStock(orderDetails, existedOrderDetails);
                            if (isUpdatedOrderDetailsChangeStock) {
                                quantityTrackerService.createStockCheckpointForUpdateOrderDetails(existedOrderDetails, orderDetails);
                            }
                            updateOrderDetailsValues(orderDetails, existedOrderDetails);
                        }
                    }
                }
            }

            //Remove deleted row
            for(OrderDetails existedOrderDetails : existedOrderDetailsList) {
                boolean exists = false;
                for(OrderDetails orderDetails : orderDetailsList) {
                    exists = false;
                    if(orderDetails.getId()!=null && orderDetails.getId()!=0) {
                        if(orderDetails.getId()==existedOrderDetails.getId()){
                            exists = true;
                            break;
                        }
                    }
                }
                if(!exists){
                    if(!isOrderHadOrderReceived(existedOrderDetails)) {
                        if (existedOrderDetails.getOrderReceivedList() != null &&
                                existedOrderDetails.getOrderReceivedList().size() > 0) {
                            throw new ServiceException("Some of order details had order recived");
                        }
                        existedOrderDetailsList.remove(existedOrderDetails);
                        quantityTrackerService.deleteStockCheckpointForCreateOrderDetails(existedOrderDetails);
                        entityManager.remove(existedOrderDetails);
                    }
                    else {
                        throw new ServiceException("Can't delete order details, caused order details had order received child");
                    }
                }

            }

            //Added new row
            for(OrderDetails orderDetails : orderDetailsList) {
                if(orderDetails.getId()==null||orderDetails.getId()==0) {
                    existedOrderDetailsList.add(orderDetails);
                    orderDetails.setStatus(OrderStatus.ORDER);
                    entityManager.persist(orderDetails);
                    quantityTrackerService.createStockCheckpointForCreateOrderDetails(orderDetails);
                }
            }
        }

        updating.setOrderDetailsList(existedOrderDetailsList);

        Set<ConstraintViolation<Purchase>> constraintViolations = validator.validate(updating);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.merge(updating);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }

        return updating;
    }

    /**
     * Is order details had order received
     * @param orderDetails orderdetails
     * @return boolean
     */
    public boolean isOrderHadOrderReceived(OrderDetails orderDetails) {
        if(orderDetails.getOrderReceivedList()!=null && orderDetails.getOrderReceivedList().size()>0) {
            return true;
        }
        return false;
    }

    public void updateOrderDetailsValues(OrderDetails src,OrderDetails target) {
        target.setItem(src.getItem());
        target.setPurchaseMeasurement(src.getPurchaseMeasurement());
        target.setQuantity(src.getQuantity());
        target.setPrice(src.getPrice());
        target.setDiscountPercent(src.getDiscountPercent());
        target.setTaxPercent(src.getTaxPercent());
        target.setOrderReceivedFull(src.isOrderReceivedFull());
        target.setStatus(src.getStatus());
        ServiceHelper.CompleteOrderDetailsDiscountTaxAndTotal(target);
    }

    public boolean isOrderDetailsChange(OrderDetails src,OrderDetails target) {
        if(
                src.getItem().getId() != target.getItem().getId() ||
                src.getPurchaseMeasurement().getId() != target.getPurchaseMeasurement().getId() ||
                src.getQuantity() != target.getQuantity() ||
                src.getPrice() != target.getPrice() ||
                src.getDiscountPercent() != target.getDiscountPercent() ||
                src.getTaxPercent() != target.getTaxPercent()
                ) {
            return true;
        }
        return false;
    }

    public boolean isUpdatedOrderDetailsChangeStock(OrderDetails src,OrderDetails target) {
        if(
            src.getItem().getId()!=target.getItem().getId() ||
            src.getPurchaseMeasurement().getId()!=target.getPurchaseMeasurement().getId() ||
            src.getQuantity()!=target.getQuantity()
        ) {
            return true;
        }
        return false;
    }

    public void delete(Purchase purchase) throws ServiceException, DatabasePersistenceException {

    }

    protected String generateWhereClause(ActiveEntitySearchCriteria criteria) throws ServiceException {
        return null;
    }

    protected void populateQueryParameters(Query query, ActiveEntitySearchCriteria criteria) throws ServiceException {

    }
}
