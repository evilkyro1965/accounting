package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.Measurement;
import com.kyrosoft.accounting.model.dto.MeasurementSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import com.kyrosoft.accounting.model.dto.WarehouseSearchCriteria;

/**
 * Created by Administrator on 10/23/15.
 */
public interface MeasurementService {

    public Measurement get(long id);

    public Measurement save(Measurement measurement) throws ServiceException, DatabasePersistenceException;

    public Measurement update(Measurement measurement) throws ServiceException, DatabasePersistenceException;

    public void delete(Measurement measurement) throws ServiceException, DatabasePersistenceException;

    public SearchResult<Measurement> search(MeasurementSearchCriteria criteria) throws ServiceException;
}
