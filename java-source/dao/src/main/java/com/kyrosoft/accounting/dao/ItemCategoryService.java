package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.ItemCategory;
import com.kyrosoft.accounting.model.dto.EmployeeSearchCriteria;
import com.kyrosoft.accounting.model.dto.ActiveEntitySearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;

/**
 * Created by Administrator on 10/22/15.
 */
public interface ItemCategoryService {

    public ItemCategory get(long id);

    public ItemCategory save(ItemCategory itemCategory) throws ServiceException, DatabasePersistenceException;

    public ItemCategory update(ItemCategory itemCategory) throws ServiceException, DatabasePersistenceException;

    public void delete(ItemCategory itemCategory) throws ServiceException, DatabasePersistenceException;

    public SearchResult<ItemCategory> search(ActiveEntitySearchCriteria criteria) throws ServiceException;
}
