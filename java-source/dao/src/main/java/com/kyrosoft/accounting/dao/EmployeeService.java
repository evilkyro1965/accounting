package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.Employee;
import com.kyrosoft.accounting.model.dto.DepartmentSearchCriteria;
import com.kyrosoft.accounting.model.dto.EmployeeSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;

/**
 * Employee Service
 *
 * @author fahrur
 * @version 1.0
 */
public interface EmployeeService {

    /**
     * Get the employee
     * @param id the id
     * @return the employee
     */
    public Employee get(long id);

    /**
     * Save the employee
     * @param employee the employee
     * @return the employee
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public Employee save(Employee employee) throws ServiceException, DatabasePersistenceException;

    /**
     * Update the employee
     * @param employee the employee
     * @return the employee
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public Employee update(Employee employee) throws ServiceException, DatabasePersistenceException;

    /**
     * Delete the employee
     * @param employee the employee
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public void delete(Employee employee) throws ServiceException, DatabasePersistenceException;

    /**
     * This method is used to search entities based on the search criteria.
     *
     * @param criteria
     *            the search criteria
     * @return the search result
     * @throws IllegalArgumentException
     *             if criteria is null; criteria.pageNumber < 0; criteria.pageSize < 0; criteria.pageNumber > 0 and
     *             criteria.pageSize <= 0; the criteria has some fields which are ids and they are not positive
     * @throws ServiceException
     *             if any other error occurred during the operation
     */
    public SearchResult<Employee> search(EmployeeSearchCriteria criteria) throws ServiceException;
}
