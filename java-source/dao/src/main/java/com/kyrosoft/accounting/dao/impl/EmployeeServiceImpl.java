package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.EmployeeService;
import com.kyrosoft.accounting.model.Department;
import com.kyrosoft.accounting.model.Employee;
import com.kyrosoft.accounting.model.dto.EmployeeSearchCriteria;
import static com.kyrosoft.accounting.dao.impl.ServiceHelper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

/**
 * Department Service Impl
 *
 * @author fahrur
 * @version 1.0
 */
@Service
@Transactional
public class EmployeeServiceImpl extends BaseServiceImpl<Employee,EmployeeSearchCriteria>
    implements EmployeeService {

    /**
     * Empty constructor
     */
    public EmployeeServiceImpl() {
        super(Employee.class);
    }

    /**
     * Generate where clause
     * @param criteria
     *            the criteria
     * @return the where query string
     * @throws ServiceException the exception
     */
    protected String generateWhereClause(EmployeeSearchCriteria criteria) throws ServiceException {
        StringBuffer sb = new StringBuffer("WHERE (1=1)");

        if (criteria.getName() != null) {
            sb.append(" AND e.name LIKE :name");
        }
        if (criteria.getUsername() != null) {
            sb.append(" AND e.username LIKE :username");
        }
        if (criteria.getDepartmentId() != null) {
            sb.append(" AND e.department.id = :departmentId");
        }
        if (criteria.getMobile() != null) {
            sb.append(" AND e.mobile LIKE :mobile");
        }
        if (criteria.getEmail() != null) {
            sb.append(" AND e.email LIKE :email");
        }
        if(criteria.getIsActive() != null) {
            sb.append(" AND e.isActive = :isActive");
        }
        return sb.toString();
    }

    /**
     * Populate query parameters
     * @param query
     *            the JPA query
     * @param criteria
     *            the criteria
     * @throws ServiceException the exception
     */
    protected void populateQueryParameters(Query query, EmployeeSearchCriteria criteria) throws ServiceException {
        if (criteria.getName() != null) {
            query.setParameter("name", getSearchLikeString(criteria.getName()));
        }
        if (criteria.getUsername() != null) {
            query.setParameter("username", getSearchLikeString(criteria.getUsername()));
        }
        if (criteria.getDepartmentId() != null) {
            query.setParameter("departmentId", criteria.getDepartmentId());
        }
        if (criteria.getMobile() != null) {
            query.setParameter("mobile", getSearchLikeString(criteria.getMobile()));
        }
        if (criteria.getEmail() != null) {
            query.setParameter("email", getSearchLikeString(criteria.getEmail()));
        }
        if (criteria.getIsActive() != null) {
            query.setParameter("isActive", criteria.getIsActive());
        }
    }

    /**
     * Get the employee
     * @param id the id
     * @return the employee
     */
    public Employee get(long id) {
        Employee employee = entityManager.find(Employee.class,id);
        return employee;
    }

    /**
     * Save the employee
     * @param employee the employee
     * @return the employee
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public Employee save(Employee employee) throws ServiceException, DatabasePersistenceException {

        if(employee.getDepartment()!=null) {
            Department department = entityManager.find(Department.class, employee.getDepartment().getId());
            employee.setDepartment(department);
        }

        Set<ConstraintViolation<Employee>> constraintViolations = validator.validate(employee);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.persist(employee);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }
        return employee;
    }

    /**
     * Update the employee
     * @param employee the employee
     * @return the employee
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public Employee update(Employee employee) throws ServiceException, DatabasePersistenceException {

        Employee updating = entityManager.find(Employee.class,employee.getId());

        if(employee.getDepartment()!=null) {
            Department department = entityManager.find(Department.class, employee.getDepartment().getId());
            updating.setDepartment(department);
        }
        else {
            updating.setDepartment(null);
        }

        updating.setName(employee.getName());
        updating.setUsername(employee.getUsername());
        updating.setMobile(employee.getMobile());
        updating.setEmail(employee.getEmail());
        updating.setIsActive(employee.getIsActive());
        updateAuditableEntity(employee,updating);

        Set<ConstraintViolation<Employee>> constraintViolations = validator.validate(updating);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.merge(updating);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }
        return updating;
    }

    /**
     * Delete the employee
     * @param employee the employee
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public void delete(Employee employee) throws ServiceException, DatabasePersistenceException {
        Employee updating = entityManager.find(Employee.class,employee.getId());

        if(updating==null) throw new DatabasePersistenceException("Entity not found");

        entityManager.remove(updating);
    }
}
