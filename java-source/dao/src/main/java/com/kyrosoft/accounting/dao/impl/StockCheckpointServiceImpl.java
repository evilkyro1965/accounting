package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.StockCheckpointService;
import com.kyrosoft.accounting.model.StockBalance;
import com.kyrosoft.accounting.model.StockCheckPoint;
import com.kyrosoft.accounting.model.StockTransactionType;
import com.kyrosoft.accounting.model.dto.StockCheckpointSearchCriteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Administrator on 2/9/2016.
 */
@Service
@Transactional
public class StockCheckpointServiceImpl extends BaseServiceImpl<StockCheckPoint,StockCheckpointSearchCriteria>
    implements StockCheckpointService {

    public StockCheckpointServiceImpl() {
        super(StockCheckPoint.class);
    }


    @Override
    protected String generateWhereClause(StockCheckpointSearchCriteria criteria) throws ServiceException {
        return null;
    }

    @Override
    protected void populateQueryParameters(Query query, StockCheckpointSearchCriteria criteria) throws ServiceException {

    }

    public StockCheckPoint get(long id) {
        StockCheckPoint stockCheckPoint = entityManager.find(StockCheckPoint.class,id);
        return stockCheckPoint;
    }

    public StockCheckPoint get(StockBalance stockBalance,
                                               StockTransactionType stockTransactionType,
                                               Long stockTransactionId) {

        String className = ServiceHelper.getClassNameForSearch(StockCheckPoint.class);

        TypedQuery<StockCheckPoint> query = entityManager.createQuery(
                "SELECT s FROM "+className+" s WHERE s.stockBalance.id= :stockBalanceId"+
                        " AND s.transactionType = :transactionType" +
                        " AND s.transactionId = :transactionId",StockCheckPoint.class)
                .setParameter("stockBalanceId",stockBalance.getId())
                .setParameter("transactionType",stockTransactionType)
                .setParameter("transactionId",stockTransactionId);

        List<StockCheckPoint> stockCheckPoints = query.getResultList();
        if(stockCheckPoints!=null&&stockCheckPoints.size()>0) {
            return stockCheckPoints.get(0);
        }
        else {
            return null;
        }
    }

    public StockCheckPoint save(StockCheckPoint stockCheckPoint) throws ServiceException, DatabasePersistenceException {
        StockBalance stockBalance = entityManager.find(StockBalance.class,stockCheckPoint.getStockBalance().getId());
        stockCheckPoint.setStockBalance(stockBalance);
        entityManager.persist(stockCheckPoint);
        return stockCheckPoint;
    }

    public StockCheckPoint update(StockCheckPoint stockCheckPoint) throws ServiceException, DatabasePersistenceException {
        return null;
    }

    public void delete(StockCheckPoint stockCheckPoint) throws ServiceException, DatabasePersistenceException {

    }
}
