package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.CustomerService;
import com.kyrosoft.accounting.model.Customer;
import com.kyrosoft.accounting.model.dto.CustomerSearchCriteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;

import static com.kyrosoft.accounting.dao.impl.ServiceHelper.getSearchLikeString;

/**
 * Created by Administrator on 2/15/2016.
 */
@Service
@Transactional
public class CustomerServiceImpl extends BaseServiceImpl<Customer,CustomerSearchCriteria>
    implements CustomerService {

    public CustomerServiceImpl() {
        super(Customer.class);
    }

    @Override
    protected String generateWhereClause(CustomerSearchCriteria criteria) throws ServiceException {
        StringBuffer sb = new StringBuffer("WHERE (1=1)");

        if (criteria.getName() != null) {
            sb.append(" AND e.name LIKE :name");
        }

        return sb.toString();
    }

    @Override
    protected void populateQueryParameters(Query query, CustomerSearchCriteria criteria) throws ServiceException {
        if (criteria.getName() != null) {
            query.setParameter("name", getSearchLikeString(criteria.getName()));
        }
    }

    public Customer get(long id) {
        Customer customer =  entityManager.find(Customer.class,id);
        return customer;
    }

    public Customer save(Customer customer) throws ServiceException, DatabasePersistenceException {
        entityManager.persist(customer);
        return customer;
    }

    public Customer update(Customer customer) throws ServiceException, DatabasePersistenceException {
        return null;
    }

    public void delete(Customer customer) throws ServiceException, DatabasePersistenceException {

    }
}
