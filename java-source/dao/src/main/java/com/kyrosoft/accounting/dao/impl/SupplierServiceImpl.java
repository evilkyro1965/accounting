package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.SupplierService;
import com.kyrosoft.accounting.model.ItemCategory;
import com.kyrosoft.accounting.model.Supplier;
import com.kyrosoft.accounting.model.dto.SupplierSearchCriteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import java.util.Set;

import static com.kyrosoft.accounting.dao.impl.ServiceHelper.entityErrorMessageHelper;
import static com.kyrosoft.accounting.dao.impl.ServiceHelper.getSearchLikeString;

/**
 * Created by Administrator on 10/22/15.
 */
@Service
@Transactional
public class SupplierServiceImpl extends BaseServiceImpl<Supplier,SupplierSearchCriteria>
    implements SupplierService {

    public SupplierServiceImpl() {
        super(Supplier.class);
    }

    protected String generateWhereClause(SupplierSearchCriteria criteria) throws ServiceException {
        StringBuffer sb = new StringBuffer("WHERE (1=1)");

        if (criteria.getName() != null) {
            sb.append(" AND e.name LIKE :name");
        }
        if (criteria.getContactPerson() != null) {
            sb.append(" AND e.contactPerson LIKE :contactPerson");
        }
        if (criteria.getAddress() != null) {
            sb.append(" AND e.address LIKE :address");
        }
        if (criteria.getPhone() != null) {
            sb.append(" AND e.phone LIKE :phone");
        }
        if (criteria.getEmail() != null) {
            sb.append(" AND e.email LIKE :email");
        }
        if(criteria.getIsActive() != null) {
            sb.append(" AND e.isActive = :isActive");
        }
        return sb.toString();
    }

    protected void populateQueryParameters(Query query, SupplierSearchCriteria criteria) throws ServiceException {
        if (criteria.getName() != null) {
            query.setParameter("name", getSearchLikeString(criteria.getName()));
        }
        if (criteria.getContactPerson() != null) {
            query.setParameter("contactPerson", getSearchLikeString(criteria.getContactPerson()));
        }
        if (criteria.getAddress() != null) {
            query.setParameter("address", getSearchLikeString(criteria.getAddress()));
        }
        if (criteria.getPhone() != null) {
            query.setParameter("phone", getSearchLikeString(criteria.getPhone()));
        }
        if (criteria.getEmail() != null) {
            query.setParameter("email", getSearchLikeString(criteria.getEmail()));
        }
        if (criteria.getIsActive() != null) {
            query.setParameter("isActive", criteria.getIsActive());
        }
    }

    public Supplier get(long id) {
        Supplier supplier = entityManager.find(Supplier.class,id);
        return supplier;
    }

    public Supplier save(Supplier supplier) throws ServiceException, DatabasePersistenceException {

        Set<ConstraintViolation<Supplier>> constraintViolations = validator.validate(supplier);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.persist(supplier);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }
        return supplier;
    }

    public Supplier update(Supplier supplier) throws ServiceException, DatabasePersistenceException {

        Supplier updating = entityManager.find(Supplier.class,supplier.getId());

        updating.setName(supplier.getName());
        updating.setContactPerson(supplier.getContactPerson());
        updating.setAddress(supplier.getAddress());
        updating.setSecondaryAddress(supplier.getSecondaryAddress());
        updating.setCity(supplier.getCity());
        updating.setPostalCode(supplier.getPostalCode());
        updating.setState(supplier.getState());
        updating.setCountry(supplier.getCountry());
        updating.setPhone(supplier.getPhone());
        updating.setFax(supplier.getFax());
        updating.setEmail(supplier.getEmail());
        updating.setIsActive(supplier.getIsActive());
        updateAuditableEntity(supplier,updating);

        Set<ConstraintViolation<Supplier>> constraintViolations = validator.validate(updating);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.merge(updating);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }
        return updating;
    }

    public void delete(Supplier supplier) throws ServiceException, DatabasePersistenceException {

        Supplier updating = entityManager.find(Supplier.class,supplier.getId());

        if(updating==null) throw new DatabasePersistenceException("Entity not found");

        entityManager.remove(updating);
    }
}
