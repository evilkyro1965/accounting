package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.OrderReceivedQuantityTrackerService;
import com.kyrosoft.accounting.model.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by Administrator on 2/11/2016.
 */
@Service
@Transactional
public class OrderReceivedQuantityTrackerServiceImpl
    extends BaseQuantityTracker
    implements OrderReceivedQuantityTrackerService {

    public void createStockCheckpointForCreateOrderReceived(OrderReceived orderReceived) throws ServiceException, DatabasePersistenceException {
        OrderDetails orderDetails = orderReceived.getOrderDetails();
        Long warehouseId = orderReceived.getWarehouse().getId();
        Long warehouseIdOrder = null;
        Long itemId = orderReceived.getOrderDetails().getItem().getId();
        StockBalanceType stockBalanceTypeOnHand = StockBalanceType.QUANTITY_ON_HAND;
        StockBalanceType stockBalanceTypeOnOrder = StockBalanceType.QUANTITY_ON_ORDER;
        StockTransactionType stockTransactionType = StockTransactionType.PURCHASE_ORDER_RECEIVE;
        Date transactionDate = orderReceived.getReceivedDate();
        Long transactionId = orderReceived.getId();

        double quantity = orderReceived.getQuantity() * orderDetails.getPurchaseMeasurement().getUnit();

        StockCheckPoint stockCheckPointOnHand = createStockCheckpointDebitStockBalance(
                warehouseId,
                itemId,
                stockBalanceTypeOnHand,
                stockTransactionType,
                transactionDate,
                transactionId,
                quantity
        );

        StockCheckPoint stockCheckPointOnOrder = createStockCheckpointCreditStockBalance(
                warehouseIdOrder,
                itemId,
                stockBalanceTypeOnOrder,
                stockTransactionType,
                transactionDate,
                transactionId,
                quantity
        );

    }

    public void updateStockCheckpointForUpdateOrderReceived(OrderReceived orderReceivedBefore,OrderReceived orderReceived)
            throws ServiceException, DatabasePersistenceException {

        OrderDetails orderDetails = orderReceived.getOrderDetails();
        Long warehouseId = orderReceived.getWarehouse().getId();
        Long beforeWarehouseId = orderReceivedBefore.getWarehouse().getId();
        Long warehouseIdOrder = null;
        Long itemId = orderReceived.getOrderDetails().getItem().getId();
        StockBalanceType stockBalanceTypeOnHand = StockBalanceType.QUANTITY_ON_HAND;
        StockBalanceType stockBalanceTypeOnOrder = StockBalanceType.QUANTITY_ON_ORDER;
        StockTransactionType stockTransactionType = StockTransactionType.PURCHASE_ORDER_RECEIVE;
        Date transactionDate = orderReceived.getReceivedDate();
        Long transactionId = orderReceived.getId();

        if(beforeWarehouseId==warehouseId) {
            double quantity = orderReceived.getQuantity() * orderDetails.getPurchaseMeasurement().getUnit();

            //Update stock checkpoint & balance on hand
            updateStockCheckpointDebitStockBalance(
                    warehouseId,
                    itemId,
                    stockBalanceTypeOnHand,
                    stockTransactionType,
                    transactionDate,
                    transactionId,
                    quantity
            );

            //Update stock checkpoint & balance on hand
            updateStockCheckpointCreditStockBalance(
                    warehouseIdOrder,
                    itemId,
                    stockBalanceTypeOnOrder,
                    stockTransactionType,
                    transactionDate,
                    transactionId,
                    quantity
            );

        }
        else {
            /**
             * When warehouse id is changed, do:
             *  1. Delete the existing checkpoint
             *  2. Update the balance to amount before checkpoint
             */
            removeStockCheckpointStockBalance(
                    beforeWarehouseId,
                    itemId,
                    stockBalanceTypeOnHand,
                    stockTransactionType,
                    transactionDate,
                    transactionId
            );

            removeStockCheckpointStockBalance(
                    warehouseIdOrder,
                    itemId,
                    stockBalanceTypeOnOrder,
                    stockTransactionType,
                    transactionDate,
                    transactionId
            );

            /**
             * And Then
             */
            double quantity = orderReceived.getQuantity() * orderDetails.getPurchaseMeasurement().getUnit();

            StockCheckPoint stockCheckPointOnHand = createStockCheckpointDebitStockBalance(
                    warehouseId,
                    itemId,
                    stockBalanceTypeOnHand,
                    stockTransactionType,
                    transactionDate,
                    transactionId,
                    quantity
            );

            StockCheckPoint stockCheckPointOnOrder = createStockCheckpointCreditStockBalance(
                    warehouseIdOrder,
                    itemId,
                    stockBalanceTypeOnOrder,
                    stockTransactionType,
                    transactionDate,
                    transactionId,
                    quantity
            );
        }

    }


}
