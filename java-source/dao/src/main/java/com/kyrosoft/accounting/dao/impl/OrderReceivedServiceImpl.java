package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.OrderDetailsQuantityTrackerService;
import com.kyrosoft.accounting.dao.OrderReceivedQuantityTrackerService;
import com.kyrosoft.accounting.dao.OrderReceivedService;
import com.kyrosoft.accounting.model.*;
import com.kyrosoft.accounting.model.dto.ActiveEntitySearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;

/**
 * Created by Administrator on 2/5/2016.
 */
@Service
@Transactional
public class OrderReceivedServiceImpl extends BaseServiceImpl<OrderReceived,ActiveEntitySearchCriteria>
    implements OrderReceivedService {


    @Autowired
    OrderReceivedQuantityTrackerService orderReceivedQuantityTrackerService;

    /**
     * Constructor
     */
    public OrderReceivedServiceImpl() {
        super(OrderReceived.class);
    }

    @Override
    protected String generateWhereClause(ActiveEntitySearchCriteria criteria) throws ServiceException {
        return null;
    }

    @Override
    protected void populateQueryParameters(Query query, ActiveEntitySearchCriteria criteria) throws ServiceException {

    }

    public OrderReceived get(long id) {
        return null;
    }

    public OrderReceived save(OrderReceived orderReceived) throws ServiceException, DatabasePersistenceException {
        Purchase purchase = entityManager.find(Purchase.class, orderReceived.getPurchase().getId());
        OrderDetails orderDetails = entityManager.find(OrderDetails.class, orderReceived.getOrderDetails().getId());
        Warehouse warehouse = entityManager.find(Warehouse.class, orderReceived.getWarehouse().getId());
        Employee receivedBy = entityManager.find(Employee.class, orderReceived.getReceivedBy().getId());
        orderReceived.setOrderDetails(orderDetails);
        orderReceived.setWarehouse(warehouse);
        orderReceived.setReceivedBy(receivedBy);
        orderReceived.setPurchase(purchase);
        entityManager.persist(orderReceived);
        orderDetails.setStatus(OrderStatus.RECEIVE);
        entityManager.merge(orderDetails);
        orderReceivedQuantityTrackerService.createStockCheckpointForCreateOrderReceived(orderReceived);
        return orderReceived;
    }

    public OrderReceived update(OrderReceived orderReceived) throws ServiceException, DatabasePersistenceException {

        OrderReceived updating = entityManager.find(OrderReceived.class,orderReceived.getId());
        Purchase purchase = entityManager.find(Purchase.class, orderReceived.getPurchase().getId());
        OrderDetails orderDetails = entityManager.find(OrderDetails.class, orderReceived.getOrderDetails().getId());
        Warehouse warehouse = entityManager.find(Warehouse.class, orderReceived.getWarehouse().getId());
        Employee receivedBy = entityManager.find(Employee.class, orderReceived.getReceivedBy().getId());

        orderReceived.setOrderDetails(orderDetails);
        orderReceived.setWarehouse(warehouse);
        orderReceived.setReceivedBy(receivedBy);
        orderReceivedQuantityTrackerService.updateStockCheckpointForUpdateOrderReceived(updating,orderReceived);

        updating.setQuantity(orderReceived.getQuantity());
        updating.setReceivedDate(orderReceived.getReceivedDate());
        updating.setOrderDetails(orderDetails);
        updating.setWarehouse(warehouse);
        updating.setReceivedBy(receivedBy);
        updating.setPurchase(purchase);
        entityManager.merge(updating);

        return updating;
    }

    public void delete(OrderReceived orderReceived) throws ServiceException, DatabasePersistenceException {

        OrderReceived deleteObj = entityManager.find(OrderReceived.class, orderReceived.getId());

        if(deleteObj==null) throw new DatabasePersistenceException("Entity not found");

        entityManager.remove(deleteObj);

    }
}
