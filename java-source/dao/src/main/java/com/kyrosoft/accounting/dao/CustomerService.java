package com.kyrosoft.accounting.dao;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.model.Customer;
import com.kyrosoft.accounting.model.dto.CustomerSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;

/**
 * Created by Administrator on 2/15/2016.
 */
public interface CustomerService {

    public Customer get(long id);

    public Customer save(Customer customer) throws ServiceException, DatabasePersistenceException;

    public Customer update(Customer customer) throws ServiceException, DatabasePersistenceException;

    public void delete(Customer customer) throws ServiceException, DatabasePersistenceException;

    public SearchResult<Customer> search(CustomerSearchCriteria customer) throws ServiceException;
}
