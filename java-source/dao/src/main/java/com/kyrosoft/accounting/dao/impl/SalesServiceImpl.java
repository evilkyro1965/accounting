package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.SalesService;
import com.kyrosoft.accounting.model.*;
import com.kyrosoft.accounting.model.dto.ActiveEntitySearchCriteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Administrator on 2/15/2016.
 */
@Service
@Transactional
public class SalesServiceImpl extends BaseServiceImpl<Sales,ActiveEntitySearchCriteria>
    implements SalesService {

    public SalesServiceImpl() {
        super(Sales.class);
    }

    @Override
    protected String generateWhereClause(ActiveEntitySearchCriteria criteria) throws ServiceException {
        return null;
    }

    @Override
    protected void populateQueryParameters(Query query, ActiveEntitySearchCriteria criteria) throws ServiceException {

    }

    public Sales get(long id) {
        Sales sales = entityManager.find(Sales.class,id);
        return sales;
    }

    public Sales save(Sales sales) throws ServiceException, DatabasePersistenceException {

        if(sales.getSalesRep()!=null) {
            Employee salesRep = entityManager.find(Employee.class, sales.getSalesRep().getId());
            sales.setSalesRep(salesRep);
        }

        if(sales.getCustomer()!=null) {
            Customer customer = entityManager.find(Customer.class, sales.getCustomer().getId());
            sales.setCustomer(customer);
        }

        if(sales.getSalesOrderDetailsList()!=null) {
            CopyOnWriteArrayList<SalesOrderDetails> salesOrderDetailsList = new CopyOnWriteArrayList<SalesOrderDetails>( sales.getSalesOrderDetailsList() );
            for(SalesOrderDetails orderDetails : salesOrderDetailsList) {
                Item item = entityManager.find(Item.class, orderDetails.getItem().getId());
                Measurement measurement = entityManager.find(Measurement.class, orderDetails.getSalesMeasurement().getId());
                orderDetails.setItem(item);
                orderDetails.setSalesMeasurement(measurement);
                orderDetails.setSales(sales);
                ServiceHelper.CompleteOrderDetailsDiscountTaxAndTotal(orderDetails);
                entityManager.persist(orderDetails);
            }
            sales.setSalesOrderDetailsList(salesOrderDetailsList);
        }

        entityManager.persist(sales);

        return sales;
    }

    public Sales update(Sales sales) throws ServiceException, DatabasePersistenceException {

        Sales updating = entityManager.find(Sales.class, sales.getId());

        updating.setNo(sales.getNo());
        updating.setDate(sales.getDate());
        updating.setRemark(sales.getRemark());
        //updating.setStatus(sales.getStatus());

        if(sales.getSalesRep()!=null) {
            Employee salesRep = entityManager.find(Employee.class, sales.getSalesRep().getId());
            sales.setSalesRep(salesRep);
        }

        if(sales.getCustomer()!=null) {
            Customer customer = entityManager.find(Customer.class, sales.getCustomer().getId());
            sales.setCustomer(customer);
        }


        CopyOnWriteArrayList<SalesOrderDetails> existedSalesOrderDetailsList =
                new CopyOnWriteArrayList<SalesOrderDetails>( updating.getSalesOrderDetailsList() );

        if(sales.getSalesOrderDetailsList()!=null) {
            List<SalesOrderDetails> orderDetailsList = sales.getSalesOrderDetailsList();
            for(SalesOrderDetails orderDetails : orderDetailsList) {
                Item item = entityManager.find(Item.class, orderDetails.getItem().getId());
                Measurement measurement = entityManager.find(Measurement.class, orderDetails.getSalesMeasurement().getId());
                orderDetails.setItem(item);
                orderDetails.setSalesMeasurement(measurement);
                orderDetails.setSales(updating);
                ServiceHelper.CompleteOrderDetailsDiscountTaxAndTotal(orderDetails);
            }

            //Update the row
            for(SalesOrderDetails orderDetails : orderDetailsList) {
                for(SalesOrderDetails existedOrderDetails : existedSalesOrderDetailsList) {
                    if(orderDetails.getId()==existedOrderDetails.getId()){
                        if(isOrderDetailsChange(existedOrderDetails,orderDetails)) {
                            /*
                            if (existedOrderDetails.getOrderReceivedList() != null &&
                                    existedOrderDetails.getOrderReceivedList().size() > 0) {
                                throw new ServiceException("Some of order details had order recived");
                            }
                            */
                            boolean isUpdatedOrderDetailsChangeStock =
                                    isUpdatedOrderDetailsChangeStock(orderDetails, existedOrderDetails);
                            /*
                            if (isUpdatedOrderDetailsChangeStock) {
                                quantityTrackerService.createStockCheckpointForUpdateOrderDetails(existedOrderDetails, orderDetails);
                            }
                            */
                            updateOrderDetailsValues(orderDetails, existedOrderDetails);
                        }
                    }
                }
            }

            //Remove deleted row
            for(SalesOrderDetails existedOrderDetails : existedSalesOrderDetailsList) {
                boolean exists = false;
                for(SalesOrderDetails orderDetails : orderDetailsList) {
                    exists = false;
                    if(orderDetails.getId()!=null && orderDetails.getId()!=0) {
                        if(orderDetails.getId()==existedOrderDetails.getId()){
                            exists = true;
                            break;
                        }
                    }
                }
                if(!exists){
                    /*
                    if(!isOrderHadOrderReceived(existedOrderDetails)) {
                        if (existedOrderDetails.getOrderReceivedList() != null &&
                                existedOrderDetails.getOrderReceivedList().size() > 0) {
                            throw new ServiceException("Some of order details had order recived");
                        }
                        existedOrderDetailsList.remove(existedOrderDetails);
                        quantityTrackerService.deleteStockCheckpointForCreateOrderDetails(existedOrderDetails);
                        entityManager.remove(existedOrderDetails);
                    }
                    else {
                        throw new ServiceException("Can't delete order details, caused order details had order received child");
                    }
                    */
                    existedSalesOrderDetailsList.remove(existedOrderDetails);
                    entityManager.remove(existedOrderDetails);
                }

            }

            //Added new row
            for(SalesOrderDetails orderDetails : orderDetailsList) {
                if(orderDetails.getId()==null||orderDetails.getId()==0) {
                    existedSalesOrderDetailsList.add(orderDetails);
                    //orderDetails.setStatus(OrderStatus.ORDER);
                    entityManager.persist(orderDetails);
                    //quantityTrackerService.createStockCheckpointForCreateOrderDetails(orderDetails);
                }
            }
        }

        updating.setSalesOrderDetailsList(existedSalesOrderDetailsList);

        return updating;
    }
    /**
     * Is order details had order received
     * @param orderDetails orderdetails
     * @return boolean
     */
    public boolean isOrderHadOrderReceived(OrderDetails orderDetails) {
        if(orderDetails.getOrderReceivedList()!=null && orderDetails.getOrderReceivedList().size()>0) {
            return true;
        }
        return false;
    }

    public void updateOrderDetailsValues(SalesOrderDetails src,SalesOrderDetails target) {
        target.setItem(src.getItem());
        target.setSalesMeasurement(src.getSalesMeasurement());
        target.setQuantity(src.getQuantity());
        target.setPrice(src.getPrice());
        target.setDiscountPercent(src.getDiscountPercent());
        target.setTaxPercent(src.getTaxPercent());
        //target.setStatus(src.getStatus());
        ServiceHelper.CompleteOrderDetailsDiscountTaxAndTotal(target);
    }

    public boolean isOrderDetailsChange(SalesOrderDetails src,SalesOrderDetails target) {
        if(
                src.getItem().getId() != target.getItem().getId() ||
                        src.getSalesMeasurement().getId() != target.getSalesMeasurement().getId() ||
                        src.getQuantity() != target.getQuantity() ||
                        src.getPrice() != target.getPrice() ||
                        src.getDiscountPercent() != target.getDiscountPercent() ||
                        src.getTaxPercent() != target.getTaxPercent()
                ) {
            return true;
        }
        return false;
    }

    public boolean isUpdatedOrderDetailsChangeStock(SalesOrderDetails src,SalesOrderDetails target) {
        if(
                src.getItem().getId()!=target.getItem().getId() ||
                        src.getSalesMeasurement().getId()!=target.getSalesMeasurement().getId() ||
                        src.getQuantity()!=target.getQuantity()
                ) {
            return true;
        }
        return false;
    }

    public void delete(Sales sales) throws ServiceException, DatabasePersistenceException {

    }

}
