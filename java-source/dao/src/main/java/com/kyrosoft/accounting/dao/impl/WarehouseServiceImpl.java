package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.WarehouseService;
import com.kyrosoft.accounting.model.Department;
import com.kyrosoft.accounting.model.Employee;
import com.kyrosoft.accounting.model.Warehouse;
import com.kyrosoft.accounting.model.dto.WarehouseSearchCriteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import java.util.Set;

import static com.kyrosoft.accounting.dao.impl.ServiceHelper.entityErrorMessageHelper;
import static com.kyrosoft.accounting.dao.impl.ServiceHelper.getSearchLikeString;

/**
 * Warehouse Service Impl
 *
 * @author fahrur
 * @version 1.0
 */
@Service
@Transactional
public class WarehouseServiceImpl extends BaseServiceImpl<Warehouse,WarehouseSearchCriteria>
    implements WarehouseService {

    /**
     * Empty Constructor.
     */
    protected WarehouseServiceImpl() {
        super(Warehouse.class);
    }

    /**
     * Generate where clause
     * @param criteria
     *            the criteria
     * @return
     * @throws ServiceException the exception
     */
    protected String generateWhereClause(WarehouseSearchCriteria criteria) throws ServiceException {
        StringBuffer sb = new StringBuffer("WHERE (1=1)");

        if (criteria.getName() != null) {
            sb.append(" AND e.name LIKE :name");
        }
        if (criteria.getDepartment() != null) {
            sb.append(" AND e.department.id = :departmentId");
        }
        if (criteria.getShelfName() != null) {
            sb.append(" AND e.shelf LIKE :shelf");
        }
        if (criteria.getLabelName() != null) {
            sb.append(" AND e.label LIKE :label");
        }
        if(criteria.getIsActive() != null) {
            sb.append(" AND e.isActive = :isActive");
        }
        return sb.toString();
    }

    /**
     * Populate query parameters
     * @param query
     *            the JPA query
     * @param criteria
     *            the criteria
     * @throws ServiceException the exception
     */
    protected void populateQueryParameters(Query query, WarehouseSearchCriteria criteria) throws ServiceException {
        if (criteria.getName() != null) {
            query.setParameter("name", getSearchLikeString(criteria.getName()));
        }
        if (criteria.getDepartment() != null) {
            query.setParameter("departmentId", criteria.getDepartment().getId());
        }
        if (criteria.getShelfName() != null) {
            query.setParameter("shelf", getSearchLikeString(criteria.getShelfName()));
        }
        if (criteria.getLabelName() != null) {
            query.setParameter("label", getSearchLikeString(criteria.getLabelName()));
        }
        if (criteria.getIsActive() != null) {
            query.setParameter("isActive", criteria.getIsActive());
        }
    }

    /**
     * Get the warehouse
     * @param id the id
     * @return the warehouse
     */
    public Warehouse get(long id) {
        Warehouse warehouse = entityManager.find(Warehouse.class,id);
        return warehouse;
    }

    /**
     * Save the warehouse
     * @param warehouse the warehouse
     * @return the warehouse
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public Warehouse save(Warehouse warehouse) throws ServiceException, DatabasePersistenceException {

        if(warehouse.getDepartment()!=null) {
            Department department = entityManager.find(Department.class, warehouse.getDepartment().getId());
            warehouse.setDepartment(department);
        }

        Set<ConstraintViolation<Warehouse>> constraintViolations = validator.validate(warehouse);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.persist(warehouse);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }
        return warehouse;
    }

    /**
     * Update warehouse
     * @param warehouse the warehouse
     * @return the warehouse
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public Warehouse update(Warehouse warehouse) throws ServiceException, DatabasePersistenceException {

        Warehouse updating = entityManager.find(Warehouse.class,warehouse.getId());

        if(warehouse.getDepartment()!=null) {
            Department department = entityManager.find(Department.class, warehouse.getDepartment().getId());
            updating.setDepartment(department);
        }
        else {
            updating.setDepartment(null);
        }

        updating.setName(warehouse.getName());
        updating.setDescription(warehouse.getDescription());
        updating.setShelf(warehouse.getShelf());
        updating.setLabel(warehouse.getLabel());
        updating.setIsActive(warehouse.getIsActive());
        updateAuditableEntity(warehouse, updating);

        Set<ConstraintViolation<Warehouse>> constraintViolations = validator.validate(updating);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.merge(updating);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }

        return updating;
    }

    /**
     * Delete warehouse
     * @param warehouse the warehouse
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public void delete(Warehouse warehouse) throws ServiceException, DatabasePersistenceException {

        Warehouse updating = entityManager.find(Warehouse.class,warehouse.getId());

        if(updating==null) throw new DatabasePersistenceException("Entity not found");

        entityManager.remove(updating);
    }
}
