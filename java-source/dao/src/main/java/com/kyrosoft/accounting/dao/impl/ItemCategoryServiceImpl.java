package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.ItemCategoryService;
import com.kyrosoft.accounting.model.Employee;
import com.kyrosoft.accounting.model.ItemCategory;
import com.kyrosoft.accounting.model.dto.ActiveEntitySearchCriteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import java.util.Set;

import static com.kyrosoft.accounting.dao.impl.ServiceHelper.entityErrorMessageHelper;
import static com.kyrosoft.accounting.dao.impl.ServiceHelper.getSearchLikeString;

@Service
@Transactional
public class ItemCategoryServiceImpl extends BaseServiceImpl<ItemCategory,ActiveEntitySearchCriteria>
    implements ItemCategoryService {

    public ItemCategoryServiceImpl() {
        super(ItemCategory.class);
    }

    protected String generateWhereClause(ActiveEntitySearchCriteria criteria) throws ServiceException {
        StringBuffer sb = new StringBuffer("WHERE (1=1)");

        if (criteria.getName() != null) {
            sb.append(" AND e.name LIKE :name");
        }
        if(criteria.getIsActive() != null) {
            sb.append(" AND e.isActive = :isActive");
        }
        return sb.toString();
    }

    protected void populateQueryParameters(Query query, ActiveEntitySearchCriteria criteria) throws ServiceException {
        if (criteria.getName() != null) {
            query.setParameter("name", getSearchLikeString(criteria.getName()));
        }
        if (criteria.getIsActive() != null) {
            query.setParameter("isActive", criteria.getIsActive());
        }
    }

    public ItemCategory get(long id) {
        ItemCategory itemCategory = entityManager.find(ItemCategory.class,id);
        return itemCategory;
    }

    public ItemCategory save(ItemCategory itemCategory) throws ServiceException, DatabasePersistenceException {

        Set<ConstraintViolation<ItemCategory>> constraintViolations = validator.validate(itemCategory);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.persist(itemCategory);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }
        return itemCategory;
    }

    public ItemCategory update(ItemCategory itemCategory) throws ServiceException, DatabasePersistenceException {

        ItemCategory updating = entityManager.find(ItemCategory.class,itemCategory.getId());

        updating.setName(itemCategory.getName());
        updating.setIsActive(itemCategory.getIsActive());
        updateAuditableEntity(itemCategory,updating);

        Set<ConstraintViolation<ItemCategory>> constraintViolations = validator.validate(updating);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.merge(updating);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }
        return itemCategory;
    }

    public void delete(ItemCategory itemCategory) throws ServiceException, DatabasePersistenceException {

        ItemCategory updating = entityManager.find(ItemCategory.class,itemCategory.getId());

        if(updating==null) throw new DatabasePersistenceException("Entity not found");

        entityManager.remove(updating);
    }
}
