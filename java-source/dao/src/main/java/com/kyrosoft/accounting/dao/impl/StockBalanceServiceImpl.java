package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.DepartmentService;
import com.kyrosoft.accounting.dao.StockBalanceService;
import com.kyrosoft.accounting.model.Department;
import com.kyrosoft.accounting.model.StockBalance;
import com.kyrosoft.accounting.model.StockBalanceType;
import com.kyrosoft.accounting.model.dto.DepartmentSearchCriteria;
import com.kyrosoft.accounting.model.dto.SearchResult;
import com.kyrosoft.accounting.model.dto.StockBalanceSearchCriteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Administrator on 2/9/2016.
 */
@Service
@Transactional
public class StockBalanceServiceImpl extends BaseServiceImpl<StockBalance,StockBalanceSearchCriteria>
        implements StockBalanceService {

    public StockBalanceServiceImpl() {
        super(StockBalance.class);
    }

    @Override
    protected String generateWhereClause(StockBalanceSearchCriteria criteria) throws ServiceException {
        return null;
    }

    @Override
    protected void populateQueryParameters(Query query, StockBalanceSearchCriteria criteria) throws ServiceException {

    }

    public StockBalance get(long id) {
        StockBalance stockBalance = entityManager.find(StockBalance.class,id);
        return stockBalance;
    }

    public StockBalance get(Long warehouseId, Long itemId, StockBalanceType stockBalanceType) throws ServiceException, DatabasePersistenceException {
        TypedQuery<StockBalance> query = null;
        String className = ServiceHelper.getClassNameForSearch(StockBalance.class);
        if(warehouseId!=null) {
            query = entityManager.createQuery(
                    "SELECT s FROM "+className+" s WHERE s.warehouse.id = :warehouseId AND s.item.id = :itemId"+
                        " AND s.stockBalanceType = :stockBalanceType",StockBalance.class)
                    .setParameter("warehouseId", warehouseId)
                    .setParameter("itemId",itemId)
                    .setParameter("stockBalanceType",stockBalanceType);
        }
        else {
            query = entityManager.createQuery(
                    "SELECT s FROM "+className+" s WHERE s.item.id = :itemId"+
                            " AND s.stockBalanceType = :stockBalanceType",StockBalance.class)
                    .setParameter("itemId",itemId)
                    .setParameter("stockBalanceType",stockBalanceType);
        }

        List<StockBalance> stockBalanceList = query.getResultList();
        if(stockBalanceList!=null&&stockBalanceList.size()>0) {
            return stockBalanceList.get(0);
        }
        else {
            return null;
        }
    }

    public StockBalance save(StockBalance stockBalance) throws ServiceException, DatabasePersistenceException {

        Long warehouseId = stockBalance.getWarehouse() != null ? stockBalance.getWarehouse().getId() : null;
        StockBalance findObj = get(warehouseId,
                stockBalance.getItem().getId(),
                stockBalance.getStockBalanceType());
        if(findObj==null) {
            entityManager.persist(stockBalance);
            return stockBalance;
        }
        else {
            throw new ServiceException("Stock Balance with same warehouse, item, and balance type had been created");
        }
    }

    public StockBalance update(StockBalance stockBalance) throws ServiceException, DatabasePersistenceException {
        return null;
    }

    public void delete(StockBalance stockBalance) throws ServiceException, DatabasePersistenceException {

    }

}
