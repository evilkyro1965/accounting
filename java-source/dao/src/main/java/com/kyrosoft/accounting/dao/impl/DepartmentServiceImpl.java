package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.DepartmentService;
import com.kyrosoft.accounting.model.Department;
import com.kyrosoft.accounting.model.dto.DepartmentSearchCriteria;

import static com.kyrosoft.accounting.dao.impl.ServiceHelper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

/**
 * Department Service Impl
 *
 * @author fahrur
 * @version 1.0
 */
@Service
@Transactional
public class DepartmentServiceImpl extends BaseServiceImpl<Department,DepartmentSearchCriteria>
        implements DepartmentService {

    /**
     * Empty constructor
     */
    public DepartmentServiceImpl() {
        super(Department.class);
    }

    /**
     * Get the department
     * @param id the id
     * @return the department
     */
    public Department get(long id) {
        Department department = entityManager.find(Department.class,id);
        return department;
    }

    /**
     * Save the department
     * @param department the department
     * @return the department
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public Department save(Department department) throws ServiceException, DatabasePersistenceException {

        Set<ConstraintViolation<Department>> constraintViolations = validator.validate(department);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.persist(department);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }
        return department;
    }

    /**
     * Update the department
     * @param department the department
     * @return the department
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public Department update(Department department) throws ServiceException, DatabasePersistenceException {

        Department updating = entityManager.find(Department.class,department.getId());
        updating.setName(department.getName());
        updating.setEmail(department.getEmail());
        updating.setIsActive(department.getIsActive());
        updateAuditableEntity(department,updating);

        Set<ConstraintViolation<Department>> constraintViolations = validator.validate(updating);
        if(constraintViolations!=null && constraintViolations.size()==0){
            entityManager.merge(updating);
        }
        else {
            throw new DatabasePersistenceException(entityErrorMessageHelper(constraintViolations));
        }
        return updating;
    }

    /**
     * Delete the department
     * @param department the department
     * @throws ServiceException the exception
     * @throws DatabasePersistenceException the exception
     */
    public void delete(Department department) throws ServiceException, DatabasePersistenceException {
        Department updating = entityManager.find(Department.class,department.getId());

        if(updating==null) throw new DatabasePersistenceException("Entity not found");

        entityManager.remove(updating);
    }

    /**
     * Generate where clause
     * @param criteria
     *            the criteria
     * @return the where string
     * @throws ServiceException the exception
     */
    protected String generateWhereClause(DepartmentSearchCriteria criteria) throws ServiceException {
        StringBuffer sb = new StringBuffer("WHERE (1=1)");
        if (criteria.getName() != null) {
            sb.append(" AND e.name LIKE :name");
        }
        if (criteria.getEmail() != null) {
            sb.append(" AND e.email LIKE :email");
        }
        if(criteria.getIsActive() != null) {
            sb.append(" AND e.isActive = :isActive");
        }
        return sb.toString();
    }

    /**
     * Populate query parameters
     * @param query
     *            the JPA query
     * @param criteria
     *            the criteria
     * @throws ServiceException the exception
     */
    protected void populateQueryParameters(Query query, DepartmentSearchCriteria criteria) throws ServiceException {
        if (criteria.getName() != null) {
            query.setParameter("name", getSearchLikeString(criteria.getName()));
        }
        if (criteria.getEmail() != null) {
            query.setParameter("email", getSearchLikeString(criteria.getEmail()));
        }
        if (criteria.getIsActive() != null) {
            query.setParameter("isActive", criteria.getIsActive());
        }
    }

}
