package com.kyrosoft.accounting.dao.impl;

import com.kyrosoft.accounting.DatabasePersistenceException;
import com.kyrosoft.accounting.ServiceException;
import com.kyrosoft.accounting.dao.*;
import com.kyrosoft.accounting.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.validation.Validator;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2/9/2016.
 */
@Service
@Transactional
public class OrderDetailsQuantityTrackerServiceImpl extends BaseQuantityTracker implements OrderDetailsQuantityTrackerService {

    public OrderDetailsQuantityTrackerServiceImpl() {}

    public void createStockCheckpointForCreateOrderDetails(OrderDetails orderDetails) throws ServiceException, DatabasePersistenceException {
        Long warehouseId = null;
        Long itemId = orderDetails.getItem().getId();

        StockBalanceType stockBalanceType = StockBalanceType.QUANTITY_ON_ORDER;
        StockTransactionType stockTransactionType = StockTransactionType.PURCHASE_ORDER_CREATED;
        Date transactionDate = orderDetails.getCreatedDate();
        Long transactionId = orderDetails.getId();

        double quantity = orderDetails.getQuantity() * orderDetails.getPurchaseMeasurement().getUnit();

        StockCheckPoint stockCheckPoint = createStockCheckpointDebitStockBalance(
                warehouseId,
                itemId,
                stockBalanceType,
                stockTransactionType,
                transactionDate,
                transactionId,
                quantity
        );

    }

    public void createStockCheckpointForUpdateOrderDetails(OrderDetails orderDetailsBefore,OrderDetails orderDetails) throws ServiceException, DatabasePersistenceException {
        Long warehouseId = null;
        Long beforeItemId = orderDetailsBefore.getItem().getId();
        Long itemId = orderDetails.getItem().getId();

        StockBalanceType stockBalanceType = StockBalanceType.QUANTITY_ON_ORDER;
        StockTransactionType stockTransactionType = StockTransactionType.PURCHASE_ORDER_CREATED;
        Date transactionDate = orderDetails.getCreatedDate();
        Long transactionId = orderDetails.getId();

        if(beforeItemId==itemId) {

            double quantity = orderDetails.getQuantity() * orderDetails.getPurchaseMeasurement().getUnit();

            updateStockCheckpointDebitStockBalance(
                    warehouseId,
                    itemId,
                    stockBalanceType,
                    stockTransactionType,
                    transactionDate,
                    transactionId,
                    quantity
            );

        }
        else {
            /**
             * When item id is changed, do:
             *  1. Delete the existing checkpoint
             *  2. Update the balance with current item id
             */
            removeStockCheckpointStockBalance(
                    warehouseId,
                    beforeItemId,
                    stockBalanceType,
                    stockTransactionType,
                    transactionDate,
                    transactionId

            );

            /**
             * And then do :
             *  1. Create new stock checkpoint
             *  2. Update balance stock of current item
             */
            double quantity = orderDetails.getQuantity() * orderDetails.getPurchaseMeasurement().getUnit();

            StockCheckPoint stockCheckPoint = createStockCheckpointDebitStockBalance(
                    warehouseId,
                    itemId,
                    stockBalanceType,
                    stockTransactionType,
                    transactionDate,
                    transactionId,
                    quantity
            );
        }
    }

    public void deleteStockCheckpointForCreateOrderDetails(OrderDetails orderDetails) throws ServiceException, DatabasePersistenceException {
        Long warehouseId = null;
        Long itemId = orderDetails.getItem().getId();
        StockBalanceType stockBalanceType = StockBalanceType.QUANTITY_ON_ORDER;
        StockTransactionType stockTransactionType = StockTransactionType.PURCHASE_ORDER_CREATED;
        Date transactionDate = orderDetails.getCreatedDate();
        Long transactionId = orderDetails.getId();

        removeStockCheckpointStockBalance(
                warehouseId,
                itemId,
                stockBalanceType,
                stockTransactionType,
                transactionDate,
                transactionId

        );
    }

}
