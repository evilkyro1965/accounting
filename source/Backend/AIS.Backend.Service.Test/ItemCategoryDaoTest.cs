﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;

namespace AIS.Backend.Service.Test
{
    [TestClass]
    public class ItemCategoryDaoTest : BaseTest
    {
        private ItemCategoryService dao;

        public ItemCategoryDaoTest()
        {
            dao = new ItemCategoryServiceImpl();
        }

        [TestMethod]
        public void ItemCategoryAddTest()
        {
            ItemCategory itemCategory = new ItemCategory() { Name = StrTest, CreatedBy = StrTest, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting };

            dao.Create(itemCategory);

            ItemCategory saved = dao.Get(itemCategory.Id);
            Assert.AreEqual(StrTest, saved.Name);
        }

        [TestMethod]
        public void ItemCategoryUpdateTest()
        {
            ItemCategory itemCategory = new ItemCategory() { Name = StrTest, CreatedBy = StrTest, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting };

            dao.Create(itemCategory);

            itemCategory.Name = "lorem";

            dao.Update(itemCategory); 

            ItemCategory saved = dao.Get(itemCategory.Id);
            Assert.AreEqual("lorem", saved.Name);
        }

        [TestMethod]
        public void ItemCategoryDeleteTest()
        {
            ItemCategory itemCategory = new ItemCategory() { Name = StrTest, CreatedBy = StrTest, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting };

            dao.Create(itemCategory);

            long[] ids = new long[1];
            ids[0] = itemCategory.Id;

            dao.Delete(ids);

            ItemCategory deleted = dao.Get(ids[0]);
            Assert.IsNull(deleted);
        }


        [TestMethod]
        public void ItemCategorySearchTest()
        {
            ItemCategory itemCategory = new ItemCategory() { Name = "item1", IsActive = true, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting };
            ItemCategory itemCategory2 = new ItemCategory() { Name = "item2", IsActive = true, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting };

            dao.Create(itemCategory);
            dao.Create(itemCategory2);

            BaseSearchCriteria criteria = new BaseSearchCriteria();
            criteria.Name = "item1";
            criteria.Page = 1;
            criteria.PageSize = 1;
            criteria.SortBy = "Name";
            criteria.SortAscending = true;
            criteria.IsActive = true;
            SearchResult<ItemCategory> result = dao.Search(criteria);
            Assert.AreEqual(1, result.Items.Count);
            Assert.AreEqual("item1", result.Items[0].Name);
        }

    }
}
