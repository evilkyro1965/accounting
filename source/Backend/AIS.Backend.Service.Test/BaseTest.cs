﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;

namespace AIS.Backend.Service.Test
{
    public class BaseTest
    {
        /// <summary>
        /// String Test
        /// </summary>
        protected String StrTest;

        /// <summary>
        /// String Number = 123
        /// </summary>
        protected String StrNumber;

        /// <summary>
        /// DECIMAL VALUE 1.00
        /// </summary>
        protected const double DecimalValue = 1.00;

        /// <summary>
        /// Email Test
        /// </summary>
        protected String EmailTest;

        /// <summary>
        /// Updated String
        /// </summary>
        protected String StrUpdatedTest;

        /// <summary>
        /// String Testing
        /// </summary>
        protected String StrTesting = "testing";

        /// <summary>
        /// Decimal Testing
        /// </summary>
        protected double DecimalTest = 1.00;

        /// <summary>
        /// DateTime testing
        /// </summary>
        protected DateTime DateTimeTesting = new DateTime(2015, 6, 1, 10, 0, 0);

        private const String DBNAME = "AIS_DB";

        public String name = ConfigurationManager.AppSettings["NAME_TEST"];

        public const long OBJECT_ID = 1;

        /// <summary>
        /// Dao Context
        /// </summary>
        protected DaoContext context;

        protected ItemService itemDao;
        protected DepartmentService departmentDao;
        protected EmployeeService employeeDao;
        protected MeasurementService measurementDao;
        protected ItemCategoryService itemCategoryDao;
        protected WarehouseService warehouseDao;
        protected PurchaseService purchaseDao;

        public BaseTest()
        {
            context = new DaoContext(ConfigurationManager.AppSettings["CONNECTION_STRING_KEY"]);

            StrTest = ConfigurationManager.AppSettings["NAME_TEST"];
            StrNumber = ConfigurationManager.AppSettings["STRING_NUMBER"];
            EmailTest = ConfigurationManager.AppSettings["EMAIL_TEST"];
            StrUpdatedTest = ConfigurationManager.AppSettings["STRING_UPDATED_TEST"];
            itemDao = new ItemServiceImpl();
            departmentDao = new DepartmentServiceImpl();
            employeeDao = new EmployeeServiceImpl();
            measurementDao = new MeasurementServiceImpl();
            itemCategoryDao = new ItemCategoryServiceImpl();
            warehouseDao = new WarehouseServiceImpl();
            purchaseDao = new PurchaseServiceImpl();
        }

        [TestInitialize]
        public void TestInitiallize()
        {
            context = new DaoContext(ConfigurationManager.AppSettings["CONNECTION_STRING_KEY"]);
            context.TestInit();
        }

        public Measurement CreatePurchaseMeasurement()
        {
            Measurement purchaseMeasurement = new Measurement();
            purchaseMeasurement.Name = "lusin";
            purchaseMeasurement.ShortName = "lusin";
            purchaseMeasurement.IsDecimal = true;
            purchaseMeasurement.Unit = 1.0;
            purchaseMeasurement.Type = MeasurementType.PURCHASE;
            purchaseMeasurement.IsActive = true;
            ActiveCreatedByAndDate(purchaseMeasurement);
            measurementDao.Create(purchaseMeasurement);
            return purchaseMeasurement;
        }

        public void CreateItem()
        {
            Item item = new Item();
            item.Name = name;
            item.ItemType = ItemType.STOCKED_PRODUCT;
            item.ItemDescription = name;
            item.DefaultUnitPrice = DecimalTest;
            item.CreatedBy = name;
            item.CreatedDate = DateTimeTesting;
            item.UpdatedBy = name;
            item.UpdatedDate = DateTimeTesting;

            ItemCategory itemCategory = itemCategoryDao.Get(OBJECT_ID);
            
            MeasurementSearchCriteria criteria = new MeasurementSearchCriteria();
            criteria.Name = name;
            criteria.Page = 1;
            criteria.PageSize = 1;
            criteria.MeasurementType = MeasurementType.CALCULATION;
            criteria.IsActive = true;
            criteria.SortBy = "Name";

            Measurement calculationMeasurment = measurementDao.Search(criteria).Items[0];

            criteria.Name = name;
            criteria.MeasurementType = MeasurementType.STOCK;

            Measurement stockMeasurement = measurementDao.Search(criteria).Items[0];

            criteria.Name = name;
            criteria.MeasurementType = MeasurementType.PURCHASE;

            Measurement purchaseMeasurement = measurementDao.Search(criteria).Items[0];

            List<Measurement> purchaseMeasurementList = new List<Measurement>() { purchaseMeasurement };

            item.Category = itemCategory;
            item.CalculationMeasurement = calculationMeasurment;
            item.StockMeasurement = stockMeasurement;
            item.PurchaseMeasurementList = purchaseMeasurementList;

            itemDao.Create(item);

            Department department = new Department();
            department.Name = StrTest;
            department.Email = EmailTest;
            ActiveCreatedByAndDate(department);
            departmentDao.Create(department);
        }

        public Item CreateItem(string itemName)
        {
            Item item = new Item();
            item.Name = itemName;
            item.ItemType = ItemType.STOCKED_PRODUCT;
            item.ItemDescription = name;
            item.DefaultUnitPrice = DecimalTest;
            item.CreatedBy = name;
            item.CreatedDate = DateTimeTesting;
            item.UpdatedBy = name;
            item.UpdatedDate = DateTimeTesting;

            ItemCategory itemCategory = itemCategoryDao.Get(OBJECT_ID);

            MeasurementSearchCriteria criteria = new MeasurementSearchCriteria();
            criteria.Name = name;
            criteria.Page = 1;
            criteria.PageSize = 1;
            criteria.MeasurementType = MeasurementType.CALCULATION;
            criteria.IsActive = true;
            criteria.SortBy = "Name";

            Measurement calculationMeasurment = measurementDao.Search(criteria).Items[0];

            criteria.Name = name;
            criteria.MeasurementType = MeasurementType.STOCK;

            Measurement stockMeasurement = measurementDao.Search(criteria).Items[0];

            criteria.Name = name;
            criteria.MeasurementType = MeasurementType.PURCHASE;

            Measurement purchaseMeasurement = measurementDao.Search(criteria).Items[0];

            List<Measurement> purchaseMeasurementList = new List<Measurement>() { purchaseMeasurement };

            item.Category = itemCategory;
            item.CalculationMeasurement = calculationMeasurment;
            item.StockMeasurement = stockMeasurement;
            item.PurchaseMeasurementList = purchaseMeasurementList;

            itemDao.Create(item);

            return item;
        }

        public Purchase CreatePurchase()
        {
            Warehouse warehouse = warehouseDao.Get(OBJECT_ID);
            Employee employee = employeeDao.Get(OBJECT_ID);
            Department department = departmentDao.Get(OBJECT_ID);
            Supplier supplier = context.Suppliers.Find(OBJECT_ID);
            Item item = itemDao.Get(OBJECT_ID);
            Measurement purchaseMeasurement = CreatePurchaseMeasurement();

            Purchase purchase = new Purchase();
            purchase.No = StrNumber;
            purchase.Date = DateTime.Now;
            purchase.DeliveryDate = DateTime.Now;
            purchase.Warehouse = warehouse;
            purchase.Freight = DecimalValue;
            purchase.RequestorDepartment = department;
            purchase.Supplier = supplier;
            purchase.Remark = StrTest;
            AuditCreatedByAndDate(purchase);

            OrderDetails orderDetails = new OrderDetails()
            {
                Item = item,
                PurchaseMeasurement = purchaseMeasurement,
                Quantity = DecimalTest,
                Price = DecimalTest,
                DiscountPercent = DecimalTest,
                TaxPercent = DecimalTest,
                Warehouse = warehouse
            };
            OrderDetails orderDetails2 = new OrderDetails()
            {
                Item = item,
                PurchaseMeasurement = purchaseMeasurement,
                Quantity = DecimalTest,
                Price = DecimalTest,
                DiscountPercent = DecimalTest,
                TaxPercent = DecimalTest,
                Warehouse = warehouse
            };
            List<OrderDetails> orderDetailsList = new List<OrderDetails>() { orderDetails, orderDetails2 };
            purchase.OrderDetailList = orderDetailsList;

            purchaseDao.Create(purchase);

            return purchase;
        }

        protected static void AuditCreatedByAndDate(AuditableEntity entity)
        {
            entity.CreatedBy = "test";
            entity.CreatedDate = DateTime.Now;
            entity.UpdatedBy = "test";
            entity.UpdatedDate = DateTime.Now;
        }

        protected static void AuditCreatedByAndDate(GLAuditableEntity entity)
        {
            entity.CreatedBy = "test";
            entity.CreatedDate = DateTime.Now;
            entity.UpdatedBy = "test";
            entity.UpdatedDate = DateTime.Now;
        }

        protected static void ActiveCreatedByAndDate(ActiveEntity entity)
        {
            entity.IsActive = true;
            entity.CreatedBy = "test";
            entity.CreatedDate = DateTime.Now;
            entity.UpdatedBy = "test";
            entity.UpdatedDate = DateTime.Now;
        }

    }
}
