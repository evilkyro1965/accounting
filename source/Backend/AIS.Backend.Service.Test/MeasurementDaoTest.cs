﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;

namespace AIS.Backend.Service.Test
{
    [TestClass]
    public class MeasurementDaoTest : BaseTest
    {
        private MeasurementService dao;

        public MeasurementDaoTest()
        {
            dao = new MeasurementServiceImpl();
        }

        [TestMethod]
        public void MeasurementAddTest()
        {
            Measurement measurement = new Measurement() { Name = StrTest, ShortName = StrTest, Type = MeasurementType.CALCULATION, Unit = DecimalTest, IsDecimal = true, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting };

            dao.Create(measurement);

            Measurement saved = dao.Get(measurement.Id);

            Assert.AreEqual(StrTest, saved.Name);
            Assert.AreEqual(DecimalTest, saved.Unit);
        }

        [TestMethod]
        public void MeasurementUpdateTest()
        {
            Measurement measurement = new Measurement() { Name = StrTest, ShortName = StrTest, Type = MeasurementType.CALCULATION, Unit = DecimalTest, IsDecimal = true, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting };

            dao.Create(measurement);

            measurement.Name = "lorem";

            dao.Update(measurement);

            Measurement saved = dao.Get(measurement.Id);
            Assert.AreEqual("lorem", saved.Name);
        }

        [TestMethod]
        public void MeasurementDeleteTest()
        {
            Measurement measurement = new Measurement() { Name = StrTest, ShortName = StrTest, Type = MeasurementType.CALCULATION, Unit = DecimalTest, IsDecimal = true, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting };

            dao.Create(measurement);

            long[] ids = new long[] { measurement.Id };
            dao.Delete(ids);

            Measurement deleted = dao.Get(ids[0]);
            Assert.IsNull(deleted);
        }

        [TestMethod]
        public void MeasurementSearchTest()
        {
            Measurement measurement = new Measurement() { Name = "Measurement1", ShortName = StrTest, Type = MeasurementType.CALCULATION, Unit = DecimalTest, IsDecimal = true, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting, IsActive = true };
            Measurement measurement2 = new Measurement() { Name = "Measurement2", ShortName = StrTest, Type = MeasurementType.CALCULATION, Unit = DecimalTest, IsDecimal = true, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting, IsActive = true };

            dao.Create(measurement);
            dao.Create(measurement);

            MeasurementSearchCriteria criteria = new MeasurementSearchCriteria();
            criteria.Name = "Measurement1";
            criteria.Page = 1;
            criteria.PageSize = 1;
            criteria.SortBy = "Name";
            criteria.SortAscending = true;
            criteria.IsActive = true;
            SearchResult<Measurement> result = dao.Search(criteria);
            Assert.AreEqual(1, result.Items.Count);
            Assert.AreEqual("Measurement1", result.Items[0].Name);
        }



    }
}
