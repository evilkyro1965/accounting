﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;
using System.Configuration;

namespace AIS.Backend.Service.Test
{
    [TestClass]
    public class WarehouseDaoTest : BaseTest
    {
        public WarehouseDaoTest() { }

        [TestMethod]
        public void TestAddAndGetWarehouse()
        {
            Warehouse warehouse = new Warehouse();
            warehouse.Name = StrTest;
            warehouse.Shelf = StrTest;
            warehouse.Label = StrTest;
            warehouse.Department = departmentDao.Get(OBJECT_ID);
            ActiveCreatedByAndDate(warehouse);

            warehouseDao.Create(warehouse);

            Warehouse saved = warehouseDao.Get(warehouse.Id);
            Assert.AreEqual(StrTest, saved.Name);
            Assert.AreEqual(OBJECT_ID, saved.Department.Id);
        }

        [TestMethod]
        public void TestUpdateWarehouse()
        {
            Warehouse warehouse = new Warehouse();
            warehouse.Name = StrTest;
            warehouse.Shelf = StrTest;
            warehouse.Label = StrTest;
            warehouse.Department = departmentDao.Get(OBJECT_ID);
            ActiveCreatedByAndDate(warehouse);

            warehouseDao.Create(warehouse);

            Warehouse update = warehouseDao.Get(warehouse.Id);
            update.Name = StrUpdatedTest;

            warehouseDao.Update(update);
            update = warehouseDao.Get(warehouse.Id);

            Assert.AreEqual(StrUpdatedTest, update.Name);
            Assert.AreEqual(OBJECT_ID, update.Department.Id);
        }


        [TestMethod]
        public void TestDeactivateWarehouse()
        {
            Warehouse warehouse = new Warehouse();
            warehouse.Name = StrTest;
            warehouse.Shelf = StrTest;
            warehouse.Label = StrTest;
            warehouse.Department = departmentDao.Get(OBJECT_ID);
            ActiveCreatedByAndDate(warehouse);

            warehouseDao.Create(warehouse);

            warehouseDao.Deactivate(new long[]{warehouse.Id});

            Warehouse saved = warehouseDao.Get(warehouse.Id);

            Assert.AreEqual(false, saved.IsActive);
        }

        [TestMethod]
        public void TestSearchWarehouse()
        {
            Warehouse warehouse = new Warehouse();
            warehouse.Name = StrTest;
            warehouse.Shelf = StrTest;
            warehouse.Label = StrTest;
            warehouse.Department = departmentDao.Get(OBJECT_ID);
            ActiveCreatedByAndDate(warehouse);

            warehouseDao.Create(warehouse);

            WarehouseSearchCriteria criteria = new WarehouseSearchCriteria();
            criteria.Page = 1;
            criteria.PageSize = 1;
            criteria.Name = StrTest;
            criteria.Department = new Department() { Id = OBJECT_ID };

            SearchResult<Warehouse> searchResult = warehouseDao.Search(criteria);
            Assert.AreEqual(1, searchResult.Items.Count);
            Assert.AreEqual(StrTest, searchResult.Items[0].Name);
        }

    }
}
