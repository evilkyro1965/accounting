﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;
using System.Configuration;

namespace AIS.Backend.Service.Test
{
    [TestClass]
    public class ItemDaoTest : BaseTest
    {


        public ItemDaoTest()
        {   
            
        }

        [TestMethod]
        public void TestAddItem()
        {
            Item item = new Item();
            item.Name = name;
            item.ItemType = ItemType.STOCKED_PRODUCT;
            item.ItemDescription = name;
            item.DefaultUnitPrice = DecimalTest;
            item.CreatedBy = name;
            item.CreatedDate = DateTimeTesting;
            item.UpdatedBy = name;
            item.UpdatedDate = DateTimeTesting;

            MeasurementSearchCriteria criteria = new MeasurementSearchCriteria();
            criteria.Name = name;
            criteria.Page = 1;
            criteria.PageSize = 1;
            criteria.MeasurementType = MeasurementType.CALCULATION;
            criteria.IsActive = null;
            criteria.SortBy = "Name";

            Measurement calculationMeasurment = measurementDao.Search(criteria).Items[0];

            criteria.Name = name;
            criteria.MeasurementType = MeasurementType.STOCK;

            Measurement stockMeasurement = measurementDao.Search(criteria).Items[0];

            criteria.Name = name;
            criteria.MeasurementType = MeasurementType.PURCHASE;

            Measurement purchaseMeasurement = measurementDao.Search(criteria).Items[0];

            List<Measurement> purchaseMeasurementList = new List<Measurement>() { purchaseMeasurement };

            item.CalculationMeasurement = calculationMeasurment;
            item.StockMeasurement = stockMeasurement;
            item.PurchaseMeasurementList = purchaseMeasurementList;

            itemDao.Create(item);

            Item update = itemDao.Get(item.Id);
            Assert.AreEqual(name, update.Name);
            Assert.AreEqual(name, update.CalculationMeasurement.Name);
            Measurement[] arrMeasurement = new Measurement[update.PurchaseMeasurementList.Count];
            update.PurchaseMeasurementList.CopyTo(arrMeasurement, 0);
            Measurement purchase = arrMeasurement[0];

            String purchaseMeasurementItemName = purchase.Name;
        }

        [TestMethod]
        public void TestUpdateItem()
        {
            CreateItem();

            MeasurementSearchCriteria criteria = new MeasurementSearchCriteria();
            criteria.Page = 1;
            criteria.PageSize = 2;
            criteria.MeasurementType = MeasurementType.PURCHASE;
            criteria.IsActive = true;
            criteria.SortBy = "Name";

            Item item = itemDao.Get(OBJECT_ID);

            // Test Add Purchase Measurement List
            List<Measurement> listOfMeasurement = measurementDao.Search(criteria).Items;
            item.PurchaseMeasurementList = listOfMeasurement;

            itemDao.Update(item);

            Assert.AreEqual(2, item.PurchaseMeasurementList.Count);

            listOfMeasurement.RemoveAt(1);

            // Test Remove Purchase Measurement List
            Item item2 = itemDao.Get(OBJECT_ID);
            item2.PurchaseMeasurementList = listOfMeasurement;
            
            itemDao.Update(item2);
            Assert.AreEqual(1, item.PurchaseMeasurementList.Count);
        }

        [TestMethod]
        public void TestDeactivateItem()
        {
            CreateItem();

            itemDao.Deactivate(new long[] { OBJECT_ID });

            Item updated = itemDao.Get(OBJECT_ID);

            Assert.AreEqual(false, updated.IsActive);
        }

        [TestMethod]
        public void TestSearchItem()
        {
            CreateItem();

            MeasurementSearchCriteria measurementCriteria = new MeasurementSearchCriteria();
            measurementCriteria.Name = name;
            measurementCriteria.Page = 1;
            measurementCriteria.PageSize = 1;
            measurementCriteria.MeasurementType = MeasurementType.STOCK;
            measurementCriteria.IsActive = null;
            measurementCriteria.SortBy = "Name";

            Measurement stockMeasurement = measurementDao.Search(measurementCriteria).Items[0];

            measurementCriteria.MeasurementType = MeasurementType.CALCULATION;
            Measurement calculationMeasurement = measurementDao.Search(measurementCriteria).Items[0];

            ItemSearchCriteria criteria = new ItemSearchCriteria();
            criteria.Name = name;
            criteria.Page = 1;
            criteria.PageSize = 1;
            criteria.SortBy = "Name";
            criteria.SortAscending = true;
            criteria.Type = ItemType.STOCKED_PRODUCT;
            criteria.StockMeasurement = measurementDao.Get(stockMeasurement.Id);
            criteria.CalculationMeasurement = measurementDao.Get(calculationMeasurement.Id);
            criteria.Category = itemCategoryDao.Get(OBJECT_ID);

            SearchResult<Item> searchResult = itemDao.Search(criteria);
            Assert.AreEqual(name, searchResult.Items[0].Name);
        }

    }
}
