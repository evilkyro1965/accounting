﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;
using System.Configuration;

namespace AIS.Backend.Service.Test
{
    [TestClass]
    public class DepartmentDaoTest : BaseTest
    {

        private DepartmentService dao;

        public DepartmentDaoTest()
        {   
            dao = new DepartmentServiceImpl();
        }

        [TestMethod]
        public void TestAddDepartment()
        {
            Department department = new Department();
            department.Email = StrTest;
            department.Name = StrTest;
            department.CreatedBy = StrTest;
            department.CreatedDate = DateTimeTesting;
            department.UpdatedBy = StrTest;
            department.UpdatedDate = DateTimeTesting;
            department.IsActive = true;

            dao.Create(department);

            Department saved = dao.Get(department.Id);
            Assert.AreEqual(StrTest, saved.Name);
        }

        [TestMethod]
        public void TestUpdateDepartment()
        {
            Department department = new Department();
            department.Email = StrTest;
            department.Name = StrTest;
            department.CreatedBy = StrTest;
            department.CreatedDate = DateTimeTesting;
            department.UpdatedBy = StrTest;
            department.UpdatedDate = DateTimeTesting;
            department.IsActive = true;

            dao.Create(department);

            Department temp = dao.Get(department.Id);

            DateTime updatedDate = EntityHelper.FormattedStringToDate(ConfigurationManager.AppSettings["DATE_TEST"]);
            temp.Name = "lorem";
            temp.CreatedBy = "update";
            temp.CreatedDate = updatedDate;
            

            dao.Update(temp);
            Department updated = dao.Get(department.Id);
            Assert.AreEqual("lorem", updated.Name, "Department Name not updated");
            Assert.AreEqual("update", updated.CreatedBy, "Department Created by is wrong");
            Assert.AreEqual(updatedDate, updated.CreatedDate, "Department Created date is wrong");
        }

        [TestMethod]
        public void TestDeleteDepartment()
        {
            Department department = new Department();
            department.Email = StrTest;
            department.Name = StrTest;
            department.CreatedBy = StrTest;
            department.CreatedDate = DateTimeTesting;
            department.UpdatedBy = StrTest;
            department.UpdatedDate = DateTimeTesting;
            department.IsActive = true;

            dao.Create(department);

            long[] ids = new long[1];
            ids[0] = department.Id;

            dao.Delete(ids);

            Department deleted = dao.Get(ids[0]);
            Assert.IsNull(deleted);
        }

        [TestMethod]
        public void TestMethod4()
        {
            Department department = new Department();
            department.Email = "a";
            department.Name = "a";
            department.CreatedBy = StrTest;
            department.CreatedDate = DateTimeTesting;
            department.UpdatedBy = StrTest;
            department.UpdatedDate = DateTimeTesting;
            department.IsActive = true;

            dao.Create(department);

            Department department2 = new Department();
            department2.Email = "b";
            department2.Name = "a";
            department2.CreatedBy = StrTest;
            department2.CreatedDate = DateTimeTesting;
            department2.UpdatedBy = StrTest;
            department2.UpdatedDate = DateTimeTesting;
            department2.IsActive = true;

            dao.Create(department2);

            DepartmentSearchCriteria criteria = new DepartmentSearchCriteria();
            criteria.Name = "a";
            criteria.Page = 2;
            criteria.PageSize = 1;
            criteria.SortBy = "Name";
            criteria.SortAscending = true;
            criteria.IsActive = true;
            SearchResult<Department> result = dao.Search(criteria);
            Assert.AreEqual(1,result.Items.Count);
        }

    }
}
