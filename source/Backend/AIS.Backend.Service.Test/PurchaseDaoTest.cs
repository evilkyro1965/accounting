﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;
using System.Configuration;
using System.Linq;

namespace AIS.Backend.Service.Test
{
    [TestClass]
    public class PurchaseDaoTest : BaseTest
    {
        [TestMethod]
        public void TestAddPurchase()
        {
            Warehouse warehouse = warehouseDao.Get(OBJECT_ID);
            Employee employee = employeeDao.Get(OBJECT_ID);
            Department department = departmentDao.Get(OBJECT_ID);
            Supplier supplier = context.Suppliers.Find(OBJECT_ID);
            Item item = itemDao.Get(OBJECT_ID);
            Measurement purchaseMeasurement = CreatePurchaseMeasurement();

            Purchase purchase = new Purchase();
            purchase.No = StrNumber;
            purchase.Date = DateTime.Now;
            purchase.DeliveryDate = DateTime.Now;
            purchase.Warehouse = warehouse;
            purchase.Freight = DecimalValue;
            purchase.RequestorDepartment = department;
            purchase.Supplier = supplier;
            purchase.Remark = StrTest;
            AuditCreatedByAndDate(purchase);

            OrderDetails orderDetails = new OrderDetails()
            {
                Item = item,
                PurchaseMeasurement = purchaseMeasurement,
                Quantity = DecimalTest,
                Price = DecimalTest,
                DiscountPercent = DecimalTest,
                TaxPercent = DecimalTest,
                Warehouse = warehouse
            };
            List<OrderDetails> orderDetailsList = new List<OrderDetails>() { orderDetails };
            purchase.OrderDetailList = orderDetailsList;

            purchaseDao.Create(purchase);

            Purchase saved = purchaseDao.Get(OBJECT_ID);

            /* Convert from icollection to list */
            IList<OrderDetails> savedOrderDetails = GeneralHelper.ConvertICollectionToList(saved.OrderDetailList);

            Assert.AreEqual(1.00, savedOrderDetails[0].Quantity);
            Assert.AreEqual(OBJECT_ID, savedOrderDetails[0].Item.Id);
            Assert.AreEqual(OBJECT_ID, savedOrderDetails[0].Warehouse.Id);
        }

        /// <summary>
        /// Test UPdate Add Order and 
        /// Modified Quantity, Price
        /// </summary>
        [TestMethod]
        public void UpdatePurchaseAddOrderDetails()
        {
            CreatePurchase();

            Purchase purchase = purchaseDao.Get(OBJECT_ID);

            Item item = itemDao.Get(OBJECT_ID);
            Measurement purchaseMeasurement = measurementDao.Get(4);
            Warehouse warehouse = warehouseDao.Get(OBJECT_ID);

            OrderDetails orderDetails = new OrderDetails()
            {
                Item = item,
                PurchaseMeasurement = purchaseMeasurement,
                Quantity = DecimalTest,
                Price = DecimalTest,
                DiscountPercent = DecimalTest,
                TaxPercent = DecimalTest,
                Warehouse = warehouse
            };

            //Update Quantity to 10
            foreach (OrderDetails temp in purchase.OrderDetailList)
            {
                temp.Quantity = 10.00;
                temp.Price = 5.00;
                temp.DiscountPercent = 5.00;
                temp.TaxPercent = 2.75;
            }
            purchase.OrderDetailList.Add(orderDetails);

            purchaseDao.Update(purchase);

            Purchase updated = purchaseDao.Get(OBJECT_ID);

            /* Convert from icollection to list */
            IList<OrderDetails> updatedOrderDetails = GeneralHelper.ConvertICollectionToList(updated.OrderDetailList);

            Assert.AreEqual(10.00, updatedOrderDetails[0].Quantity);
            Assert.AreEqual(5.00, updatedOrderDetails[0].Price);
            Assert.AreEqual(5.00, updatedOrderDetails[0].DiscountPercent);
            Assert.AreEqual(2.75, updatedOrderDetails[0].TaxPercent);

            Assert.AreEqual(1.00, updatedOrderDetails[2].Quantity);
        }

        /// <summary>
        /// Test Delete Order and 
        /// Modified Quantity, Price
        /// </summary>
        [TestMethod]
        public void UpdatePurchaseAddRemoveOrderDetails()
        {
            CreatePurchase();

            Purchase purchase = purchaseDao.Get(OBJECT_ID);

            Item item = itemDao.Get(OBJECT_ID);
            Measurement purchaseMeasurement = measurementDao.Get(4);
            Warehouse warehouse = warehouseDao.Get(OBJECT_ID);
            
            OrderDetails orderDetails = new OrderDetails()
            {
                Item = item,
                PurchaseMeasurement = purchaseMeasurement,
                Quantity = DecimalTest,
                Price = DecimalTest,
                DiscountPercent = DecimalTest,
                TaxPercent = DecimalTest,
                Warehouse = warehouse
            };

            purchase.OrderDetailList.Add(orderDetails);

            /* Convert from icollection to list */
            IList<OrderDetails> orderDetailList = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);
            foreach (OrderDetails temp in purchase.OrderDetailList)
            {
                temp.Quantity = 10.00;
                temp.Price = 5.00;
                temp.DiscountPercent = 5.00;
                temp.TaxPercent = 2.75;
            }

            purchase.OrderDetailList.Remove(orderDetailList[1]);

            purchaseDao.Update(purchase);

            Purchase updated = purchaseDao.Get(OBJECT_ID);
            
            /* Convert from icollection to list */
            IList<OrderDetails> updatedOrderDetails = GeneralHelper.ConvertICollectionToList(updated.OrderDetailList);

            Assert.AreEqual(2, updated.OrderDetailList.Count);
            Assert.AreEqual(10.00, updatedOrderDetails[0].Quantity);
            Assert.AreEqual(5.00, updatedOrderDetails[0].Price);
            Assert.AreEqual(5.00, updatedOrderDetails[0].DiscountPercent);
            Assert.AreEqual(2.75, updatedOrderDetails[0].TaxPercent);
        }

        /// <summary>
        /// Add Purchase and check stock tracker
        /// </summary>
        [TestMethod]
        public void TestAddPurchaseAndCheckStockTracker()
        {
            Warehouse warehouse = warehouseDao.Get(OBJECT_ID);
            Employee employee = employeeDao.Get(OBJECT_ID);
            Department department = departmentDao.Get(OBJECT_ID);
            Supplier supplier = context.Suppliers.Find(OBJECT_ID);
            Item item = itemDao.Get(OBJECT_ID);
            Measurement purchaseMeasurement = CreatePurchaseMeasurement();

            Purchase purchase = new Purchase();
            purchase.No = StrNumber;
            purchase.Date = DateTime.Now;
            purchase.DeliveryDate = DateTime.Now;
            purchase.Warehouse = warehouse;
            purchase.Freight = DecimalValue;
            purchase.RequestorDepartment = department;
            purchase.Supplier = supplier;
            purchase.Remark = StrTest;
            AuditCreatedByAndDate(purchase);

            OrderDetails orderDetails = new OrderDetails()
            {
                Item = item,
                PurchaseMeasurement = purchaseMeasurement,
                Quantity = DecimalTest,
                Price = DecimalTest,
                DiscountPercent = DecimalTest,
                TaxPercent = DecimalTest,
                Warehouse = warehouse
            };
            OrderDetails orderDetails2 = new OrderDetails()
            {
                Item = item,
                PurchaseMeasurement = purchaseMeasurement,
                Quantity = DecimalTest,
                Price = DecimalTest,
                DiscountPercent = DecimalTest,
                TaxPercent = DecimalTest,
                Warehouse = warehouse
            };
            List<OrderDetails> orderDetailsList = new List<OrderDetails>() { orderDetails, orderDetails2 };
            purchase.OrderDetailList = orderDetailsList;

            purchaseDao.Create(purchase);

            Purchase saved = purchaseDao.Get(purchase.Id);

            /* Convert from icollection to list */
            IList<OrderDetails> savedOrderDetails = GeneralHelper.ConvertICollectionToList(saved.OrderDetailList);

            Assert.AreEqual(1.00, savedOrderDetails[0].Quantity);
            Assert.AreEqual(OBJECT_ID, savedOrderDetails[0].Item.Id);

            long itemId = savedOrderDetails[0].Item.Id;

            Purchase purchaseSaved = context.Purchase
                        .Include("Warehouse")
                        .Include("RequestorDepartment")
                        .Include("Supplier")
                        .Include("OrderDetailList")
                        .Include("OrderDetailList.Item")
                        .Include("OrderDetailList.PurchaseMeasurement")
                        .Where(p => p.Id == purchase.Id)
                        .First();

            long warehouseId = purchaseSaved.Warehouse.Id;

            StockAccount stockAccount = context.StockAccount
                .Where(s => s.Item.Id == itemId && s.Warehouse.Id == warehouseId)
                .FirstOrDefault<StockAccount>();

            Assert.IsNotNull(stockAccount);

            List<StockCheckpoint> stockCheckpoints = context.StockCheckpoint
                .Where(
                    s => s.StockAccount.Item.Id == itemId 
                    && s.TransactionType == StockTransactionType.PURCHASE_ORDER_CREATED
                    )
                .ToList();

            var stockCheckpoint = stockCheckpoints[0];
            var stockCheckpoint2 = stockCheckpoints[1];

            Assert.IsNotNull(stockCheckpoint);
            Assert.AreEqual(savedOrderDetails[0].Id,stockCheckpoint.TransactionEntityId);
            Assert.AreEqual(stockAccount.Id, stockCheckpoint.StockAccount.Id);
            Assert.AreEqual(1.00, stockCheckpoint.Amount);
            Assert.AreEqual(1.00, stockCheckpoint.BalanceAfter);

            Assert.IsNotNull(stockCheckpoint2);
            Assert.AreEqual(savedOrderDetails[1].Id, stockCheckpoint2.TransactionEntityId);
            Assert.AreEqual(stockAccount.Id, stockCheckpoint2.StockAccount.Id);
            Assert.AreEqual(1.00, stockCheckpoint2.Amount);
            Assert.AreEqual(2.00, stockCheckpoint2.BalanceAfter);

            StockBalance stockBalance = context.StockBalance
                .Where(s => s.Id == 1)
                .FirstOrDefault<StockBalance>();

            Assert.IsNotNull(stockBalance);
            Assert.AreEqual(2.00, stockBalance.Balance);
        }

        /// <summary>
        /// Add Purchase and remove order details and check stock tracker
        /// </summary>
        [TestMethod]
        public void UpdatePurchaseAddRemoveOrderDetailsWithStockTransaction()
        {
            CreatePurchase();

            Purchase purchase = purchaseDao.Get(OBJECT_ID);

            Warehouse warehouse = warehouseDao.Get(OBJECT_ID);

            Measurement purchaseMeasurement = measurementDao.Get(4);

            /* Convert from icollection to list */
            IList<OrderDetails> orderDetailList = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);
            purchase.OrderDetailList.Remove(orderDetailList[1]);

            purchaseDao.Update(purchase);

            Purchase updated = purchaseDao.Get(OBJECT_ID);

            /* Convert from icollection to list */
            IList<OrderDetails> updatedOrderDetails = GeneralHelper.ConvertICollectionToList(updated.OrderDetailList);

            long itemId = updatedOrderDetails[0].Item.Id;
            long warehouseId = warehouse.Id;

            StockAccount stockAccount = context.StockAccount
                .Where(s => s.Item.Id == itemId && s.Warehouse.Id == warehouseId)
                .FirstOrDefault<StockAccount>();

            Assert.IsNotNull(stockAccount);

            List<StockCheckpoint> stockCheckpoints = context.StockCheckpoint
                .Where(
                    s => s.StockAccount.Item.Id == itemId
                    && s.TransactionType == StockTransactionType.PURCHASE_ORDER_CREATED
                    )
                .ToList();

            var stockCheckpoint = stockCheckpoints[0];

            Assert.AreEqual(1, stockCheckpoints.Count);
            Assert.IsNotNull(stockCheckpoint);
            Assert.AreEqual(updatedOrderDetails[0].Id, stockCheckpoint.TransactionEntityId);
            Assert.AreEqual(stockAccount.Id, stockCheckpoint.StockAccount.Id);
            Assert.AreEqual(1.00, stockCheckpoint.Amount);
            Assert.AreEqual(1.00, stockCheckpoint.BalanceAfter);


            StockBalance stockBalance = context.StockBalance
                .Where(s => s.Id == stockAccount.Id)
                .FirstOrDefault<StockBalance>();

            Assert.IsNotNull(stockBalance);
            Assert.AreEqual(1.00, stockBalance.Balance);

        }

        /// <summary>
        /// Add Purchase and change order details with other item 
        /// and check stock tracker
        /// </summary>
        [TestMethod]
        public void UpdatePurchaseAddChangeOrderDetailsWithStockTransaction()
        {
            CreatePurchase();

            Purchase purchase = purchaseDao.Get(OBJECT_ID);

            Warehouse warehouse = warehouseDao.Get(OBJECT_ID);

            Measurement purchaseMeasurement = measurementDao.Get(4);

            Item item = CreateItem("item1");

            /* Convert from icollection to list */
            IList<OrderDetails> orderDetailList = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);
            orderDetailList[1].Item = item;
            orderDetailList[1].Quantity = 2.00;

            purchase.OrderDetailList = orderDetailList;

            purchaseDao.Update(purchase);

            Purchase updated = purchaseDao.Get(OBJECT_ID);

            /* Convert from icollection to list */
            IList<OrderDetails> updatedOrderDetails = GeneralHelper.ConvertICollectionToList(updated.OrderDetailList);

            long itemId = updatedOrderDetails[0].Item.Id;
            long itemId2 = updatedOrderDetails[1].Item.Id;
            long warehouseId = warehouse.Id;

            StockAccount stockAccount = context.StockAccount
                .Where(s => s.Item.Id == itemId && s.Warehouse.Id == warehouseId)
                .FirstOrDefault<StockAccount>();

            StockAccount stockAccount2 = context.StockAccount
                .Where(s => s.Item.Id == itemId2 && s.Warehouse.Id == warehouseId)
                .FirstOrDefault<StockAccount>();

            Assert.IsNotNull(stockAccount);

            List<StockCheckpoint> stockCheckpoints = context.StockCheckpoint
                .Where(
                    s => s.TransactionType == StockTransactionType.PURCHASE_ORDER_CREATED
                    )
                .ToList();

            var stockCheckpoint = stockCheckpoints[0];
            var stockCheckpoint2 = stockCheckpoints[1];

            Assert.AreEqual(2, stockCheckpoints.Count);
            Assert.IsNotNull(stockCheckpoint);
            Assert.AreEqual(updatedOrderDetails[0].Id, stockCheckpoint.TransactionEntityId);
            Assert.AreEqual(stockAccount.Id, stockCheckpoint.StockAccount.Id);
            Assert.AreEqual(1.00, stockCheckpoint.Amount);
            Assert.AreEqual(1.00, stockCheckpoint.BalanceAfter);

            Assert.IsNotNull(stockCheckpoint2);
            Assert.AreEqual(updatedOrderDetails[1].Id, stockCheckpoint2.TransactionEntityId);
            Assert.AreEqual(stockAccount2.Id, stockCheckpoint2.StockAccount.Id);
            Assert.AreEqual(2.00, stockCheckpoint2.Amount);
            Assert.AreEqual(2.00, stockCheckpoint2.BalanceAfter);


            StockBalance stockBalance = context.StockBalance
                .Where(s => s.Id == stockAccount.Id)
                .FirstOrDefault<StockBalance>();

            Assert.IsNotNull(stockBalance);
            Assert.AreEqual(1.00, stockBalance.Balance);

        }

        /// <summary>
        /// Add Purchase and change order details quantity 
        /// and check stock tracker
        /// </summary>
        [TestMethod]
        public void UpdatePurchaseAddChangeOrderDetailsQuantityWithStockTransaction()
        {
            CreatePurchase();

            Purchase purchase = purchaseDao.Get(OBJECT_ID);

            Warehouse warehouse = warehouseDao.Get(OBJECT_ID);

            Measurement purchaseMeasurement = measurementDao.Get(4);


            /* Convert from icollection to list */
            IList<OrderDetails> orderDetailList = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);
            orderDetailList[0].Quantity = 2.00;
            orderDetailList[1].Quantity = 3.00;

            purchase.OrderDetailList = orderDetailList;

            purchaseDao.Update(purchase);

            Purchase updated = purchaseDao.Get(OBJECT_ID);

            /* Convert from icollection to list */
            IList<OrderDetails> updatedOrderDetails = GeneralHelper.ConvertICollectionToList(updated.OrderDetailList);

            long itemId = updatedOrderDetails[0].Item.Id;
            long warehouseId = warehouse.Id;

            StockAccount stockAccount = context.StockAccount
                .Where(s => s.Item.Id == itemId && s.Warehouse.Id == warehouseId)
                .FirstOrDefault<StockAccount>();

            Assert.IsNotNull(stockAccount);

            List<StockCheckpoint> stockCheckpoints = context.StockCheckpoint
                .Where(
                    s => s.TransactionType == StockTransactionType.PURCHASE_ORDER_CREATED
                    )
                .ToList();

            var stockCheckpoint = stockCheckpoints[0];
            var stockCheckpoint2 = stockCheckpoints[1];

            Assert.AreEqual(2, stockCheckpoints.Count);
            Assert.IsNotNull(stockCheckpoint);
            Assert.AreEqual(updatedOrderDetails[0].Id, stockCheckpoint.TransactionEntityId);
            Assert.AreEqual(stockAccount.Id, stockCheckpoint.StockAccount.Id);
            Assert.AreEqual(2.00, stockCheckpoint.Amount);
            Assert.AreEqual(2.00, stockCheckpoint.BalanceAfter);

            Assert.IsNotNull(stockCheckpoint2);
            Assert.AreEqual(updatedOrderDetails[1].Id, stockCheckpoint2.TransactionEntityId);
            Assert.AreEqual(stockAccount.Id, stockCheckpoint2.StockAccount.Id);
            Assert.AreEqual(3.00, stockCheckpoint2.Amount);
            Assert.AreEqual(5.00, stockCheckpoint2.BalanceAfter);


            StockBalance stockBalance = context.StockBalance
                .Where(s => s.Id == stockAccount.Id)
                .FirstOrDefault<StockBalance>();

            Assert.IsNotNull(stockBalance);
            Assert.AreEqual(5.00, stockBalance.Balance);

        }



    }
}
