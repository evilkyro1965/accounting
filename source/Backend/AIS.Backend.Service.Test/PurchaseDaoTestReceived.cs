﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;
using System.Configuration;
using System.Linq;

namespace AIS.Backend.Service.Test
{
    [TestClass]
    public class PurchaseDaoTestReceived : BaseTest
    {
        [TestMethod]
        public void TestUpdateReceiveItemStockTracker()
        {
            CreatePurchase();
            Purchase purchase = purchaseDao.Get(OBJECT_ID);

            /* Convert from icollection to list */
            IList<OrderDetails> orderDetailList = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);
            orderDetailList[0].OrderReceivedQuantity = 1;
            orderDetailList[1].OrderReceivedQuantity = 1;

            purchase.OrderDetailList = orderDetailList;

            purchaseDao.UpdateReceivedStock(purchase);

            long itemId = orderDetailList[0].Item.Id;
            long warehouseId = orderDetailList[0].Warehouse.Id;

            StockAccount stockAccountOnHand = context.StockAccount
                .Where(s => 
                    s.Item.Id == itemId && 
                    s.Warehouse.Id == warehouseId &&
                    s.Type == StockAccountType.QUANTITY_ON_HAND
                    )
                .FirstOrDefault<StockAccount>();

            StockAccount stockAccountOnOrder = context.StockAccount
                .Where(s =>
                    s.Item.Id == itemId &&
                    s.Warehouse.Id == warehouseId &&
                    s.Type == StockAccountType.QUANTITY_ON_ORDER
                    )
                .FirstOrDefault<StockAccount>();

            List<StockCheckpoint> stockCheckpointsOnHand = context.StockCheckpoint
                .Where(
                    s => s.StockAccount.Id == stockAccountOnHand.Id &&
                        s.TransactionType == StockTransactionType.PURCHASE_ORDER_RECEIVE
                    )
                .ToList();

            StockCheckpoint stockCheckpointOnHand = stockCheckpointsOnHand[0];
            StockCheckpoint stockCheckpointOnHand2 = stockCheckpointsOnHand[1];

            Assert.AreEqual(2, stockCheckpointsOnHand.Count);

            /**
             * Check Stock Check Point On Hand
             */
            Assert.AreEqual(orderDetailList[0].Id, stockCheckpointOnHand.TransactionEntityId);
            Assert.AreEqual(stockAccountOnHand.Id, stockCheckpointOnHand.StockAccount.Id);
            Assert.AreEqual(1.00, stockCheckpointOnHand.Amount);
            Assert.AreEqual(1.00, stockCheckpointOnHand.BalanceAfter);

            Assert.AreEqual(orderDetailList[1].Id, stockCheckpointOnHand2.TransactionEntityId);
            Assert.AreEqual(stockAccountOnHand.Id, stockCheckpointOnHand2.StockAccount.Id);
            Assert.AreEqual(1.00, stockCheckpointOnHand2.Amount);
            Assert.AreEqual(2.00, stockCheckpointOnHand2.BalanceAfter);

            List<StockCheckpoint> stockCheckpointsOnOrder = context.StockCheckpoint
                .Where(
                    s => s.StockAccount.Id == stockAccountOnOrder.Id &&
                        s.TransactionType == StockTransactionType.PURCHASE_ORDER_RECEIVE
                    )
                .ToList();

            StockCheckpoint stockCheckpointOnOrder = stockCheckpointsOnOrder[0];
            StockCheckpoint stockCheckpointOnOrder2 = stockCheckpointsOnOrder[1];

            Assert.AreEqual(2, stockCheckpointsOnOrder.Count);

            /**
             * Check Stock Check Point On Order
             */
            Assert.AreEqual(orderDetailList[0].Id, stockCheckpointOnOrder.TransactionEntityId);
            Assert.AreEqual(stockAccountOnOrder.Id, stockCheckpointOnOrder.StockAccount.Id);
            Assert.AreEqual(-1.00, stockCheckpointOnOrder.Amount);
            Assert.AreEqual(1.00, stockCheckpointOnOrder.BalanceAfter);

            Assert.AreEqual(orderDetailList[1].Id, stockCheckpointOnOrder2.TransactionEntityId);
            Assert.AreEqual(stockAccountOnOrder.Id, stockCheckpointOnOrder2.StockAccount.Id);
            Assert.AreEqual(-1.00, stockCheckpointOnOrder2.Amount);
            Assert.AreEqual(0.00, stockCheckpointOnOrder2.BalanceAfter);

            /**
             * Check Stock Balance
             */
            StockBalance stockBalanceOnHand = context.StockBalance
                .Where(s => s.StockAccount.Id == stockAccountOnHand.Id)
                .FirstOrDefault<StockBalance>();

            Assert.AreEqual(2.00, stockBalanceOnHand.Balance);

            StockBalance stockBalanceOnOrder = context.StockBalance
                .Where(s => s.StockAccount.Id == stockAccountOnOrder.Id)
                .FirstOrDefault<StockBalance>();

            Assert.AreEqual(0.00, stockBalanceOnOrder.Balance);
            
        }

        [TestMethod]
        public void TestAddPurchaseAndCheckStockTracker()
        {
            CreatePurchase();
            Purchase purchase = purchaseDao.Get(OBJECT_ID);

            /* Convert from icollection to list */
            IList<OrderDetails> orderDetailListUpdateQuantity = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);
            orderDetailListUpdateQuantity[0].Quantity = 2;
            orderDetailListUpdateQuantity[1].Quantity = 3;

            purchase.OrderDetailList = orderDetailListUpdateQuantity;

            purchaseDao.Update(purchase);

            /* Convert from icollection to list */
            IList<OrderDetails> orderDetailList = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);
            orderDetailList[0].OrderReceivedQuantity = 1;
            orderDetailList[1].OrderReceivedQuantity = 1;

            purchase.OrderDetailList = orderDetailList;

            purchaseDao.UpdateReceivedStock(purchase);

            /* Convert from icollection to list */
            orderDetailList = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);
            orderDetailList[0].OrderReceivedQuantity = 2;
            orderDetailList[1].OrderReceivedQuantity = 3;

            purchase.OrderDetailList = orderDetailList;

            purchaseDao.UpdateReceivedStock(purchase);

            long itemId = orderDetailList[0].Item.Id;
            long warehouseId = orderDetailList[0].Warehouse.Id;

            StockAccount stockAccountOnHand = context.StockAccount
                .Where(s =>
                    s.Item.Id == itemId &&
                    s.Warehouse.Id == warehouseId &&
                    s.Type == StockAccountType.QUANTITY_ON_HAND
                    )
                .FirstOrDefault<StockAccount>();

            StockAccount stockAccountOnOrder = context.StockAccount
                .Where(s =>
                    s.Item.Id == itemId &&
                    s.Warehouse.Id == warehouseId &&
                    s.Type == StockAccountType.QUANTITY_ON_ORDER
                    )
                .FirstOrDefault<StockAccount>();

            List<StockCheckpoint> stockCheckpointsOnHand = context.StockCheckpoint
                .Where(
                    s => s.StockAccount.Id == stockAccountOnHand.Id &&
                        s.TransactionType == StockTransactionType.PURCHASE_ORDER_RECEIVE
                    )
                .ToList();

            StockCheckpoint stockCheckpointOnHand = stockCheckpointsOnHand[0];
            StockCheckpoint stockCheckpointOnHand2 = stockCheckpointsOnHand[1];

            Assert.AreEqual(2, stockCheckpointsOnHand.Count);

            /**
             * Check Stock Check Point On Hand
             */
            Assert.AreEqual(orderDetailList[0].Id, stockCheckpointOnHand.TransactionEntityId);
            Assert.AreEqual(stockAccountOnHand.Id, stockCheckpointOnHand.StockAccount.Id);
            Assert.AreEqual(2.00, stockCheckpointOnHand.Amount);
            Assert.AreEqual(2.00, stockCheckpointOnHand.BalanceAfter);

            Assert.AreEqual(orderDetailList[1].Id, stockCheckpointOnHand2.TransactionEntityId);
            Assert.AreEqual(stockAccountOnHand.Id, stockCheckpointOnHand2.StockAccount.Id);
            Assert.AreEqual(3.00, stockCheckpointOnHand2.Amount);
            Assert.AreEqual(5.00, stockCheckpointOnHand2.BalanceAfter);

            List<StockCheckpoint> stockCheckpointsOnOrder = context.StockCheckpoint
                .Where(
                    s => s.StockAccount.Id == stockAccountOnOrder.Id &&
                        s.TransactionType == StockTransactionType.PURCHASE_ORDER_RECEIVE
                    )
                .ToList();

            StockCheckpoint stockCheckpointOnOrder = stockCheckpointsOnOrder[0];
            StockCheckpoint stockCheckpointOnOrder2 = stockCheckpointsOnOrder[1];

            Assert.AreEqual(2, stockCheckpointsOnOrder.Count);

            /**
             * Check Stock Check Point On Order
             */
            Assert.AreEqual(orderDetailList[0].Id, stockCheckpointOnOrder.TransactionEntityId);
            Assert.AreEqual(stockAccountOnOrder.Id, stockCheckpointOnOrder.StockAccount.Id);
            Assert.AreEqual(-2.00, stockCheckpointOnOrder.Amount);
            Assert.AreEqual(3.00, stockCheckpointOnOrder.BalanceAfter);

            Assert.AreEqual(orderDetailList[1].Id, stockCheckpointOnOrder2.TransactionEntityId);
            Assert.AreEqual(stockAccountOnOrder.Id, stockCheckpointOnOrder2.StockAccount.Id);
            Assert.AreEqual(-3.00, stockCheckpointOnOrder2.Amount);
            Assert.AreEqual(0.00, stockCheckpointOnOrder2.BalanceAfter);

            /**
             * Check stock balance in on hand and on order
             */
            StockBalance stockBalanceOnHand = context.StockBalance
                .Where(s => s.Id == stockAccountOnHand.Id)
                .FirstOrDefault<StockBalance>();

            Assert.AreEqual(5.00, stockBalanceOnHand.Balance);

            StockBalance stockBalanceOnOrder = context.StockBalance
                .Where(s => s.Id == stockAccountOnOrder.Id)
                .FirstOrDefault<StockBalance>();

            Assert.AreEqual(0.00, stockBalanceOnOrder.Balance);

        }

        [TestMethod]
        public void TestAddPurchaseAndReturnQtyStockTracker()
        {
            CreatePurchase();
            Purchase purchase = purchaseDao.Get(OBJECT_ID);

            /* Convert from icollection to list */
            IList<OrderDetails> orderDetailListUpdateQuantity = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);
            orderDetailListUpdateQuantity[0].Quantity = 2;
            orderDetailListUpdateQuantity[1].Quantity = 3;

            purchase.OrderDetailList = orderDetailListUpdateQuantity;

            purchaseDao.Update(purchase);

            /* Convert from icollection to list */
            IList<OrderDetails> orderDetailList = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);

            /* Convert from icollection to list */
            orderDetailList = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);
            orderDetailList[0].ReturnQuantity = 2;
            orderDetailList[1].ReturnQuantity = 3;

            purchase.OrderDetailList = orderDetailList;

            purchaseDao.UpdateReturnStock(purchase);

            long itemId = orderDetailList[0].Item.Id;
            long warehouseId = orderDetailList[0].Warehouse.Id;

            StockAccount stockAccountOnHand = context.StockAccount
                .Where(s =>
                    s.Item.Id == itemId &&
                    s.Warehouse.Id == warehouseId &&
                    s.Type == StockAccountType.QUANTITY_ON_HAND
                    )
                .FirstOrDefault<StockAccount>();

            StockAccount stockAccountOnReturn = context.StockAccount
                .Where(s =>
                    s.Item.Id == itemId &&
                    s.Warehouse.Id == warehouseId &&
                    s.Type == StockAccountType.QUANTITY_ON_RETURN
                    )
                .FirstOrDefault<StockAccount>();

            List<StockCheckpoint> stockCheckpointsOnHand = context.StockCheckpoint
                .Where(
                    s => s.StockAccount.Id == stockAccountOnHand.Id &&
                        s.TransactionType == StockTransactionType.PURCHASE_ORDER_RETURN
                    )
                .ToList();

            StockCheckpoint stockCheckpointOnHand = stockCheckpointsOnHand[0];
            StockCheckpoint stockCheckpointOnHand2 = stockCheckpointsOnHand[1];

            Assert.AreEqual(2, stockCheckpointsOnHand.Count);

            /**
             * Check Stock Check Point On Hand
             */
            Assert.AreEqual(orderDetailList[0].Id, stockCheckpointOnHand.TransactionEntityId);
            Assert.AreEqual(stockAccountOnHand.Id, stockCheckpointOnHand.StockAccount.Id);
            Assert.AreEqual(-2.00, stockCheckpointOnHand.Amount);
            Assert.AreEqual(-2.00, stockCheckpointOnHand.BalanceAfter);

            Assert.AreEqual(orderDetailList[1].Id, stockCheckpointOnHand2.TransactionEntityId);
            Assert.AreEqual(stockAccountOnHand.Id, stockCheckpointOnHand2.StockAccount.Id);
            Assert.AreEqual(-3.00, stockCheckpointOnHand2.Amount);
            Assert.AreEqual(-5.00, stockCheckpointOnHand2.BalanceAfter);

            List<StockCheckpoint> stockCheckpointsOnReturn = context.StockCheckpoint
                .Where(
                    s => s.StockAccount.Id == stockAccountOnReturn.Id &&
                        s.TransactionType == StockTransactionType.PURCHASE_ORDER_RETURN
                    )
                .ToList();

            StockCheckpoint stockCheckpointOnReturn = stockCheckpointsOnReturn[0];
            StockCheckpoint stockCheckpointOnReturn2 = stockCheckpointsOnReturn[1];

            Assert.AreEqual(2, stockCheckpointsOnReturn.Count);


            /**
             * Check Stock Check Point On Return
             */
            Assert.AreEqual(orderDetailList[0].Id, stockCheckpointOnReturn.TransactionEntityId);
            Assert.AreEqual(stockAccountOnReturn.Id, stockCheckpointOnReturn.StockAccount.Id);
            Assert.AreEqual(2.00, stockCheckpointOnReturn.Amount);
            Assert.AreEqual(2.00, stockCheckpointOnReturn.BalanceAfter);

            Assert.AreEqual(orderDetailList[1].Id, stockCheckpointOnReturn2.TransactionEntityId);
            Assert.AreEqual(stockAccountOnReturn.Id, stockCheckpointOnReturn2.StockAccount.Id);
            Assert.AreEqual(3.00, stockCheckpointOnReturn2.Amount);
            Assert.AreEqual(5.00, stockCheckpointOnReturn2.BalanceAfter);

            /**
             * Check stock balance in on hand and on order
             */
            StockBalance stockBalanceOnHand = context.StockBalance
                .Where(s => s.Id == stockAccountOnHand.Id)
                .FirstOrDefault<StockBalance>();

            Assert.AreEqual(-5.00, stockBalanceOnHand.Balance);

            StockBalance stockBalanceOnReturn = context.StockBalance
                .Where(s => s.Id == stockAccountOnReturn.Id)
                .FirstOrDefault<StockBalance>();

            Assert.AreEqual(5.00, stockBalanceOnReturn.Balance);

        }

        [TestMethod]
        public void TestAddPurchaseAndReturnQtyStockTracker2()
        {
            CreatePurchase();
            Purchase purchase = purchaseDao.Get(OBJECT_ID);

            /* Convert from icollection to list */
            IList<OrderDetails> orderDetailListUpdateQuantity = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);
            orderDetailListUpdateQuantity[0].Quantity = 2;
            orderDetailListUpdateQuantity[1].Quantity = 3;

            purchase.OrderDetailList = orderDetailListUpdateQuantity;

            purchaseDao.Update(purchase);

            /* Convert from icollection to list */
            IList<OrderDetails> orderDetailList = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);

            /* Convert from icollection to list */
            orderDetailList = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);
            orderDetailList[0].ReturnQuantity = 1;
            orderDetailList[1].ReturnQuantity = 1;

            purchase.OrderDetailList = orderDetailList;

            purchaseDao.UpdateReturnStock(purchase);

            /* Convert from icollection to list */
            orderDetailList = GeneralHelper.ConvertICollectionToList(purchase.OrderDetailList);
            orderDetailList[0].ReturnQuantity = 2;
            orderDetailList[1].ReturnQuantity = 3;

            purchase.OrderDetailList = orderDetailList;

            purchaseDao.UpdateReturnStock(purchase);

            long itemId = orderDetailList[0].Item.Id;
            long warehouseId = orderDetailList[0].Warehouse.Id;

            StockAccount stockAccountOnHand = context.StockAccount
                .Where(s =>
                    s.Item.Id == itemId &&
                    s.Warehouse.Id == warehouseId &&
                    s.Type == StockAccountType.QUANTITY_ON_HAND
                    )
                .FirstOrDefault<StockAccount>();

            StockAccount stockAccountOnReturn = context.StockAccount
                .Where(s =>
                    s.Item.Id == itemId &&
                    s.Warehouse.Id == warehouseId &&
                    s.Type == StockAccountType.QUANTITY_ON_RETURN
                    )
                .FirstOrDefault<StockAccount>();

            List<StockCheckpoint> stockCheckpointsOnHand = context.StockCheckpoint
                .Where(
                    s => s.StockAccount.Id == stockAccountOnHand.Id &&
                        s.TransactionType == StockTransactionType.PURCHASE_ORDER_RETURN
                    )
                .ToList();

            StockCheckpoint stockCheckpointOnHand = stockCheckpointsOnHand[0];
            StockCheckpoint stockCheckpointOnHand2 = stockCheckpointsOnHand[1];

            Assert.AreEqual(2, stockCheckpointsOnHand.Count);

            /**
             * Check Stock Check Point On Hand
             */
            Assert.AreEqual(orderDetailList[0].Id, stockCheckpointOnHand.TransactionEntityId);
            Assert.AreEqual(stockAccountOnHand.Id, stockCheckpointOnHand.StockAccount.Id);
            Assert.AreEqual(-2.00, stockCheckpointOnHand.Amount);
            Assert.AreEqual(-2.00, stockCheckpointOnHand.BalanceAfter);

            Assert.AreEqual(orderDetailList[1].Id, stockCheckpointOnHand2.TransactionEntityId);
            Assert.AreEqual(stockAccountOnHand.Id, stockCheckpointOnHand2.StockAccount.Id);
            Assert.AreEqual(-3.00, stockCheckpointOnHand2.Amount);
            Assert.AreEqual(-5.00, stockCheckpointOnHand2.BalanceAfter);

            List<StockCheckpoint> stockCheckpointsOnReturn = context.StockCheckpoint
                .Where(
                    s => s.StockAccount.Id == stockAccountOnReturn.Id &&
                        s.TransactionType == StockTransactionType.PURCHASE_ORDER_RETURN
                    )
                .ToList();

            StockCheckpoint stockCheckpointOnReturn = stockCheckpointsOnReturn[0];
            StockCheckpoint stockCheckpointOnReturn2 = stockCheckpointsOnReturn[1];

            Assert.AreEqual(2, stockCheckpointsOnReturn.Count);


            /**
             * Check Stock Check Point On Return
             */
            Assert.AreEqual(orderDetailList[0].Id, stockCheckpointOnReturn.TransactionEntityId);
            Assert.AreEqual(stockAccountOnReturn.Id, stockCheckpointOnReturn.StockAccount.Id);
            Assert.AreEqual(2.00, stockCheckpointOnReturn.Amount);
            Assert.AreEqual(2.00, stockCheckpointOnReturn.BalanceAfter);

            Assert.AreEqual(orderDetailList[1].Id, stockCheckpointOnReturn2.TransactionEntityId);
            Assert.AreEqual(stockAccountOnReturn.Id, stockCheckpointOnReturn2.StockAccount.Id);
            Assert.AreEqual(3.00, stockCheckpointOnReturn2.Amount);
            Assert.AreEqual(5.00, stockCheckpointOnReturn2.BalanceAfter);

            /**
             * Check stock balance in on hand and on order
             */
            StockBalance stockBalanceOnHand = context.StockBalance
                .Where(s => s.Id == stockAccountOnHand.Id)
                .FirstOrDefault<StockBalance>();

            Assert.AreEqual(-5.00, stockBalanceOnHand.Balance);

            StockBalance stockBalanceOnReturn = context.StockBalance
                .Where(s => s.Id == stockAccountOnReturn.Id)
                .FirstOrDefault<StockBalance>();

            Assert.AreEqual(5.00, stockBalanceOnReturn.Balance);

        }

    }
}
