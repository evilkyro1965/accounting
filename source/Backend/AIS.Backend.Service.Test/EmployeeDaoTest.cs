﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;

namespace AIS.Backend.Service.Test
{
    /// <summary>
    /// Summary description for EmployeeDaoTest
    /// </summary>
    [TestClass]
    public class EmployeeDaoTest : BaseTest
    {
        private EmployeeService dao;

        private UnitOfWork unitOfWork;

        public EmployeeDaoTest()
        {
            dao = new EmployeeServiceImpl();
        }

        [TestMethod]
        public void EmployeeAddTest()
        {
            Employee employee = new Employee() { Username = StrTest, Password = StrTest, Mobile = StrTest, Email = StrTest, CreatedBy = StrTest, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting };
            Department department = new Department() { Id = OBJECT_ID };
            employee.Department = department;
            dao.Create(employee);

            Employee saved = dao.Get(employee.Id);
            Assert.AreEqual(StrTest, saved.Username);
            Assert.AreEqual(OBJECT_ID, saved.Department.Id);
        }

        [TestMethod]
        public void EmployeeUpdateTest()
        {
            Employee employee = new Employee() { Username = StrTest, Password = StrTest, Mobile = StrTest, Email = StrTest, CreatedBy = StrTest, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting };

            dao.Create(employee);

            employee.Username = "lorem";
            Department department = new Department() { Id = OBJECT_ID };
            employee.Department = department;
            dao.Update(employee);

            Employee saved = dao.Get(employee.Id);
            Assert.AreEqual("lorem", saved.Username);
            Assert.AreEqual(OBJECT_ID, saved.Department.Id);
        }

        [TestMethod]
        public void EmployeeDeleteTest()
        {
            Employee employee = new Employee() { Username = StrTest, Password = StrTest, Mobile = StrTest, Email = StrTest, CreatedBy = StrTest, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting };

            dao.Create(employee);

            long[] ids = new long[1];
            ids[0] = employee.Id;

            dao.Delete(ids);

            Employee deleted = dao.Get(ids[0]);
            Assert.IsNull(deleted);
        }

        [TestMethod]
        public void EmployeeSearchTest()
        {
            Employee employee = new Employee() { Username = "user1", IsActive = true, Password = StrTest, Mobile = StrTest, Email = StrTest, CreatedBy = StrTest, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting };
            Employee employee2 = new Employee() { Username = "user2", IsActive = true, Password = StrTest, Mobile = StrTest, Email = StrTest, CreatedBy = StrTest, CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting };

            dao.Create(employee);
            dao.Create(employee2);

            EmployeeSearchCriteria criteria = new EmployeeSearchCriteria();
            criteria.Username = "user1";
            criteria.Page = 1;
            criteria.PageSize = 1;
            criteria.SortBy = "Name";
            criteria.SortAscending = true;
            criteria.IsActive = true;
            SearchResult<Employee> result = dao.Search(criteria);
            Assert.AreEqual(1, result.Items.Count);
            Assert.AreEqual("user1", result.Items[0].Username);
        }

    }
}
