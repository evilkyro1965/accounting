﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Linq.Expressions;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Linq;
using Microsoft.Practices.Unity;

namespace AIS.Backend.Service.Test
{
    [TestClass]
    public class LearnEntityFrameworkTest : BaseTest
    {
        [TestMethod]
        public void testGetPocoObject()
        {
            context.Configuration.ProxyCreationEnabled = false;
            var objctx = DaoHelper.GetObjectContext(context);
            Item item = context.Items
                            .Include("Category")
                            .Include("StockMeasurement")
                            //.Include("CalculationMeasurement")
                            //.Include("PurchaseMeasurementList")
                            .Where(i => i.Id == 1)
                            .First();

            objctx.LoadProperty(item, i => i.CalculationMeasurement);

            Console.WriteLine("Test");
            if (ObjectContext.GetObjectType(item.GetType()) == item.GetType())
            {
                Console.WriteLine("DB Context Success Get Plain Object");
            }
        }

        [TestMethod]
        public void testDetectChangesOfPOCO()
        {
            using (DaoContext daoContext = new DaoContext(DaoHelper.DB_NAME))
            {
                daoContext.Configuration.ProxyCreationEnabled = false;
                var objctx = DaoHelper.GetObjectContext(daoContext);
                Item item = daoContext.Items
                                .Include("Category")
                                //.Include("StockMeasurement")
                                .Include("CalculationMeasurement")
                                //.Include("PurchaseMeasurementList")
                                .Where(i => i.Id == 1)
                                .First();

                item.Name = "test1";
                //item.StockMeasurementId = 2;
                item.ItemCode = "123";
                objctx.ApplyCurrentValues("Items", item);
                objctx.SaveChanges();

                Item saved = context.Items
                                .Include("Category")
                                .Include("StockMeasurement")
                                .Include("CalculationMeasurement")
                                .Include("PurchaseMeasurementList")
                                .Where(i => i.Id == 1)
                                .First();

                Assert.AreEqual("test1", saved.Name);
                Assert.AreEqual(1, saved.StockMeasurement.Id);
                Assert.AreEqual("123", saved.ItemCode);
                Assert.AreEqual(2, saved.PurchaseMeasurementList.Count);
            }
        }

        [TestMethod]
        public void testTransactionAndInjection()
        {
            var container = new UnityContainer();
            container.RegisterType<IDisposable, DaoContext>();
            container.RegisterType<IObjectContextAdapter, DaoContext>(new HierarchicalLifetimeManager(), new InjectionConstructor("AIS_DB"));

            container.RegisterType<SupplierService, SupplierServiceInject>(new HierarchicalLifetimeManager());

            SupplierService supplierDao = container.Resolve<SupplierService>();

            Supplier supplier = new Supplier();
            supplier.Name = "test2";
            supplier.CreatedDate = DateTime.Now;
            supplier.UpdatedDate = DateTime.Now;

            supplierDao.Create(supplier);

            Assert.AreEqual("test2", supplier.Name);

            
        }


    }
}
