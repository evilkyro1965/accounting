﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Configuration;

namespace AIS.Backend.Model
{
    public class DatabaseInitializerDropCreate : DropCreateDatabaseAlways<DaoContext>
    {
        private DaoContext context { get; set; }

        protected override void Seed(DaoContext context)
        {
            this.context = context;
            CreateDepartmentDummy();
            base.Seed(context);
        }

        private void SetAuditableEntityDate<T>(T entity) where T : AuditableEntity
        {
            String name = ConfigurationManager.AppSettings["NAME_TEST"];
            DateTime createdDate = EntityHelper.FormattedStringToDate(ConfigurationManager.AppSettings["DATE_TEST"]);

            AuditableEntity update = (AuditableEntity)entity;
            update.CreatedBy = name;
            update.CreatedDate = createdDate;
            update.UpdatedBy = name;
            update.UpdatedDate = createdDate;
        }

        private void SetActiveEntityDate<T>(T entity) where T : ActiveEntity
        {
            String name = ConfigurationManager.AppSettings["NAME_TEST"];
            DateTime createdDate = EntityHelper.FormattedStringToDate(ConfigurationManager.AppSettings["DATE_TEST"]);

            ActiveEntity update = (ActiveEntity)entity;
            update.IsActive = true;
            update.CreatedBy = name;
            update.CreatedDate = createdDate;
            update.UpdatedBy = name;
            update.UpdatedDate = createdDate;
        }

        public void CreateDepartmentDummy()
        {
            String name = ConfigurationManager.AppSettings["NAME_TEST"];
            DateTime createdDate =  EntityHelper.FormattedStringToDate(ConfigurationManager.AppSettings["DATE_TEST"]);

            Department department = new Department();
            department.Name = name;
            department.Email = name;
            SetActiveEntityDate(department);

            this.context.Departments.Add(department);

            Measurement calculationMeasurement = new Measurement();
            calculationMeasurement.Name = name;
            calculationMeasurement.ShortName = name;
            calculationMeasurement.IsDecimal = true;
            calculationMeasurement.Unit = 1.0;
            calculationMeasurement.Type = MeasurementType.CALCULATION;
            calculationMeasurement.IsActive = true;
            SetActiveEntityDate(calculationMeasurement);

            this.context.Measurements.Add(calculationMeasurement);

            Measurement stockMeasurement = new Measurement();
            stockMeasurement.Name = name;
            stockMeasurement.ShortName = name;
            stockMeasurement.IsDecimal = true;
            stockMeasurement.Unit = 1.0;
            stockMeasurement.Type = MeasurementType.STOCK;
            stockMeasurement.IsActive = true;
            SetActiveEntityDate(stockMeasurement);

            this.context.Measurements.Add(stockMeasurement);

            Measurement purchaseMeasurement = new Measurement();
            purchaseMeasurement.Name = name;
            purchaseMeasurement.ShortName = name;
            purchaseMeasurement.IsDecimal = true;
            purchaseMeasurement.Unit = 1.0;
            purchaseMeasurement.Type = MeasurementType.PURCHASE;
            purchaseMeasurement.IsActive = true;
            SetActiveEntityDate(purchaseMeasurement);

            this.context.Measurements.Add(purchaseMeasurement);

            Measurement purchaseMeasurement2 = new Measurement();
            purchaseMeasurement2.Name = name + "2";
            purchaseMeasurement2.ShortName = name;
            purchaseMeasurement2.IsDecimal = true;
            purchaseMeasurement2.Unit = 1.0;
            purchaseMeasurement2.Type = MeasurementType.PURCHASE;
            purchaseMeasurement2.IsActive = true;
            SetActiveEntityDate(purchaseMeasurement2);

            this.context.Measurements.Add(purchaseMeasurement2);

            ItemCategory itemCategory = new ItemCategory();
            itemCategory.Name = name;
            SetActiveEntityDate(itemCategory);

            this.context.ItemCategories.Add(itemCategory);

            Warehouse warehouse = new Warehouse() { Name = name, Shelf = name, Label = name, Department = department};
            SetActiveEntityDate(warehouse);

            this.context.Warehouse.Add(warehouse);

            Employee employee = new Employee() { Name = name, Mobile = name, Password = name};
            SetActiveEntityDate(employee);

            this.context.Employee.Add(employee);

            Supplier supplier = new Supplier() { Name = name, Phone = name, Address = name, City = name,
                ContactPerson = name, Fax = name, PostalCode = name, Country = name, State = name
            };
            SetActiveEntityDate(supplier);

            this.context.Suppliers.Add(supplier);

            List<Measurement> purchaseMeasurementList = new List<Measurement>() { purchaseMeasurement, purchaseMeasurement2 };

            Item item = new Item() { 
                ItemCode = "123", ItemType = ItemType.STOCKED_PRODUCT, Category = itemCategory,
                Name = "test", ItemDescription = name, StockMeasurement = stockMeasurement,
                CalculationMeasurement = calculationMeasurement,
                PurchaseMeasurementList = purchaseMeasurementList, DefaultUnitPrice = 1.00
            };
            SetActiveEntityDate(item);

            this.context.Items.Add(item);

            context.SaveChanges();
        }
    }
}
