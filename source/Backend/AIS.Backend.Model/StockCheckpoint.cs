﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model
{
    public class StockCheckpoint : IdentifiableEntity
    {
        /// <summary>
        /// Last Transaction Date Time
        /// </summary>
        public DateTime TransactionDateTime { get; set; }

        /// <summary>
        /// Last Transaction Type
        /// </summary>
        public StockTransactionType TransactionType { get; set; }

        /// <summary>
        /// Last Transaction Entity Id
        /// </summary>
        public long TransactionEntityId { get; set; }

        /// <summary>
        /// Stock Account
        /// </summary>
        public StockAccount StockAccount { get; set; }

        /// <summary>
        /// Amount
        /// </summary>
        public double Amount { get; set; }

        /// <summary>
        /// Balance After
        /// </summary>
        public double BalanceAfter { get; set; }

        /// <summary>
        /// Timestamp
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public StockCheckpoint() { }
    }
}
