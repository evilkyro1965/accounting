﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model
{
    public enum StockAccountType
    {
        QUANTITY_ON_HAND,
        QUANTITY_RESERVED,
        QUANTITY_AVAILABLE,
        QUANTITY_ON_ORDER,
        QUANTITY_ON_RETURN
    }
}
