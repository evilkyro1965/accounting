﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AIS.Backend.Model
{
    /// <summary>
    /// Measurement
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    public class Measurement : ActiveEntity
    {

        /// <summary>
        /// Contact Person
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public virtual MeasurementType Type { get; set; }

        /// <summary>
        /// Unit
        /// </summary>
        public double Unit { get; set; }

        /// <summary>
        /// IsDecimal
        /// </summary>
        public bool IsDecimal { get; set; }

        public virtual ICollection<Item> ItemPurchaseList { get; set; }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public Measurement() { }
    }
}
