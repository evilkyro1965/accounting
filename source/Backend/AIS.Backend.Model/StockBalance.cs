﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model
{
    public class StockBalance : IdentifiableEntity
    {
        /// <summary>
        /// Last Transaction Date Time
        /// </summary>
        public DateTime LastTransactionDateTime { get; set; }

        /// <summary>
        /// Last Transaction Type
        /// </summary>
        public StockTransactionType LastTransactionType { get; set; }

        /// <summary>
        /// Last Transaction Entity Id
        /// </summary>
        public long LastTransactionEntityId { get; set; }

        /// <summary>
        /// Stock Account
        /// </summary>
        public StockAccount StockAccount { get; set; }

        /// <summary>
        /// Balance
        /// </summary>
        public double Balance { get; set; }

        /// <summary>
        /// Timestamp
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public StockBalance() {}
    }
}
