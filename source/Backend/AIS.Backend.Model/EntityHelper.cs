﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model
{
    public class EntityHelper
    {
        public static DateTime FormattedStringToDate(string str)
        {
            //Format is : 01/31/2015 11:11:11
            int month = Int32.Parse(str.Substring(0, 2));
            int day = Int32.Parse(str.Substring(3, 2));
            int year = Int32.Parse(str.Substring(6, 4));
            int hour = Int32.Parse(str.Substring(11, 2));
            int minute = Int32.Parse(str.Substring(14, 2));
            int second = Int32.Parse(str.Substring(17, 2));

            DateTime date = new DateTime(year, month, day, hour, minute, second);

            return date;
        }
    }
}
