﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model
{
    public class StockAccount : IdentifiableEntity
    {
        /// <summary>
        /// Warehouse
        /// </summary>
        public virtual Warehouse Warehouse { get; set; }

        /// <summary>
        /// Item
        /// </summary>
        public virtual Item Item { get; set; }

        /// <summary>
        /// StockAccountType
        /// </summary>
        public StockAccountType Type { get; set; }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public StockAccount() { }
    }
}
