﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AIS.Backend.Model
{
    public class ShipAddress : IdentifiableEntity
    {
        /// <summary>
        /// Address Name
        /// </summary>
        public String AddressName { get; set; }

        /// <summary>
        /// Address of address 
        /// </summary>
        public String AddressLocation { get; set; }

        /// <summary>
        /// City of address
        /// </summary>
        public String City { get; set; }

        /// <summary>
        /// State of address 
        /// </summary>
        public String State { get; set; }

        /// <summary>
        /// Zip of address 
        /// </summary>
        public String Zip { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        public String Country { get; set; }

        /// <summary>
        /// Note
        /// </summary>
        public String Note { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public long CustomerId { get; set; }

        /// <summary>
        /// Customer 
        /// </summary>
        public virtual Customer Customer { get; set; }

        /// <summary>
        /// Constructor of address
        /// </summary>
        public ShipAddress() { }
    }
}
