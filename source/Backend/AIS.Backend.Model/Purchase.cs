﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model
{
    public class Purchase : GLAuditableEntity
    {
        /// <summary>
        /// No
        /// </summary>
        public string No { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Delivery Date
        /// </summary>
        public DateTime DeliveryDate { get; set; }

        /// <summary>
        /// Warehouse
        /// </summary>
        public Warehouse Warehouse { get; set; }

        /// <summary>
        /// Freight
        /// </summary>
        public double Freight { get; set; }

        /// <summary>
        /// Requestor Department
        /// </summary>
        public Department RequestorDepartment { get; set; }

        /// <summary>
        /// Supplier
        /// </summary>
        public Supplier Supplier { get; set; }

        /// <summary>
        /// Remark
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// Order Detail List
        /// </summary>
        public virtual ICollection<OrderDetails> OrderDetailList { get; set; }

        /// <summary>
        /// Delivery Status
        /// </summary>
        public PurchaseDeliveryStatus DeliveryStatus { get; set; }

        /// <summary>+
        /// Is Paid
        /// </summary>
        public PurchasePaidType PaidStatus { get; set; }

        /// <summary>
        /// Paid Date
        /// </summary>
        public DateTime? PaidDate { get; set; }

        /// <summary>
        /// Other Cost
        /// </summary>
        public double OtherCost { get; set; }

        /// <summary>
        /// Grand Total
        /// </summary>
        public double GrandTotal { get; set; }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public Purchase() { }

    }
}
