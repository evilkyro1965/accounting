﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model
{
    public enum DataState
    {
        Added,
        Unchanged,
        Modified,
        Deleted
    }
}
