﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace AIS.Backend.Model
{
    /// <summary>
    /// Address, to bill or ship of customer, vendor, etc
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    [Table("address")] 
    public class Address : IdentifiableEntity
    {
        /// <summary>
        /// Address Name
        /// </summary>
        [Column("address_name")] 
        public String AddressName { get; set; }

        /// <summary>
        /// Address of address 
        /// </summary>
        [Column("address_location")] 
        public String AddressLocation { get; set; }

        /// <summary>
        /// City of address
        /// </summary>
        [Column("city")] 
        public String City { get; set; }

        /// <summary>
        /// State of address 
        /// </summary>
        [Column("state")] 
        public String State { get; set; }

        /// <summary>
        /// Zip of address 
        /// </summary>
        [Column("zip")] 
        public String Zip { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        [Column("country")] 
        public String Country { get; set; }

        /// <summary>
        /// Note
        /// </summary>
        [Column("note")] 
        public String Note { get; set; }

        /// <summary>
        /// Constructor of address
        /// </summary>
        public Address() { }

    }
}
