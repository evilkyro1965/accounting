﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model
{
    public class UnitOfWork : IUnitOfWork
    {
        protected string ConnectionString;

        private DaoContext context;

        public UnitOfWork(string connectionString)
        {
            this.ConnectionString = connectionString;
        }
        public DaoContext DaoContext
        {
            get
            {
                if (context == null)
                {
                    context = new DaoContext(ConnectionString);
                }
                return context;
            }
        }

        public int Save()
        {
            return context.SaveChanges();
        }

        public void Dispose()
        {
            if (context != null)
            {
                context.Dispose();
                context = null;
            }
        }

    }
}
