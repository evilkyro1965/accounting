﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AIS.Backend.Model
{
    /// <summary>
    /// Customer entity
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    public class Customer : NameableEntity
    {
        /// <summary>
        /// Company Name
        /// </summary>
        public String CompanyName { get; set; }

        /// <summary>
        /// MrMs
        /// </summary>
        public String MrMs { get; set; }

        /// <summary>
        /// First Name
        /// </summary>
        public String FirstName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
        public String LastName { get; set; }

        /// <summary>
        /// Contact
        /// </summary>
        public String Contact { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        public String Phone { get; set; }

        /// <summary>
        /// AltPhone
        /// </summary>
        public String AltPhone { get; set; }

        /// <summary>
        /// Fax
        /// </summary>
        public String Fax { get; set; }

        /// <summary>
        /// AltContact
        /// </summary>
        public String AltContact { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public String Email { get; set; }

        /// <summary>
        /// CC
        /// </summary>
        public String CC { get; set; }

        /// <summary>
        /// Bill To
        /// </summary>
        public Address BillTo { get; set; }

        /// <summary>
        /// Ship To
        /// </summary>
        public List<ShipAddress> ShipTo { get; set; }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public Customer() { }

    }
}
