﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace AIS.Backend.Model
{
    /// <summary>
    /// Dao Context of Entity Framework
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    public class DaoContext : DbContext
    {
        /// <summary>
        /// Constructor that use connection string AIS_DB
        /// Set Initializer to drop and create database
        /// </summary>
        public DaoContext(String connectionString)
            : base(nameOrConnectionString: connectionString)
        {

        }

        public void TestInit()
        {
            Database.SetInitializer<DaoContext>(new DatabaseInitializerDropCreate());
            Database.Initialize(true);
        }

        /// <summary>
        /// Address DBSet
        /// </summary>
        public DbSet<Address> Address { get; set; }

        /// <summary>
        /// Customer DBSet
        /// </summary>
        public DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Department DBSet
        /// </summary>
        public DbSet<Department> Departments { get; set; }

        /// <summary>
        /// Employee DBSet
        /// </summary>
        public DbSet<Employee> Employee { get; set; }

        /// <summary>
        /// Item Category DBSet
        /// </summary>
        public DbSet<ItemCategory> ItemCategories { get; set; }

        /// <summary>
        /// Measurement DBSet
        /// </summary>
        public DbSet<Measurement> Measurements { get; set; }

        /// <summary>
        /// Items DBSet
        /// </summary>
        public DbSet<Item> Items { get; set; }

        /// <summary>
        /// Supplier
        /// </summary>
        public DbSet<Supplier> Suppliers { get; set; }

        /// <summary>
        /// Purchase
        /// </summary>
        public DbSet<Purchase> Purchase { get; set; }

        /// <summary>
        /// Order Details
        /// </summary>
        public DbSet<OrderDetails> OrderDetails { get; set; }

        /// <summary>
        /// Warehouse
        /// </summary>
        public DbSet<Warehouse> Warehouse { get; set; }

        /// <summary>
        /// StockAccount
        /// </summary>
        public DbSet<StockAccount> StockAccount { get; set; }

        /// <summary>
        /// StockBalance
        /// </summary>
        public DbSet<StockBalance> StockBalance { get; set; }

        /// <summary>
        /// StockCheckpoint
        /// </summary>
        public DbSet<StockCheckpoint> StockCheckpoint { get; set; }

        /// <summary>
        /// On Model Creating, set relationship of ORM
        /// </summary>
        /// <param name="modelBuilder">The modelBuilder</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Customer>()
                .HasOptional<Address>(c => c.BillTo)
                .WithOptionalDependent()
                .Map(m => m.MapKey("BillToId"));

            modelBuilder.Entity<Customer>()
                .HasMany<ShipAddress>(c => c.ShipTo)
                .WithRequired(a => a.Customer)
                .HasForeignKey(a => a.CustomerId);

            //Department and Employee one to many
            modelBuilder.Entity<Employee>()
                        .HasOptional<Department>(e => e.Department)
                        .WithMany(d => d.EmployeeList)
                        .HasForeignKey(e => e.DepartmentId);

            //Item 
            modelBuilder.Entity<Item>()
                        .HasOptional<ItemCategory>(i => i.Category)
                        .WithMany()
                        .Map(m => m.MapKey("CategoryId"));

            modelBuilder.Entity<Item>()
                        .HasOptional<Measurement>(i => i.StockMeasurement)
                        .WithMany()
                        .HasForeignKey(i => i.StockMeasurementId);
                        //.WithMany()
                        //.Map(m => m.MapKey("StockMeasurementId"));

            modelBuilder.Entity<Item>()
                        .HasOptional<Measurement>(i => i.CalculationMeasurement)
                        .WithMany()
                        .Map(m => m.MapKey("CalculationMeasurementId"));

            modelBuilder.Entity<Item>()
                        .HasMany<Measurement>(i => i.PurchaseMeasurementList)
                        .WithMany(m => m.ItemPurchaseList)
                        .Map(map =>
                        {
                            map.MapLeftKey("ItemId");
                            map.MapRightKey("MeasurementId");
                            map.ToTable("ItemPurchaseMeasurementList");
                        });

            //Warehouse -> Department, one to one
            modelBuilder.Entity<Warehouse>()
                        .HasOptional<Department>(w => w.Department)
                        .WithMany()
                        .Map(m => m.MapKey("DepartmentId"));

            //Purchase -> OrderDetails, One to many
            modelBuilder.Entity<Purchase>()
                .HasMany<OrderDetails>(p => p.OrderDetailList)
                .WithRequired(o => o.Purchase)
                .HasForeignKey(o => o.PurchaseId);

            //Purchase -> Warehouse, one to one
            modelBuilder.Entity<Purchase>()
                        .HasOptional<Warehouse>(p => p.Warehouse)
                        .WithMany()
                        .Map(m => m.MapKey("WarehouseId"));

            //Purchase -> RequestorDepartment, one to one
            modelBuilder.Entity<Purchase>()
                        .HasOptional<Department>(p => p.RequestorDepartment)
                        .WithMany()
                        .Map(m => m.MapKey("RequestorDepartmentId"));

            //Purchase -> Supplier, one to one
            modelBuilder.Entity<Purchase>()
                        .HasOptional<Supplier>(p => p.Supplier)
                        .WithMany()
                        .Map(m => m.MapKey("SupplierId"));

            //Order Details -> Item, one to one
            modelBuilder.Entity<OrderDetails>()
                        .HasOptional<Item>(o => o.Item)
                        .WithMany()
                        .Map(m => m.MapKey("ItemId"));

            //Order Details -> Warehouse, one to one
            modelBuilder.Entity<OrderDetails>()
                        .HasOptional<Warehouse>(o => o.Warehouse)
                        .WithMany()
                        .Map(m => m.MapKey("WarehouseId"));

            //Order Details -> Measurement, one to one
            modelBuilder.Entity<OrderDetails>()
                        .HasOptional<Measurement>(o => o.PurchaseMeasurement)
                        .WithMany()
                        .Map(m => m.MapKey("PurchaseMeasurementId"));

            //StockAccount -> Warehouse, one to one
            modelBuilder.Entity<StockAccount>()
                        .HasOptional<Warehouse>(s => s.Warehouse)
                        .WithMany()
                        .Map(m => m.MapKey("WarehouseId"));

            //StockBalance -> Item, one to one
            modelBuilder.Entity<StockBalance>()
                        .HasOptional<StockAccount>(s => s.StockAccount)
                        .WithMany()
                        .Map(m => m.MapKey("StockAccountId"));

            //StockCheckpoint -> Item, one to one
            modelBuilder.Entity<StockCheckpoint>()
                        .HasOptional<StockAccount>(s => s.StockAccount)
                        .WithMany()
                        .Map(m => m.MapKey("StockAccountId"));

        }

        /*
        public DbEntityEntry Entry<T>(T obj)
        {
            return Entry(obj);
        }
        */
    }
}
