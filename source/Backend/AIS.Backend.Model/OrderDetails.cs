﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model
{
    /// <summary>
    /// Order Details
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    public class OrderDetails : IdentifiableEntity
    {
        /// <summary>
        /// Purchase
        /// </summary>
        public virtual Purchase Purchase { get; set; }

        /// <summary>
        /// Item
        /// </summary>
        public Item Item { get; set; }
        
        /// <summary>
        /// Purchase Measurement
        /// </summary>
        public Measurement PurchaseMeasurement { get; set; }

        /// <summary>
        /// Quantity
        /// </summary>
        public double Quantity { get; set; }

        /// <summary>
        /// Price
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// Discount Percent
        /// </summary>
        public double DiscountPercent { get; set; }

        /// <summary>
        /// Tax Percent
        /// </summary>
        public double TaxPercent { get; set; }

        /// <summary>
        /// Total Price
        /// </summary>
        public double TotalPrice { get; set; }

        /// <summary>
        /// Order Received Quantity
        /// </summary>
        public double OrderReceivedQuantity { get; set; }

        /// <summary>
        /// Order Received Date
        /// </summary>
        public DateTime? OrderReceivedDate { get; set; }

        /// <summary>
        /// Order Received Full
        /// </summary>
        public bool OrderReceivedFull { get; set; }

        /// <summary>
        /// Return quantity
        /// </summary>
        public double ReturnQuantity { get; set; }

        /// <summary>
        /// Return Date
        /// </summary>
        public DateTime? ReturnDate { get; set; }

        /// <summary>
        /// PurchaseId
        /// </summary>
        public long PurchaseId { get; set; }

        /// <summary>
        /// Warehouse
        /// </summary>
        public Warehouse Warehouse { get; set; }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public OrderDetails() { }

    }
}
