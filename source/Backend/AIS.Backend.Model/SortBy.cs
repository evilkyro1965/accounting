﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model
{
    public enum SortBy
    {
        NAME,
        EMAIL,
        USERNAME,
        MOBILE,
        MEASUREMENT_TYPE
    }
}
