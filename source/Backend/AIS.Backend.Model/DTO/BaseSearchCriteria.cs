﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model.DTO
{
    public class BaseSearchCriteria
    {
        public int Page { get; set; }

        public int PageSize { get; set; }

        public String Name { get; set; }

        public Boolean? IsActive { get; set; }

        public String SortBy { get; set; }

        public bool SortAscending { get; set; }

    }
}
