﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model.DTO
{
    public class WarehouseSearchCriteria : BaseSearchCriteria
    {
        public Department Department { get; set; }
    }
}
