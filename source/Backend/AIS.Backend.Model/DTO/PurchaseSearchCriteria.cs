﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model.DTO
{
    /// <summary>
    /// PurchaseSearchCriteria
    /// </summary>
    public class PurchaseSearchCriteria : BaseSearchCriteria
    {
        public String No { get; set; }

        public DateTime Date { get; set; }
    }
}
