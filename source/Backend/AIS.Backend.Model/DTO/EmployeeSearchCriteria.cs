﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model.DTO
{
    public class EmployeeSearchCriteria : BaseSearchCriteria
    {
        public String Username { get; set; }

        public String Password { get; set; }

        public String Mobile { get; set; }

        public String Email { get; set; }
    }
}
