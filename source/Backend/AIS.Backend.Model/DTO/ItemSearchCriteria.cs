﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model.DTO
{
    public class ItemSearchCriteria : BaseSearchCriteria
    {
        public ItemType? Type { get; set; }
        public ItemCategory Category { get; set; }
        public Measurement StockMeasurement { get; set; }
        public Measurement CalculationMeasurement { get; set; }
    }
}
