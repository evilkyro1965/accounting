﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model.DTO
{
    public class DepartmentSearchCriteria : BaseSearchCriteria
    {
        public String Email { get; set; }
    }
}
