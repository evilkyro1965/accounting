﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model
{
    public interface IUnitOfWork : IDisposable
    {
        DaoContext DaoContext { get; }
        int Save();
    }
}
