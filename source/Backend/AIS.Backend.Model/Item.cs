﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AIS.Backend.Model
{
    /// <summary>
    /// Item
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    public class Item : ActiveEntity
    {
        /// <summary>
        /// Item Code
        /// </summary>
        public String ItemCode { get; set; }

        /// <summary>
        /// Item Type
        /// </summary>
        public virtual ItemType ItemType { get; set; }
        
        /// <summary>
        /// Category
        /// </summary>
        public virtual ItemCategory Category { get; set; }

        /// <summary>
        /// Item Description
        /// </summary>
        public String ItemDescription { get; set; }

        /// <summary>
        /// Stock Measurement
        /// </summary>
        public virtual Measurement StockMeasurement { get; set; }
        public long? StockMeasurementId { get; set; }

        /// <summary>
        /// Calculation Measurement
        /// </summary>
        public virtual Measurement CalculationMeasurement { get; set; }

        /// <summary>
        /// Purchase Measurement List
        /// </summary>
        public virtual ICollection<Measurement> PurchaseMeasurementList { get; set; }

        /// <summary>
        /// Default unit price
        /// </summary>
        public Double DefaultUnitPrice;

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public Item() { }
    }
}
