﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Model
{
    public enum StockTransactionType
    {
        STOCK_ADJUSTMENT,
        COUNT_SHEET_ADJUSTMENT,
        STOCK_TRANSFER_SEND,
        STOCK_TRANSFER_RECEIVE,
        WORK_ORDER_PICKING,
        WORK_ORDER_PUT_AWAY,
        PURCHASE_ORDER_CREATED,
        PURCHASE_ORDER_RECEIVE,
        PURCHASE_ORDER_RETURN,
        PURCHASE_ORDER_UNSTOCK,
        SALES_ORDER_PICKING,
        SALES_ORDER_FULFILLMENT
    }
}
