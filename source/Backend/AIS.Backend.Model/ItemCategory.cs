﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AIS.Backend.Model
{
    /// <summary>
    /// Item Category entity
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    public class ItemCategory : ActiveEntity
    {

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public ItemCategory() { }
    }
}
