﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AIS.Backend.Model
{
    /// <summary>
    /// Base of entity class that had id
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    public abstract class IdentifiableEntity : IObjectWithState
    {

        /// <summary>
        /// Represents the identity
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Represents state 
        /// </summary>
        public DataState DataState { get; set; }

        /// <summary>
        /// The constructor
        /// </summary>
        public IdentifiableEntity() { }
    }
}
