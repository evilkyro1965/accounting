﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;

namespace AIS.Backend.Service
{
    public interface EmployeeService
    {
        Employee Create(Employee employee);
        Employee Update(Employee employee);
        Employee Get(long id);
        void Delete(long[] ids);
        void Deactivate(long[] ids);
        SearchResult<Employee> Search(EmployeeSearchCriteria criteria);

    }
}
