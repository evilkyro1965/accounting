﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq.Expressions;
using AIS.Backend.Service;
using SystemEntity = System.Data.Entity;

namespace AIS.Backend.Service.Impl
{
    public class DepartmentServiceImpl : DepartmentService
    {

        /// <summary>
        /// Dao Context
        /// </summary>
        protected DaoContext context;

        public DepartmentServiceImpl() {}

        public Department Create(Department department) 
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    DaoHelper.CheckSameNameInsert<Department>(context.Departments, department);

                    context.Departments.Add(department);
                    context.SaveChanges();
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return department;
        }

        public Department Update(Department department)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    Department update = context.Departments.Find(department.Id);

                    DaoHelper.CheckSameNameUpdate<Department>(context.Departments, department);

                    update.Email = department.Email;

                    DaoHelper.SetFieldActiveEntityFromObject(department, update);

                    context.Entry(update).State = SystemEntity.EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return department; 
        }

        public Department Get(long id)
        {
            Department department = null;
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    department = context.Departments.Find(id);
                }
            }
            catch (DataException e)
            {
                throw e;
            }

            return department;
        }

        public void Delete(long[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                foreach (long id in ids)
                {
                    try 
                    {
                        using (context = new DaoContext(DaoHelper.DB_NAME))
                        {
                            Department department = context.Departments.Find(id);
                            context.Departments.Remove(department);
                            context.SaveChanges();
                        }
                    }
                    catch (DataException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public void Deactivate(long[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                foreach (long id in ids)
                {
                    try
                    {
                        using (context = new DaoContext(DaoHelper.DB_NAME))
                        {
                            Department department = context.Departments.Find(id);
                            department.IsActive = false;
                            context.SaveChanges();
                        }                        
                    }
                    catch (DataException e)
                    {
                        throw e;
                    }
                }
            }
        }

        //delegate TResult SortBy<T,TResult>(T x);

        public SearchResult<Department> Search(DepartmentSearchCriteria criteria)
        {
            SearchResult<Department> searchResult = new SearchResult<Department>();

            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    var objctx = DaoHelper.GetObjectContext(context);
                    SearchHelperValue searchHelperVal = DaoHelper.SearchHelperValueBuilder(criteria);
                    List<ObjectParameter> param = new List<ObjectParameter>();

                    StringBuilder sqlString = new StringBuilder();
                    SearchQueryBuilder.QuerySelectBuilder(sqlString, "Departments");

                    SearchQueryBuilder.QueryLikeBuilder(param, sqlString, "Name", criteria.Name);
                    SearchQueryBuilder.QueryEqualBuilder<Boolean?>(param, sqlString, "IsActive", criteria.IsActive);
                    SearchQueryBuilder.QueryLikeBuilder(param, sqlString, "Email", criteria.Email);

                    SearchQueryBuilder.QueryGetTotal<Department>(param, sqlString, searchHelperVal, context);

                    SearchQueryBuilder.QuerySortByBuilder(param, sqlString, searchHelperVal);
                    SearchQueryBuilder.QueryPagingBuilder(param, sqlString, searchHelperVal);

                    ObjectQuery<Department> queryResult = objctx.CreateQuery<Department>(sqlString.ToString(), param.ToArray());

                    SearchQueryBuilder.QueryGetSearchResult<Department, DepartmentSearchCriteria>(param, sqlString, criteria, searchHelperVal, searchResult, context);
                }
            }
            catch (DataException e)
            {
                throw e;
            }
            return searchResult;
        }




    }
}
