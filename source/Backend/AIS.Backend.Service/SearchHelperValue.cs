﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Service
{
    /// <summary>
    /// Search Helper Value 
    /// Containt total, page, skip, limit and other helper value
    /// </summary>
    public class SearchHelperValue
    {
        /// <summary>
        /// Total of search result
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// Page size of search result
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Page Number 
        /// </summary>
        public int PageNum { get; set; }

        /// <summary>
        /// Skipped row
        /// </summary>
        public int Skip { get; set; }

        /// <summary>
        /// Limit row
        /// </summary>
        public int Limit { get; set; }
        
        /// <summary>
        /// Order, ASC or DESC
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// Order by field
        /// </summary>
        public string OrderBy { get; set; }
        

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public SearchHelperValue(){}

    }
}
