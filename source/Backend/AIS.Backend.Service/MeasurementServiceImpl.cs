﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq.Expressions;
using SystemEntity = System.Data.Entity;

namespace AIS.Backend.Service.Impl
{
    public class MeasurementServiceImpl : MeasurementService
    {
        /// <summary>
        /// Dao Context
        /// </summary>
        protected DaoContext context;

        public MeasurementServiceImpl()
        {
        }

        public Measurement Create(Measurement measurement)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    DaoHelper.CheckSameNameInsert<Measurement>(context.Measurements, measurement);

                    context.Measurements.Add(measurement);
                    context.SaveChanges();
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return measurement;
        }

        public Measurement Update(Measurement measurement)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    Measurement update = context.Measurements.Find(measurement.Id);

                    DaoHelper.CheckSameNameUpdate<Measurement>(context.Measurements, measurement);

                    update.ShortName = measurement.ShortName;
                    update.Description = measurement.Description;
                    update.Type = measurement.Type;
                    update.ShortName = measurement.ShortName;
                    update.Unit = measurement.Unit;
                    update.IsDecimal = measurement.IsDecimal;

                    DaoHelper.SetFieldActiveEntityFromObject(measurement, update);

                    context.Entry(update).State = SystemEntity.EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return measurement;
        }

        public Measurement Get(long id)
        {
            Measurement measurement = null;
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    measurement = context.Measurements.Find(id);
                }
            }
            catch (DataException e)
            {
                throw e;
            }

            return measurement;
        }

        public void Delete(long[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                foreach (long id in ids)
                {
                    try
                    {
                        using (context = new DaoContext(DaoHelper.DB_NAME))
                        {
                            Measurement measurement = context.Measurements.Find(id);
                            context.Measurements.Remove(measurement);
                            context.SaveChanges();
                        }
                    }
                    catch (DataException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public void Deactivate(long[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                foreach (long id in ids)
                {
                    try
                    {
                        using (context = new DaoContext(DaoHelper.DB_NAME))
                        {
                            Measurement measurement = context.Measurements.Find(id);
                            measurement.IsActive = false;
                            context.SaveChanges();
                        }
                    }
                    catch (DataException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public SearchResult<Measurement> Search(MeasurementSearchCriteria criteria)
        {
            SearchResult<Measurement> searchResult = new SearchResult<Measurement>();
            
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    var objctx = DaoHelper.GetObjectContext(context);
                    SearchHelperValue searchHelperVal = DaoHelper.SearchHelperValueBuilder(criteria);
                    List<ObjectParameter> param = new List<ObjectParameter>();

                    StringBuilder sqlString = new StringBuilder();
                    SearchQueryBuilder.QuerySelectBuilder(sqlString, "Measurements");

                    SearchQueryBuilder.QueryLikeBuilder(param, sqlString, "Name", criteria.Name);
                    SearchQueryBuilder.QueryEqualBuilder<Boolean?>(param, sqlString, "IsActive", criteria.IsActive);
                    SearchQueryBuilder.QueryEqualBuilder(param, sqlString, "Type", criteria.MeasurementType);

                    SearchQueryBuilder.QueryGetTotal<Measurement>(param, sqlString, searchHelperVal, context);

                    SearchQueryBuilder.QuerySortByBuilder(param, sqlString, searchHelperVal);
                    SearchQueryBuilder.QueryPagingBuilder(param, sqlString, searchHelperVal);

                    ObjectQuery<Measurement> queryResult = objctx.CreateQuery<Measurement>(sqlString.ToString(), param.ToArray());

                    SearchQueryBuilder.QueryGetSearchResult<Measurement, MeasurementSearchCriteria>(param, sqlString, criteria, searchHelperVal, searchResult, context);
                }
            }
            catch (DataException e)
            {
                throw e;
            }
            return searchResult;
        }

    }
}
