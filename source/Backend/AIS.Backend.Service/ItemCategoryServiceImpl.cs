﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq.Expressions;
using SystemEntity = System.Data.Entity;

namespace AIS.Backend.Service.Impl
{
    public class ItemCategoryServiceImpl : ItemCategoryService
    {
        /// <summary>
        /// Dao Context
        /// </summary>
        protected DaoContext context;

        public ItemCategoryServiceImpl()
        {
        }

        public ItemCategory Create(ItemCategory itemCategory)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME)) 
                {
                    DaoHelper.CheckSameNameInsert<ItemCategory>(context.ItemCategories, itemCategory);

                    context.ItemCategories.Add(itemCategory);
                    context.SaveChanges();
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return itemCategory;
        }

        public ItemCategory Update(ItemCategory itemCategory)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    ItemCategory update = context.ItemCategories.Find(itemCategory.Id);

                    DaoHelper.CheckSameNameUpdate<ItemCategory>(context.ItemCategories, itemCategory);

                    DaoHelper.SetFieldActiveEntityFromObject(itemCategory, update);

                    context.Entry(update).State = SystemEntity.EntityState.Modified;
                    context.SaveChanges();
                }
                
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return itemCategory;
        }

        public ItemCategory Get(long id)
        {
            ItemCategory itemCategory = null;
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME)) 
                { 
                    itemCategory = context.ItemCategories.Find(id);
                }
            }
            catch (DataException e)
            {
                throw e;
            }

            return itemCategory;
        }

        public void Delete(long[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                foreach (long id in ids)
                {
                    try
                    {
                        using (context = new DaoContext(DaoHelper.DB_NAME))
                        {
                            ItemCategory itemCategory = context.ItemCategories.Find(id);
                            context.ItemCategories.Remove(itemCategory);
                            context.SaveChanges();
                        }
                    }
                    catch (DataException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public void Deactivate(long[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                foreach (long id in ids)
                {
                    try
                    {
                        using (context = new DaoContext(DaoHelper.DB_NAME))
                        {
                            ItemCategory itemCategory = context.ItemCategories.Find(id);
                            itemCategory.IsActive = false;
                            context.SaveChanges();
                        }
                    }
                    catch (DataException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public SearchResult<ItemCategory> Search(BaseSearchCriteria criteria)
        {
            SearchResult<ItemCategory> searchResult = new SearchResult<ItemCategory>();

            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    var objctx = DaoHelper.GetObjectContext(context);
                    SearchHelperValue searchHelperVal = DaoHelper.SearchHelperValueBuilder(criteria);
                    List<ObjectParameter> param = new List<ObjectParameter>();

                    StringBuilder sqlString = new StringBuilder();
                    SearchQueryBuilder.QuerySelectBuilder(sqlString, "ItemCategories");

                    SearchQueryBuilder.QueryLikeBuilder(param, sqlString, "Name", criteria.Name);
                    SearchQueryBuilder.QueryEqualBuilder<Boolean?>(param, sqlString, "IsActive", criteria.IsActive);

                    SearchQueryBuilder.QueryGetTotal<ItemCategory>(param, sqlString, searchHelperVal, context);

                    SearchQueryBuilder.QuerySortByBuilder(param, sqlString, searchHelperVal);
                    SearchQueryBuilder.QueryPagingBuilder(param, sqlString, searchHelperVal);

                    ObjectQuery<ItemCategory> queryResult = objctx.CreateQuery<ItemCategory>(sqlString.ToString(), param.ToArray());

                    SearchQueryBuilder.QueryGetSearchResult<ItemCategory, BaseSearchCriteria>(param, sqlString, criteria, searchHelperVal, searchResult, context);
                }
            }
            catch (DataException e)
            {
                throw e;
            }
            return searchResult;
        }

    }
}
