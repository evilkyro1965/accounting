﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;

namespace AIS.Backend.Service
{
    public interface DepartmentService
    {
        Department Create(Department department);
        Department Update(Department department);
        Department Get(long id);
        void Delete(long[] ids);
        void Deactivate(long[] ids);
        SearchResult<Department> Search(DepartmentSearchCriteria criteria);
    }
}
