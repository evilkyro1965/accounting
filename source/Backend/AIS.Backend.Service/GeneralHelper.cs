﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Backend.Service
{
    public class GeneralHelper
    {
        public static List<T> ConvertICollectionToList<T>(ICollection<T> collection)
        {
            List<T> list = new List<T>();
            T[] arr = new T[collection.Count];
            collection.CopyTo(arr, 0);
            list = new List<T>(arr);
            return list;
        }
    }
}
