﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;

namespace AIS.Backend.Service
{
    /// <summary>
    /// PurchaseService
    /// </summary>
    public interface PurchaseService
    {
        Purchase Create(Purchase purchase);
        Purchase Update(Purchase purchase);
        Purchase UpdateReceivedStock(Purchase purchase);
        Purchase UpdateReturnStock(Purchase purchase);
        Purchase Get(long id);
        void Delete(long[] ids);
        SearchResult<Purchase> Search(PurchaseSearchCriteria criteria);
    }
}
