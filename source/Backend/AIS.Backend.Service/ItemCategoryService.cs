﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;

namespace AIS.Backend.Service
{
    public interface ItemCategoryService
    {
        ItemCategory Create(ItemCategory itemCategory);
        ItemCategory Update(ItemCategory itemCategory);
        ItemCategory Get(long id);
        void Delete(long[] ids);
        void Deactivate(long[] ids);
        SearchResult<ItemCategory> Search(BaseSearchCriteria criteria);
    }
}
