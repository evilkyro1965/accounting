﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;

namespace AIS.Backend.Service
{
    public interface WarehouseService
    {
        Warehouse Create(Warehouse warehouse);
        Warehouse Update(Warehouse warehouse);
        Warehouse Get(long id);
        void Delete(long[] ids);
        void Deactivate(long[] ids);
        SearchResult<Warehouse> Search(WarehouseSearchCriteria criteria);
    }
}
