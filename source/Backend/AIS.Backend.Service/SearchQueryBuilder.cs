﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using AIS.Backend.Model.DTO;

namespace AIS.Backend.Service
{
    /// <summary>
    /// Search Query Builder, is used for build entity query
    /// </summary>
    public class SearchQueryBuilder
    {
        private const string DaoContextName = "DaoContext";

        /// <summary>
        /// Empty constructor
        /// </summary>
        public SearchQueryBuilder() {}

        /// <summary>
        /// SearchResultBuilder, build search result from criteria, ObjectQuery, 
        /// and searchHelperValue
        /// </summary>
        /// <typeparam name="T">Entity</typeparam>
        /// <typeparam name="C">BaseSearchCriteria</typeparam>
        /// <param name="criteria">The criteria</param>
        /// <param name="objectQuery">The Object Query</param>
        /// <param name="searchHelperValue">The Search Helper Value</param>
        /// <returns></returns>
        public static void SearchResultBuilder<T, C>(C criteria, ObjectQuery<T> objectQuery, SearchHelperValue searchHelperValue,SearchResult<T> searchResult, DbContext context)
            where C : BaseSearchCriteria
        {
            searchResult.Page = criteria.Page;
            searchResult.PageSize = criteria.PageSize;
            searchResult.Total = searchHelperValue.Total;
            
            ObjectContext objctx = DaoHelper.GetObjectContext(context);
            List<T> result = new List<T>();

            List<T> list = objectQuery.ToList<T>();

            if (list != null && list.Count > 0)
            {
                foreach (T item in list)
                {
                    objctx.Detach(item);
                    result.Add(item);
                }
            }

            searchResult.Items = result;
        }

        public static void QuerySelectBuilder(StringBuilder queryString,string ObjectName)
        {
            queryString.Append("SELECT VALUE val FROM " + DaoContextName + "." + ObjectName + " AS val");
            queryString.Append(" WHERE 1=1");
        }

        /// <summary>
        /// QueryLikeBuilder, build where field like value string query and list param
        /// </summary>
        /// <param name="listObjectParam">List Of Object Param</param>
        /// <param name="queryString">The query string</param>
        /// <param name="field">The field name</param>
        /// <param name="fieldValue">The field value</param>
        public static void QueryLikeBuilder(List<ObjectParameter> listObjectParam, StringBuilder queryString, String field, String fieldValue)
        {
            ObjectParameter objParam = null;
            int fieldCounter = listObjectParam.Count;
            string queryFieldName = "field" + fieldCounter;
            if (fieldValue != null && fieldValue != "")
            {
                queryString.Append(" AND val." + field + " LIKE @" + queryFieldName);
                objParam = new ObjectParameter(queryFieldName, "%" + fieldValue + "%");
                listObjectParam.Add(objParam);
            }
        }

        /// <summary>
        /// QueryEqualBuilder, build where field equal value string query and list param
        /// </summary>
        /// <param name="listObjectParam">List Of Object Param</param>
        /// <param name="queryString">The query string</param>
        /// <param name="field">The field name</param>
        /// <param name="fieldValue">The field value</param>
        public static void QueryEqualBuilder<T>(List<ObjectParameter> listObjectParam, StringBuilder queryString, String field, T fieldValue)
        {
            ObjectParameter objParam = null;
            int fieldCounter = listObjectParam.Count;
            string queryFieldName = "field" + fieldCounter;
            if (fieldValue != null)
            {
                queryString.Append(" AND val." + field + " == @" + queryFieldName);
                objParam = new ObjectParameter(queryFieldName, fieldValue);
                listObjectParam.Add(objParam);
            }
        }

        public static void QuerySortByBuilder(List<ObjectParameter> listObjectParam, StringBuilder queryString, SearchHelperValue searchHelperVal)
        {
            if(searchHelperVal.OrderBy!=null && searchHelperVal.OrderBy!="")
            {
                queryString.Append(" ORDER BY val." + searchHelperVal.OrderBy + " " + searchHelperVal.Order);
            }
            else
            {
                queryString.Append(" ORDER BY val.Id ASC");
            }
        }

        public static void QueryPagingBuilder(List<ObjectParameter> listObjectParam, StringBuilder queryString, SearchHelperValue searchHelperVal)
        {
            queryString.Append(" SKIP @skip LIMIT @limit");
            listObjectParam.Add(new ObjectParameter("skip", searchHelperVal.Skip));
            listObjectParam.Add(new ObjectParameter("limit", searchHelperVal.Limit));
        }

        public static void QueryGetTotal<T>(List<ObjectParameter> listObjectParam, StringBuilder queryString, SearchHelperValue searchHelperVal, DbContext context)
        {
            ObjectContext objctx = DaoHelper.GetObjectContext(context);
            ObjectQuery<T> totalResult = objctx.CreateQuery<T>(queryString.ToString(), listObjectParam.ToArray());
            searchHelperVal.Total = totalResult.Count();
        }

        public static void QueryGetSearchResult<T,C>(List<ObjectParameter> listObjectParam, StringBuilder queryString,C criteria, SearchHelperValue searchHelperVal, 
            SearchResult<T> searchResult, DbContext context, String[] includes = null)
            where C : BaseSearchCriteria
        {
            ObjectContext objctx = DaoHelper.GetObjectContext(context);
            ObjectQuery<T> queryResult = objctx.CreateQuery<T>(queryString.ToString(), listObjectParam.ToArray());

            if(includes!=null)
            {
                foreach(String include in includes){
                    queryResult.Include(include);
                }
            }
            SearchResultBuilder<T,C>(criteria, queryResult, searchHelperVal, searchResult, context);
        }


    }
}
