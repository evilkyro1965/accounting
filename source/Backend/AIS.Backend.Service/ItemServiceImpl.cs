﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq.Expressions;
using SystemEntity = System.Data.Entity;

namespace AIS.Backend.Service.Impl
{
    public class ItemServiceImpl : ItemService
    {
        /// <summary>
        /// Dao Context
        /// </summary>
        protected DaoContext context;

        public ItemServiceImpl()
        {
        }

        public Item Create(Item item)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    DaoHelper.CheckSameNameInsert<Item>(context.Items, item);
                    
                    if (item.Category != null && item.Category.Id > 0)
                        item.Category = context.ItemCategories.Find(item.Category.Id);
                    else
                        item.Category = null;

                    if (item.CalculationMeasurement != null && item.CalculationMeasurement.Id > 0)
                        item.CalculationMeasurement = context.Measurements.Find(item.CalculationMeasurement.Id);
                    else
                        item.CalculationMeasurement = null;

                    if (item.StockMeasurement != null && item.StockMeasurement.Id > 0)
                        item.StockMeasurement = context.Measurements.Find(item.StockMeasurement.Id);
                    else
                        item.StockMeasurement = null;

                    if (item.PurchaseMeasurementList != null)
                    {
                        IList<Measurement> measurementList = item.PurchaseMeasurementList.ToList();
                        item.PurchaseMeasurementList = new List<Measurement>();
                        foreach (Measurement measurement in measurementList)
                        {
                            item.PurchaseMeasurementList.Add(context.Measurements.Find(measurement.Id));
                        }
                            
                    }

                    context.Items.Add(item);
                    context.SaveChanges();
                    var objctx = DaoHelper.GetObjectContext(context);
                    objctx.Detach(item);
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return item;
        }

        public Item Update(Item item)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    DaoHelper.CheckSameNameUpdate<Item>(context.Items, item);
                    
                    Item update = context.Items
                            .Include("Category")
                            .Include("StockMeasurement")
                            .Include("CalculationMeasurement")
                            .Include("PurchaseMeasurementList")
                            .Where(i => i.Id == item.Id)
                            .First();

                    update.Name = item.Name != update.Name ? item.Name : update.Name;
                    update.ItemDescription = item.ItemDescription != update.ItemDescription ? item.ItemDescription : update.ItemDescription;
                    update.DefaultUnitPrice = item.DefaultUnitPrice != update.DefaultUnitPrice ? item.DefaultUnitPrice : update.DefaultUnitPrice;

                    update.Category = context.ItemCategories.Find(item.Category.Id);
                    update.StockMeasurement = context.Measurements.Find(item.StockMeasurement.Id);
                    update.CalculationMeasurement = context.Measurements.Find(item.CalculationMeasurement.Id);

                    List<Measurement> purchaseMeasurements = new List<Measurement>();

                    if (item.PurchaseMeasurementList != null)
                    {
                        foreach (Measurement measurement in item.PurchaseMeasurementList)
                        {
                            bool isExist = false;
                            foreach (Measurement existing in update.PurchaseMeasurementList)
                            {
                                if (measurement.Id == existing.Id)
                                {
                                    isExist = true;
                                    break;
                                }
                            }
                            if (!isExist)
                            {
                                Measurement purchaseMeasurement = context.Measurements.Find(measurement.Id);
                                update.PurchaseMeasurementList.Add(purchaseMeasurement);
                            }
                        }
                    }

                    if (update.PurchaseMeasurementList != null)
                    {
                        foreach (Measurement measurement in update.PurchaseMeasurementList)
                        {
                            bool isExist = false;
                            foreach (Measurement existing in item.PurchaseMeasurementList)
                            {
                                if (measurement.Id == existing.Id)
                                {
                                    isExist = true;
                                    break;
                                }
                            }
                            if (!isExist)
                            {
                                measurement.ItemPurchaseList.Remove(update);
                            }
                        }
                    }

                    DaoHelper.SetFieldActiveEntityFromObject(item, update);

                    context.Entry(update).State = SystemEntity.EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return item;
        }

        public Item Get(long id)
        {
            Item item = null;
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    var objCtx = DaoHelper.GetObjectContext(context);

                    item = context.Items.AsNoTracking()
                        .Include("Category")
                        .Include("StockMeasurement")
                        .Include("CalculationMeasurement")
                        .Include("PurchaseMeasurementList")
                        .Where(i => i.Id == id)
                        .First();
                }
            }
            catch (DataException e)
            {
                throw e;
            }

            return item;
        }

        public void Delete(long[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                foreach (long id in ids)
                {
                    try
                    {
                        using (context = new DaoContext(DaoHelper.DB_NAME))
                        {
                            Item item = context.Items.Find(id);
                            context.Items.Remove(item);
                            context.SaveChanges();
                        }
                    }
                    catch (DataException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public void Deactivate(long[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                foreach (long id in ids)
                {
                    try
                    {
                        using (context = new DaoContext(DaoHelper.DB_NAME))
                        {
                            Item item = context.Items.Find(id);
                            item.IsActive = false;
                            context.SaveChanges();
                        }
                    }
                    catch (DataException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public SearchResult<Item> Search(ItemSearchCriteria criteria)
        {
            SearchResult<Item> searchResult = new SearchResult<Item>();

            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    var objctx = DaoHelper.GetObjectContext(context);
                    SearchHelperValue searchHelperVal = DaoHelper.SearchHelperValueBuilder(criteria);
                    List<ObjectParameter> param = new List<ObjectParameter>();

                    StringBuilder sqlString = new StringBuilder();
                    SearchQueryBuilder.QuerySelectBuilder(sqlString, "Items");

                    SearchQueryBuilder.QueryLikeBuilder(param, sqlString, "Name", criteria.Name);
                    SearchQueryBuilder.QueryEqualBuilder<Boolean?>(param, sqlString, "IsActive", criteria.IsActive);
                    SearchQueryBuilder.QueryEqualBuilder<ItemType?>(param, sqlString, "ItemType", criteria.Type);

                    if (criteria.Category != null && criteria.Category.Id > 0)
                        SearchQueryBuilder.QueryEqualBuilder<long>(param, sqlString, "Category.Id", criteria.Category.Id);

                    if(criteria.StockMeasurement!=null && criteria.StockMeasurement.Id > 0 )
                        SearchQueryBuilder.QueryEqualBuilder<long>(param, sqlString, "StockMeasurement.Id", criteria.StockMeasurement.Id);

                    if (criteria.CalculationMeasurement != null && criteria.CalculationMeasurement.Id > 0)
                        SearchQueryBuilder.QueryEqualBuilder<long>(param, sqlString, "CalculationMeasurement.Id", criteria.CalculationMeasurement.Id);

                    SearchQueryBuilder.QueryGetTotal<Item>(param, sqlString, searchHelperVal, context);

                    SearchQueryBuilder.QuerySortByBuilder(param, sqlString, searchHelperVal);
                    SearchQueryBuilder.QueryPagingBuilder(param, sqlString, searchHelperVal);

                    ObjectQuery<Item> queryResult = objctx.CreateQuery<Item>(sqlString.ToString(), param.ToArray())
                        .Include("Category")
                        .Include("StockMeasurement")
                        .Include("CalculationMeasurement")
                        .Include("PurchaseMeasurementList");

                    List<Item> srcList = queryResult.ToList();
                    List<Item> list = new List<Item>();
                    foreach (Item item in srcList)
                    {
                        Item poco = null;
                        try
                        {
                            context.Configuration.ProxyCreationEnabled = false;
                            poco = context.Entry(item).CurrentValues.ToObject() as Item;
                            if (item.Category != null)
                                poco.Category = context.Entry(item.Category).CurrentValues.ToObject() as ItemCategory;
                            if (item.StockMeasurement != null)
                                poco.StockMeasurement = context.Entry(item.StockMeasurement).CurrentValues.ToObject() as Measurement;
                            if (item.CalculationMeasurement != null) 
                                poco.CalculationMeasurement = context.Entry(item.CalculationMeasurement).CurrentValues.ToObject() as Measurement;
                            
                            if (item.PurchaseMeasurementList != null)
                            {
                                List<Measurement> purchaseMeasurementList = new List<Measurement>();
                                foreach (Measurement measurement in item.PurchaseMeasurementList)
                                {
                                    Measurement noProxy = context.Entry(measurement).CurrentValues.ToObject() as Measurement;
                                    purchaseMeasurementList.Add(noProxy);
                                }
                                poco.PurchaseMeasurementList = purchaseMeasurementList;
                            }

                            list.Add(poco);
                        }
                        finally
                        {
                            context.Configuration.ProxyCreationEnabled = true;
                        }
                    }

                    searchResult.Items = list;
                    searchResult.Page = criteria.Page;
                    searchResult.PageSize = criteria.PageSize;
                    searchResult.Total = searchHelperVal.Total;
                }
            }
            catch (DataException e)
            {
                throw e;
            }
            return searchResult;
        }

    }
}
