﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;

namespace AIS.Backend.Service
{
    public interface SupplierService
    {
        Supplier Create(Supplier measurement);
        Supplier Update(Supplier measurement);
        Supplier Get(long id);
        void Delete(long[] ids);
        void Deactivate(long[] ids);
        SearchResult<Supplier> Search(SupplierSearchCriteria criteria);
    }
}
