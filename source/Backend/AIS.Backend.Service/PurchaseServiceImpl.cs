﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq.Expressions;
using AIS.Backend.Service;
using SystemEntity = System.Data.Entity;

namespace AIS.Backend.Service.Impl
{
    public class PurchaseServiceImpl : PurchaseService
    {
        /// <summary>
        /// Dao Context
        /// </summary>
        protected DaoContext context;

        public PurchaseServiceImpl() 
        {
        }

        public Purchase Create(Purchase purchase)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    if (purchase.Warehouse != null && purchase.Warehouse.Id > 0)
                        purchase.Warehouse = context.Warehouse.Find(purchase.Warehouse.Id);
                    else
                        purchase.Warehouse = null;

                    if (purchase.RequestorDepartment != null && purchase.RequestorDepartment.Id > 0)
                        purchase.RequestorDepartment = context.Departments.Find(purchase.RequestorDepartment.Id);
                    else
                        purchase.RequestorDepartment = null;

                    if (purchase.Supplier != null && purchase.Supplier.Id > 0)
                        purchase.Supplier = context.Suppliers.Find(purchase.Supplier.Id);
                    else
                        purchase.Supplier = null;

                    if (purchase.OrderDetailList != null) 
                    {
                        foreach (OrderDetails orderDetails in purchase.OrderDetailList)
                        {
                            //Set total amount in backend, for security reason
                            orderDetails.TotalPrice = DaoHelper.TotalAmountOfOrderDetails(orderDetails);
                            
                            if (orderDetails.Item != null && orderDetails.Item.Id > 0)
                                orderDetails.Item = context.Items.Find(orderDetails.Item.Id);
                            else
                                orderDetails.Item = null;

                            if (orderDetails.PurchaseMeasurement != null && orderDetails.PurchaseMeasurement.Id > 0)
                                orderDetails.PurchaseMeasurement = context.Measurements.Find(orderDetails.PurchaseMeasurement.Id);
                            else
                                orderDetails.PurchaseMeasurement = null;

                            if (orderDetails.Warehouse != null && orderDetails.Warehouse.Id > 0)
                                orderDetails.Warehouse = context.Warehouse.Find(orderDetails.Warehouse.Id);
                            else
                                orderDetails.Warehouse = purchase.Warehouse;
                        }
                    }
                    else
                        purchase.OrderDetailList = null;

                    setOrderDetailsTotal(purchase);

                    context.Purchase.Add(purchase);
                    context.SaveChanges();

                    /**
                     * Quantity Tracker Helper is create Stock Account, Stock Balance,
                     * and Stock Checkpoint for order details transaction
                     */
                    if (purchase.OrderDetailList != null)
                    {
                        foreach (OrderDetails orderDetails in purchase.OrderDetailList)
                        {
                            /**
                             * Quantity Tracker Helper is create Stock Account, Stock Balance,
                             * and Stock Checkpoint for order details transaction
                             */
                            QuantityTrackerHelper.CreateOrderDetailsQuantityTracker(orderDetails, context);
                        }
                    }

                    var objctx = DaoHelper.GetObjectContext(context);
                    objctx.Detach(purchase);
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return purchase;
        }

        private static void setOrderDetailsTotal(Purchase purchase)
        {
            double grandTotal = 0;
            if (purchase.OrderDetailList != null)
            {
                foreach (OrderDetails orderDetails in purchase.OrderDetailList)
                {
                    double subTotal = 0;

                    subTotal = orderDetails.Quantity * orderDetails.Price;
                    double disc = subTotal * (orderDetails.DiscountPercent / 100.00);
                    subTotal -= disc;
                    double taxTotal = subTotal * (orderDetails.TaxPercent / 100.00);
                    subTotal += taxTotal;
                    grandTotal += subTotal;
                }
                grandTotal += purchase.Freight;
            }
            purchase.GrandTotal = grandTotal;
        }

        public Purchase Update(Purchase purchase)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    Purchase update = context.Purchase
                        .Include("Warehouse")
                        .Include("RequestorDepartment")
                        .Include("Supplier")
                        .Include("OrderDetailList")
                        .Include("OrderDetailList.Item")
                        .Include("OrderDetailList.PurchaseMeasurement")

                        .Where(i => i.Id == purchase.Id)
                        .First();
                    update.No = purchase.No;
                    update.Date = purchase.Date;
                    update.DeliveryDate = purchase.DeliveryDate;
                    update.Freight = purchase.Freight;
                    update.Remark = purchase.Remark;
                    update.PaidStatus = purchase.PaidStatus != null ? purchase.PaidStatus : update.PaidStatus;
                    update.PaidDate = purchase.PaidDate != null ? purchase.PaidDate : update.PaidDate;
                    update.DeliveryStatus = purchase.DeliveryStatus != null ? purchase.DeliveryStatus : update.DeliveryStatus;
                    update.OtherCost = purchase.OtherCost;
                    update.IsClosed = purchase.IsClosed;

                    if (purchase.Warehouse != null && purchase.Warehouse.Id > 0)
                        update.Warehouse = context.Warehouse.Find(purchase.Warehouse.Id);
                    else
                        update.Warehouse = null;

                    if (purchase.RequestorDepartment != null && purchase.RequestorDepartment.Id > 0)
                        update.RequestorDepartment = context.Departments.Find(purchase.RequestorDepartment.Id);
                    else
                        update.RequestorDepartment = null;

                    if (purchase.Supplier != null && purchase.Supplier.Id > 0)
                        update.Supplier = context.Suppliers.Find(purchase.Supplier.Id);
                    else
                        update.Supplier = null;

                    UpdateOrderDetailsList(update.OrderDetailList, purchase.OrderDetailList, update.OrderDetailList, context);

                    setOrderDetailsTotal(purchase);

                    update.UpdatedBy = purchase.UpdatedBy;
                    update.UpdatedDate = purchase.UpdatedDate;

                    /**
                     * Added new Order Details for purchase transaction
                     */
                    if (purchase.OrderDetailList != null)
                    {
                        foreach (OrderDetails orderDetails in purchase.OrderDetailList)
                        {
                            if (orderDetails.Id == 0)
                            {
                                //Set total amount in backend, for security reason
                                orderDetails.TotalPrice = DaoHelper.TotalAmountOfOrderDetails(orderDetails);

                                if (orderDetails.Item != null && orderDetails.Item.Id > 0)
                                    orderDetails.Item = context.Items.Find(orderDetails.Item.Id);
                                else
                                    orderDetails.Item = null;

                                if (orderDetails.PurchaseMeasurement != null && orderDetails.PurchaseMeasurement.Id > 0)
                                    orderDetails.PurchaseMeasurement = context.Measurements.Find(orderDetails.PurchaseMeasurement.Id);
                                else
                                    orderDetails.PurchaseMeasurement = null;

                                if (orderDetails.Warehouse != null && orderDetails.Warehouse.Id > 0)
                                    orderDetails.Warehouse = context.Warehouse.Find(orderDetails.Warehouse.Id);
                                else
                                    orderDetails.Warehouse = purchase.Warehouse;
                                update.OrderDetailList.Add(orderDetails);
                            }
                        }
                    }

                    context.Entry(update).State = SystemEntity.EntityState.Modified;
                    context.SaveChanges();

                    List<OrderDetails> updatedOrderDetails = GeneralHelper.ConvertICollectionToList(update.OrderDetailList);

                    /**
                     * Added new Order Details for purchase transaction
                     */
                    if (purchase.OrderDetailList != null)
                    {
                        int i = 0;
                        foreach (OrderDetails orderDetails in purchase.OrderDetailList)
                        {
                            if (orderDetails.Id == 0)
                            {
                                /**
                                 * Quantity Tracker Helper is create Stock Account, Stock Balance,
                                 * and Stock Checkpoint for order details transaction
                                 */
                                QuantityTrackerHelper.CreateOrderDetailsQuantityTracker(updatedOrderDetails[i], context);
                            }
                            i++;
                        }
                    }

                    var objctx = DaoHelper.GetObjectContext(context);
                    try
                    {
                        objctx.Detach(purchase);
                    }
                    catch (Exception e) {}
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return purchase;
        }

        public void UpdateOrderDetailsList(ICollection<OrderDetails> existingList, ICollection<OrderDetails> updatedList,
            ICollection<OrderDetails> targetList, DaoContext context)
        {
            /**
             * Added new Order Details for purchase transaction
             
            if (updatedList != null)
            {
                foreach (OrderDetails orderDetails in updatedList)
                {
                    if (orderDetails.Id == 0)
                    {
                        targetList.Add(orderDetails);

                        QuantityTrackerHelper.CreateOrderDetailsQuantityTracker(orderDetails, context);
                    }
                }
            }
            */

            /**
             * Remove deleted Order Details for purchase transaction
             */
            if (updatedList != null)
            {
                List<OrderDetails> orderDetailList = GeneralHelper.ConvertICollectionToList<OrderDetails>(targetList);

                foreach (OrderDetails orderDetails in orderDetailList)
                {
                    bool isExist = false;
                    foreach (OrderDetails updatedOrderDetails in updatedList)
                    {
                        if (orderDetails.Id == updatedOrderDetails.Id)
                        {
                            isExist = true;
                            break;
                        }
                    }

                    if (!isExist)
                    {
                        /**
                         * Quantity Tracker Helper is remove Stock Checkpoint,
                         * and Changes Stock Balance for order details transaction
                         */
                        QuantityTrackerHelper.RemoveOrderDetailsQuantityTracker(orderDetails, context);

                        context.OrderDetails.Remove(orderDetails);
                    }
                }
            }

            /**
             * Update Order Details for purchase transaction
             */
            if (updatedList != null)
            {
                foreach (OrderDetails orderDetails in targetList)
                {
                    foreach (OrderDetails updatedOrderDetails in updatedList)
                    {
                        if (orderDetails.Id == updatedOrderDetails.Id)
                        {
                            CheckAndUpdateOrderDetails(updatedOrderDetails, orderDetails, context);
                        }
                    }

                }
            }
        }

        public void CheckAndUpdateOrderDetails(OrderDetails src, OrderDetails target, DaoContext context)
        {
            if (
                src.Item.Id != target.Item.Id ||
                src.Quantity != target.Quantity ||
                src.Price != target.Price ||
                src.DiscountPercent != target.DiscountPercent ||
                src.TaxPercent != target.TaxPercent ||
                src.TotalPrice != target.TotalPrice ||
                src.OrderReceivedFull != target.OrderReceivedFull ||
                src.OrderReceivedQuantity != target.OrderReceivedQuantity
                )
            {
                UpdateOrderDetails(src, target, context);
            }
        }

        public void UpdateOrderDetails(OrderDetails src, OrderDetails target, DaoContext context)
        {

            if (target.Item.Id != src.Item.Id)
            {
                /**
                 * Quantity Tracker Helper is create Stock Account, Stock Balance,
                 * and Stock Checkpoint for order details transaction
                 */
                QuantityTrackerHelper.CreateOrderDetailsQuantityTracker(src, context);

                /**
                 * Quantity Tracker Helper is remove Stock Checkpoint,
                 * and Changes Stock Balance for order details transaction
                 */
                QuantityTrackerHelper.RemoveOrderDetailsQuantityTracker(target, context);
            }
            else
            {
                /**
                 * Quantity Tracker Helper is update Stock Checkpoint,
                 * and Changes Stock Balance for order details transaction
                 */
                QuantityTrackerHelper.UpdateOrderDetailsQuantityTracker(src,target.Quantity, context);
            }

            target.Item = context.Items.Find(src.Item.Id);
            target.PurchaseMeasurement = context.Measurements.Find(src.PurchaseMeasurement.Id);
            target.Quantity = src.Quantity;
            target.Price = src.Price;
            target.DiscountPercent = src.DiscountPercent;
            target.TaxPercent = src.TaxPercent;
            target.TotalPrice = DaoHelper.TotalAmountOfOrderDetails(src);
            target.OrderReceivedFull = src.OrderReceivedFull;
            target.OrderReceivedQuantity = src.OrderReceivedQuantity;
            target.Warehouse = context.Warehouse.Find(src.Warehouse.Id);
        }

        public Purchase UpdateReceivedStock(Purchase purchase)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    Purchase update = context.Purchase
                        .Include("Warehouse")
                        .Include("RequestorDepartment")
                        .Include("Supplier")
                        .Include("OrderDetailList")
                        .Include("OrderDetailList.Item")
                        .Include("OrderDetailList.PurchaseMeasurement")
                        .Where(i => i.Id == purchase.Id)
                        .First();

                    SaveOrderDetailsReceivedQty(purchase.OrderDetailList,context);
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return purchase;
        }

        public Purchase UpdateReturnStock(Purchase purchase)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    Purchase update = context.Purchase
                        .Include("Warehouse")
                        .Include("RequestorDepartment")
                        .Include("Supplier")
                        .Include("OrderDetailList")
                        .Include("OrderDetailList.Item")
                        .Include("OrderDetailList.PurchaseMeasurement")
                        .Where(i => i.Id == purchase.Id)
                        .First();

                    SaveOrderDetailReturnQty(purchase.OrderDetailList, context);
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return purchase;
        }


        public void SaveOrderDetailsReceivedQty(ICollection<OrderDetails> orderDetailList, DaoContext context)
        {
            if (orderDetailList != null)
            {
                foreach (OrderDetails orderDetails in orderDetailList)
                {
                    double updatedReceivedQuantity = orderDetails.OrderReceivedQuantity;

                    OrderDetails orderDetailsLoad = context.OrderDetails.Find(orderDetails.Id);
                    orderDetailsLoad.OrderReceivedQuantity = updatedReceivedQuantity;
                    orderDetailsLoad.OrderReceivedDate = DateTime.Now;
                    context.Entry<OrderDetails>(orderDetailsLoad).State = SystemEntity.EntityState.Modified;
                    context.SaveChanges();

                    /**
                     * Create Quantity Tracker 
                     */
                    QuantityTrackerHelper.CreateUpdateReceivedStock(orderDetails, context);
                }
            }
        }

        public void SaveOrderDetailReturnQty(ICollection<OrderDetails> orderDetailList, DaoContext context)
        {
            if (orderDetailList != null)
            {
                foreach (OrderDetails orderDetails in orderDetailList)
                {
                    double updatedReturnQty= orderDetails.ReturnQuantity;

                    OrderDetails orderDetailsLoad = context.OrderDetails.Find(orderDetails.Id);
                    orderDetailsLoad.ReturnQuantity = updatedReturnQty;
                    context.Entry<OrderDetails>(orderDetailsLoad).State = SystemEntity.EntityState.Modified;
                    context.SaveChanges();

                    /**
                     * Create Quantity Tracker 
                     */
                    QuantityTrackerHelper.CreateUpdateReturnStock(orderDetails, context);
                }
            }
        }


        public Purchase Get(long id)
        {
            Purchase purchase = null;
            Purchase poco = null;
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    var objCtx = DaoHelper.GetObjectContext(context);

                    purchase = context.Purchase
                        .Include("Warehouse")
                        .Include("RequestorDepartment")
                        .Include("Supplier")
                        .Include("OrderDetailList")
                        .Include("OrderDetailList.Item")
                        .Include("OrderDetailList.PurchaseMeasurement")
                        
                        .Where(i => i.Id == id)
                        .First();

                    
                    try
                    {
                        context.Configuration.ProxyCreationEnabled = false;
                        poco = context.Entry(purchase).CurrentValues.ToObject() as Purchase;
                        if (purchase.Warehouse != null)
                            poco.Warehouse = context.Entry(purchase.Warehouse).CurrentValues.ToObject() as Warehouse;
                        if (purchase.RequestorDepartment != null)
                            poco.RequestorDepartment = context.Entry(purchase.RequestorDepartment).CurrentValues.ToObject() as Department;
                        if (purchase.Supplier != null)
                            poco.Supplier = context.Entry(purchase.Supplier).CurrentValues.ToObject() as Supplier;

                        if (purchase.OrderDetailList != null)
                        {
                            List<OrderDetails> orderDetailList = new List<OrderDetails>();
                            foreach (OrderDetails orderDetails in purchase.OrderDetailList)
                            {
                                OrderDetails noProxy = context.Entry(orderDetails).CurrentValues.ToObject() as OrderDetails;
                                if (orderDetails.Item != null)
                                    noProxy.Item = context.Entry(orderDetails.Item).CurrentValues.ToObject() as Item;
                                if (orderDetails.PurchaseMeasurement != null)
                                    noProxy.PurchaseMeasurement = context.Entry(orderDetails.PurchaseMeasurement).CurrentValues.ToObject() as Measurement;
                                if (orderDetails.Purchase != null)
                                    noProxy.Purchase = context.Entry(orderDetails.Purchase).CurrentValues.ToObject() as Purchase;
                                if (orderDetails.Warehouse != null)
                                    noProxy.Warehouse = context.Entry(orderDetails.Warehouse).CurrentValues.ToObject() as Warehouse;

                                orderDetailList.Add(noProxy);
                            }
                            poco.OrderDetailList = orderDetailList;
                        }
                    }
                    finally
                    {
                        context.Configuration.ProxyCreationEnabled = true;
                    }
                }
            }
            catch (DataException e)
            {
                throw e;
            }

            return poco;
        }

        public void Delete(long[] ids)
        {

        }

        public SearchResult<Purchase> Search(PurchaseSearchCriteria criteria)
        {
            return null;
        }
    }
}
