﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq.Expressions;
using SystemEntity = System.Data.Entity;
namespace AIS.Backend.Service.Impl
{
    public class WarehouseServiceImpl : WarehouseService
    {
        /// <summary>
        /// Dao Context
        /// </summary>
        protected DaoContext context;

        public Warehouse Create(Warehouse warehouse)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    DaoHelper.CheckSameNameInsert<Warehouse>(context.Warehouse, warehouse);

                    if (warehouse.Department != null && warehouse.Department.Id > 0)
                        warehouse.Department = context.Departments.Find(warehouse.Department.Id);
                    else
                        warehouse.Department = null;

                    context.Warehouse.Add(warehouse);
                    context.SaveChanges();
                    var objctx = DaoHelper.GetObjectContext(context);
                    objctx.Detach(warehouse);
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return warehouse;
        }

        public Warehouse Update(Warehouse warehouse)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    DaoHelper.CheckSameNameUpdate<Warehouse>(context.Warehouse, warehouse);

                    Warehouse update = context.Warehouse
                            .Include("Department")
                            .Where(i => i.Id == warehouse.Id)
                            .First();

                    update.Name = warehouse.Name != update.Name ? warehouse.Name : update.Name;
                    update.Description = warehouse.Description != update.Description ? warehouse.Description : update.Description;
                    update.Shelf = warehouse.Shelf != update.Shelf ? warehouse.Shelf : update.Shelf;
                    update.Label = warehouse.Label != update.Label ? warehouse.Label : update.Label;

                    if (warehouse.Department != null && warehouse.Department.Id > 0)
                        update.Department = context.Departments.Find(warehouse.Department.Id);
                    else
                        update.Department = null;

                    DaoHelper.SetFieldActiveEntityFromObject(warehouse, update);

                    context.Entry(update).State = SystemEntity.EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return warehouse;
        }

        public Warehouse Get(long id)
        {
            Warehouse warehouse = null;
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    var objCtx = DaoHelper.GetObjectContext(context);

                    warehouse = context.Warehouse
                            .Include("Department")
                            .Where(i => i.Id == id)
                            .First();
                }
            }
            catch (DataException e)
            {
                throw e;
            }

            return warehouse;
        }

        public void Delete(long[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                foreach (long id in ids)
                {
                    try
                    {
                        using (context = new DaoContext(DaoHelper.DB_NAME))
                        {
                            Warehouse warehouse = context.Warehouse.Find(id);
                            context.Warehouse.Remove(warehouse);
                            context.SaveChanges();
                        }
                    }
                    catch (DataException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public void Deactivate(long[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                foreach (long id in ids)
                {
                    try
                    {
                        using (context = new DaoContext(DaoHelper.DB_NAME))
                        {
                            Warehouse warehouse = context.Warehouse.Find(id);
                            warehouse.IsActive = false;
                            context.Entry(warehouse).State = SystemEntity.EntityState.Modified;
                            context.SaveChanges();
                        }
                    }
                    catch (DataException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public SearchResult<Warehouse> Search(WarehouseSearchCriteria criteria)
        {
            SearchResult<Warehouse> searchResult = new SearchResult<Warehouse>();

            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    var objctx = DaoHelper.GetObjectContext(context);
                    SearchHelperValue searchHelperVal = DaoHelper.SearchHelperValueBuilder(criteria);
                    List<ObjectParameter> param = new List<ObjectParameter>();

                    StringBuilder sqlString = new StringBuilder();
                    SearchQueryBuilder.QuerySelectBuilder(sqlString, "Warehouse");

                    SearchQueryBuilder.QueryLikeBuilder(param, sqlString, "Name", criteria.Name);
                    SearchQueryBuilder.QueryEqualBuilder<Boolean?>(param, sqlString, "IsActive", criteria.IsActive);
                    if (criteria.Department != null && criteria.Department.Id > 0)
                    {
                        SearchQueryBuilder.QueryEqualBuilder<long>(param, sqlString, "Department.Id", criteria.Department.Id);
                    }
                    if (criteria.Department != null && criteria.Department.Name !=null)
                    {
                        SearchQueryBuilder.QueryEqualBuilder<string>(param, sqlString, "Department.Name", criteria.Department.Name);
                    }


                    SearchQueryBuilder.QueryGetTotal<Department>(param, sqlString, searchHelperVal, context);

                    SearchQueryBuilder.QuerySortByBuilder(param, sqlString, searchHelperVal);
                    SearchQueryBuilder.QueryPagingBuilder(param, sqlString, searchHelperVal);

                    ObjectQuery<Department> queryResult = objctx.CreateQuery<Department>(sqlString.ToString(), param.ToArray());

                    SearchQueryBuilder.QueryGetSearchResult<Warehouse, WarehouseSearchCriteria>(param, sqlString, criteria, searchHelperVal, searchResult, context);
                }
            }
            catch (DataException e)
            {
                throw e;
            }
            return searchResult;
        }
    }
}
