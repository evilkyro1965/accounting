﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq.Expressions;
using SystemEntity = System.Data.Entity;

namespace AIS.Backend.Service.Impl
{
    public class EmployeeServiceImpl : EmployeeService
    {
        /// <summary>
        /// Dao Context
        /// </summary>
        protected DaoContext context;

        public EmployeeServiceImpl()
        {
        }

        public Employee Create(Employee employee)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    int count = context.Employee.Where(obj => obj.Username == employee.Username).Count();
                    if (count > 0)
                    {
                        throw new Exception("Employee with that username is already exist");
                    }

                    if (employee.Department != null && employee.Department.Id > 0)
                    {
                        employee.Department = context.Departments.Find(employee.Department.Id);
                    }
                    else
                    {
                        employee.Department = null;
                    }
                    context.Employee.Add(employee);
                    context.SaveChanges();
                    var objctx = DaoHelper.GetObjectContext(context);
                    objctx.Detach(employee);
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return employee;
        }

        public Employee Update(Employee employee)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    Employee update = context.Employee.Find(employee.Id);

                    int count = context.Employee.Where(obj => obj.Username == employee.Username)
                        .Where(obj => obj.Id != employee.Id)
                        .Count();
                    if (count > 0)
                    {
                        throw new Exception("Employee with that username is already exist");
                    }

                    if (employee.Department != null && employee.Department.Id > 0)
                    {
                        update.Department = context.Departments.Find(employee.Department.Id);
                    }
                    else
                    {
                        employee.Department = null;
                    }

                    update.Username = employee.Username;
                    update.Password = employee.Password;
                    update.Mobile = employee.Mobile;
                    update.Email = employee.Email;
                    
                    DaoHelper.SetFieldActiveEntityFromObject(employee, update);

                    context.Entry(update).State = SystemEntity.EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return employee;
        }

        public Employee Get(long id)
        {
            Employee employee = null;
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    employee = context.Employee.Include("Department")
                        .Where(obj => obj.Id == id)
                        .FirstOrDefault();
                }
            }
            catch (DataException e)
            {
                throw e;
            }

            return employee;
        }

        public void Delete(long[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                foreach (long id in ids)
                {
                    try
                    {
                        using (context = new DaoContext(DaoHelper.DB_NAME))
                        {
                            Employee employee = context.Employee.Find(id);
                            context.Employee.Remove(employee);
                            context.SaveChanges();
                        }
                        
                    }
                    catch (DataException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public void Deactivate(long[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                foreach (long id in ids)
                {
                    try
                    {
                        using (context = new DaoContext(DaoHelper.DB_NAME))
                        {
                            Employee employee = context.Employee.Find(id);
                            employee.IsActive = false;
                            context.SaveChanges();
                        }
                    }
                    catch (DataException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public SearchResult<Employee> Search(EmployeeSearchCriteria criteria)
        {
            SearchResult<Employee> searchResult = new SearchResult<Employee>();

            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    var objctx = DaoHelper.GetObjectContext(context);
                    SearchHelperValue searchHelperVal = DaoHelper.SearchHelperValueBuilder(criteria);
                    List<ObjectParameter> param = new List<ObjectParameter>();

                    StringBuilder sqlString = new StringBuilder();
                    SearchQueryBuilder.QuerySelectBuilder(sqlString, "Employee");

                    SearchQueryBuilder.QueryLikeBuilder(param, sqlString, "Name", criteria.Name);
                    SearchQueryBuilder.QueryEqualBuilder<Boolean?>(param, sqlString, "IsActive", criteria.IsActive);
                    SearchQueryBuilder.QueryLikeBuilder(param, sqlString, "Username", criteria.Username);
                    SearchQueryBuilder.QueryLikeBuilder(param, sqlString, "Email", criteria.Email);
                    SearchQueryBuilder.QueryLikeBuilder(param, sqlString, "Mobile", criteria.Mobile);

                    SearchQueryBuilder.QueryGetTotal<Department>(param, sqlString, searchHelperVal, context);

                    SearchQueryBuilder.QuerySortByBuilder(param, sqlString, searchHelperVal);
                    SearchQueryBuilder.QueryPagingBuilder(param, sqlString, searchHelperVal);

                    ObjectQuery<Employee> queryResult = objctx.CreateQuery<Employee>(sqlString.ToString(), param.ToArray()).Include("Department");

                    List<Employee> srcList = queryResult.ToList();
                    List<Employee> list = new List<Employee>();
                    foreach (Employee item in srcList)
                    {
                        Employee poco = null;
                        try
                        {
                            context.Configuration.ProxyCreationEnabled = false;
                            poco = context.Entry(item).CurrentValues.ToObject() as Employee;
                            if (item.Department != null)
                            {
                                poco.Department = context.Entry(item.Department).CurrentValues.ToObject() as Department;
                            }
                            list.Add(poco);
                        }
                        finally
                        {
                            context.Configuration.ProxyCreationEnabled = true;
                        }
                    }

                    searchResult.Items = list;
                    searchResult.Page = criteria.Page;
                    searchResult.PageSize = criteria.PageSize;
                    searchResult.Total = searchHelperVal.Total;
                }
            }
            catch (DataException e)
            {
                throw e;
            }
            return searchResult;
        }

    }
}
