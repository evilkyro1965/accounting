﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;

namespace AIS.Backend.Service
{
    public interface ItemService
    {
        Item Create(Item item);
        Item Update(Item item);
        Item Get(long id);
        void Delete(long[] ids);
        void Deactivate(long[] ids);
        SearchResult<Item> Search(ItemSearchCriteria criteria);

    }
}
