﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq.Expressions;
using AIS.Backend.Service;
using SystemEntity = System.Data.Entity;

namespace AIS.Backend.Service
{
    public class QuantityTrackerHelper
    {
        private static string StockFieldTable = DaoHelper.STOCK_CHECKPOINT_TABLE;

        private static string IdField = DaoHelper.ID_FIELD;

        private static string StockAccountField = DaoHelper.STOCK_ACCOUNT_FIELD;

        private static string BalanceAfterField = DaoHelper.BALANCE_AFTER_FIELD;

        private static string GetQueryUpdateMinusBalanceAfterForNextCheckpoint(double amountUpdate,long stockAccountIdUpdate, long stockCheckpointIdUpdate)
        {
            string queryUpdateBalanceAfterForNextCheckpoint = 
            "UPDATE " + StockFieldTable +
                " SET " + BalanceAfterField + " = " + BalanceAfterField + " - ( " + amountUpdate + " )" +
                " WHERE " + StockAccountField + " = " + stockAccountIdUpdate +
                " AND " + IdField + " > " + stockCheckpointIdUpdate;

            return queryUpdateBalanceAfterForNextCheckpoint;
        }

        private static string GetQueryUpdatePlusBalanceAfterForNextCheckpoint(double amountUpdate, long stockAccountIdUpdate, long stockCheckpointIdUpdate)
        {
            string queryUpdateBalanceAfterForNextCheckpoint =
            "UPDATE " + StockFieldTable +
                " SET " + BalanceAfterField + " = " + BalanceAfterField + " + ( " + amountUpdate + " )" +
                " WHERE " + StockAccountField + " = " + stockAccountIdUpdate +
                " AND " + IdField + " > " + stockCheckpointIdUpdate;

            return queryUpdateBalanceAfterForNextCheckpoint;
        }

        private static int CheckpointBalanceSubstractForNextCheckpoint(double amountUpdate, long stockAccountIdUpdate, long stockCheckpointIdUpdate, DaoContext context) 
        {
            string queryUpdated = GetQueryUpdateMinusBalanceAfterForNextCheckpoint(amountUpdate, stockAccountIdUpdate, stockCheckpointIdUpdate);
            int rowUpdated = context.Database.ExecuteSqlCommand(queryUpdated);
            return rowUpdated;
        }

        private static int CheckpointBalanceAdditionForNextCheckpoint(double amountUpdate, long stockAccountIdUpdate, long stockCheckpointIdUpdate, DaoContext context)
        {
            string queryUpdated = GetQueryUpdatePlusBalanceAfterForNextCheckpoint(amountUpdate, stockAccountIdUpdate, stockCheckpointIdUpdate);
            int rowUpdated = context.Database.ExecuteSqlCommand(queryUpdated);
            return rowUpdated;
        }

        public static void CreateOrderDetailsQuantityTracker(OrderDetails orderDetails, DaoContext context)
        {
            StockTransactionType transactionType = StockTransactionType.PURCHASE_ORDER_CREATED;
            StockAccountType accountType = StockAccountType.QUANTITY_ON_ORDER;

            StockBalance stockBalance = GetStockBalance(orderDetails, transactionType, accountType, context);

            StockCheckpoint stockCheckpoint = CreateStockCheckpointForCreateOrderDetails(
                    orderDetails, stockBalance, context
                );

            context.SaveChanges();
        }

        public static void RemoveOrderDetailsQuantityTracker(OrderDetails orderDetails, DaoContext context)
        {
            StockTransactionType transactionType = StockTransactionType.PURCHASE_ORDER_CREATED;
            StockAccountType accountType = StockAccountType.QUANTITY_ON_ORDER;

            StockBalance stockBalance = GetStockBalance(orderDetails, transactionType, accountType, context);

            StockCheckpoint stockCheckpoint = RemoveStockCheckpointForRemoveOrderDetails(
                    orderDetails, stockBalance, context
                );

            context.StockCheckpoint.Remove(stockCheckpoint);
            context.SaveChanges();
        }

        public static void UpdateOrderDetailsQuantityTracker(OrderDetails orderDetails, double beforeAmount, DaoContext context)
        {
            StockTransactionType transactionType = StockTransactionType.PURCHASE_ORDER_CREATED;
            StockAccountType accountType = StockAccountType.QUANTITY_ON_ORDER;

            StockBalance stockBalance = GetStockBalance(orderDetails, transactionType, accountType, context);

            UpdateStockCheckpointForUpdateOrderDetails(
                    orderDetails, beforeAmount, stockBalance, context
                );
        }

        public static StockCheckpoint CreateStockCheckpointForCreateOrderDetails(
            OrderDetails orderDetails,
            StockBalance stockBalance,
            DaoContext context
            )
        {
            StockTransactionType transactionType = StockTransactionType.PURCHASE_ORDER_CREATED;
            StockAccountType accountType = StockAccountType.QUANTITY_ON_ORDER;
            double unitPerMeasurement = GetPurchaseMeasurementQuantity(orderDetails.PurchaseMeasurement,context);
            double orderDetailsQuantity = unitPerMeasurement * orderDetails.Quantity;
            double amount = orderDetailsQuantity;
            double balanceAfter = stockBalance.Balance + orderDetailsQuantity;
            double balance = stockBalance.Balance + orderDetailsQuantity;

            StockCheckpoint stockCheckpoint = CreateStockCheckpoint(
                orderDetails,
                transactionType,
                accountType,
                amount,
                balanceAfter,
                context);

            UpdateStockBalance(stockBalance, orderDetails, transactionType, balance, context);

            return stockCheckpoint;
        }

        public static void UpdateStockCheckpointForUpdateOrderDetails(
            OrderDetails orderDetails,
            double beforeAmount,
            StockBalance stockBalance,
            DaoContext context
            )
        {
            StockAccountType accountType = StockAccountType.QUANTITY_ON_ORDER;
            StockTransactionType transactionType = StockTransactionType.PURCHASE_ORDER_CREATED;
            double unitPerMeasurement = GetPurchaseMeasurementQuantity(orderDetails.PurchaseMeasurement, context);
            double orderDetailsQuantity = unitPerMeasurement * orderDetails.Quantity;

            double amount = orderDetailsQuantity;
            double diffAmount = amount - beforeAmount;

            long stockAccountId = stockBalance.StockAccount.Id;
            long orderDetailsId = orderDetails.Id;

            StockCheckpoint stockCheckpoint = GetStockCheckpoint(stockBalance.StockAccount.Id, orderDetails.Id, transactionType, context);

            if (stockCheckpoint != null)
            {
                StockCheckpoint stockCheckpointLatest = GetLatestStockCheckpointOfAccount(stockBalance.Id, stockCheckpoint.Id, context);

                double latestCheckpointBalance = stockCheckpointLatest != null ? stockCheckpointLatest.BalanceAfter : 0.00;
                double balanceAfter = latestCheckpointBalance + orderDetailsQuantity;
                double balance = stockBalance.Balance + diffAmount;

                stockCheckpoint.TransactionDateTime = DateTime.Now;
                stockCheckpoint.Timestamp = DateTime.Now;
                stockCheckpoint.Amount = amount;
                stockCheckpoint.BalanceAfter = balanceAfter;

                UpdateStockBalance(stockBalance, orderDetails, transactionType, balance, context);

                context.Entry<StockCheckpoint>(stockCheckpoint).State = SystemEntity.EntityState.Modified;

                context.SaveChanges();
            }
            else
            {
                double balanceAfter = stockBalance.Balance + orderDetailsQuantity;

                stockCheckpoint = CreateStockCheckpoint(
                    orderDetails,
                    transactionType,
                    accountType,
                    amount,
                    balanceAfter,
                    context);
            }

            //Update next checkpoint balance after, since the current balance get from previous balance
            CheckpointBalanceAdditionForNextCheckpoint(diffAmount, stockAccountId, stockCheckpoint.Id, context);
        }

        public static StockCheckpoint RemoveStockCheckpointForRemoveOrderDetails(
            OrderDetails orderDetails,
            StockBalance stockBalance,
            DaoContext context
            )
        {
            StockTransactionType transactionType = StockTransactionType.PURCHASE_ORDER_CREATED;
            long stockAccountId = stockBalance.StockAccount.Id;
            long orderDetailsId = orderDetails.Id;

            StockCheckpoint stockCheckpoint = GetStockCheckpoint(stockBalance.StockAccount.Id, orderDetails.Id, transactionType, context);

            double amount = stockCheckpoint.Amount;

            stockBalance.Balance = stockBalance.Balance - amount;

            context.Entry<StockBalance>(stockBalance).State = SystemEntity.EntityState.Modified;

            //Update next checkpoint balance after, since the current balance get from previous balance
            CheckpointBalanceSubstractForNextCheckpoint(amount, stockAccountId, stockCheckpoint.Id, context);

            context.SaveChanges();

            return stockCheckpoint;
        }

        public static void CreateUpdateReceivedStock(OrderDetails orderDetails, DaoContext context)
        {
                StockTransactionType receiveTransactionType = StockTransactionType.PURCHASE_ORDER_RECEIVE;
                StockAccountType accountTypeOnOrder = StockAccountType.QUANTITY_ON_ORDER;
                StockAccountType accountTypeOnHand = StockAccountType.QUANTITY_ON_HAND;

                long orderDetailsId = orderDetails.Id;

                StockBalance stockBalanceOnOrder = GetStockBalance(orderDetails, receiveTransactionType,
                    accountTypeOnOrder, context);
                StockBalance stockBalanceOnHand = GetStockBalance(orderDetails, receiveTransactionType,
                    accountTypeOnHand, context);

                StockCheckpoint stockCheckpointOnOrder = GetStockCheckpoint(stockBalanceOnOrder.StockAccount.Id, orderDetails.Id, receiveTransactionType, context);
                long currentOnOrderCheckpointId = stockCheckpointOnOrder == null ? 0 : stockCheckpointOnOrder.Id;
                StockCheckpoint stockCheckpointOnOrderLatest = GetLatestStockCheckpointOfAccount(stockBalanceOnOrder.StockAccount.Id, currentOnOrderCheckpointId, context);
                StockCheckpoint stockCheckpointOnHand = GetStockCheckpoint(stockBalanceOnHand.StockAccount.Id, orderDetails.Id, receiveTransactionType, context);
                long currentOnHandCheckpointId = stockCheckpointOnHand == null ? 0 : stockCheckpointOnHand.Id;
                StockCheckpoint stockCheckpointOnHandLatest = GetLatestStockCheckpointOfAccount(stockBalanceOnHand.StockAccount.Id, currentOnHandCheckpointId, context);
                
                double unitPerMeasurement = GetPurchaseMeasurementQuantity(orderDetails.PurchaseMeasurement, context);
                double orderReceivedQuantity = unitPerMeasurement * orderDetails.OrderReceivedQuantity;

                if (stockCheckpointOnHand == null)
                {
                    double amountOnHand = orderReceivedQuantity;
                    double balanceAfterOnHand = stockBalanceOnHand.Balance + orderReceivedQuantity;
                    double balanceOnHand = stockBalanceOnHand.Balance + orderReceivedQuantity;

                    stockCheckpointOnHand = CreateStockCheckpoint(
                        orderDetails,
                        receiveTransactionType,
                        accountTypeOnHand,
                        amountOnHand,
                        balanceAfterOnHand,
                        context);

                    UpdateStockBalance(stockBalanceOnHand, orderDetails, receiveTransactionType, balanceOnHand, context);
                }
                else
                {
                    double amountDiff = orderReceivedQuantity - stockCheckpointOnHand.Amount;
                    double latestCheckpointBalance = stockCheckpointOnHandLatest != null ? stockCheckpointOnHandLatest.BalanceAfter : 0.00;

                    double amountOnHand = orderReceivedQuantity;
                    double balanceAfterOnHand = latestCheckpointBalance + amountOnHand;
                    double balanceOnHand = stockBalanceOnHand.Balance + amountDiff;

                    stockCheckpointOnHand.TransactionDateTime = DateTime.Now;
                    stockCheckpointOnHand.Timestamp = DateTime.Now;
                    stockCheckpointOnHand.Amount = amountOnHand;
                    stockCheckpointOnHand.BalanceAfter = balanceAfterOnHand;

                    UpdateStockBalance(stockBalanceOnHand, orderDetails, receiveTransactionType, balanceOnHand, context);

                    //Update next checkpoint balance after, since the current balance get from previous balance
                    CheckpointBalanceAdditionForNextCheckpoint(amountDiff, stockBalanceOnHand.StockAccount.Id, stockCheckpointOnHand.Id, context);

                    context.Entry<StockCheckpoint>(stockCheckpointOnHand).State = SystemEntity.EntityState.Modified;
                    context.SaveChanges();
                }

                if (stockCheckpointOnOrder == null)
                {
                    double amountOnOrder = -1 * orderReceivedQuantity;
                    double balanceAfterOnOrder = stockBalanceOnOrder.Balance - orderReceivedQuantity;
                    double balanceOnOrder = stockBalanceOnOrder.Balance - orderReceivedQuantity;

                    stockCheckpointOnOrder = CreateStockCheckpoint(
                        orderDetails,
                        receiveTransactionType,
                        accountTypeOnOrder,
                        amountOnOrder,
                        balanceAfterOnOrder,
                        context);

                    UpdateStockBalance(stockBalanceOnOrder, orderDetails, receiveTransactionType, balanceOnOrder, context);
                }
                else
                {
                    double amountDiff = orderReceivedQuantity - (-1 * stockCheckpointOnOrder.Amount);
                    double latestCheckpointBalance = stockCheckpointOnOrderLatest != null ? stockCheckpointOnOrderLatest.BalanceAfter : 0.00;

                    double amountOnOrder = -1 * orderReceivedQuantity;
                    double balanceAfterOnOrder = latestCheckpointBalance - orderReceivedQuantity;
                    double balanceOnOrder = stockBalanceOnOrder.Balance - amountDiff;

                    stockCheckpointOnOrder.TransactionDateTime = DateTime.Now;
                    stockCheckpointOnOrder.Timestamp = DateTime.Now;
                    stockCheckpointOnOrder.Amount = amountOnOrder;
                    stockCheckpointOnOrder.BalanceAfter = balanceAfterOnOrder;

                    UpdateStockBalance(stockBalanceOnOrder, orderDetails, receiveTransactionType, balanceOnOrder, context);

                    //Update next checkpoint balance after, since the current balance get from previous balance
                    CheckpointBalanceSubstractForNextCheckpoint(amountDiff, stockBalanceOnOrder.StockAccount.Id, stockCheckpointOnOrder.Id, context);

                    context.Entry<StockCheckpoint>(stockCheckpointOnOrder).State = SystemEntity.EntityState.Modified;
                    context.SaveChanges();
                }


        }

        public static void CreateUpdateReturnStock(OrderDetails orderDetails, DaoContext context)
        {
            StockTransactionType returnTransactionType = StockTransactionType.PURCHASE_ORDER_RETURN;
            StockAccountType accountTypeOnReturn = StockAccountType.QUANTITY_ON_RETURN;
            StockAccountType accountTypeOnHand = StockAccountType.QUANTITY_ON_HAND;

            long orderDetailsId = orderDetails.Id;

            StockBalance stockBalanceOnReturn = GetStockBalance(orderDetails, returnTransactionType,
                accountTypeOnReturn, context);
            StockBalance stockBalanceOnHand = GetStockBalance(orderDetails, returnTransactionType,
                accountTypeOnHand, context);

            StockCheckpoint stockCheckpointOnReturn = GetStockCheckpoint(stockBalanceOnReturn.StockAccount.Id, orderDetails.Id, returnTransactionType, context);
            long currentOnOrderCheckpointId = stockCheckpointOnReturn == null ? 0 : stockCheckpointOnReturn.Id;
            StockCheckpoint stockCheckpointOnReturnLatest = GetLatestStockCheckpointOfAccount(stockBalanceOnReturn.StockAccount.Id, currentOnOrderCheckpointId, context);
            StockCheckpoint stockCheckpointOnHand = GetStockCheckpoint(stockBalanceOnHand.StockAccount.Id, orderDetails.Id, returnTransactionType, context);
            long currentOnHandCheckpointId = stockCheckpointOnHand == null ? 0 : stockCheckpointOnHand.Id;
            StockCheckpoint stockCheckpointOnHandLatest = GetLatestStockCheckpointOfAccount(stockBalanceOnHand.StockAccount.Id, currentOnHandCheckpointId, context);

            double unitPerMeasurement = GetPurchaseMeasurementQuantity(orderDetails.PurchaseMeasurement, context);
            double orderReturnQuantity = unitPerMeasurement * orderDetails.ReturnQuantity;

            if (stockCheckpointOnHand == null)
            {
                double amountOnHand = -1 * orderReturnQuantity;
                double balanceAfterOnHand = stockBalanceOnHand.Balance - orderReturnQuantity;
                double balanceOnHand = stockBalanceOnHand.Balance - orderReturnQuantity;

                stockCheckpointOnHand = CreateStockCheckpoint(
                    orderDetails,
                    returnTransactionType,
                    accountTypeOnHand,
                    amountOnHand,
                    balanceAfterOnHand,
                    context);

                UpdateStockBalance(stockBalanceOnHand, orderDetails, returnTransactionType, balanceOnHand, context);
            }
            else
            {
                double amountDiff = orderDetails.ReturnQuantity - (-1 * stockCheckpointOnHand.Amount);
                double latestCheckpointBalance = stockCheckpointOnHandLatest != null ? stockCheckpointOnHandLatest.BalanceAfter : 0.00;

                double amountOnHand = -1 * orderReturnQuantity;
                double balanceAfterOnHand = latestCheckpointBalance - orderReturnQuantity;
                double balanceOnHand = stockBalanceOnHand.Balance - amountDiff;

                stockCheckpointOnHand.TransactionDateTime = DateTime.Now;
                stockCheckpointOnHand.Timestamp = DateTime.Now;
                stockCheckpointOnHand.Amount = amountOnHand;
                stockCheckpointOnHand.BalanceAfter = balanceAfterOnHand;

                UpdateStockBalance(stockBalanceOnHand, orderDetails, returnTransactionType, balanceOnHand, context);

                //Update next checkpoint balance after, since the current balance get from previous balance
                CheckpointBalanceSubstractForNextCheckpoint(amountDiff, stockBalanceOnHand.StockAccount.Id, stockCheckpointOnHand.Id, context);


                context.Entry<StockCheckpoint>(stockCheckpointOnHand).State = SystemEntity.EntityState.Modified;
                context.SaveChanges();
            }

            if (stockCheckpointOnReturn == null)
            {
                double amountOnOrder = orderReturnQuantity;
                double balanceAfterOnOrder = stockBalanceOnReturn.Balance + orderReturnQuantity;
                double balanceOnOrder = stockBalanceOnReturn.Balance + orderReturnQuantity;

                stockCheckpointOnReturn = CreateStockCheckpoint(
                    orderDetails,
                    returnTransactionType,
                    accountTypeOnReturn,
                    amountOnOrder,
                    balanceAfterOnOrder,
                    context);

                UpdateStockBalance(stockBalanceOnReturn, orderDetails, returnTransactionType, balanceOnOrder, context);
            }
            else
            {
                double amountDiff = orderReturnQuantity - stockCheckpointOnReturn.Amount;
                double latestCheckpointBalance = stockCheckpointOnReturnLatest != null ? stockCheckpointOnReturnLatest.BalanceAfter : 0.00;

                double amountOnOrder = orderReturnQuantity;
                double balanceAfterOnOrder = latestCheckpointBalance + orderReturnQuantity;
                double balanceOnOrder = stockBalanceOnReturn.Balance + amountDiff;

                stockCheckpointOnReturn.TransactionDateTime = DateTime.Now;
                stockCheckpointOnReturn.Timestamp = DateTime.Now;
                stockCheckpointOnReturn.Amount = amountOnOrder;
                stockCheckpointOnReturn.BalanceAfter = balanceAfterOnOrder;

                UpdateStockBalance(stockBalanceOnReturn, orderDetails, returnTransactionType, balanceOnOrder, context);

                //Update next checkpoint balance after, since the current balance get from previous balance
                CheckpointBalanceAdditionForNextCheckpoint(amountDiff, stockBalanceOnReturn.StockAccount.Id, stockCheckpointOnReturn.Id, context);

                context.Entry<StockCheckpoint>(stockCheckpointOnReturn).State = SystemEntity.EntityState.Modified;
                context.SaveChanges();
            }


        }

        public static double GetPurchaseMeasurementQuantity(Measurement purchaseMeasurement, DaoContext context)
        {
            double ret = 1.00;
            Measurement measurement = context.Measurements.Find(purchaseMeasurement.Id);
            ret = measurement.Unit;
            return ret;
        }

        public static StockCheckpoint GetStockCheckpoint(long stockAccountId, long transactionEntityId, StockTransactionType type, DaoContext context)
        {
            StockCheckpoint stockCheckpoint =
                        context.StockCheckpoint
                            .Where
                            (
                                s =>
                                    s.StockAccount.Id == stockAccountId &&
                                    s.TransactionEntityId == transactionEntityId &&
                                    s.TransactionType == type
                            )
                            .FirstOrDefault();
            return stockCheckpoint;
        }

        public static StockCheckpoint GetLatestStockCheckpointOfAccount(long stockAccountId,long stockCheckpointId, DaoContext context)
        {
            StockCheckpoint stockCheckpoint = null;
            var query =
                        context.StockCheckpoint
                            .Where
                            (
                                s =>
                                    s.StockAccount.Id == stockAccountId
                            )
                            .OrderByDescending(s => s.Id);

            if(stockCheckpointId>0)
            {
                query = context.StockCheckpoint
                    .Where
                    (s => 
                        s.StockAccount.Id == stockAccountId &&
                        s.Id < stockCheckpointId
                    )
                    .OrderByDescending(s => s.Id);
            }

            int count = query.Count();
            if (count > 0) stockCheckpoint = query.First();
            return stockCheckpoint;
        }

        public static StockCheckpoint CreateStockCheckpoint(
            OrderDetails orderDetails,
            StockTransactionType stockTransactionType,
            StockAccountType stockAccountType,
            double amount,
            double balanceAfter,
            DaoContext context
            )
        {
            StockCheckpoint stockCheckpoint = new StockCheckpoint();
            stockCheckpoint.TransactionDateTime = DateTime.Now;
            stockCheckpoint.TransactionEntityId = orderDetails.Id;
            stockCheckpoint.TransactionType = stockTransactionType;
            stockCheckpoint.StockAccount = GetStockAccount(orderDetails, stockAccountType, context);
            stockCheckpoint.Amount = amount;
            stockCheckpoint.BalanceAfter = balanceAfter;
            stockCheckpoint.Timestamp = DateTime.Now;

            context.StockCheckpoint.Add(stockCheckpoint);
            context.SaveChanges();

            return stockCheckpoint;
        }


        public static StockBalance UpdateStockBalance(
            StockBalance stockBalance,
            OrderDetails orderDetails,
            StockTransactionType transactionType,
            double balanceAfter,
            DaoContext context
            )
        {
            stockBalance.LastTransactionDateTime = DateTime.Now;
            stockBalance.LastTransactionEntityId = orderDetails.Id;
            stockBalance.LastTransactionType = transactionType;
            stockBalance.Balance = balanceAfter;

            context.Entry<StockBalance>(stockBalance).State = SystemEntity.EntityState.Modified;
            context.SaveChanges();

            return stockBalance;
        }

        public static StockAccount GetStockAccount(OrderDetails orderDetails,StockAccountType type, DaoContext context)
        {
            StockAccount stockAccount = null;

            bool IsExist = IsStockAccountExist(orderDetails, type, context);

            if (!IsExist)
            {
                stockAccount = CreateStockAccount(orderDetails, type, context);
            }
            else
            {
                stockAccount = context.StockAccount
                    .Where(s => 
                        s.Item.Id == orderDetails.Item.Id &&
                        s.Type == type &&
                        s.Warehouse.Id == orderDetails.Warehouse.Id)
                    .FirstOrDefault<StockAccount>();
            }
            return stockAccount;
        }

        public static bool IsStockAccountExist(OrderDetails orderDetails, StockAccountType type, DaoContext context)
        {
            long itemId = orderDetails.Item.Id;
            long warehouseId = orderDetails.Warehouse.Id;

            StockAccount existingStockAccount = context.StockAccount
                .Where(s => 
                    s.Item.Id == itemId && 
                    s.Type == type &&
                    s.Warehouse.Id == warehouseId)
                .FirstOrDefault<StockAccount>();

            if (existingStockAccount != null)
            {
                return true;
            }

            return false;
        }

        public static StockAccount CreateStockAccount(OrderDetails orderDetails, StockAccountType type, DaoContext context)
        {
            Item item = context.Items.Find(orderDetails.Item.Id);
            Warehouse warehouse = context.Warehouse.Find(orderDetails.Warehouse.Id);

            StockAccount stockAccount = new StockAccount() 
            {
                Item = item,
                Warehouse = warehouse,
                Type = type
            };
            context.StockAccount.Add(stockAccount);
            context.SaveChanges();
            return stockAccount;
        }

        public static StockBalance GetStockBalance(
            OrderDetails orderDetails, 
            StockTransactionType transactionType, 
            StockAccountType accountType, 
            DaoContext context)
        {
            StockAccount stockAccount = GetStockAccount(orderDetails, accountType, context);
            long stockAccountId = stockAccount.Id;

            StockBalance stockBalance = context.StockBalance
                .Where(s => 
                    s.StockAccount.Id == stockAccountId
                )
                .FirstOrDefault<StockBalance>();

            if (stockBalance == null)
            {
                stockBalance = new StockBalance();
                stockBalance.LastTransactionDateTime = DateTime.Now;
                stockBalance.LastTransactionEntityId = orderDetails.Id;
                stockBalance.LastTransactionType = transactionType;
                stockBalance.StockAccount = GetStockAccount(orderDetails, accountType, context);
                stockBalance.Balance = 0.00;
                stockBalance.Timestamp = DateTime.Now;
                context.StockBalance.Add(stockBalance);
                context.SaveChanges();
            }

            return stockBalance;
        }



    }
}
