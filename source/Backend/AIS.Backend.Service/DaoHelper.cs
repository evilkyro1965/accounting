﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using AIS.Backend.Model.DTO;
using SystemEntity = System.Data.Entity;

namespace AIS.Backend.Service
{
    public class DaoHelper
    {
        public const string DB_NAME = "AIS_DB";

        public const string STOCK_CHECKPOINT_TABLE = "StockCheckpoints";

        public const string ID_FIELD = "Id";

        public const string STOCK_ACCOUNT_FIELD = "StockAccountId";

        public const string BALANCE_AFTER_FIELD = "BalanceAfter";

        public const int ALL_ROW = 1000000;

        /// <summary>
        /// Set Field Of Auditable Entity 
        /// Use this for update entity from source entity
        /// </summary>
        /// <param name="source">Source Of Entity</param>
        /// <param name="target">Target Of Entity</param>
        public static void SetFieldAuditableEntityFromObject<T>(T source, T target)
            where T : AuditableEntity
        {
            AuditableEntity sourceObj = (AuditableEntity) source;
            AuditableEntity targetObj = (AuditableEntity) target;

            targetObj.Name = sourceObj.Name;
            targetObj.CreatedBy = sourceObj.CreatedBy;
            targetObj.CreatedDate = sourceObj.CreatedDate;
            targetObj.UpdatedBy = sourceObj.UpdatedBy;
            targetObj.UpdatedDate = sourceObj.UpdatedDate;
        }

        /// <summary>
        /// Set Field Of GLAuditable Entity 
        /// Use this for update entity from source entity
        /// </summary>
        /// <param name="source">Source Of Entity</param>
        /// <param name="target">Target Of Entity</param>
        public static void SetFieldGLAuditableEntityFromObject<T>(T source, T target)
            where T : GLAuditableEntity
        {
            GLAuditableEntity sourceObj = (GLAuditableEntity)source;
            GLAuditableEntity targetObj = (GLAuditableEntity)target;

            targetObj.CreatedBy = sourceObj.CreatedBy;
            targetObj.CreatedDate = sourceObj.CreatedDate;
            targetObj.UpdatedBy = sourceObj.UpdatedBy;
            targetObj.UpdatedDate = sourceObj.UpdatedDate;
        }

        /// <summary>
        /// Set Field Of Active Entity 
        /// Use this for active entity from source entity
        /// </summary>
        /// <param name="source">Source Of Entity</param>
        /// <param name="target">Target Of Entity</param>
        public static void SetFieldActiveEntityFromObject<T>(T source, T target)
            where T : ActiveEntity
        {
            ActiveEntity sourceObj = (ActiveEntity)source;
            ActiveEntity targetObj = (ActiveEntity)target;

            targetObj.Name = sourceObj.Name;
            targetObj.IsActive = sourceObj.IsActive;
            targetObj.CreatedBy = sourceObj.CreatedBy;
            targetObj.CreatedDate = sourceObj.CreatedDate;
            targetObj.UpdatedBy = sourceObj.UpdatedBy;
            targetObj.UpdatedDate = sourceObj.UpdatedDate;
        }

        /// <summary>
        /// Get Object Context
        /// </summary>
        /// <param name="dbContext">Get Object Context</param>
        /// <returns>Object Context</returns>
        public static ObjectContext GetObjectContext(DbContext dbContext)
        {
            var objctx = (dbContext as IObjectContextAdapter).ObjectContext;
            return objctx;
        }

        /// <summary>
        /// Detach Entity Helper
        /// </summary>
        /// <param name="entity">Entity to detach</param>
        /// <param name="dbContext">DbContext used to detach</param>
        public static void DetachEntity<T>(T entity, DbContext dbContext)
        {
            var objctx = (dbContext as IObjectContextAdapter).ObjectContext;
            objctx.Detach(entity);
        }

        /// <summary>
        /// SearchHelperValueBuilder, build search helper from criteria
        /// </summary>
        /// <typeparam name="T">BaseSearchCriteria</typeparam>
        /// <param name="criteria">Criteria</param>
        /// <returns>Search Helper Value</returns>
        public static SearchHelperValue SearchHelperValueBuilder<T>(T criteria)
            where T : BaseSearchCriteria
        {
            SearchHelperValue searchHelperValue = new SearchHelperValue();
            searchHelperValue.Total = 0;
            searchHelperValue.PageSize = criteria.PageSize;
            searchHelperValue.PageNum = criteria.Page;
            searchHelperValue.Skip = (searchHelperValue.PageNum - 1) * searchHelperValue.PageSize;
            searchHelperValue.Limit = searchHelperValue.PageSize;
            searchHelperValue.Order = criteria.SortAscending == true ? "ASC" : "DESC";
            searchHelperValue.OrderBy = criteria.SortBy;
            return searchHelperValue;
        }

        public static IQueryable<T> PagedResult<T, TResult>(IQueryable<T> query, int pageNum, int pageSize,
Expression<Func<T, TResult>> orderByProperty, bool isAscendingOrder, out int rowsCount)
        {
            if (pageSize <= 0) pageSize = 20;

            //Total result count
            rowsCount = query.Count();

            //If page number should be > 0 else set to first page
            if (rowsCount <= pageSize || pageNum <= 0) pageNum = 1;

            //Calculate nunber of rows to skip on pagesize
            int excludedRows = (pageNum - 1) * pageSize;

            query = isAscendingOrder ? query.OrderBy(orderByProperty) : query.OrderByDescending(orderByProperty);

            //Skip the required rows for the current page and take the next records of pagesize count
            return query.Skip(excludedRows).Take(pageSize);
        }

        public static SearchResult<T> SetSearchResultPagingValue<T>(BaseSearchCriteria criteria)
        {
            SearchResult<T> ret = new SearchResult<T>();
            ret.Page = criteria.Page;
            ret.PageSize = criteria.PageSize;
            return ret;
        }

        public static String GetSearchField(BaseSearchCriteria criteria)
        {
            String ret = "Id";
            return ret;
        }

        public static List<T> GetAddedPropertyList<T>(ICollection<T> sourceList, ICollection<T> targetList)
            where T : IdentifiableEntity
        {
            List<T> added = new List<T>();
            if (sourceList != null)
            {
                foreach (T source in sourceList)
                {
                    bool isExist = false;
                    foreach (T existing in targetList)
                    {
                        if (source.Id == existing.Id)
                        {
                            isExist = true;
                            break;
                        }
                    }
                    if (!isExist)
                    {
                        added.Add(source);
                    }
                }
            }
            return added;
        }

        public static List<T> GetRemovedPropertyList<T>(ICollection<T> sourceList, ICollection<T> targetList)
            where T : IdentifiableEntity
        {
            List<T> removed = new List<T>();
            if (targetList != null)
            {
                foreach (T existing in targetList)
                {
                    bool isExist = false;
                    foreach (T source in sourceList)
                    {
                        if (source.Id == existing.Id)
                        {
                            isExist = true;
                            break;
                        }
                    }
                    if (!isExist)
                    {
                        removed.Add(existing);
                    }
                }
            }
            return removed;
        }

        public static void CheckSameNameInsert<T>(DbSet<T> dbSet, T entity)
            where T : NameableEntity
        {
            
            int count = dbSet.Where(obj => obj.Name == entity.Name).Count();
            if (count > 0)
            {
                throw new Exception("Master Data with that name is already exist");
            }
        }

        public static void CheckSameNameUpdate<T>(DbSet<T> dbSet, T entity)
            where T : NameableEntity
        {
            int count = dbSet.Where(obj => obj.Name == entity.Name)
                .Where(obj => obj.Id != entity.Id).Count();
            if (count > 0)
            {
                throw new Exception("Master Data with that name is already exist");
            }
        }

        public static double TotalAmountOfOrderDetails(OrderDetails orderDetails)
        {
            double total = 0.00;
            double totalBeforeTax = 0.00;
            double quantity = orderDetails.Quantity;
            double price = orderDetails.Price;
            double discount = orderDetails.DiscountPercent;
            double tax = orderDetails.TaxPercent;

            totalBeforeTax = (quantity * total) - ((quantity * total) * (discount / 100.00));
            total = totalBeforeTax + (totalBeforeTax * (tax/ 100.00));
            return total;
        }

    }
}
