﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq.Expressions;
using SystemEntity = System.Data.Entity;

namespace AIS.Backend.Service
{
    public class SupplierServiceImpl : SupplierService
    {
        /// <summary>
        /// Dao Context
        /// </summary>
        protected DaoContext context;

        public SupplierServiceImpl()
        {
        }

        public Supplier Create(Supplier supplier)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    DaoHelper.CheckSameNameInsert<Supplier>(context.Suppliers, supplier);

                    context.Suppliers.Add(supplier);
                    context.SaveChanges();
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return supplier;
        }

        public Supplier Update(Supplier supplier)
        {
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    Supplier update = context.Suppliers.Find(supplier.Id);

                    DaoHelper.CheckSameNameUpdate<Supplier>(context.Suppliers, supplier);

                    update.Name = supplier.Name;
                    update.ContactPerson = supplier.ContactPerson;
                    update.SecondaryAddress = supplier.SecondaryAddress;
                    update.City = supplier.City;
                    update.PostalCode = supplier.PostalCode;
                    update.State = supplier.State;
                    update.Country = supplier.Country;
                    update.Phone = supplier.Phone;
                    update.Fax = supplier.Fax;
                    update.Email = supplier.Email;

                    DaoHelper.SetFieldActiveEntityFromObject(supplier, update);

                    context.Entry(update).State = SystemEntity.EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return supplier;
        }

        public Supplier Get(long id)
        {
            Supplier supplier = null;
            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    supplier = context.Suppliers.Find(id);
                }
            }
            catch (DataException e)
            {
                throw e;
            }

            return supplier;
        }

        public void Delete(long[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                foreach (long id in ids)
                {
                    try
                    {
                        using (context = new DaoContext(DaoHelper.DB_NAME))
                        {
                            Supplier supplier = context.Suppliers.Find(id);
                            context.Suppliers.Remove(supplier);
                            context.SaveChanges();
                        }
                    }
                    catch (DataException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public void Deactivate(long[] ids)
        {
            if (ids != null && ids.Length > 0)
            {
                foreach (long id in ids)
                {
                    try
                    {
                        using (context = new DaoContext(DaoHelper.DB_NAME))
                        {
                            Supplier supplier = context.Suppliers.Find(id);
                            supplier.IsActive = false;
                            context.SaveChanges();
                        }
                    }
                    catch (DataException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public SearchResult<Supplier> Search(SupplierSearchCriteria criteria)
        {
            SearchResult<Supplier> searchResult = new SearchResult<Supplier>();

            try
            {
                using (context = new DaoContext(DaoHelper.DB_NAME))
                {
                    var objctx = DaoHelper.GetObjectContext(context);
                    SearchHelperValue searchHelperVal = DaoHelper.SearchHelperValueBuilder(criteria);
                    List<ObjectParameter> param = new List<ObjectParameter>();

                    StringBuilder sqlString = new StringBuilder();
                    SearchQueryBuilder.QuerySelectBuilder(sqlString, "Suppliers");

                    SearchQueryBuilder.QueryLikeBuilder(param, sqlString, "Name", criteria.Name);
                    SearchQueryBuilder.QueryEqualBuilder<Boolean?>(param, sqlString, "IsActive", criteria.IsActive);

                    SearchQueryBuilder.QueryGetTotal<Supplier>(param, sqlString, searchHelperVal, context);

                    SearchQueryBuilder.QuerySortByBuilder(param, sqlString, searchHelperVal);
                    SearchQueryBuilder.QueryPagingBuilder(param, sqlString, searchHelperVal);

                    ObjectQuery<Supplier> queryResult = objctx.CreateQuery<Supplier>(sqlString.ToString(), param.ToArray());

                    SearchQueryBuilder.QueryGetSearchResult<Supplier, SupplierSearchCriteria>(param, sqlString, criteria, searchHelperVal, searchResult, context);
                }
            }
            catch (DataException e)
            {
                throw e;
            }
            return searchResult;
        }

    }
}
