﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;

namespace AIS.Backend.Service
{
    public interface MeasurementService
    {
        Measurement Create(Measurement measurement);
        Measurement Update(Measurement measurement);
        Measurement Get(long id);
        void Delete(long[] ids);
        void Deactivate(long[] ids);
        SearchResult<Measurement> Search(MeasurementSearchCriteria criteria);
    }
}
