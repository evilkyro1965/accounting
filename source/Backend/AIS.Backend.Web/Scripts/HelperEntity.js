﻿
function MeasurementTypeName(code) {
    switch(code) {
        case 0: 
            return "Stock"; break;
        case 1: 
            return "Calculation"; break;
        case 2: 
            return "Purchase"; break;
    } 
}

function ItemTypeName(code) {
    switch (code) {
        case 0:
            return "INVENTORY"; break;
        case 1:
            return "ASSET"; break;
    }
}