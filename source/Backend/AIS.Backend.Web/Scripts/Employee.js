﻿$(document).ready(function () {
    $.jqx.theme = theme;

    $("input[type='text'],input[type='password']").jqxInput({ height: buttonHeight, minLength: 1, width: 200 });

    if ($("#mainPanel").length) {
        //$("#mainPanel").jqxExpander({ toggleMode: 'none', width: '320px', showArrow: false });
    }
    
    // initialize validator.
    if ($("#saveButton").length) {

        $('#formCreateEmployee').jqxValidator({
            rules: [
                   { input: '#username', message: 'Username is required!', action: 'keyup, blur', rule: 'required' },
                   { input: '#password', message: 'Password is required!', action: 'keyup, blur', rule: 'required' },
                   { input: '#mobile', message: 'Mobile is required!', action: 'keyup, blur', rule: 'required' },
                   { input: '#email', message: 'Email is required!', action: 'keyup, blur', rule: 'required' },
                   { input: '#email', message: 'Email is not valid!', action: 'keyup, blur', rule: 'email' }
            ]
        });

        $('#saveButton').jqxButton({ width: 60, height: buttonHeight });
        $('#saveButton').click(function () {
            var isValid = $('#formCreateEmployee').jqxValidator('validate');
            console.log(isValid);
            if (isValid) {
                var employee = {};
                employee.Username = $("#username").val();
                employee.Password = $("#password").val();
                employee.Mobile = $("#mobile").val();
                employee.Email = $("#email").val();

                $.ajax({
                    type: 'POST',
                    url: baseUrl+"/api/employee/create",
                    data: employee,
                    error: function () {
                        console.log("error");
                    },
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        var url = baseUrl + "/Employee";
                        $(location).attr('href', url);
                    }
                });
                console.log(isValid);
            }

        });
    }
    

    /**
     * Update
     */

    // initialize validator.
    if ($("#UpdateButton").length) {
        $('#UpdateButton').jqxButton({ width: 60, height: buttonHeight });

        $('#formUpdateEmployee').jqxValidator({
            rules: [
                   { input: '#username', message: 'Username is required!', action: 'keyup, blur', rule: 'required' },
                   { input: '#password', message: 'Password is required!', action: 'keyup, blur', rule: 'required' },
                   { input: '#mobile', message: 'Mobile is required!', action: 'keyup, blur', rule: 'required' },
                   { input: '#email', message: 'Email is required!', action: 'keyup, blur', rule: 'required' },
                   { input: '#email', message: 'Email is not valid!', action: 'keyup, blur', rule: 'email' }
            ]
        });

        $('#UpdateButton').click(function () {
            var isValid = $('#formUpdateEmployee').jqxValidator('validate');
            console.log(isValid);
            if (isValid) {
                var employee = {};
                employee.Id = $("#Id").val();
                employee.Username = $("#username").val();
                employee.Password = $("#password").val();
                employee.Mobile = $("#mobile").val();
                employee.Email = $("#email").val();
                console.log(employee);
                $.ajax({
                    type: 'POST',
                    url: baseUrl + "/api/employee/update",
                    data: employee,
                    error: function () {
                        console.log("error");
                    },
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        var url = baseUrl + "/Employee";
                        $(location).attr('href', url);
                    }
                });
                console.log(isValid);
            }

        });
    }


    if ($("#BackButton").length) {
        $("#BackButton").jqxButton({ width: 60, height: buttonHeight });
        $("#BackButton").on('click', function () {
            var url = baseUrl + "/Employee";
            $(location).attr('href', url);
        });
    }

    if ($("#jqxgrid").length) {

        grid();

        if ($("#addButton").length) {
            $('#addButton').jqxButton({ width: 60, height: buttonHeight });
            $("#addButton").on('click', function () {
                var url = baseUrl + "/Employee/Create/";
                $(location).attr('href', url);
            });
        }

        if ($("#editButton").length) {
            $('#editButton').jqxButton({ width: 60, height: buttonHeight });
            $("#editButton").on('click', function () {
                var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
                if (getselectedrowindexes.length > 0) {
                    // returns the selected row's data.
                    var selectedRowData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                    console.log(selectedRowData);
                    var id = selectedRowData.Id;
                    var url = baseUrl + "/Employee/Update/" + id;
                    $(location).attr('href', url);
                }
            });
        }

        $("#jqxwindow ").jqxWindow({ autoOpen: false, height:120, width:250 });
        $('#deleteButton').jqxButton({ width: 60, height: buttonHeight });
        $('#deleteOk').jqxButton({ width: 60, height: buttonHeight });
        $('#deleteCancel').jqxButton({ width: 60, height: buttonHeight });
        
        $("#deleteButton").on('click', function () {
            var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0) {
                // returns the selected row's data.
                var selectedRowData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                var id = selectedRowData.Id;
                var username = selectedRowData.Username;
                $("#deleteLabel").html(username);
                $('#jqxwindow').jqxWindow('open');
            }
        });

        $("#deleteOk").on('click', function () {
            var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0) {
                // returns the selected row's data.
                var selectedRowData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                var id = selectedRowData.Id;
                $.ajax({
                    type: 'DELETE',
                    url: baseUrl + "/api/employee/delete/"+id,
                    error: function () {
                        console.log("error");
                    },
                    dataType: 'json',
                    success: function (data) {
                        grid();
                    }
                });
                $('#jqxwindow').jqxWindow('close');
            }
        });

        $("#deleteCancel").on('click', function () {
            $('#jqxwindow').jqxWindow('close');
        });



        if ($("#searchButton").length) {
            $('#searchButton').jqxButton({ width: 60, height: buttonHeight });
            $("#searchButton").on('click', function () {
                grid();
            });
        }
        $("#search").jqxInput({ placeHolder: "Search", height: buttonHeight, minLength: 1 });
    }
});

function grid() {
    var search = $("#search").val();
    var param = {}
    if (search != "") param.username = search;

    var source =
    {
        data: param,
        datatype: "json",
        datafields: [
             { name: 'Id' },
             { name: 'Username' },
             { name: 'Password' },
             { name: 'Mobile' },
             { name: 'Email' }
        ],
        url: baseUrl + "/api/employee/search",
        root: 'Items',
        pagenum: 0,
        pagesize: 5,
        beforeprocessing: function (data) {
            source.totalrecords = data.Total;
        }
    };

    var gridDataAdapter = new $.jqx.dataAdapter(source);
    $("#jqxgrid").jqxGrid(
    {
        width: '100%',
        source: gridDataAdapter,
        autoheight: true,
        selectionmode: 'singlerow',
        pageable: true,
        virtualmode: true,
        rendergridrows: function () {
            return gridDataAdapter.records;
        },
        columns: [
            { text: 'Username', datafield: 'Username' },
            { text: 'Password', datafield: 'Password'},
            { text: 'Mobile', datafield: 'Mobile'},
            { text: 'Email', datafield: 'Email'}
        ]
    });

}