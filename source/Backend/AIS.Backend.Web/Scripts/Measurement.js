﻿$(document).ready(function () {

    $("input[type='text'],input[type='password']").jqxInput({ height: buttonHeight, minLength: 1, width: 200 });

    if ($("#mainPanel").length) {
    }

    // initialize validator.
    if ($("#saveButton").length) {

        $('#formCreateMeasurement').jqxValidator({
            rules: [
                    { input: '#name', message: 'Name is required!', action: 'keyup, blur', rule: 'required' },
                    {
                        input: "#type", message: "Type is required!", action: 'blur', rule: function (input, commit) {
                            var index = $("#type").jqxDropDownList('getSelectedIndex');
                            return index != 0;
                        }
                    },
                    { input: '#unit', message: 'Unit is required!', action: 'keyup, blur', rule: 'required' },
                    { input: '#unit', message: 'Unit must be number or decimal!', action: 'keyup, blur', rule: 'number' },
                    { input: '#shortName', message: 'Short Name is required!', action: 'keyup, blur', rule: 'required' },

            ]
        });

        $("#type").jqxDropDownList({});
        $("#isDecimal").jqxCheckBox({});

        $('#saveButton').jqxButton({ width: 60, height: buttonHeight });
        $('#saveButton').click(function () {
            var isValid = $('#formCreateMeasurement').jqxValidator('validate');
            console.log(isValid);
            if (isValid) {
                var measurement = {};
                measurement.Name = $("#name").val();
                measurement.ShortName = $("#shortName").val();
                measurement.Type = $("#type").val();
                measurement.Unit = $("#unit").val();
                measurement.IsDecimal = $("#isDecimal").jqxCheckBox('checked');
                measurement.Description = $("#description").val();

                $.ajax({
                    type: 'POST',
                    url: baseUrl + "/api/measurement/create",
                    data: measurement,
                    error: function () {
                        console.log("error");
                    },
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        var url = baseUrl + "/Measurement";
                        $(location).attr('href', url);
                    }
                });
                console.log(isValid);
            }

        });
    }


    /**
     * Update
     */

    // initialize validator.
    if ($("#UpdateButton").length) {
        $('#UpdateButton').jqxButton({ width: 60, height: buttonHeight });

        $('#formUpdateMeasurement').jqxValidator({
            rules: [
                    { input: '#name', message: 'Name is required!', action: 'keyup, blur', rule: 'required' },
                    {
                        input: "#type", message: "Type is required!", action: 'blur', rule: function (input, commit) {
                            var index = $("#type").jqxDropDownList('getSelectedIndex');
                            return index != 0;
                        }
                    },
                    { input: '#unit', message: 'Unit is required!', action: 'keyup, blur', rule: 'required' },
                    { input: '#unit', message: 'Unit must be number or decimal!', action: 'keyup, blur', rule: 'number' },
                    { input: '#shortName', message: 'Short Name is required!', action: 'keyup, blur', rule: 'required' },

            ]
        });
        var hiddenType = $("#HiddenType").val().toUpperCase();
        var isDecimal = $("#HiddenIsDecimal").val() == "true" ? true : false;
        $("#type").jqxDropDownList({});
        $("#type").jqxDropDownList('selectItem', hiddenType);
        $("#isDecimal").jqxCheckBox({ checked: isDecimal });

        $('#UpdateButton').click(function () {
            var isValid = $('#formUpdateMeasurement').jqxValidator('validate');
            console.log(isValid);
            if (isValid) {
                var measurement = {};
                measurement.Id = $("#Id").val();
                measurement.Name = $("#name").val();
                measurement.ShortName = $("#shortName").val();
                measurement.Type = $("#type").val();
                measurement.Unit = $("#unit").val();
                measurement.IsDecimal = $("#isDecimal").jqxCheckBox('checked');
                measurement.Description = $("#description").val();

                $.ajax({
                    type: 'POST',
                    url: baseUrl + "/api/measurement/update",
                    data: measurement,
                    error: function () {
                        console.log("error");
                    },
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        var url = baseUrl + "/Measurement";
                        $(location).attr('href', url);
                    }
                });
            }

        });
    }


    if ($("#BackButton").length) {
        $("#BackButton").jqxButton({ width: 60, height: buttonHeight });
        $("#BackButton").on('click', function () {
            var url = baseUrl + "/Measurement";
            $(location).attr('href', url);
        });
    }

    if ($("#jqxgrid").length) {

        grid();

        if ($("#addButton").length) {
            $('#addButton').jqxButton({ width: 60, height: buttonHeight });
            $("#addButton").on('click', function () {
                var url = baseUrl + "/Measurement/Create/";
                $(location).attr('href', url);
            });
        }

        if ($("#editButton").length) {
            $('#editButton').jqxButton({ width: 60, height: buttonHeight });
            $("#editButton").on('click', function () {
                var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
                if (getselectedrowindexes.length > 0) {
                    // returns the selected row's data.
                    var selectedRowData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                    console.log(selectedRowData);
                    var id = selectedRowData.Id;
                    var url = baseUrl + "/Measurement/Update/" + id;
                    $(location).attr('href', url);
                }
            });
        }

        $("#jqxwindow ").jqxWindow({ autoOpen: false, height: 120, width: 250 });
        $('#deleteButton').jqxButton({ width: 60, height: buttonHeight });
        $('#deleteOk').jqxButton({ width: 60, height: buttonHeight });
        $('#deleteCancel').jqxButton({ width: 60, height: buttonHeight });

        $("#deleteButton").on('click', function () {
            var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0) {
                // returns the selected row's data.
                var selectedRowData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                var id = selectedRowData.Id;
                var name = selectedRowData.Name;
                $("#deleteLabel").html(name);
                $('#jqxwindow').jqxWindow('open');
            }
        });

        $("#deleteOk").on('click', function () {
            var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0) {
                // returns the selected row's data.
                var selectedRowData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                var id = selectedRowData.Id;
                $.ajax({
                    type: 'DELETE',
                    url: baseUrl + "/api/Measurement/delete/" + id,
                    error: function () {
                        console.log("error");
                    },
                    dataType: 'json',
                    success: function (data) {
                        grid();
                    }
                });
                $('#jqxwindow').jqxWindow('close');
            }
        });

        $("#deleteCancel").on('click', function () {
            $('#jqxwindow').jqxWindow('close');
        });



        if ($("#searchButton").length) {
            $('#searchButton').jqxButton({ width: 60, height: buttonHeight });
            $("#searchButton").on('click', function () {
                grid();
            });
        }
        $("#search").jqxInput({ placeHolder: "Search", height: buttonHeight, minLength: 1 });
    }
});

function grid() {
    var search = $("#search").val();
    var param = {}
    if (search != "") param.name = search;

    var source =
    {
        data: param,
        datatype: "json",
        datafields: [
             { name: 'Id' },
             { name: 'Name' },
             { name: 'ShortName' },
             { name: 'Type' },
             { name: 'TypeName' },
             { name: 'Unit' }
        ],
        url: baseUrl + "/api/measurement/search",
        root: 'Items',
        pagenum: 0,
        pagesize: 1,
        beforeprocessing: function (data) {
            source.totalrecords = data.Total;
        }
    };

    var gridDataAdapter = new $.jqx.dataAdapter(source);
    $("#jqxgrid").jqxGrid(
    {
        width: '100%',
        source: gridDataAdapter,
        autoheight: true,
        selectionmode: 'singlerow',
        pageable: true,
        virtualmode: true,
        rendergridrows: function () {
            console.log(gridDataAdapter.records);
            var records = gridDataAdapter.records;
            var length = records.length;
            for (var i = 0; i < length; i++) {
                gridDataAdapter.records[i].TypeName = MeasurementTypeName(gridDataAdapter.records[i].Type);
            }
            return gridDataAdapter.records;
        },
        columns: [
            { text: 'Name', datafield: 'Name' },
            { text: 'Short Name', datafield: 'ShortName' },
            { text: 'Type', datafield: 'TypeName' },
            { text: 'Unit', datafield: 'Unit' }
        ]
    });

}