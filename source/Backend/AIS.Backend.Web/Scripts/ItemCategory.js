﻿$(document).ready(function () {
    $.jqx.theme = theme;

    $("input[type='text'],input[type='password']").jqxInput({ height: buttonHeight, minLength: 1, width: 200 });

    // initialize validator.
    if ($("#saveButton").length) {

        $('#formCreateItemCategory').jqxValidator({
            rules: [
                   { input: '#name', message: 'Name is required!', action: 'keyup, blur', rule: 'required' }
            ]
        });

        $('#saveButton').jqxButton({ width: 60, height: buttonHeight });
        $('#saveButton').click(function () {
            var isValid = $('#formCreateItemCategory').jqxValidator('validate');
            console.log(isValid);
            if (isValid) {
                var itemCategory = {};
                itemCategory.Name = $("#name").val();

                $.ajax({
                    type: 'POST',
                    url: baseUrl + "/api/itemcategory/create",
                    data: itemCategory,
                    error: function () {
                        console.log("error");
                    },
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        var url = baseUrl + "/ItemCategory";
                        $(location).attr('href', url);
                    }
                });
                console.log(isValid);
            }

        });
    }


    /**
     * Update
     */

    // initialize validator.
    if ($("#UpdateButton").length) {
        $('#UpdateButton').jqxButton({ width: 60, height: buttonHeight });

        $('#formUpdateItemCategory').jqxValidator({
            rules: [
                   { input: '#name', message: 'Name is required!', action: 'keyup, blur', rule: 'required' }
            ]
        });

        $('#UpdateButton').click(function () {
            var isValid = $('#formUpdateItemCategory').jqxValidator('validate');
            console.log(isValid);
            if (isValid) {
                var itemCategory = {};
                itemCategory.Id = $("#Id").val();
                itemCategory.Name = $("#name").val();

                $.ajax({
                    type: 'POST',
                    url: baseUrl + "/api/ItemCategory/update",
                    data: itemCategory,
                    error: function () {
                        console.log("error");
                    },
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        var url = baseUrl + "/ItemCategory";
                        $(location).attr('href', url);
                    }
                });
                console.log(isValid);
            }

        });
    }


    if ($("#BackButton").length) {
        $("#BackButton").jqxButton({ width: 60, height: buttonHeight });
        $("#BackButton").on('click', function () {
            var url = baseUrl + "/ItemCategory";
            $(location).attr('href', url);
        });
    }

    if ($("#jqxgrid").length) {

        grid();

        if ($("#addButton").length) {
            $('#addButton').jqxButton({ width: 60, height: buttonHeight });
            $("#addButton").on('click', function () {
                var url = baseUrl + "/ItemCategory/Create/";
                $(location).attr('href', url);
            });
        }

        if ($("#editButton").length) {
            $('#editButton').jqxButton({ width: 60, height: buttonHeight });
            $("#editButton").on('click', function () {
                var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
                if (getselectedrowindexes.length > 0) {
                    // returns the selected row's data.
                    var selectedRowData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                    console.log(selectedRowData);
                    var id = selectedRowData.Id;
                    var url = baseUrl + "/ItemCategory/Update/" + id;
                    $(location).attr('href', url);
                }
            });
        }

        $("#jqxwindow ").jqxWindow({ autoOpen: false, height: 120, width: 250 });
        $('#deleteButton').jqxButton({ width: 60, height: buttonHeight });
        $('#deleteOk').jqxButton({ width: 60, height: buttonHeight });
        $('#deleteCancel').jqxButton({ width: 60, height: buttonHeight });

        $("#deleteButton").on('click', function () {
            var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0) {
                // returns the selected row's data.
                var selectedRowData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                var id = selectedRowData.Id;
                var name = selectedRowData.Name;
                $("#deleteLabel").html(name);
                $('#jqxwindow').jqxWindow('open');
            }
        });

        $("#deleteOk").on('click', function () {
            var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0) {
                // returns the selected row's data.
                var selectedRowData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                var id = selectedRowData.Id;
                $.ajax({
                    type: 'DELETE',
                    url: baseUrl + "/api/ItemCategory/delete/" + id,
                    error: function () {
                        console.log("error");
                    },
                    dataType: 'json',
                    success: function (data) {
                        grid();
                    }
                });
                $('#jqxwindow').jqxWindow('close');
            }
        });

        $("#deleteCancel").on('click', function () {
            $('#jqxwindow').jqxWindow('close');
        });



        if ($("#searchButton").length) {
            $('#searchButton').jqxButton({ width: 60, height: buttonHeight });
            $("#searchButton").on('click', function () {
                grid();
            });
        }
        $("#search").jqxInput({ placeHolder: "Search", height: buttonHeight, minLength: 1 });
    }
});

function grid() {
    var search = $("#search").val();
    var param = {}
    if (search != "") param.name = search;

    var source =
    {
        data: param,
        datatype: "json",
        datafields: [
             { name: 'Id' },
             { name: 'Name' }
        ],
        url: baseUrl + "/api/itemcategory/search",
        root: 'Items',
        pagenum: 0,
        pagesize: 5,
        beforeprocessing: function (data) {
            source.totalrecords = data.Total;
        }
    };

    var gridDataAdapter = new $.jqx.dataAdapter(source);
    $("#jqxgrid").jqxGrid(
    {
        width: '100%',
        source: gridDataAdapter,
        autoheight: true,
        selectionmode: 'singlerow',
        pageable: true,
        virtualmode: true,
        rendergridrows: function () {
            return gridDataAdapter.records;
        },
        columns: [
            { text: 'Name', datafield: 'Name' }
        ]
    });

}