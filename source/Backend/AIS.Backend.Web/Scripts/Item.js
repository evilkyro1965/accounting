﻿$(document).ready(function () {
    $.jqx.theme = theme;

    $("input[type='text'],input[type='password']").jqxInput({ height: buttonHeight, minLength: 1, width: 200 });

    if ($("#mainPanel").length) {
    }

    if ($("#selectMeasurementModal").length) {
        $("#selectMeasurementModal ").jqxWindow({
            isModal: true, resizable: false, autoOpen: false,
            width: "400px", height: "330px",
            okButton: $('#measurementSelectBtn'),
            cancelButton: $('#cancelBtn'),
        });
        $('#measurementSelectBtn').jqxButton({ width: 60, height: buttonHeight });
        $('#cancelBtn').jqxButton({ width: 60, height: buttonHeight });
    }

    // initialize validator.
    if ($("#saveButton").length) {

        $('#formCreateItem').jqxValidator({
            rules: [
                    { input: '#name', message: 'Name is required!', action: 'keyup, blur', rule: 'required' },
                    {
                        input: "#category", message: "Category is required!", action: 'blur', rule: function (input, commit) {
                            var index = $("#category").jqxDropDownList('getSelectedIndex');
                            return index != 0;
                        }
                    },
                    {
                        input: "#type", message: "Type is required!", action: 'blur', rule: function (input, commit) {
                            var index = $("#type").jqxDropDownList('getSelectedIndex');
                            return index != 0;
                        }
                    },
                    {
                        input: "#stockMeasurement", message: "Stock Measurement is required!", action: 'blur', rule: function (input, commit) {
                            var index = $("#stockMeasurement").jqxDropDownList('getSelectedIndex');
                            return index != 0;
                        }
                    },
                    {
                        input: "#calculationMeasurement", message: "Calculation Measurement is required!", action: 'blur', rule: function (input, commit) {
                            var index = $("#calculationMeasurement").jqxDropDownList('getSelectedIndex');
                            return index != 0;
                        }
                    },
                    {
                        input: "#purchaseMeasurement", message: "Must be at least one Purchase Measurement!", action: 'blur', rule: function (input, commit) {
                            var items = $("#purchaseMeasurement").jqxListBox('getItems');
                            return items.length > 0;
                        }
                    }
            ]
        });

        $("#category").jqxDropDownList({});
        $("#type").jqxDropDownList({});
        $("#stockMeasurement").jqxDropDownList({});
        $("#calculationMeasurement").jqxDropDownList({});
        $("#purchaseMeasurementList").jqxListBox({width:370});
        $("#purchaseMeasurement").jqxListBox({});

        $('#selectPurchaseMeasurement').jqxButton({ width: 60, height: buttonHeight });
        $('#deletePurchaseMeasurement').jqxButton({ width: 60, height: buttonHeight });

        $("#selectPurchaseMeasurement").on('click', function () {
            $("#selectMeasurementModal ").jqxWindow('open');
        });
        $("#measurementSelectBtn").on('click', function () {
            var item = $('#purchaseMeasurementList').jqxListBox('getSelectedItem');
            if (item != null) {
                var items = $("#purchaseMeasurement").jqxListBox('getItems');
                var isExist = false;
                for (var i = 0; items.length; i++) {
                    if (items[i].value == item.value) {
                        isExist = true;
                        break;
                    }
                }
                if(!isExist)
                    $("#purchaseMeasurement").jqxListBox('addItem', item);
            }
        });
        $("#deletePurchaseMeasurement").on('click', function () {
            var item = $('#purchaseMeasurement').jqxListBox('getSelectedItem');
            if (item != null) {
                $("#purchaseMeasurement").jqxListBox('removeItem', item.value);
            }
        });

        $('#saveButton').jqxButton({ width: 60, height: buttonHeight });
        $('#saveButton').click(function () {
            var isValid = $('#formCreateItem').jqxValidator('validate');
            console.log(isValid);
            if (isValid) {
                var item = {};
                item.Name = $("#name").val();
                item.ItemDescription = $("#description").val();
                item.ItemPrice = $("#price").val();
                item.Category = {};
                item.Category.Id = $("#category").jqxDropDownList('getSelectedItem').value;
                item.StockMeasurement = {};
                item.StockMeasurement.Id = $("#stockMeasurement").jqxDropDownList('getSelectedItem').value;
                item.CalculationMeasurement = {};
                item.CalculationMeasurement.Id = $("#calculationMeasurement").jqxDropDownList('getSelectedItem').value;
                item.PurchaseMeasurementList = [];
                var purchaseMeasurementItems = $('#purchaseMeasurement').jqxListBox('getItems');
                for (var i = 0; i < purchaseMeasurementItems.length; i++) {
                    var purchaseMeasurement = { Id: purchaseMeasurementItems[i].value };
                    item.PurchaseMeasurementList[i] = purchaseMeasurement;
                }
                item.Type = $("#type").val();
                console.log(item);

                $.ajax({
                    type: 'POST',
                    url: baseUrl + "/api/item/create",
                    data: item,
                    error: function () {
                        console.log("error");
                    },
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        var url = baseUrl + "/Item";
                        $(location).attr('href', url);
                    }
                });
                console.log(isValid);
            }

        });
    }


    /**
     * Update
     */

    // initialize validator.
    if ($("#updateButton").length) {
        $('#updateButton').jqxButton({ width: 60, height: buttonHeight });

        $('#formUpdateItem').jqxValidator({
            rules: [
                    { input: '#name', message: 'Name is required!', action: 'keyup, blur', rule: 'required' },
                    {
                        input: "#category", message: "Category is required!", action: 'blur', rule: function (input, commit) {
                            var index = $("#category").jqxDropDownList('getSelectedIndex');
                            return index != 0;
                        }
                    },
                    {
                        input: "#type", message: "Type is required!", action: 'blur', rule: function (input, commit) {
                            var index = $("#type").jqxDropDownList('getSelectedIndex');
                            return index != 0;
                        }
                    },
                    {
                        input: "#stockMeasurement", message: "Stock Measurement is required!", action: 'blur', rule: function (input, commit) {
                            var index = $("#stockMeasurement").jqxDropDownList('getSelectedIndex');
                            return index != 0;
                        }
                    },
                    {
                        input: "#calculationMeasurement", message: "Calculation Measurement is required!", action: 'blur', rule: function (input, commit) {
                            var index = $("#calculationMeasurement").jqxDropDownList('getSelectedIndex');
                            return index != 0;
                        }
                    },
                    {
                        input: "#purchaseMeasurement", message: "Must be at least one Purchase Measurement!", action: 'blur', rule: function (input, commit) {
                            var items = $("#purchaseMeasurement").jqxListBox('getItems');
                            return items.length > 0;
                        }
                    }
            ]
        });

        $("#category").jqxDropDownList({});
        $("#type").jqxDropDownList({});
        $("#stockMeasurement").jqxDropDownList({});
        $("#calculationMeasurement").jqxDropDownList({});
        $("#purchaseMeasurementList").jqxListBox({ width: 370 });
        $("#purchaseMeasurement").jqxListBox({});
        if (purchaseMeasurement != null) {
            for (var i = 0; i < purchaseMeasurement.length; i++) {
                $("#purchaseMeasurement").jqxListBox('addItem', purchaseMeasurement[i]);
            }
        }

        $('#selectPurchaseMeasurement').jqxButton({ width: 60, height: buttonHeight });
        $('#deletePurchaseMeasurement').jqxButton({ width: 60, height: buttonHeight });

        $("#selectPurchaseMeasurement").on('click', function () {
            $("#selectMeasurementModal ").jqxWindow('open');
        });
        $("#measurementSelectBtn").on('click', function () {
            var item = $('#purchaseMeasurementList').jqxListBox('getSelectedItem');
            var itemData = { label: item.label, value: item.value };
            var items = $("#purchaseMeasurement").jqxListBox('getItems');
            if (item != null && items!=null) {
                var isExist = false;
                for (var i = 0; i<items.length; i++) {
                    if (items[i].value == item.value) {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    $("#purchaseMeasurement").jqxListBox('addItem', itemData, 0);
                    console.log('hi');
                }
            }
        });
        $("#deletePurchaseMeasurement").on('click', function () {
            var index = $('#purchaseMeasurement').jqxListBox('getSelectedIndex');
            if (index != null) {
                $("#purchaseMeasurement").jqxListBox('removeAt', index);
            }
        });

        $('#updateButton').jqxButton({ width: 60, height: buttonHeight });
        $('#updateButton').click(function () {
            var isValid = $('#formUpdateItem').jqxValidator('validate');
            console.log(isValid);
            if (isValid) {
                var item = {};
                item.Id = $("#Id").val();
                item.Name = $("#name").val();
                item.ItemDescription = $("#description").val();
                item.ItemPrice = $("#price").val();
                item.Category = {};
                item.Category.Id = $("#category").jqxDropDownList('getSelectedItem').value;
                item.StockMeasurement = {};
                item.StockMeasurement.Id = $("#stockMeasurement").jqxDropDownList('getSelectedItem').value;
                item.CalculationMeasurement = {};
                item.CalculationMeasurement.Id = $("#calculationMeasurement").jqxDropDownList('getSelectedItem').value;
                item.PurchaseMeasurementList = [];
                var purchaseMeasurementItems = $('#purchaseMeasurement').jqxListBox('getItems');
                for (var i = 0; i < purchaseMeasurementItems.length; i++) {
                    var purchaseMeasurement = { Id: purchaseMeasurementItems[i].value };
                    item.PurchaseMeasurementList[i] = purchaseMeasurement;
                }
                item.Type = $("#type").val();
                console.log(item);

                $.ajax({
                    type: 'POST',
                    url: baseUrl + "/api/item/update",
                    data: item,
                    error: function () {
                        console.log("error");
                    },
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        var url = baseUrl + "/Item";
                        $(location).attr('href', url);
                    }
                });
                console.log(isValid);
            }

        });
    }


    if ($("#BackButton").length) {
        $("#BackButton").jqxButton({ width: 60, height: buttonHeight });
        $("#BackButton").on('click', function () {
            var url = baseUrl + "/Item";
            $(location).attr('href', url);
        });
    }

    if ($("#jqxgrid").length) {

        grid();

        if ($("#addButton").length) {
            $('#addButton').jqxButton({ width: 60, height: buttonHeight });
            $("#addButton").on('click', function () {
                var url = baseUrl + "/Item/Create/";
                $(location).attr('href', url);
            });
        }

        if ($("#editButton").length) {
            $('#editButton').jqxButton({ width: 60, height: buttonHeight });
            $("#editButton").on('click', function () {
                var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
                if (getselectedrowindexes.length > 0) {
                    // returns the selected row's data.
                    var selectedRowData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                    console.log(selectedRowData);
                    var id = selectedRowData.Id;
                    var url = baseUrl + "/Item/Update/" + id;
                    $(location).attr('href', url);
                }
            });
        }

        $("#jqxwindow ").jqxWindow({ autoOpen: false, height: 120, width: 250 });
        $('#deleteButton').jqxButton({ width: 60, height: buttonHeight });
        $('#deleteOk').jqxButton({ width: 60, height: buttonHeight });
        $('#deleteCancel').jqxButton({ width: 60, height: buttonHeight });

        $("#deleteButton").on('click', function () {
            var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0) {
                // returns the selected row's data.
                var selectedRowData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                var id = selectedRowData.Id;
                var name = selectedRowData.Name;
                $("#deleteLabel").html(name);
                $('#jqxwindow').jqxWindow('open');
            }
        });

        $("#deleteOk").on('click', function () {
            var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0) {
                // returns the selected row's data.
                var selectedRowData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                var id = selectedRowData.Id;
                $.ajax({
                    type: 'DELETE',
                    url: baseUrl + "/api/Measurement/delete/" + id,
                    error: function () {
                        console.log("error");
                    },
                    dataType: 'json',
                    success: function (data) {
                        grid();
                    }
                });
                $('#jqxwindow').jqxWindow('close');
            }
        });

        $("#deleteCancel").on('click', function () {
            $('#jqxwindow').jqxWindow('close');
        });



        if ($("#searchButton").length) {
            $('#searchButton').jqxButton({ width: 60, height: buttonHeight });
            $("#searchButton").on('click', function () {
                grid();
            });
        }
        $("#search").jqxInput({ placeHolder: "Search", height: buttonHeight, minLength: 1 });
    }
});

function grid() {
    var search = $("#search").val();
    var param = {}
    if (search != "") param.name = search;

    var source =
    {
        data: param,
        datatype: "json",
        datafields: [
             { name: 'Id' },
             { name: 'Name' },
             { name: 'ItemPrice' }
        ],
        url: baseUrl + "/api/item/search",
        root: 'Items',
        pagenum: 0,
        pagesize: 5,
        beforeprocessing: function (data) {
            source.totalrecords = data.Total;
        }
    };

    var gridDataAdapter = new $.jqx.dataAdapter(source);
    $("#jqxgrid").jqxGrid(
    {
        width: '100%',
        source: gridDataAdapter,
        autoheight: true,
        selectionmode: 'singlerow',
        pageable: true,
        virtualmode: true,
        rendergridrows: function () {
            console.log(gridDataAdapter.records);
            var records = gridDataAdapter.records;
            var length = records.length;
            for (var i = 0; i < length; i++) {
                gridDataAdapter.records[i].TypeName = MeasurementTypeName(gridDataAdapter.records[i].Type);
            }
            return gridDataAdapter.records;
        },
        columns: [
            { text: 'Name', datafield: 'Name' },
            { text: 'Item Price', datafield: 'ItemPrice' }
        ]
    });

}