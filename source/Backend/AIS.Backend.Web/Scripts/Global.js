﻿var buttonHeight = 28;
var textInputWidth = '100%';
var textInputHeight = '28px';
var buttonInputHeight = '28px';

var defaultGridSize = 20;

var knockoutValidationConfig = {
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: true,
    parseInputAttributes: true,
    messageTemplate: null
};

var saveAjax = function (baseUrl,url,data,redirect)
{
    $.ajax({
        type: 'POST',
        url: baseUrl + url,
        data: JSON.stringify(data),
        error: function (error) {
            alert(error.responseJSON);
        },
        contentType:"application/json",
        success: function (data) {
            console.log(data);
            var url = baseUrl + redirect;
            $(location).attr('href', url);
        }
    });
};

var deleteAjax = function (baseUrl, url, id, gridInit) {

    $.ajax({
        type: 'DELETE',
        url: baseUrl + url + id,
        error: function (error) {
            alert(error.responseJSON);
        },
        dataType: 'json',
        success: function (data) {
            gridInit();
        }
    });

}

// Generic Grid Model
function genericGridModel(className, gridInitFunc) {
    self = this;
    self.add = function () {
        var url = baseUrl + "/" + className + "/Create/";
        $(location).attr('href', url);
    }
    self.edit = function () {
        var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
        if (getselectedrowindexes.length > 0) {
            // returns the selected row's data.
            var selectedRowData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            var id = selectedRowData.Id;
            var url = baseUrl + "/" + className + "/Update/" + id;
            $(location).attr('href', url);
        }
    }
    self.deleteItem = function () {
        var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
        if (getselectedrowindexes.length > 0) {
            // returns the selected row's data.
            var selectedRowData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);

            var id = selectedRowData.Id;
            var name = selectedRowData.Name;
            bootbox.confirm("Are you sure to delete " + name + "?", function (result) {
                if (result) {
                    deleteAjax(baseUrl, "/api/" + className + "/delete/", id, gridInitFunc);
                }
            });
        }
    }
};

/*
if ($("input[type='text']").length > 0) $("input[type='text']").jqxInput({ height: textInputHeight });
if ($("select").length > 0) $("select").jqxDropDownList({ height: textInputHeight });
if ($(".radioInput").length > 0) $(".radioInput").jqxRadioButton({ height: textInputHeight });
if ($(".checkBoxInput").length > 0) $(".checkBoxInput").jqxCheckBox({ height: textInputHeight });
if ($("input[type='button']").length > 0) $("input[type='button']").jqxButton({ height: buttonInputHeight });

if ($(".form-horizontal input[type='text']").length > 0) $("input[type='text']").jqxInput({ height: textInputHeight, width: textInputWidth });
if ($(".form-horizontal select").length > 0) $("select").jqxDropDownList({ height: textInputHeight, width: textInputWidth });
*/