﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using AIS.Backend.Model;

namespace AIS.Backend.Web.Helper
{
    public static class CustomHelper
    {
        public static String StateDropDownListFor<T>(this HtmlHelper html, ICollection<T> list) 
            where T : NameableEntity
        {
            String dropdown = "<select>";
            foreach(NameableEntity row in list)
            {
                dropdown += "<option name=\"" + row.Id + "\">" + row.Name + "</option>";
            }
            dropdown += "</select>";
            
            return dropdown;
        }
    }
}