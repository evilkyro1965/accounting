﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;

namespace AIS.Backend.Web.Controllers
{
    [RoutePrefix("api/employee")]
    public class EmployeeRestController : ApiController
    {
        private EmployeeService dao;

        public EmployeeRestController(EmployeeService employeeService)
        {
            dao = employeeService;
        }

        [Route("Get/{id:int}")] 
        public Employee GetEmployee(long id)
        {
            Employee employee = dao.Get(id);
            return employee;
        }

        [Route("Create")] 
        public HttpResponseMessage CreateEmployee(Employee employee)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ControllerHelper.AuditCreatedByAndDate(employee, User);
                    employee.IsActive = true;
                    Employee saved = dao.Create(employee);
                    var response = Request.CreateResponse<Employee>(HttpStatusCode.Created, employee);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("Update")]
        public HttpResponseMessage UpdateEmployee(Employee update)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Employee ori = dao.Get(update.Id);
                    ori.Username = update.Username != ori.Username ? update.Username : ori.Username;
                    ori.Name = update.Name != ori.Name ? update.Name : ori.Name;
                    ori.Password = update.Password != ori.Password ? update.Password : ori.Password;
                    ori.Mobile = update.Mobile != ori.Mobile ? update.Mobile : ori.Mobile;
                    ori.Email = update.Email != ori.Email ? update.Email : ori.Email;
                    ori.IsActive = update.IsActive != ori.IsActive ? update.IsActive : ori.IsActive;
                    ori.Department = update.Department != null ? update.Department : ori.Department;

                    ControllerHelper.AuditUpdatedByAndDate(update, User);
                    Employee saved = dao.Update(ori);
                    var response = Request.CreateResponse<Employee>(HttpStatusCode.Created, update);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.InnerException.InnerException.Message);
            }
        }

        [Route("Delete/{id:int}")]
        public void DeleteEmployee(long id)
        {
            try
            {
                long[] ids = new long[1];
                ids[0] = id;
                dao.Deactivate(ids);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [Route("Search")]
        [HttpGet]
        
        public HttpResponseMessage SearchEmployee()
        {
            var queryPairs = ControllerContext.Request.GetQueryNameValuePairs();

            EmployeeSearchCriteria criteria = new EmployeeSearchCriteria();
            ControllerHelper.SetPageDataInCriteria(criteria, queryPairs);

            String name = ControllerHelper.GetQueryParam(queryPairs, "name");
            if (name != null && name != "")
            {
                criteria.Name = name;
                criteria.SortBy = "Name";
                criteria.SortAscending = true;
            }

            try
            {
                if (ModelState.IsValid)
                {
                    SearchResult<Employee> result = dao.Search(criteria);

                    var response = Request.CreateResponse<SearchResult<Employee>>(HttpStatusCode.Created, result);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.InnerException.InnerException.Message);
            }
        }

    }
}
