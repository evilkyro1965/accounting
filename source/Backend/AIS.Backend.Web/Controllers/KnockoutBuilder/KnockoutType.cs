﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AIS.Backend.Web.Controllers.KnockoutBuilder
{
    public enum KnockoutType
    {
        TEXT,
        NUMBER_TEXT,
        CHECKBOX,
        RADIO,
        DROPDOWN,
        LISTBOX,
        BUTTON,
        COMBO,
        DATE
    }
}