﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AIS.Backend.Web.Controllers.KnockoutBuilder
{
    public class DropdownValue
    {
        public String Value { get; set; }
        public String Label { get; set; }
    }
}