﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AIS.Backend.Web.Controllers.KnockoutBuilder
{
    public class KnockoutInput
    {
        public String Caption { get; set; }
        public String Binding { get; set; }
        public KnockoutType Type { get; set; }
        public String Value { get; set; }
        public Int32 IntegerValue { get; set; }
        public Double DecimalValue { get; set; }
        public Boolean BooleanValue { get; set; }
        public String Setting { get; set; }
        public String ValidationSetting { get; set; }
        public String ClassName { get; set; }
        public Dictionary<string,string> Items { get; set; }
    }
}