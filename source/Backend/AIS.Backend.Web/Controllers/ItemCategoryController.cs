﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;

namespace AIS.Backend.Web.Controllers
{
    public class ItemCategoryController : Controller
    {

        private ItemCategoryService dao;

        public ItemCategoryController(ItemCategoryService itemCategoryService)
        {
            dao = itemCategoryService;
        }

        // GET: ItemCategory
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Update(long id)
        {
            ItemCategory itemCategory = dao.Get(id);
            return View(itemCategory);
        }

    }
}