﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace AIS.Backend.Web.Controllers
{
    [RoutePrefix("api/purchase")]
    public class PurchaseRestController : ApiController
    {
        private const int AllRow = 1000000;

        private PurchaseService purchaseDao;

        private ItemService itemDao;

        private MeasurementService measurementDao;

        private WarehouseService warehouseDao;

        private SupplierService supplierDao;

        public PurchaseRestController(
            PurchaseService purchaseService, 
            MeasurementService measurementService, 
            ItemService itemService,
            WarehouseService warehouseService,
            SupplierService supplierService
            )
        {
            this.purchaseDao = purchaseService;
            this.measurementDao = measurementService;
            this.itemDao = itemService;
            this.warehouseDao = warehouseService;
            this.supplierDao = supplierService;
        }

        [Route("Get/{id:int}")]
        public Purchase GetPurchase(long id)
        {
            Purchase purchase = purchaseDao.Get(id);
            return purchase;
        }

        [Route("Create")]
        public HttpResponseMessage CreatePurchase(Purchase purchase)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ControllerHelper.AuditCreatedByAndDate(purchase, User);
                    Purchase saved = purchaseDao.Create(purchase);
                    var response = Request.CreateResponse<Purchase>(HttpStatusCode.Created, purchase);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("Update")]
        public HttpResponseMessage UpdatePurchase(Purchase purchase)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ControllerHelper.AuditUpdatedByAndDate(purchase, User);
                    Purchase update = purchaseDao.Update(purchase);
                    var response = Request.CreateResponse<Purchase>(HttpStatusCode.Created, new Purchase());
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
