﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;

namespace AIS.Backend.Web.Controllers
{
    [RoutePrefix("api/itemcategory")]
    public class ItemCategoryRestController : ApiController
    {
        private ItemCategoryService dao;

        public ItemCategoryRestController(ItemCategoryService itemCategoryService)
        {
            dao = itemCategoryService;
        }

        [Route("Get/{id:int}")]
        public ItemCategory GetItemCategory(long id)
        {
            ItemCategory itemCategory = dao.Get(id);
            return itemCategory;
        }

        [Route("Create")]
        public HttpResponseMessage CreateItemCategory(ItemCategory itemCategory)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ControllerHelper.AuditCreatedByAndDate(itemCategory, User);
                    itemCategory.IsActive = true;
                    ItemCategory saved = dao.Create(itemCategory);
                    var response = Request.CreateResponse<ItemCategory>(HttpStatusCode.Created, saved);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }


        [Route("Update")]
        public HttpResponseMessage UpdateItemCategory(ItemCategory update)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ItemCategory ori = dao.Get(update.Id);
                    ori.Name = update.Name != ori.Name ? update.Name : ori.Name;

                    ControllerHelper.AuditUpdatedByAndDate(ori, User);
                    ItemCategory saved = dao.Update(ori);
                    var response = Request.CreateResponse<ItemCategory>(HttpStatusCode.Created, update);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("Delete/{id:int}")]
        public void DeleteItemCategory(long id)
        {
            try
            {
                long[] ids = new long[1];
                ids[0] = id;
                dao.Deactivate(ids);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [Route("Search")]
        [HttpGet]
        public HttpResponseMessage SearchItemCategory()
        {
            var queryPairs = ControllerContext.Request.GetQueryNameValuePairs();

            BaseSearchCriteria criteria = new BaseSearchCriteria();
            ControllerHelper.SetPageDataInCriteria(criteria, queryPairs);

            String name = ControllerHelper.GetQueryParam(queryPairs, "name");
            if (name != null && name != "")
            {
                criteria.Name = name;
                criteria.SortBy = "Name";
                criteria.SortAscending = true;
            }

            try
            {
                if (ModelState.IsValid)
                {
                    SearchResult<ItemCategory> result = dao.Search(criteria);
                    var response = Request.CreateResponse<SearchResult<ItemCategory>>(HttpStatusCode.Created, result);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

    }
}
