﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;

namespace AIS.Backend.Web.Controllers.DTO
{
    public class ItemControllerDTO
    {
        public Item item {get; set;}
        public SearchResult<ItemCategory> itemCategories {get; set;}
        public SearchResult<Measurement> stockMeasurements {get; set;}
        public SearchResult<Measurement> calculationMeasurements {get; set;}
        public SearchResult<Measurement> purchaseMeasurements {get; set;}

    }
}