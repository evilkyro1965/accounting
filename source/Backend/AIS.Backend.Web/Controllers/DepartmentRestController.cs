﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;

namespace AIS.Backend.Web.Controllers
{
    [RoutePrefix("api/department")]
    public class DepartmentRestController : ApiController
    {
        private DepartmentService dao;

        public DepartmentRestController(DepartmentService departmentService)
        {
            dao = departmentService;
        }

        [Route("Get/{id:int}")]
        public Department GetDepartment(long id)
        {
            Department department = dao.Get(id);
            return department;
        }

        [Route("Create")]
        public HttpResponseMessage CreateDepartment(Department department)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ControllerHelper.AuditCreatedByAndDate(department, User);
                    department.IsActive = true;
                    Department saved = dao.Create(department);
                    var response = Request.CreateResponse<Department>(HttpStatusCode.Created, saved);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.InnerException.InnerException.Message);
            }
        }

        [Route("Update")]
        public HttpResponseMessage UpdateDepartment(Department update)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Department ori = dao.Get(update.Id);
                    ori.Name = update.Name != ori.Name ? update.Name : ori.Name;

                    ControllerHelper.AuditUpdatedByAndDate(ori, User);

                    Department saved = dao.Update(ori);
                    var response = Request.CreateResponse<Department>(HttpStatusCode.Created, update);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.InnerException.InnerException.Message);
            }
        }

        [Route("Delete/{id:int}")]
        public void DeleteDepartment(long id)
        {
            try
            {
                long[] ids = new long[1];
                ids[0] = id;
                dao.Deactivate(ids);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("Search")]
        [HttpGet]
        public HttpResponseMessage SearchDepartment()
        {
            var queryPairs = ControllerContext.Request.GetQueryNameValuePairs();

            DepartmentSearchCriteria criteria = new DepartmentSearchCriteria();
            ControllerHelper.SetPageDataInCriteria(criteria, queryPairs);

            String name = ControllerHelper.GetQueryParam(queryPairs, "name");
            if (name != null && name != "")
            {
                criteria.Name = name;
                criteria.SortBy = "Name";
                criteria.SortAscending = true;
            }

            try
            {
                if (ModelState.IsValid)
                {
                    SearchResult<Department> result = dao.Search(criteria);
                    var response = Request.CreateResponse<SearchResult<Department>>(HttpStatusCode.Created, result);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.InnerException.InnerException.Message);
            }
        }
    }
}
