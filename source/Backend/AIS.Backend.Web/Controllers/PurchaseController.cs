﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;
using AIS.Backend.Web.Controllers.DTO;

namespace AIS.Backend.Web.Controllers
{
    public class PurchaseController : Controller
    {
        private const int AllRow = 1000000;

        private PurchaseService purchaseDao;

        private ItemService itemDao;

        private MeasurementService measurementDao;

        private WarehouseService warehouseDao;

        private SupplierService supplierDao;

        private DepartmentService departmentDao;

        public PurchaseController(
            PurchaseService purchaseService, 
            MeasurementService measurementService, 
            ItemService itemService,
            WarehouseService warehouseService,
            SupplierService supplierService,
            DepartmentService departmentService
            )
        {
            this.purchaseDao = purchaseService;
            this.measurementDao = measurementService;
            this.itemDao = itemService;
            this.warehouseDao = warehouseService;
            this.supplierDao = supplierService;
            this.departmentDao = departmentService;
        }

        // GET: Purchase
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            BaseSearchCriteria getAllCriteria = new BaseSearchCriteria();
            getAllCriteria.IsActive = true;
            getAllCriteria.Page = 1;
            getAllCriteria.PageSize = AllRow;

            MeasurementSearchCriteria purchaseMeasurementCriteria = new MeasurementSearchCriteria();
            purchaseMeasurementCriteria.IsActive = true;
            purchaseMeasurementCriteria.Page = 1;
            purchaseMeasurementCriteria.PageSize = AllRow;
            purchaseMeasurementCriteria.MeasurementType = MeasurementType.PURCHASE;
            SearchResult<Measurement> purchaseMeasurementItems = measurementDao.Search(purchaseMeasurementCriteria);

            WarehouseSearchCriteria warehouseCriteria = new WarehouseSearchCriteria();
            warehouseCriteria.IsActive = true;
            warehouseCriteria.Page = 1;
            warehouseCriteria.PageSize = AllRow;
            SearchResult<Warehouse> warehouses = warehouseDao.Search(warehouseCriteria);

            ItemSearchCriteria itemCriteria = new ItemSearchCriteria();
            itemCriteria.IsActive = true;
            itemCriteria.Page = 1;
            itemCriteria.PageSize = AllRow;
            SearchResult<Item> items = itemDao.Search(itemCriteria);

            SupplierSearchCriteria supplierCriteria = new SupplierSearchCriteria();
            supplierCriteria.IsActive = true;
            supplierCriteria.Page = 1;
            supplierCriteria.PageSize = AllRow;
            SearchResult<Supplier> suppliers = supplierDao.Search(supplierCriteria);

            DepartmentSearchCriteria departmentCriteria = new DepartmentSearchCriteria();
            departmentCriteria.IsActive = true;
            departmentCriteria.Page = 1;
            departmentCriteria.PageSize = AllRow;
            SearchResult<Department> departments = departmentDao.Search(departmentCriteria);

            ViewBag.purchaseMeasurements = purchaseMeasurementItems.Items;
            ViewBag.warehouses = warehouses.Items;
            ViewBag.items = items.Items;
            ViewBag.suppliers = suppliers.Items;
            ViewBag.departments = departments.Items;

            return View();
        }

        public ActionResult Update(long id)
        {
            Purchase purchase = purchaseDao.Get(id);

            BaseSearchCriteria getAllCriteria = new BaseSearchCriteria();
            getAllCriteria.IsActive = true;
            getAllCriteria.Page = 1;
            getAllCriteria.PageSize = AllRow;

            MeasurementSearchCriteria purchaseMeasurementCriteria = new MeasurementSearchCriteria();
            purchaseMeasurementCriteria.IsActive = true;
            purchaseMeasurementCriteria.Page = 1;
            purchaseMeasurementCriteria.PageSize = AllRow;
            purchaseMeasurementCriteria.MeasurementType = MeasurementType.PURCHASE;
            SearchResult<Measurement> purchaseMeasurementItems = measurementDao.Search(purchaseMeasurementCriteria);

            WarehouseSearchCriteria warehouseCriteria = new WarehouseSearchCriteria();
            warehouseCriteria.IsActive = true;
            warehouseCriteria.Page = 1;
            warehouseCriteria.PageSize = AllRow;
            SearchResult<Warehouse> warehouses = warehouseDao.Search(warehouseCriteria);

            ItemSearchCriteria itemCriteria = new ItemSearchCriteria();
            itemCriteria.IsActive = true;
            itemCriteria.Page = 1;
            itemCriteria.PageSize = AllRow;
            SearchResult<Item> items = itemDao.Search(itemCriteria);

            SupplierSearchCriteria supplierCriteria = new SupplierSearchCriteria();
            supplierCriteria.IsActive = true;
            supplierCriteria.Page = 1;
            supplierCriteria.PageSize = AllRow;
            SearchResult<Supplier> suppliers = supplierDao.Search(supplierCriteria);

            DepartmentSearchCriteria departmentCriteria = new DepartmentSearchCriteria();
            departmentCriteria.IsActive = true;
            departmentCriteria.Page = 1;
            departmentCriteria.PageSize = AllRow;
            SearchResult<Department> departments = departmentDao.Search(departmentCriteria);

            ViewBag.purchaseMeasurements = purchaseMeasurementItems.Items;
            ViewBag.warehouses = warehouses.Items;
            ViewBag.items = items.Items;
            ViewBag.suppliers = suppliers.Items;
            ViewBag.departments = departments.Items;

            return View(purchase);
        }

    }
}