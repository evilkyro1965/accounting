﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace AIS.Backend.Web.Controllers
{
    [RoutePrefix("api/item")]
    public class ItemRestController : ApiController
    {
        private DaoContext context;

        private ItemService dao;

        private MeasurementService measurementDao;

        private ItemCategoryService itemCategoryDao;

        public ItemRestController(ItemService itemService, MeasurementService measurementService, ItemCategoryService itemCategoryService)
        {
            this.dao = itemService;
            this.measurementDao = measurementService;
            this.itemCategoryDao = itemCategoryService;
        }

        [Route("Get/{id:int}")]
        public Item GetItem(long id)
        {
            Item item = dao.Get(id);
            return item;
        }

        [Route("Create")]
        public HttpResponseMessage CreateItem(Item item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ControllerHelper.AuditCreatedByAndDate(item, User);
                    item.IsActive = true;
                    Item saved = dao.Create(item);
                    var response = Request.CreateResponse<Item>(HttpStatusCode.Created, item);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("Update")]
        public HttpResponseMessage UpdateItem(Item update)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Item ori = dao.Get(update.Id);
                    ori.Name = update.Name;
                    ori.ItemDescription = update.ItemDescription;
                    ori.ItemType = update.ItemType;
                    ori.Category = update.Category != null ? update.Category : ori.Category;
                    ori.StockMeasurement = update.StockMeasurement != null ? update.StockMeasurement : ori.StockMeasurement;
                    ori.CalculationMeasurement = update.CalculationMeasurement != null ? update.CalculationMeasurement : ori.CalculationMeasurement;
                    ori.PurchaseMeasurementList = update.PurchaseMeasurementList != null ? update.PurchaseMeasurementList : ori.PurchaseMeasurementList;
                    ori.IsActive = update.IsActive;
                    ControllerHelper.AuditUpdatedByAndDate(update, User);
                    Item saved = dao.Update(ori);
                    var response = Request.CreateResponse<Item>(HttpStatusCode.Created, update);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("Search")]
        [HttpGet]
        public HttpResponseMessage SearchItem()
        {
            var queryPairs = ControllerContext.Request.GetQueryNameValuePairs();

            ItemSearchCriteria criteria = new ItemSearchCriteria();
            ControllerHelper.SetPageDataInCriteria(criteria, queryPairs);

            String name = ControllerHelper.GetQueryParam(queryPairs, "name");
            if (name != null && name != "")
            {
                criteria.Name = name;
                criteria.SortBy = "Name";
                criteria.SortAscending = true;
            }

            try
            {
                if (ModelState.IsValid)
                {
                    SearchResult<Item> result = dao.Search(criteria);

                    var response = Request.CreateResponse<SearchResult<Item>>(HttpStatusCode.Created, result);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("Delete/{id:int}")]
        public void DeleteItemy(long id)
        {
            try
            {
                long[] ids = new long[1];
                ids[0] = id;
                dao.Deactivate(ids);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
