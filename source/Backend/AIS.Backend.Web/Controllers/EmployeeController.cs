﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;

namespace AIS.Backend.Web.Controllers
{
    public class EmployeeController : Controller
    {
        private EmployeeService dao;
        private DepartmentService departmentDao;

        public EmployeeController(EmployeeService employeeService,DepartmentService departmentService)
        {
            dao = employeeService;
            departmentDao = departmentService;
        }

        // GET: Employee
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            DepartmentSearchCriteria criteria = new DepartmentSearchCriteria()
            {
                Page = 1,
                PageSize = DaoHelper.ALL_ROW,
                IsActive = true
            };
            SearchResult<Department> departments = departmentDao.Search(criteria);
            ViewBag.departments = departments.Items;
            return View();
        }

        public ActionResult Update(long id)
        {
            Employee employee = dao.Get(id);
            DepartmentSearchCriteria criteria = new DepartmentSearchCriteria()
            {
                Page = 1,
                PageSize = DaoHelper.ALL_ROW,
                IsActive = true
            };
            SearchResult<Department> departments = departmentDao.Search(criteria);
            ViewBag.departments = departments.Items;
            return View(employee);
        }
    }
}