﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Web.Controllers.KnockoutBuilder;
using Microsoft.AspNet.Identity;
using System.Security.Principal;

namespace AIS.Backend.Web.Controllers
{
    public class ControllerHelper
    {
        public static List<DropdownValue> GetMeasurementList()
        {
            List<DropdownValue> ret = new List<DropdownValue>();
            ret.Add(new DropdownValue() { Value = "STOCK", Label = "Stock" });
            ret.Add(new DropdownValue() { Value = "CALCULATION", Label = "Calculation" });
            ret.Add(new DropdownValue() { Value = "PURCHASE", Label = "Purchase" });
            return ret;
        }

        public static List<DropdownValue> GetItemTypeList()
        {
            List<DropdownValue> ret = new List<DropdownValue>();
            ret.Add(new DropdownValue() { Value = "STOCKED_PRODUCT", Label = "Stocked Product" });
            ret.Add(new DropdownValue() { Value = "NON_STOCKED_PRODUCT", Label = "Non Stocked Product" });
            ret.Add(new DropdownValue() { Value = "SERVICE", Label = "Service" });
            return ret;
        }

        public static string GetDropdownJsonFromDict(String binding,List<DropdownValue> src)
        {
            string json = JsonConvert.SerializeObject(src, Formatting.Indented);
            string ret = "self." + binding + "= ko.observableArray(" + json + ");";
            ret += "self." + binding + "SelectedIndex=ko.observable()";
            return ret;
        }

        public static string GetQueryParam(IEnumerable<KeyValuePair<string, string>> queryValuePairs,string key)
        {
            string ret = null;
            KeyValuePair<string, string> valuePair = queryValuePairs.SingleOrDefault(x => x.Key == key);
            if (valuePair.Value != null)
            {
                ret = valuePair.Value;
            }
            return ret;
        }

        public static int GetPageNumFromQuery(IEnumerable<KeyValuePair<string, string>> queryValuePairs)
        {
            int pageNum = 0;
            KeyValuePair<string, string> valuePair = queryValuePairs.SingleOrDefault(x => x.Key == "pagenum");
            if (valuePair.Value!=null)
            {
                pageNum = Int32.Parse(valuePair.Value);
            }
            return pageNum;
        }

        public static int GetPageSizeFromQuery(IEnumerable<KeyValuePair<string, string>> queryValuePairs)
        {
            int pageNum = 0;
            KeyValuePair<string, string> valuePair = queryValuePairs.SingleOrDefault(x => x.Key == "pagesize");
            if (valuePair.Value != null)
            {
                pageNum = Int32.Parse(valuePair.Value);
            }
            return pageNum;
        }

        public static void SetPageDataInCriteria(BaseSearchCriteria criteria, IEnumerable<KeyValuePair<string, string>> queryValuePairs)
        {
            int Page = GetPageNumFromQuery(queryValuePairs);
            int PageSize = GetPageSizeFromQuery(queryValuePairs);
            criteria.Page = Page + 1;
            criteria.PageSize = PageSize;
        }

        public static void AuditCreatedByAndDate(AuditableEntity entity, IPrincipal user)
        {
            var userId = user.Identity.GetUserId();
            entity.CreatedBy = userId;
            entity.CreatedDate = DateTime.Now;
            entity.UpdatedBy = userId;
            entity.UpdatedDate = DateTime.Now;
        }
        public static void AuditUpdatedByAndDate(AuditableEntity entity, IPrincipal user)
        {
            var userId = user.Identity.GetUserId();
            entity.UpdatedBy = userId;
            entity.UpdatedDate = DateTime.Now;
        }

        public static void AuditCreatedByAndDate(GLAuditableEntity entity, IPrincipal user)
        {
            var userId = user.Identity.GetUserId();
            entity.CreatedBy = userId;
            entity.CreatedDate = DateTime.Now;
            entity.UpdatedBy = userId;
            entity.UpdatedDate = DateTime.Now;
        }

        public static void AuditUpdatedByAndDate(GLAuditableEntity entity, IPrincipal user)
        {
            var userId = user.Identity.GetUserId();
            entity.UpdatedBy = userId;
            entity.UpdatedDate = DateTime.Now;
        }
    }
}