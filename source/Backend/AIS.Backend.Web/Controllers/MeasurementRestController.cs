﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;
using Microsoft.AspNet.Identity;

namespace AIS.Backend.Web.Controllers
{
    [RoutePrefix("api/measurement")]
    public class MeasurementRestController : ApiController
    {
        private MeasurementService dao;

        public MeasurementRestController(MeasurementService measurementService)
        {
            dao = measurementService;
        }


        [Route("Get/{id:int}")]
        public Measurement GetMeasurement(long id)
        {
            Measurement measurement = dao.Get(id);
            return measurement;
        }

        [Route("Create")]
        public HttpResponseMessage CreateMeasurement(Measurement measurement)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ControllerHelper.AuditCreatedByAndDate(measurement, User);
                    measurement.IsActive = true;
                    Measurement saved = dao.Create(measurement);
                    var response = Request.CreateResponse<Measurement>(HttpStatusCode.Created, saved);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("Update")]
        public HttpResponseMessage UpdateMeasurement(Measurement update)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Measurement ori = dao.Get(update.Id);
                    ori.Name = update.Name;
                    ori.ShortName = update.ShortName;
                    ori.Type = update.Type;
                    ori.Unit = update.Unit;
                    ori.IsDecimal = update.IsDecimal;
                    ori.Description = update.Description;
                    ori.IsActive = update.IsActive;

                    ControllerHelper.AuditUpdatedByAndDate(ori, User);
                    Measurement saved = dao.Update(ori);
                    var response = Request.CreateResponse<Measurement>(HttpStatusCode.OK, update);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("Delete/{id:int}")]
        public void DeleteEmployee(long id)
        {
            try
            {
                long[] ids = new long[1];
                ids[0] = id;
                dao.Deactivate(ids);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("Search")]
        [HttpGet]
        public HttpResponseMessage SearchMeasurement()
        {
            var queryPairs = ControllerContext.Request.GetQueryNameValuePairs();

            MeasurementSearchCriteria criteria = new MeasurementSearchCriteria();
            ControllerHelper.SetPageDataInCriteria(criteria, queryPairs);

            String name = ControllerHelper.GetQueryParam(queryPairs, "name");
            if (name != null && name != "")
            {
                criteria.Name = name;
                criteria.SortBy = "Name";
                criteria.SortAscending = true;
            }

            try
            {
                if (ModelState.IsValid)
                {
                    SearchResult<Measurement> result = dao.Search(criteria);
                    var response = Request.CreateResponse<SearchResult<Measurement>>(HttpStatusCode.Created, result);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

    }
}
