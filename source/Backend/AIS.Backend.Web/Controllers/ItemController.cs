﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;
using AIS.Backend.Web.Controllers.DTO;


namespace AIS.Backend.Web.Controllers
{
    public class ItemController : Controller
    {
        private const int AllRow = 1000000;

        private ItemService dao;

        private MeasurementService measurementDao;

        private ItemCategoryService itemCategoryDao;

        public ItemController(ItemService itemService,MeasurementService measurementService,ItemCategoryService itemCategoryService)
        {
            this.dao = itemService;
            this.measurementDao = measurementService;
            this.itemCategoryDao = itemCategoryService;
        }

        // GET: Item
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            BaseSearchCriteria catCriteria = new BaseSearchCriteria();
            catCriteria.IsActive = true;
            catCriteria.Page = 1;
            catCriteria.PageSize = AllRow;

            SearchResult<ItemCategory> categories = itemCategoryDao.Search(catCriteria);

            MeasurementSearchCriteria criteria = new MeasurementSearchCriteria();
            criteria.MeasurementType = MeasurementType.STOCK;
            criteria.IsActive = true;
            criteria.Page = 1;
            criteria.PageSize = AllRow;

            SearchResult<Measurement> stockMeasurementItems = measurementDao.Search(criteria);

            criteria.MeasurementType = MeasurementType.CALCULATION;
            SearchResult<Measurement> calculationMeasurementItems = measurementDao.Search(criteria);

            criteria.MeasurementType = MeasurementType.PURCHASE;
            SearchResult<Measurement> purchaseMeasurementItems = measurementDao.Search(criteria);

            ViewBag.categories = categories.Items;
            ViewBag.stockMeasurementItems = stockMeasurementItems.Items;
            ViewBag.calculationMeasurementItems = calculationMeasurementItems.Items;
            ViewBag.purchaseMeasurementItems = purchaseMeasurementItems.Items;

            return View();
        }

        public ActionResult Update(long id)
        {
            Item item = dao.Get(id);

            BaseSearchCriteria catCriteria = new BaseSearchCriteria();
            catCriteria.IsActive = true;
            catCriteria.Page = 1;
            catCriteria.PageSize = AllRow;

            SearchResult<ItemCategory> categories = itemCategoryDao.Search(catCriteria);

            MeasurementSearchCriteria criteria = new MeasurementSearchCriteria();
            criteria.MeasurementType = MeasurementType.STOCK;
            criteria.IsActive = true;
            criteria.Page = 1;
            criteria.PageSize = AllRow;

            SearchResult<Measurement> stockMeasurementItems = measurementDao.Search(criteria);

            criteria.MeasurementType = MeasurementType.CALCULATION;
            SearchResult<Measurement> calculationMeasurementItems = measurementDao.Search(criteria);

            criteria.MeasurementType = MeasurementType.PURCHASE;
            SearchResult<Measurement> purchaseMeasurementItems = measurementDao.Search(criteria);

            ViewBag.categories = categories.Items;
            ViewBag.stockMeasurementItems = stockMeasurementItems.Items;
            ViewBag.calculationMeasurementItems = calculationMeasurementItems.Items;
            ViewBag.purchaseMeasurementItems = purchaseMeasurementItems.Items;

            return View(item);
        }
    }
}