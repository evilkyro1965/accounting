﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;

namespace AIS.Backend.Web.Controllers
{
    public class DepartmentController : Controller
    {
        private DepartmentService dao;

        public DepartmentController(DepartmentService departmentService)
        {
            dao = departmentService;
        }

        // GET: Employee
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Update(long id)
        {
            Department department = dao.Get(id);
            return View(department);
        }
    }
}