﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AIS.Backend.Model;

namespace AIS.Backend.Web.Controllers
{
    public class FormTestController : Controller
    {
        // GET: FormTest
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            Measurement measurement = new Measurement() { Id = 1, Name = "lorem" };
            List<Measurement> list = new List<Measurement>() { measurement };
            ViewBag.list = list;
            return View();
        }

        public String SelectBuilder<T>(T obj,String propName)
        {
            String ret = obj.GetType().GetProperty(propName).GetValue(obj, null).ToString();
            return "";
        }
    }
}