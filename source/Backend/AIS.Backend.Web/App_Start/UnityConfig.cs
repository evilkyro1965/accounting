using System;
using System.Web.Http;
using Unity.WebApi;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;


namespace AIS.Backend.Web.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your types here
            // container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<IUnitOfWork, UnitOfWork>(new HierarchicalLifetimeManager(),
        new InjectionConstructor("AIS_DB"));
            container.RegisterType<DepartmentService, DepartmentServiceImpl>();
            container.RegisterType<EmployeeService, EmployeeServiceImpl>();
            container.RegisterType<ItemCategoryService, ItemCategoryServiceImpl>();
            container.RegisterType<ItemService, ItemServiceImpl>();
            container.RegisterType<MeasurementService, MeasurementServiceImpl>();
            container.RegisterType<PurchaseService, PurchaseServiceImpl>();
            container.RegisterType<WarehouseService, WarehouseServiceImpl>();
            container.RegisterType<SupplierService, SupplierServiceImpl>();
            
            /** Konfigurasi untuk web api memakai unity **/
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}
