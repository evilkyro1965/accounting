﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AIS.Backend.Model;
using SystemEntity = System.Data.Entity;

namespace AIS.Backend.Model.Test
{
    [TestClass]
    public class TestEntityState
    {
        protected const String CONNECTION_STRING = "AIS_DB";
        [TestMethod]
        public void TestSaveDepartment()
        {
            using (var context = new DaoContext(CONNECTION_STRING))
            {
                context.TestInit();
                Department department = new Department();
                department.Name = "test";
                department.CreatedDate = new DateTime(2015, 6, 1, 10, 0, 0);
                department.UpdatedDate = new DateTime(2015, 6, 1, 10, 0, 0);
                department.IsActive = true;
                department.DataState = DataState.Added;

                SaveDepartment(department);
            }
        }

        public static void SaveDepartment(Department department)
        {
            using (var context = new DaoContext(CONNECTION_STRING))
            {
                context.Set<Department>().Add(department);

                foreach (var entry in context.ChangeTracker.Entries<IObjectWithState>())
                {
                    IObjectWithState stateInfo = entry.Entity;
                    entry.State = ConvertState(stateInfo.DataState);
                }

                context.SaveChanges();
            }
        }

        public static SystemEntity.EntityState ConvertState(DataState state)
        {
            switch (state)
            {
                case DataState.Added: return SystemEntity.EntityState.Added;
                case DataState.Modified: return SystemEntity.EntityState.Modified;
                case DataState.Deleted: return SystemEntity.EntityState.Deleted;
                default: return SystemEntity.EntityState.Unchanged;
            }
        }

        [TestMethod]
        public void TestApplyChanges()
        {
            using (var context = new DaoContext(CONNECTION_STRING))
            {
                context.TestInit();
                Department department = new Department();
                department.Name = "test";
                department.CreatedDate = new DateTime(2015, 6, 1, 10, 0, 0);
                department.UpdatedDate = new DateTime(2015, 6, 1, 10, 0, 0);
                department.IsActive = true;
                department.DataState = DataState.Added;

                ApplyChanges(department);
            }

            using (var context = new DaoContext(CONNECTION_STRING))
            {
                Department load = context.Departments.Find(1);
                Assert.AreEqual("test", load.Name);
            }
        }

        public static void ApplyChanges<TEntity>(TEntity root)
            where TEntity : class, IObjectWithState
        {
            using (var context = new DaoContext(CONNECTION_STRING))
            {
                context.Set<TEntity>().Add(root);

                foreach (var entry in context.ChangeTracker.Entries<IObjectWithState>())
                {
                    IObjectWithState stateInfo = entry.Entity;
                    entry.State = ConvertState(stateInfo.DataState);
                }

                context.SaveChanges();
            }
        }

    }
}
