﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AIS.Backend.Model.Test
{
    /// <summary>
    /// Address Test
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    [TestClass]
    public class AddressTest : BaseTest
    {
        /// <summary>
        /// Dao Context
        /// </summary>
        private DaoContext context = new DaoContext("AIS_DB");

        /// <summary>
        /// Test initialization
        /// </summary>
        [TestInitialize]
        public void TestInitiallize()
        {
            context.TestInit();
        }

        /// <summary>
        /// Test teardown
        /// </summary>
        [TestCleanup()]
        public void Cleanup() { }

        /// <summary>
        /// Test add address
        /// </summary>
        [TestMethod]
        public void TestAddAddress()
        {
            Address address = new Address();
            address.AddressName = StrTest;
            address.AddressLocation = StrTest;
            address.City = StrTest;
            address.State = StrTest;
            address.Zip = StrTest;
            address.Country = StrTest;
            address.Note = StrTest;

            context.Address.Add(address);
            context.SaveChanges();

            Address saved = context.Address.Find(1);
            Assert.AreEqual(StrTest, saved.AddressName);
            Assert.AreEqual(StrTest, saved.AddressLocation);
            Assert.AreEqual(StrTest, saved.City);
            Assert.AreEqual(StrTest, saved.State);
            Assert.AreEqual(StrTest, saved.Zip);
            Assert.AreEqual(StrTest, saved.Country);
            Assert.AreEqual(StrTest, saved.Note);
        }

    }
}
