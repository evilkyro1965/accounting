﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;

namespace AIS.Backend.Model.Test
{
    /// <summary>
    /// Base Test
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    public class BaseTest
    {
        /// <summary>
        /// String Test
        /// </summary>
        protected String StrTest = "test";

        /// <summary>
        /// String Testing
        /// </summary>
        protected String StrTesting = "testing";

        /// <summary>
        /// Decimal Testing
        /// </summary>
        protected double DecimalTest = 1.00;

        /// <summary>
        /// DateTime testing
        /// </summary>
        protected DateTime DateTimeTesting = new DateTime(2015, 6, 1, 10, 0, 0);

        /// <summary>
        /// Dao Context
        /// </summary>
        protected DaoContext context;

        /// <summary>
        /// Test initialization
        /// </summary>
        [TestInitialize]
        public void TestInitiallize()
        {
            context = new DaoContext(ConfigurationManager.AppSettings["CONNECTION_STRING_KEY"]);
            context.TestInit();
        }

        /// <summary>
        /// Test teardown
        /// </summary>
        [TestCleanup()]
        public void Cleanup() { }

        /// <summary>
        /// Create Item Dummy
        /// </summary>
        /// <returns>Return item</returns> 
        protected Item CreateItemDummy()
        {
            Measurement measurement = new Measurement();
            measurement.Name = "Kilogram";
            measurement.ShortName = "kg";
            measurement.Type = MeasurementType.CALCULATION;
            measurement.Unit = 1.0;
            measurement.IsActive = true;
            measurement.CreatedBy = StrTest;
            measurement.CreatedDate = DateTimeTesting;
            measurement.UpdatedBy = StrTest;
            measurement.UpdatedDate = DateTimeTesting;

            List<Measurement> purchaseMeasurement = new List<Measurement>();
            purchaseMeasurement.Add(measurement);

            ItemCategory itemCategory = new ItemCategory();
            itemCategory.Name = StrTest;
            itemCategory.IsActive = true;
            itemCategory.CreatedBy = StrTest;
            itemCategory.CreatedDate = DateTimeTesting;
            itemCategory.UpdatedBy = StrTest;
            itemCategory.UpdatedDate = DateTimeTesting;

            Item item = new Item();
            item.ItemDescription = StrTest;
            item.ItemPrice = DecimalTest;
            item.Category = itemCategory;
            item.StockMeasurement = measurement;
            item.CalculationMeasurement = measurement;
            item.PurchaseMeasurementList = purchaseMeasurement;
            item.CreatedBy = StrTest;
            item.CreatedDate = DateTimeTesting;
            item.UpdatedBy = StrTest;
            item.UpdatedDate = DateTimeTesting;

            return item;
        }

        /// <summary>
        /// Create Department Dummy
        /// </summary>
        /// <returns>Return item</returns> 
        protected Department CreateDepartmentDummy()
        {
            Department department = new Department();
            department.Email = StrTest;
            department.Name = StrTest;
            department.CreatedBy = StrTest;
            department.CreatedDate = DateTimeTesting;
            department.UpdatedBy = StrTest;
            department.UpdatedDate = DateTimeTesting;
            department.IsActive = true;

            return department;
        }

        /// <summary>
        /// Create Employee Dummy
        /// </summary>
        /// <returns>Return item</returns> 
        protected Employee CreateEmployeeDummy()
        {
            Department department = new Department();
            department.Email = StrTest;
            department.Name = StrTest;
            department.CreatedBy = StrTest;
            department.CreatedDate = DateTimeTesting;
            department.UpdatedBy = StrTest;
            department.UpdatedDate = DateTimeTesting;
            department.IsActive = true;

            Employee employee = new Employee();
            employee.Name = StrTest;
            employee.Username = StrTest;
            employee.Password = StrTest;
            employee.Mobile = StrTest;
            employee.Email = StrTest;
            employee.CreatedBy = StrTest;
            employee.CreatedDate = DateTimeTesting;
            employee.UpdatedBy = StrTest;
            employee.UpdatedDate = DateTimeTesting;
            employee.IsActive = true;
            employee.Department = department;

            return employee;
        }

        /// <summary>
        /// Create Supplier Dummy
        /// </summary>
        /// <returns>Return item</returns> 
        protected Supplier CreateSupplierDummy()
        {
            Supplier supplier = new Supplier();
            supplier.ContactPerson = StrTest;
            supplier.Address = StrTest;
            supplier.City = StrTest;
            supplier.PostalCode = StrTest;
            supplier.State = StrTest;
            supplier.Country = StrTest;
            supplier.Phone = StrTest;
            supplier.Fax = StrTest;
            supplier.Email = StrTest;
            supplier.CreatedBy = StrTest;
            supplier.CreatedDate = DateTimeTesting;
            supplier.UpdatedBy = StrTest;
            supplier.UpdatedDate = DateTimeTesting;

            return supplier;
        }

    }
}
