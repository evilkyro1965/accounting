﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AIS.Backend.Model.Test
{
    /// <summary>
    /// Supplier Test
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    [TestClass]
    public class SupplierTest : BaseTest
    {
        /// <summary>
        /// Add Supplier Test
        /// </summary>
        [TestMethod]
        public void AddSupplierTest()
        {
            Supplier supplier = new Supplier();
            supplier.ContactPerson = StrTest;
            supplier.Address = StrTest;
            supplier.City = StrTest;
            supplier.PostalCode = StrTest;
            supplier.State = StrTest;
            supplier.Country = StrTest;
            supplier.Phone = StrTest;
            supplier.Fax = StrTest;
            supplier.Email = StrTest;
            supplier.CreatedBy = StrTest;
            supplier.CreatedDate = DateTimeTesting;
            supplier.UpdatedBy = StrTest;
            supplier.UpdatedDate = DateTimeTesting;

            context.Suppliers.Add(supplier);
            context.SaveChanges();
        }
    }
}
