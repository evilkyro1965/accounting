﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;

namespace AIS.Backend.Model.Test
{
    [TestClass]
    public class ItemTestDeleteCollection
    {
        /// <summary>
        /// String Test
        /// </summary>
        protected String StrTest = "test";

        /// <summary>
        /// String Testing
        /// </summary>
        protected String StrTesting = "testing";

        /// <summary>
        /// Decimal Testing
        /// </summary>
        protected double DecimalTest = 1.00;

        /// <summary>
        /// DateTime testing
        /// </summary>
        protected DateTime DateTimeTesting = new DateTime(2015, 6, 1, 10, 0, 0);

        [TestMethod]
        public void TestMethod1()
        {
            using (var context = new DaoContext("AIS_DB"))
            {
                Measurement measurement = new Measurement();
                measurement.Name = "Kilogram";
                measurement.ShortName = "kg";
                measurement.Type = MeasurementType.CALCULATION;
                measurement.Unit = 1.0;
                measurement.IsActive = true;
                measurement.CreatedBy = StrTest;
                measurement.CreatedDate = DateTimeTesting;
                measurement.UpdatedBy = StrTest;
                measurement.UpdatedDate = DateTimeTesting;

                List<Measurement> purchaseMeasurement = new List<Measurement>();
                purchaseMeasurement.Add(measurement);

                ItemCategory itemCategory = new ItemCategory();
                itemCategory.Name = StrTest;
                itemCategory.IsActive = true;
                itemCategory.CreatedBy = StrTest;
                itemCategory.CreatedDate = DateTimeTesting;
                itemCategory.UpdatedBy = StrTest;
                itemCategory.UpdatedDate = DateTimeTesting;

                Item item = new Item();
                item.ItemDescription = StrTest;
                item.ItemPrice = DecimalTest;
                item.Category = itemCategory;
                item.StockMeasurement = measurement;
                item.CalculationMeasurement = measurement;
                item.PurchaseMeasurementList = purchaseMeasurement;
                item.CreatedBy = StrTest;
                item.CreatedDate = DateTimeTesting;
                item.UpdatedBy = StrTest;
                item.UpdatedDate = DateTimeTesting;

                context.Items.Add(item);
                context.SaveChanges();
            }
        }

        [TestMethod]
        public void TestMethod2()
        {
            using (var context = new DaoContext("AIS_DB"))
            {
                Item item = context.Items.Find(9);
                Measurement deleted = item.PurchaseMeasurementList.ElementAt(0);
                context.Entry(deleted).State = EntityState.Deleted;

                context.SaveChanges();
            }
        }
    }
}
