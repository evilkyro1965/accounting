﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AIS.Backend.Model.Test
{
    /// <summary>
    /// Item Category Test
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    [TestClass]
    public class ItemCategoryTest : BaseTest
    {
        /// <summary>
        /// Add Item Category Test
        /// </summary>
        [TestMethod]
        public void AddItemCategoryTest()
        {
            ItemCategory itemCategory = new ItemCategory();
            itemCategory.Name = StrTest;
            itemCategory.IsActive = true;
            itemCategory.CreatedBy = StrTest;
            itemCategory.CreatedDate = DateTimeTesting;
            itemCategory.UpdatedBy = StrTest;
            itemCategory.UpdatedDate = DateTimeTesting;

            context.ItemCategories.Add(itemCategory);
            context.SaveChanges();

            ItemCategory saved = new ItemCategory();
            Assert.AreEqual(StrTest, itemCategory.Name);
            Assert.AreEqual(true, itemCategory.IsActive);
            Assert.AreEqual(StrTest, itemCategory.CreatedBy);
            Assert.AreEqual(DateTimeTesting, itemCategory.CreatedDate);
            Assert.AreEqual(StrTest, itemCategory.UpdatedBy);
            Assert.AreEqual(DateTimeTesting, itemCategory.UpdatedDate);
        }
    }
}
