﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AIS.Backend.Model.Test
{
    /// <summary>
    /// Item Test
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    [TestClass]
    public class ItemTest : BaseTest
    {
        /// <summary>
        /// Add Item Category Test
        /// </summary>
        [TestMethod]
        public void AddItemTest()
        {
            Measurement measurement = new Measurement();
            measurement.Name = "Kilogram";
            measurement.ShortName = "kg";
            measurement.Type = MeasurementType.CALCULATION;
            measurement.Unit = 1.0;
            measurement.IsActive = true;
            measurement.CreatedBy = StrTest;
            measurement.CreatedDate = DateTimeTesting;
            measurement.UpdatedBy = StrTest;
            measurement.UpdatedDate = DateTimeTesting;

            List<Measurement> purchaseMeasurement = new List<Measurement>();
            purchaseMeasurement.Add(measurement);

            ItemCategory itemCategory = new ItemCategory();
            itemCategory.Name = StrTest;
            itemCategory.IsActive = true;
            itemCategory.CreatedBy = StrTest;
            itemCategory.CreatedDate = DateTimeTesting;
            itemCategory.UpdatedBy = StrTest;
            itemCategory.UpdatedDate = DateTimeTesting;

            Item item = new Item();
            item.ItemDescription = StrTest;
            item.ItemPrice = DecimalTest;
            item.Category = itemCategory;
            item.StockMeasurement = measurement;
            item.CalculationMeasurement = measurement;
            item.PurchaseMeasurementList = purchaseMeasurement;
            item.CreatedBy = StrTest;
            item.CreatedDate = DateTimeTesting;
            item.UpdatedBy = StrTest;
            item.UpdatedDate = DateTimeTesting;

            context.Items.Add(item);
            context.SaveChanges();
        }
    }
}
