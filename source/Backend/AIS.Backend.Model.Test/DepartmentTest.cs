﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;

namespace AIS.Backend.Model.Test
{
    /// <summary>
    /// Department Test
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    [TestClass]
    public class DepartmentTest : BaseTest
    {
        /// <summary>
        /// Add Department Test
        /// </summary>
        [TestMethod]
        public void AddDepartmentTest()
        {
            String name = ConfigurationManager.AppSettings["NAME_TEST"];
            DateTime createdDate = EntityHelper.FormattedStringToDate(ConfigurationManager.AppSettings["DATE_TEST"]);

            Department department = new Department();
            department.Email = name;
            department.Name = name;
            department.CreatedBy = name;
            department.CreatedDate = createdDate;
            department.UpdatedBy = name;
            department.UpdatedDate = createdDate;
            department.IsActive = true;

            context.Departments.Add(department);
            context.SaveChanges();

            Department saved = context.Departments.Find(2);
            Assert.AreEqual(name, saved.Name);
            Assert.AreEqual(name, saved.Email);
            Assert.AreEqual(name, saved.CreatedBy);
            Assert.AreEqual(createdDate, saved.CreatedDate);
            Assert.AreEqual(name, saved.UpdatedBy);
            Assert.AreEqual(createdDate, saved.UpdatedDate);
            Assert.AreEqual(true, saved.IsActive);
        }

    }
}
