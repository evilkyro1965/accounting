﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AIS.Backend.Model.Test
{
    /// <summary>
    /// Customer Test
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    [TestClass]
    public class CustomerTest : BaseTest
    {

        /// <summary>
        /// Test add customer
        /// </summary>
        [TestMethod]
        public void TestAddCustomer()
        {
            Address address = new Address();
            address.AddressName = StrTest;
            address.AddressLocation = StrTest;
            address.City = StrTest;
            address.State = StrTest;
            address.Zip = StrTest;
            address.Country = StrTest;
            address.Note = StrTest;

            ShipAddress shipAddress = new ShipAddress();
            address.AddressName = StrTest;
            address.AddressLocation = StrTest;
            address.City = StrTest;
            address.State = StrTest;
            address.Zip = StrTest;
            address.Country = StrTest;
            address.Note = StrTest;

            Customer customer = new Customer();
            customer.Name = StrTest;
            customer.CompanyName = StrTest;
            customer.MrMs = StrTest;
            customer.FirstName = StrTest;
            customer.LastName = StrTest;
            customer.Contact = StrTest;
            customer.Phone = StrTest;
            customer.AltPhone = StrTest;
            customer.Fax = StrTest;
            customer.Email = StrTest;
            customer.CC = StrTest;
            customer.BillTo = address;

            List<ShipAddress> shipTo = new List<ShipAddress>();
            shipTo.Add(shipAddress);
            customer.ShipTo = shipTo;

            context.Customers.Add(customer);
            context.SaveChanges();

            Customer saved = context.Customers.Find(1);

            Assert.AreEqual(StrTest, saved.CompanyName);
            Assert.AreEqual(StrTest, saved.MrMs);
            Assert.AreEqual(StrTest, saved.FirstName);
            Assert.AreEqual(StrTest, saved.LastName);
            Assert.AreEqual(StrTest, saved.Contact);
            Assert.AreEqual(StrTest, saved.Phone);
            Assert.AreEqual(StrTest, saved.AltPhone);
            Assert.AreEqual(StrTest, saved.Fax);
            Assert.AreEqual(StrTest, saved.Email);
            Assert.AreEqual(StrTest, saved.CC);

        }
    }
}
