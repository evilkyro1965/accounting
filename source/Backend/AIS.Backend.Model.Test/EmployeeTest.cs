﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AIS.Backend.Model.Test
{
    /// <summary>
    /// Employee Test
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    [TestClass]
    public class EmployeeTest : BaseTest
    {
        /// <summary>
        /// Add Employee Test
        /// </summary>
        [TestMethod]
        public void AddEmployeeTest()
        {
            Department department = new Department();
            department.Email = StrTest;
            department.Name = StrTest;
            department.CreatedBy = StrTest;
            department.CreatedDate = DateTimeTesting;
            department.UpdatedBy = StrTest;
            department.UpdatedDate = DateTimeTesting;
            department.IsActive = true;

            Employee employee = new Employee();
            employee.Name = StrTest;
            employee.Username = StrTest;
            employee.Password = StrTest;
            employee.Mobile = StrTest;
            employee.Email = StrTest;
            employee.CreatedBy = StrTest;
            employee.CreatedDate = DateTimeTesting;
            employee.UpdatedBy = StrTest;
            employee.UpdatedDate = DateTimeTesting;
            employee.IsActive = true;
            employee.Department = department;

            context.Employee.Add(employee);
            context.SaveChanges();

            Employee saved = context.Employee.Find(1);
            Assert.AreEqual(StrTest, saved.Name);
            Assert.AreEqual(StrTest, saved.Username);
            Assert.AreEqual(StrTest, saved.Password);
            Assert.AreEqual(StrTest, saved.Mobile);
            Assert.AreEqual(StrTest, saved.Email);
            Assert.AreEqual(StrTest, saved.CreatedBy);
            Assert.AreEqual(DateTimeTesting, saved.CreatedDate);
            Assert.AreEqual(StrTest, saved.UpdatedBy);
            Assert.AreEqual(DateTimeTesting, saved.UpdatedDate);
            Assert.AreEqual(true, saved.IsActive);
            Assert.AreEqual(department, saved.Department);
            
        }
    }
}
