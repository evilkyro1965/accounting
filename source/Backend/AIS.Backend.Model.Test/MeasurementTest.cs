﻿/*
*  Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AIS.Backend.Model.Test
{
    /// <summary>
    /// Measurement Test
    /// </summary>
    ///
    /// <author>Fahrur</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, Kyrosoft, Inc. All rights reserved.</copyright>
    [TestClass]
    public class MeasurementTest : BaseTest
    {
        /// <summary>
        /// Add Item Category Test
        /// </summary>
        [TestMethod]
        public void AddMeasurementTest()
        {
            Measurement measurement = new Measurement();
            measurement.Name = StrTest;
            measurement.ShortName = StrTest;
            measurement.Type = MeasurementType.CALCULATION;
            measurement.Unit = DecimalTest;
            measurement.IsActive = true;
            measurement.CreatedBy = StrTest;
            measurement.CreatedDate = DateTimeTesting;
            measurement.UpdatedBy = StrTest;
            measurement.UpdatedDate = DateTimeTesting;

            context.Measurements.Add(measurement);
            context.SaveChanges();
        }
    }
}
