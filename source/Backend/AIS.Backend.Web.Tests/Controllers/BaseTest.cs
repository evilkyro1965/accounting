﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;
using AIS.Backend.Web;
using AIS.Backend.Web.Controllers;
using System.Web.Http;
using System.Net.Http;
using System.Web.Http.Routing;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;

namespace AIS.Backend.Web.Tests.Controllers
{
    public class BaseTest
    {
        /// <summary>
        /// String Test
        /// </summary>
        protected String StrTest;

        /// <summary>
        /// String Testing
        /// </summary>
        protected String StrTesting = "testing";

        /// <summary>
        /// Decimal Testing
        /// </summary>
        protected double DecimalTest = 1.00;

        /// <summary>
        /// DateTime testing
        /// </summary>
        protected DateTime DateTimeTesting = new DateTime(2015, 6, 1, 10, 0, 0);

        private const String DBNAME = "AIS_DB";

        public String name = ConfigurationManager.AppSettings["NAME_TEST"];

        public const long OBJECT_ID = 1;

        protected DaoContext context;
        protected MeasurementService measurementService;
        protected MeasurementRestController measurementRestController;

        public BaseTest()
        {
            context = new DaoContext(DaoHelper.DB_NAME);
            measurementService = new MeasurementServiceImpl();
            measurementRestController = new MeasurementRestController(measurementService);
        }
        [TestInitialize]
        public void TestInitiallize()
        {
            context = new DaoContext(ConfigurationManager.AppSettings["CONNECTION_STRING_KEY"]);
            context.TestInit();
        }

        protected static void SetupControllerForTests(ApiController controller)
        {
            var config = new HttpConfiguration();
            var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/api/products");
            var route = config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}");
            var routeData = new HttpRouteData(route, new HttpRouteValueDictionary { { "controller", "products" } });

            controller.ControllerContext = new HttpControllerContext(config, routeData, request);
            controller.Request = request;
            controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;
        }

        protected static void SetupControllerWithGetData(ApiController controller,string queryString)
        {
            var config = new HttpConfiguration();
            var request = new HttpRequestMessage(HttpMethod.Get, "http://localhost/api/measurement/Search"+queryString);
            var route = config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}");
            var routeData = new HttpRouteData(route, new HttpRouteValueDictionary { { "controller", "products" } });

            controller.ControllerContext = new HttpControllerContext(config, routeData, request);
            controller.Request = request;
            controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;

        }

        protected static void AuditCreatedByAndDate(AuditableEntity entity)
        {
            entity.CreatedBy = "test";
            entity.CreatedDate = DateTime.Now;
            entity.UpdatedBy = "test";
            entity.UpdatedDate = DateTime.Now;
        }

        protected static void AuditCreatedByAndDate(GLAuditableEntity entity)
        {
            entity.CreatedBy = "test";
            entity.CreatedDate = DateTime.Now;
            entity.UpdatedBy = "test";
            entity.UpdatedDate = DateTime.Now;
        }
    }
}
