﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;
using AIS.Backend.Web;
using AIS.Backend.Web.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Http;
using System.Net.Http;
using System.Web.Http.Routing;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Net;

namespace AIS.Backend.Web.Tests.Controllers
{
    [TestClass]
    public class TestMeasurementRestController : BaseTest
    {
        private DaoContext daoContext;
        private MeasurementService measurementService;
        private MeasurementRestController measurementRestController;

        public TestMeasurementRestController()
        {
            daoContext = new DaoContext(DaoHelper.DB_NAME);
            measurementService = new MeasurementServiceImpl();
            measurementRestController = new MeasurementRestController(measurementService);
            SetupControllerForTests(measurementRestController);
        }

        [TestMethod]
        public void TestCreateMeasurement()
        {
            Measurement measurement = new Measurement() { Name = StrTest, ShortName = StrTest, 
                Type = MeasurementType.CALCULATION, Unit = DecimalTest, IsDecimal = true, 
                CreatedDate = DateTimeTesting, UpdatedBy = StrTest, UpdatedDate = DateTimeTesting };

            measurementRestController.CreateMeasurement(measurement);

            Measurement saved = measurementService.Get(measurement.Id);
            Assert.IsNotNull(saved.Id);
            Assert.AreNotEqual(0, saved.Id);
        }

        [TestMethod]
        public void TestGetMeasurement()
        {
            Measurement measurement = new Measurement()
            {
                Name = StrTest,
                ShortName = StrTest,
                Type = MeasurementType.CALCULATION,
                Unit = DecimalTest,
                IsDecimal = true,
                CreatedDate = DateTimeTesting,
                UpdatedBy = StrTest,
                UpdatedDate = DateTimeTesting
            };

            measurementService.Create(measurement);

            measurementRestController = new MeasurementRestController(measurementService);

            SetupControllerForTests(measurementRestController);

            Measurement ret = measurementRestController.GetMeasurement(measurement.Id);

            Assert.AreEqual(StrTest, ret.Name);
        }

        [TestMethod]
        public void TestUpdateMeasurement()
        {
            Measurement measurement = new Measurement()
            {
                Name = StrTest,
                ShortName = StrTest,
                Type = MeasurementType.CALCULATION,
                Unit = DecimalTest,
                IsDecimal = true,
                CreatedDate = DateTimeTesting,
                UpdatedBy = StrTest,
                UpdatedDate = DateTimeTesting
            };

            measurementService.Create(measurement);
            measurement.Name = "updated";

            measurementRestController = new MeasurementRestController(measurementService);

            SetupControllerForTests(measurementRestController);

            var result = measurementRestController.UpdateMeasurement(measurement);

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);

            Measurement updated = measurementService.Get(measurement.Id);

            Assert.AreEqual("updated", updated.Name);
        }

        [TestMethod]
        public void TestSearchMeasurement()
        {
            measurementRestController = new MeasurementRestController(measurementService);
            SetupControllerWithGetData(measurementRestController,"?pagenum=0&pagesize=10");
            var result = measurementRestController.SearchMeasurement();
            var i = 0;
        }




    }
}
