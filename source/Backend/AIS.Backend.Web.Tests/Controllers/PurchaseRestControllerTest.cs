﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIS.Backend.Model;
using AIS.Backend.Model.DTO;
using AIS.Backend.Service;
using AIS.Backend.Service.Impl;
using AIS.Backend.Web;
using AIS.Backend.Web.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Http;
using System.Net.Http;
using System.Web.Http.Routing;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Net;

namespace AIS.Backend.Web.Tests.Controllers
{
    [TestClass]
    public class PurchaseRestControllerTest : BaseTest
    {
        private DaoContext daoContext;

        private const int AllRow = 1000000;

        private PurchaseService purchaseDao;

        private ItemService itemDao;

        private MeasurementService measurementDao;

        private WarehouseService warehouseDao;

        private SupplierService supplierDao;

        private PurchaseRestController purchaseRestController;

        public PurchaseRestControllerTest()
        {
            daoContext = new DaoContext(DaoHelper.DB_NAME);
            this.purchaseDao = new PurchaseServiceImpl();
            this.measurementDao = new MeasurementServiceImpl();
            this.itemDao = new ItemServiceImpl();
            this.warehouseDao = new WarehouseServiceImpl();
            this.supplierDao = new SupplierServiceImpl();

            purchaseRestController = new PurchaseRestController(purchaseDao, measurementDao, 
                itemDao, warehouseDao, supplierDao);
            SetupControllerForTests(purchaseRestController);
        }

        [TestMethod]
        public void TestCreatePurchase()
        {
            Warehouse warehouse = warehouseDao.Get(OBJECT_ID);
            //Employee employee = employeeDao.Get(OBJECT_ID);
            //Department department = departmentDao.Get(OBJECT_ID);
            Supplier supplier = context.Suppliers.Find(OBJECT_ID);
            Item item = itemDao.Get(OBJECT_ID);
            Measurement purchaseMeasurement = measurementDao.Get(OBJECT_ID);

            Purchase purchase = new Purchase();
            purchase.No = StrTest;
            purchase.Date = DateTime.Now;
            purchase.DeliveryDate = DateTime.Now;
            purchase.Warehouse = warehouse;
            purchase.Freight = DecimalTest;
            //purchase.OpenBy = employee;
            //purchase.RequestorDepartment = department;
            purchase.Supplier = supplier;
            purchase.Remark = StrTest;
            AuditCreatedByAndDate(purchase);

            OrderDetails orderDetails = new OrderDetails()
            {
                Item = item,
                PurchaseMeasurement = purchaseMeasurement,
                Quantity = DecimalTest,
                Price = DecimalTest,
                DiscountPercent = DecimalTest,
                TaxPercent = DecimalTest,
                Warehouse = warehouse
            };
            List<OrderDetails> orderDetailsList = new List<OrderDetails>() { orderDetails };
            purchase.OrderDetailList = orderDetailsList;

            purchaseRestController.CreatePurchase(purchase);

            Purchase saved = purchaseDao.Get(OBJECT_ID);

            /* Convert from icollection to list */
            IList<OrderDetails> savedOrderDetails = GeneralHelper.ConvertICollectionToList(saved.OrderDetailList);

            Assert.AreEqual(1.00, savedOrderDetails[0].Quantity);
            Assert.AreEqual(OBJECT_ID, savedOrderDetails[0].Item.Id);
            Assert.AreEqual(OBJECT_ID, savedOrderDetails[0].Warehouse.Id);
        }
    }
}
