"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var primeng_1 = require('primeng/primeng');
var form_row_1 = require('./app/view/form.row');
var common_validator_1 = require('./app/view/common.validator');
var DepartmentAddController = (function () {
    function DepartmentAddController() {
        this.name = "";
    }
    DepartmentAddController.prototype.routerOnActivate = function (curr, prev, currTree) {
    };
    DepartmentAddController.prototype.save = function () {
        this.nameValidation = common_validator_1.formValidator(this.name, common_validator_1.ValidationType.NOT_EMPTY);
    };
    DepartmentAddController = __decorate([
        core_1.Component({
            template: "\n    <p-panel header=\"Create Department\">\n    \t<div class=\"ui-grid fullwidthForm\">\n\t\t    <form-row label=\"Name\" required=\"true\" errorMessage=\"{{nameValidation}}\">\n\t\t    \t<input type=\"text\" pInputText [(ngModel)]=\"name\" />\n\t\t    </form-row>\n\t\t    <form-row label=\"\">\n\t\t    \t<button pButton type=\"button\" icon=\"fa-check\" iconPos=\"left\" label=\"Save\" (click)=\"save()\" ></button>\n\t\t    \t<button pButton type=\"button\" icon=\"fa-times\" iconPos=\"left\" label=\"Cancel\"></button>\n\t\t    </form-row>\n\t\t</div>\n\t</p-panel>\n  ",
            directives: [primeng_1.Panel, primeng_1.InputText, primeng_1.Button, form_row_1.FormRow]
        }), 
        __metadata('design:paramtypes', [])
    ], DepartmentAddController);
    return DepartmentAddController;
}());
exports.DepartmentAddController = DepartmentAddController;
//# sourceMappingURL=department.add.controller.js.map