import { Component } from '@angular/core';
import { OnActivate, Router, RouteSegment, RouteTree } from '@angular/router';

@Component({
  template: `
    Department Details
  `,
})

export class DepartmentDetailsController implements OnActivate {

  constructor() { }

  routerOnActivate(curr: RouteSegment, prev: RouteSegment, currTree: RouteTree) {

  }

}