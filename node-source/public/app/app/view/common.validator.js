"use strict";
(function (ValidationType) {
    ValidationType[ValidationType["NOT_EMPTY"] = 0] = "NOT_EMPTY";
})(exports.ValidationType || (exports.ValidationType = {}));
var ValidationType = exports.ValidationType;
exports.formValidator = function (value, validationType) {
    var isValid = '';
    if (value === null || value === undefined || typeof value === "string") {
        if (validationType == ValidationType.NOT_EMPTY) {
            if (value.trim() == '') {
                return 'Field can\'t be empty';
            }
        }
    }
    return isValid;
};
//# sourceMappingURL=common.validator.js.map