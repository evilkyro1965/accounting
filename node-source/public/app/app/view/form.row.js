"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_view_helper_1 = require('./common.view.helper');
var FormRow = (function () {
    function FormRow() {
        this.required = false;
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], FormRow.prototype, "label", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], FormRow.prototype, "errorMessage", void 0);
    __decorate([
        core_1.Input(),
        common_view_helper_1.InputConverter(common_view_helper_1.BooleanConverter), 
        __metadata('design:type', Boolean)
    ], FormRow.prototype, "required", void 0);
    FormRow = __decorate([
        core_1.Component({
            selector: 'form-row',
            template: "\n\t\t<div class=\"ui-grid-row formRow\">\n\t        <div class=\"ui-grid-col-3\">\n\t        \t<label>{{label}}</label>\n\t        \t<span *ngIf=\"required == true\" class=\"required\">\n\t        \t*\n\t        \t</span>\n\t        </div>\n\t        <div class=\"ui-grid-col-5 {{errorMessage != '' && errorMessage != null ? 'inputError' : ''}}\">\n\t        \t<ng-content></ng-content>\n\t        </div>\n\t        <div class=\"ui-grid-col-2\">\n\t        \t<span *ngIf=\"errorMessage != '' && errorMessage != null\" class=\"error\">\n\t        \t\t* {{errorMessage}}\n\t        \t</span>\n\t        </div>\n\t    </div>\t\n\t"
        }), 
        __metadata('design:paramtypes', [])
    ], FormRow);
    return FormRow;
}());
exports.FormRow = FormRow;
//# sourceMappingURL=form.row.js.map