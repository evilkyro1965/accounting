"use strict";
exports.StringConverter = function (value) {
    if (value === null || value === undefined || typeof value === "string")
        return value;
    return value.toString();
};
exports.BooleanConverter = function (value) {
    if (value === null || value === undefined || typeof value === "boolean")
        return value;
    return value.toString() === "true";
};
exports.NumberConverter = function (value) {
    if (value === null || value === undefined || typeof value === "number")
        return value;
    return parseFloat(value.toString());
};
function InputConverter(converter) {
    return function (target, key) {
        if (converter === undefined) {
            var metadata = Reflect.getMetadata("design:type", target, key);
            if (metadata === undefined || metadata === null)
                throw new Error("The reflection metadata could not be found.");
            if (metadata.name === "String")
                converter = exports.StringConverter;
            else if (metadata.name === "Boolean")
                converter = exports.BooleanConverter;
            else if (metadata.name === "Number")
                converter = exports.NumberConverter;
            else
                throw new Error("There is no converter for the given property type '" + metadata.name + "'.");
        }
        var definition = Object.getOwnPropertyDescriptor(target, key);
        if (definition) {
            Object.defineProperty(target, key, {
                get: definition.get,
                set: function (newValue) {
                    definition.set(converter(newValue));
                },
                enumerable: true,
                configurable: true
            });
        }
        else {
            Object.defineProperty(target, key, {
                get: function () {
                    return this["__" + key];
                },
                set: function (newValue) {
                    this["__" + key] = converter(newValue);
                },
                enumerable: true,
                configurable: true
            });
        }
    };
}
exports.InputConverter = InputConverter;
//# sourceMappingURL=common.view.helper.js.map