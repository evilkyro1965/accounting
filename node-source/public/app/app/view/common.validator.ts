export enum ValidationType {
    NOT_EMPTY
}

export var formValidator = (value: any, validationType : ValidationType) => {

	var isValid : string = '';

    if (value === null || value === undefined || typeof value === "string") {
        
        if(validationType==ValidationType.NOT_EMPTY) {

        	if(value.trim()=='') {
        		return 'Field can\'t be empty';
        	}

        }

    }

    return isValid;
}