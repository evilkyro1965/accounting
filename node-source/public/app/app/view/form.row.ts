import { Component, Input } from '@angular/core';
import { BooleanConverter, InputConverter } from './common.view.helper';

@Component({
	selector: 'form-row',
	template: `
		<div class="ui-grid-row formRow">
	        <div class="ui-grid-col-3">
	        	<label>{{label}}</label>
	        	<span *ngIf="required == true" class="required">
	        	*
	        	</span>
	        </div>
	        <div class="ui-grid-col-5 {{errorMessage != '' && errorMessage != null ? 'inputError' : ''}}">
	        	<ng-content></ng-content>
	        </div>
	        <div class="ui-grid-col-2">
	        	<span *ngIf="errorMessage != '' && errorMessage != null" class="error">
	        		* {{errorMessage}}
	        	</span>
	        </div>
	    </div>	
	`
})

export class FormRow {
	@Input() label: string;
	@Input() errorMessage : string;
	@Input() @InputConverter(BooleanConverter) required : boolean

	constructor() {
		this.required = false;
	}
}