"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require('@angular/router');
var primeng_1 = require('primeng/primeng');
var department_controller_1 = require('./department.controller');
var department_details_controller_1 = require('./department.details.controller');
var department_add_controller_1 = require('./department.add.controller');
var MainApp = (function () {
    function MainApp(router) {
        this.router = router;
        this.items = [
            {
                label: 'Master Data',
                items: [
                    { label: 'Department', url: '/departments' },
                    { label: 'Department Add', url: '/department/add' }
                ]
            }
        ];
    }
    MainApp.prototype.ngOnInit = function () {
        this.router.navigate(['/departments']);
    };
    MainApp = __decorate([
        core_1.Component({
            selector: 'main-app',
            template: "\n    <p-menubar [model]=\"items\"></p-menubar>\n    <div>&nbsp;</div>\n    <router-outlet></router-outlet>\n  ",
            directives: [router_1.ROUTER_DIRECTIVES, primeng_1.Menubar]
        }),
        router_1.Routes([
            { path: '/departments', component: department_controller_1.DepartmentController },
            { path: '/department/details', component: department_details_controller_1.DepartmentDetailsController },
            { path: '/department/add', component: department_add_controller_1.DepartmentAddController }
        ]), 
        __metadata('design:paramtypes', [router_1.Router])
    ], MainApp);
    return MainApp;
}());
exports.MainApp = MainApp;
//# sourceMappingURL=main.js.map