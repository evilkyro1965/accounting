import {bootstrap} from '@angular/platform-browser-dynamic';
import { Component, EventEmitter, OnInit  } from "@angular/core";
import { Routes, Router, ROUTER_DIRECTIVES } from '@angular/router';
import {Menubar,MenuItem} from 'primeng/primeng';

import { DepartmentController } from './department.controller';
import { DepartmentDetailsController } from './department.details.controller';
import { DepartmentAddController } from './department.add.controller';


@Component({
  selector: 'main-app',
  template: `
    <p-menubar [model]="items"></p-menubar>
    <div>&nbsp;</div>
    <router-outlet></router-outlet>
  `,
  directives: [ROUTER_DIRECTIVES,Menubar]
})

@Routes([
  {path: '/departments',  component: DepartmentController},
  {path: '/department/details',  component: DepartmentDetailsController},
  {path: '/department/add',  component: DepartmentAddController}
])

export class MainApp implements	OnInit {
  
  private items: MenuItem[];

	constructor(private router: Router) {
    this.items = [
            {
                label: 'Master Data',
                items: [
                    {label: 'Department',url: '/departments'},
                    {label: 'Department Add',url: '/department/add'}
                ]
            }
        ];

  }

	ngOnInit() {
    	this.router.navigate(['/departments']);
  	}
}