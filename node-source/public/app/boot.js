"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var router_1 = require('@angular/router');
var main_1 = require('./main');
platform_browser_dynamic_1.bootstrap(main_1.MainApp, [router_1.ROUTER_PROVIDERS]);
//# sourceMappingURL=boot.js.map