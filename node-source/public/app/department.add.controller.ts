import { Component } from '@angular/core';
import { OnActivate, Router, RouteSegment, RouteTree } from '@angular/router';
import { Panel, InputText, Button} from 'primeng/primeng';
import { FormRow } from './app/view/form.row';
import { ValidationType,formValidator } from './app/view/common.validator';

@Component({
  template: `
    <p-panel header="Create Department">
    	<div class="ui-grid fullwidthForm">
		    <form-row label="Name" required="true" errorMessage="{{nameValidation}}">
		    	<input type="text" pInputText [(ngModel)]="name" />
		    </form-row>
		    <form-row label="">
		    	<button pButton type="button" icon="fa-check" iconPos="left" label="Save" (click)="save()" ></button>
		    	<button pButton type="button" icon="fa-times" iconPos="left" label="Cancel"></button>
		    </form-row>
		</div>
	</p-panel>
  `,
  directives: [Panel,InputText,Button,FormRow]
})

export class DepartmentAddController implements OnActivate {
	
  name : string;
  nameValidation : string;

  constructor() {
  	this.name = "";
  }
  

  routerOnActivate(curr: RouteSegment, prev: RouteSegment, currTree: RouteTree) {

  }

  save() {
  	this.nameValidation = formValidator(this.name,ValidationType.NOT_EMPTY);
  }

}