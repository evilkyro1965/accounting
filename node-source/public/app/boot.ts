import { bootstrap }        from '@angular/platform-browser-dynamic';
import { ROUTER_PROVIDERS } from '@angular/router';
import { MainApp }     from './main';

bootstrap(MainApp, [ROUTER_PROVIDERS]);