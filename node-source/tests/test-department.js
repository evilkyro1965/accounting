var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();
var Helper = require('./helper');

chai.use(chaiHttp);

var strTest = "test";
var intTest = 1;
var doubleTest = 1.0;
var boolTest = true;

describe('Department Rest', function() {
  
	it('Should add a department on /departments POST', function(done) {
		
		Helper.beforeTest(function() {

		    Helper.chaiWrapper(
		    	chai,
		    	'post',
		    	'/departments',
		    	{name: strTest, isActive : boolTest},
		    	function(err, res) {
		    		res.should.have.status(200);
		    		res.should.be.json;
		    		res.body.name.should.equal(strTest);
		    		done();
		    	}
		    );

		});

	});

	it('Should get a department on /departments/:id GET', function(done) {
		
		Helper.beforeTest(function() {

		    Helper.chaiWrapper(
		    	chai,
		    	'post',
		    	'/departments',
		    	{name: strTest, isActive: boolTest},
		    	function(err, res) {

		    		var saved = res.body;

		    		Helper.chaiWrapper(
				    	chai,
				    	'get',
				    	'/departments/'+saved._id,
				    	null,
				    	function(err, res) {
				    		res.should.have.status(200);
				    		res.should.be.json;
				    		res.body.name.should.equal(strTest);
				    		done();
				    		
				    	}
				    );
		    	}
		    );

		});

	});

	it('Should update a department on /departments/:id PUT', function(done) {
		
		Helper.beforeTest(function() {

		    Helper.chaiWrapper(
		    	chai,
		    	'post',
		    	'/departments',
		    	{name: strTest, isActive: boolTest},
		    	function(err, res) {

		    		var saved = res.body;
		    		var update = res.body;
		    		update.name = "updated";

		    		Helper.chaiWrapper(
				    	chai,
				    	'put',
				    	'/departments/'+saved._id,
				    	update,
				    	function(err, res) {
				    		res.should.have.status(200);
				    		res.should.be.json;
				    		res.body.name.should.equal("updated");
				    		done();
				    		
				    	}
				    );
		    	}
		    );

		});

	});

	it('Should get departments on /departments/search POST', function(done) {
		
		Helper.beforeTest(function() {

			var searchCriteria = {"name":"test","isActive":true,"pageSize":5, "page":1, "sortBy": "name", "sortType": "ASC"}

		    Helper.chaiWrapper(
		    	chai,
		    	'post',
		    	'/departments/search',
		    	searchCriteria,
		    	function(err, res) {

		    		var searchResult = res.body;

		    		res.should.have.status(200);
		    		res.should.be.json;
		    		res.body.total.should.equal(5);

				    done();
		    	}
		    );

		});

	});



}); 
