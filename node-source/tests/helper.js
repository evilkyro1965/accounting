var server = require('../app');
var mongoose = require('mongoose');
require('../app_api/models/db');
var Token = mongoose.model('Token');
var Department = mongoose.model('Department');

var collections = ["tokens","departments"];
var token = "test";
var userTest = "test";

async = require("async");

module.exports.token = token;
module.exports.userTest = userTest;

module.exports.beforeTest = function(callback) {

	async.each(collections,
		function(collection, callback){
			mongoose.connection.db.dropCollection(collection, function(err, result) {
				callback();
			});
		},
		//Done
		function(err){
			if(err) throw err;

			generateTestData(callback);		
		}
	);

};

module.exports.chaiWrapper = function(chai,protocol,url,data,assertFunc) {
	var chaiObj = chai.request(server);

	if(protocol === 'post') {
		chaiObj = chaiObj.post(url);
	}
	else if(protocol == 'put') {
		chaiObj = chaiObj.put(url);
	}
	else if(protocol == 'get') {
		chaiObj = chaiObj.get(url);
	}

	chaiObj = chaiObj.set('Authorization', 'Basic '+token);
	
	if(data) {
		chaiObj = chaiObj.send(data);
	}

	chaiObj.end(assertFunc);

};

var arrDepartments = [
	{name:"test1",isActive : true},
	{name:"test2",isActive : true},
	{name:"test3",isActive : true},
	{name:"test4",isActive : true},
	{name:"test5",isActive : true}
];

var generateTestData = function(done) {
	async.waterfall([
		function (callback) {

			Token.create({
				token : token,
				username : userTest
			},
			function(err, token){
				if(err) {
					callback(new Error(err.message));
				}

				//Main CallBack
				callback(null);
			});
		},
		function (callback) {

			Department.collection.insert(arrDepartments, 
				function(err, token){
				if(err) {
					callback(new Error(err.message));
				}

				//Main CallBack
				callback(null);
			});

		},
	], done);
};
