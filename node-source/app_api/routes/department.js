var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Department = mongoose.model('Department');
var controller = require('../controllers/department');

router.post('/', controller.departmentCreate);
router.put('/:id', controller.departmentUpdate);
router.get('/:id', controller.departmentGet);
router.post('/search/', controller.departmentSearch);

module.exports = router;
