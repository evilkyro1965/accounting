 var mongoose = require('mongoose');

 var departmentSchema = new mongoose.Schema({
 	name : String,
 	isActive : Boolean,
 	createdBy : String,
 	createdDate : Date,
 	updatedBy : String,
 	updatedDate : Date
 });

mongoose.model('Department', departmentSchema);