 var mongoose = require('mongoose');

 var tokenSchema = new mongoose.Schema({
 	token : String,
 	username : String,
 	expiration : Date
 });

mongoose.model('Token', tokenSchema); 
