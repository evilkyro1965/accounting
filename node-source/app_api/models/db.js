 var mongoose = require('mongoose');
 var dbURI = 'mongodb://localhost:27017/accounting';
 mongoose.connect(dbURI);

 mongoose.connection.on('connected', function () {
 	console.log('Mongoose connected');
 });

require('./token');
require('./department');