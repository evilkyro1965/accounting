var mongoose = require('mongoose');
var Department = mongoose.model('Department');
var Helper = require('./helper');

module.exports.departmentCreate = function(req,res) {
	var department = {
		name : req.body.name,
		isActive : req.body.isActive
	};
	Helper.setDateSaving(department,req);

	Department.create(department,
	function(err, saved){
		if(err) {
			Helper.sendResponse(res,400,{"message":"Error when create department"});
		}
		else {
			Helper.sendResponse(res,200,saved);	
		}
	});
	
};

module.exports.departmentUpdate = function(req,res) {
	var id = req.params.id;
	var update = req.body;

	Helper.setDateUpdating(update,req);

	Department.findByIdAndUpdate(id, update, {new:true}, function(err,updated){
		if(err) {
			Helper.sendResponse(res,400,{"message":"Error when update department"});
		}
		else {
			Helper.sendResponse(res,200,updated);	
		}
	});
	
};

module.exports.departmentGet = function(req,res) {

	if(req.params && req.params.id) {
		Department.findOne({ '_id': req.params.id }, function (err, department) {
	  		if(err || department == null) {
				Helper.sendResponse(res,400,{"status":"error"});
			}
			else {
				Helper.sendResponse(res,200,department);	
			}
		});
	}
	
};

module.exports.departmentSearch = function(req,res) {

	var searchCriteria = req.body;

	var findObj = {};
	if(searchCriteria.name) {
		findObj.name = new RegExp(searchCriteria.name,'i');
	}

	if(typeof searchCriteria.isActive !== undefined) {
		findObj.isActive = searchCriteria.isActive;
	}

	var query = Department.find(findObj);
	var queryTotal = Department.find(findObj);

	Helper.searchPaging(query,searchCriteria);

	queryTotal.select().
	exec(function(err,all) {
		query.select().
	  	exec(function(err,departments){
	  		var searchResult = {};
	  		searchResult.values = departments;

	  		Helper.setSearchResult(searchResult,searchCriteria,all);

	  		if(err || searchResult == null) {
				Helper.sendResponse(res,400,{"status":"error"});
			}
			else {
				Helper.sendResponse(res,200,searchResult);	
			}

	  	});
	});
	
};

