module.exports.sendResponse = function(res,status,content) {
	res.status(status);
	res.json(content);
};

module.exports.setDateSaving = function(obj,req) {
	obj.createdBy = req.userLogin;
	obj.createdDate = new Date();
};

module.exports.setDateUpdating = function(obj,req) {
	obj.updateBy = req.userLogin;
	obj.updateDate = new Date();
};

module.exports.searchPaging = function(query, criteria) {

	criteria.page = criteria.page ? criteria.page : 1;

	criteria.pageSize = criteria.pageSize ? criteria.pageSize : 1;

	query = query.skip( ( criteria.page - 1 ) * criteria.pageSize );

	query = query.limit( criteria.pageSize );

	if(criteria.sortBy) {
		var sort = {};
		sort[criteria.sortBy] = criteria.sortType === 'ASC' ? 1  : -1;

		query = query.sort(sort);
	}
};

module.exports.setSearchResult = function(searchResult,criteria,allData) {
	var total = allData.length;
	var pageSize = criteria.pageSize;

	searchResult.page = criteria.page;
	searchResult.pageSize = criteria.pageSize;
	searchResult.total = allData.length;
	searchResult.totalPages = parseInt(total / pageSize);
	if(total % pageSize>0) {
		searchResult.totalPages++;
	}
};
